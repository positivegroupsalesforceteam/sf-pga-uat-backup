/*
	@Description: handler class of LeadTrigger
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	11/29/2017 - Created, Transferred existing functionalities (updateRelatedObjsOnLeadConvert and updateCreatedDateFieldsLivingExpCalc)
		- 	12/15/2017 - Rex David - Positive Group- [LIG-724 SF - Update NVM Fields - LimitException due to Bizible Update > LIG-726]
		-	12/20/2017 - Rex David - [LIG-743 SF - Query Bug Fix] Added "ALL ROWS" in Task query
*/
public class LeadTriggerHandler {

	static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

	public static boolean firstRunBU = true; //prevents trigger to run multiple times
	public static boolean firstRunAU = true; //prevents trigger to run multiple times

	//execute all Before Update event triggers
	public static void onBeforeUpdate(map<Id,Lead> oldLeadMap, map <Id,Lead> newLeadMap){
		//Living Expenses Calculation
		Set<Id> updatedLeadIdsForNVMUpdate = new Set<Id>();//LIG-726
		Boolean enableNVMFieldUpdates = false;//LIG-726

		for (Lead ld : newLeadMap.values()){
			if(ld.Lender_and_Dependant_Expenses__c > ld.Living_Expenses_Customer__c){
		    	ld.Expenses_Considered__c= ld.Lender_and_Dependant_Expenses__c ;
		    }
		    else{
		    	ld.Expenses_Considered__c= ld.Living_Expenses_Customer__c;
		    }

		    if(trigSet.Enable_NVM_Field_Updates__c){//LIG-726
		    	enableNVMFieldUpdates = true;
		    	updatedLeadIdsForNVMUpdate.add(ld.Id);
		    }
		}
		//LIG-726
		if(enableNVMFieldUpdates && updatedLeadIdsForNVMUpdate.size() > 0){
			updateNVMRelatedFieldsInLead(updatedLeadIdsForNVMUpdate, newLeadMap.values());
		}
		//if (trigSet.Enable_Lead_updateCreatedDateFields__c) updateCreatedDateFieldsLivingExpCalc(oldleadMap,newLeadMap);
	} 

	//execute all After Update event triggers
	public static void onAfterUpdate(map<Id,Lead> oldLeadMap, map <Id,Lead> newLeadMap){
		list <Lead> ldtoUpdate = new list <Lead>();
		for (Lead ld : newLeadMap.values()){
			if (ld.isConverted && !oldLeadMap.get(ld.Id).isConverted){
				ldToUpdate.add(ld);
			}
		}
		if (ldToUpdate.size() > 0) {
			if (trigSet.Enable_Lead_updateRelObjsOnLeadConvert__c) updateRelatedObjsOnLeadConvert(ldToUpdate);
		}
	}

	/*//method for updating Created Date fields of Lead and Calculating Living Expenses
	private static void updateCreatedDateFieldsLivingExpCalc(map<Id,Lead> oldLeadMap, map <Id, Lead> newLeadMap){
		for (Lead ld : newLeadMap.values()){

			//Living Expenses Calculation
			if(ld.Lender_and_Dependant_Expenses__c > ld.Living_Expenses_Customer__c){
		    	ld.Expenses_Considered__c= ld.Lender_and_Dependant_Expenses__c ;
		    }
		    else{
		    	ld.Expenses_Considered__c= ld.Living_Expenses_Customer__c;
		    }

		    if(ld.CreatedDate_Time__c != null){
	            ld.CreatedTime__c = ld.CreatedDate_Time__c.format('KK:mm a');
	        }

	        //CreatedDate Fields
	        if (ld.CreatedTime__c != null){
			    if(string.valueOf(ld.CreatedTime__c).startsWith('07') && ld.CreatedTime__c.contains('AM')){
		            ld.CreatedTime24__c = '07:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('08') && ld.CreatedTime__c.contains('AM')){
		            ld.CreatedTime24__c = '08:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('09') && ld.CreatedTime__c.contains('AM')){
		            ld.CreatedTime24__c = '09:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('10') && ld.CreatedTime__c.contains('AM')){
		            ld.CreatedTime24__c = '10:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('11') && ld.CreatedTime__c.contains('AM')){
		            ld.CreatedTime24__c = '11:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('00') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '12:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('01') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '13:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('02') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '14:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('03') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '15:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('04') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '16:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('05') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '17:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('06') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '18:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('07') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '19:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('08') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '20:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('09') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '21:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('10') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '22:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('11') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '23:00';
		        }
		        else if(string.valueOf(ld.CreatedTime__c).startsWith('12') && ld.CreatedTime__c.contains('PM')){
		            ld.CreatedTime24__c = '24:00';
		        }
	    	} 

		}
	} */

	//method for updating Lead's related Objects on Lead Convert
	private static void updateRelatedObjsOnLeadConvert(list <Lead> ldlist){

		//Add convertedAccount to related Applicant 2 of Converted Leads
		list <Applicant_2__c> app2list = new list <Applicant_2__c>();
		for (Applicant_2__c app2 : [Select Account__c, Lead__c, Lead__r.isConverted, Lead__r.ConvertedAccountId From Applicant_2__c Where Lead__c in : ldlist]){
			if (app2.Lead__c != null){
				if (app2.Lead__r.isConverted){
					app2.Account__c = app2.lead__r.ConvertedAccountId;
					app2list.add(app2);
				}
			}
		}
		if (app2list.size() > 0) update app2list;

		//Add convertedAccount to related Assets of Converted Leads
		list <Assets__c> assetlist = new list <Assets__c>();
		for (Assets__c asset : [Select Account__c, Lead__c, Lead__r.isConverted, Lead__r.ConvertedAccountId  From Assets__c Where Lead__c in : ldlist]){
			if (asset.Lead__c != null){
				if (asset.Lead__r.isConverted){
					asset.Account__c = asset.lead__r.ConvertedAccountId;
					assetlist.add(asset);
				}
			}
		}
		if (assetlist.size() > 0) update assetlist;

		//Add convertedAccount to related Employers of Converted Leads
		list <Employee__c> emplist = new list <Employee__c>();
		for (Employee__c emp : [Select Account__c, Lead__c, Lead__r.isConverted, Lead__r.ConvertedAccountId From Employee__c Where Lead__c in : ldlist]){
			if (emp.Lead__c != null){
				if (emp.Lead__r.isConverted){
					emp.Account__c = emp.lead__r.ConvertedAccountId;
					emplist.add(emp);
				}
			}
		}
		if (emplist.size() > 0) update emplist;

		//Add convertedAccount to related Liabilities of Converted Leads
		list <Liability__c> liabilitylist = new list <Liability__c>();
		for (Liability__c lia : [Select Account__c, Lead__c, Lead__r.isConverted, Lead__r.ConvertedAccountId From Liability__c Where Lead__c in : ldlist]){
			if (lia.Lead__c != null){
				if (lia.Lead__r.isConverted){
					lia.Account__c = lia.lead__r.ConvertedAccountId;
					liabilitylist.add(lia);
				}
			}
		}
		if (liabilitylist.size() > 0) update liabilitylist;

		//Add convertedAccount to related References of Converted Leads
		list <Reference__c> reflist = new list <Reference__c>();
		for (Reference__c ref : [Select Account__c, Lead__c, Lead__r.isConverted, Lead__r.ConvertedAccountId From Reference__c Where Lead__c in : ldlist]){
			if (ref.Lead__c != null){
				if (ref.Lead__r.isConverted){
					ref.Account__c = ref.lead__r.ConvertedAccountId;
					reflist.add(ref);
				}
			}
		}
		if (reflist.size() > 0) update reflist;

		//Add convertedAccount to related Form Submissions of Converted Leads
		list <Form_Submissions__c> formsublist = new list <Form_Submissions__c>();
		for (Form_Submissions__c formsub : [Select Opportunity__c, Lead__c, Lead__r.isConverted, Lead__r.ConvertedOpportunityId From Form_Submissions__c Where Lead__c in : ldlist]){
			if (formsub.Lead__c != null){
				if (formsub.Lead__r.isConverted){
					formsub.Opportunity__c = formsub.lead__r.ConvertedOpportunityId;
					formsublist.add(formsub);
				}
			}
		}
		if (formsublist.size() > 0) update formsublist;

	}

	//LIG-726 - Method for updating NVM Related Fields based on Pending_Action__c records.
	private static void updateNVMRelatedFieldsInLead(Set<Id> updatedLeadIdsForNVMUpdate, List<Lead> updatedLeads){

		List<Pending_Action__c> pendingActionList = CommonTriggerController.queryPendingActionList(updatedLeadIdsForNVMUpdate,'updateNVMRelatedFields');

		Map<Id,Pending_Action__c> pendingActionMap = new Map<Id,Pending_Action__c>();

		if(pendingActionList.size() > 0){

			for(Pending_Action__c pa : pendingActionList){
				if(!pendingActionMap.containsKey(pa.Related_Record_Id__c)){
					pendingActionMap.put(pa.Related_Record_Id__c,pa);
				}
			}

			Map<Id,List<Task>> leadTaskMap = new Map<Id,List<Task>>();
			
			for(Task tsk : [SELECT Id, WhoId, CallType FROM Task WHERE CallType=:CommonConstants.TASK_CALLTYPE_OUTBOUND AND Status=:CommonConstants.TASK_STATUS_COMPLETED AND WhoId IN : updatedLeadIdsForNVMUpdate ALL ROWS]){
				if(!leadTaskMap.containsKey(tsk.WhoId)){
					leadTaskMap.put(tsk.WhoId,new List<Task>());
				}
				leadTaskMap.get(tsk.WhoId).add(tsk);
			}

			for(Lead leadInstance : updatedLeads){
				if(leadTaskMap.containsKey(leadInstance.Id)){
					leadInstance.Calls_Made__c = leadTaskMap.get(leadInstance.Id).size();
				}
				if(pendingActionMap.containsKey(leadInstance.Id)){
					if(pendingActionMap.get(leadInstance.Id).CW_Call_End_Time__c != NULL){
						leadInstance.Last_Call_Time_Ended__c = pendingActionMap.get(leadInstance.Id).CW_Call_End_Time__c;
					}
				}
			}
			if(pendingActionList.size()>0) delete pendingActionList;
		}
	}
}