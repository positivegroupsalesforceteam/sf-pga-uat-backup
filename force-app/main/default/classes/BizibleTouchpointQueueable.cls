/** 
* @FileName: BizibleTouchpointQueueable
* @Description: Class implements Queueable Apex 
* @Copyright: Positivve (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 17/10/2019 extra/task26438892-BizibleTriggerIssue - created to resolve Bizible Touchpoint Trigger issue (hitting Apex CPU time limit and Unable to lock row)
* 1.1 22/10 RDAVID extra/task26490057-PLSCaseAssignment22102019 - Fix Issue on creating Case TPs
* 1.2 25/10/2019 RDAVID tasks/26493146-BizibleTPITriggerIssue - Fix Issue on creating TPs duplicate
**/ 

public class BizibleTouchpointQueueable implements Queueable {
    public Map<Id,Account> accMap;
    @testVisible public static Boolean doChainJob = true;

    public Set<Id> accIdSet = new Set<Id>();
    public Boolean isDel;
    public List<Case_Touch_point__c> ctpToInsertList = new List<Case_Touch_point__c>();
    public Map<Id,Case_Touch_point__c> ctpToUpdateMap = new Map<Id,Case_Touch_point__c>(); 
    public List<Case_Touch_point__c> ctpToUpdateListOnDel = new List<Case_Touch_point__c>();

    public BizibleTouchpointQueueable(Set<Id> accountIds, Boolean isDelete){//Map<ID,Account> accountMap, List<Case_Touch_point__c> ctpToInsertList, Map<Id,Case_Touch_point__c> ctpToUpdateMap){
        // this.accMap = accountMap;  
        this.accIdSet = accountIds;
        this.isDel = isDelete;
    }

    public void execute(QueueableContext context) {
        Map<Id,Case> caseWithCTPMap = new Map<Id,Case>();
        Map<Id,Set<Id>> casewithCTPIdsMap = new Map<Id,Set<Id>>(); //get the Current Case Touchpoints

        if(accIdSet.size() > 0){ // Account Ids of Bizible Inserted/Deleted from Trigger
            accMap = new Map<Id,Account> ([ SELECT Id, (SELECT Id,bizible2__Touchpoint_Date__c FROM bizible2__Bizible_Touchpoints__r ORDER BY bizible2__Touchpoint_Date__c ASC), (SELECT Id,Case__c FROM Roles__r WHERE Primary_Contact_for_Application__c = TRUE ORDER BY CreatedDate DESC LIMIT 1) FROM Account WHERE Id IN: accIdSet]);
            if(!accMap.isEmpty()){
                Set<Id> caseIdSet = BizibleTouchpointGateway.getCaseIDs(accMap);
                if(caseIdSet.size() > 0){
                    //Get Cases with CTP 
                    caseWithCTPMap = new Map<Id,Case> ([SELECT Id,(SELECT Id, Latest_Touchpoint__c, Bizible_Touchpoint__c, Case__c FROM Case_Touch_points__r ORDER BY Bizible_Touchpoint__r.bizible2__Touchpoint_Date__c ASC) FROM Case WHERE Id IN : caseIdSet]);
                    //Get Current Cases with Ids of Case Touchpoint to avoid duplicates
                    casewithCTPIdsMap = BizibleTouchpointGateway.getCasewithCTPIdsMap(caseWithCTPMap);
                    for(Account acc : accMap.values()){ //Loop in Account
                        if(acc.Roles__r != NULL && acc.Roles__r.size() > 0){ //Get the latest Primary Contact for Application Role of Account 
                            Id caseId = acc.Roles__r[0].Case__c; //Get Case Id of the latest Role  
                            if(!isDel){ //If Trigger is Insert OR Update
                                for(bizible2__Bizible_Touchpoint__c bztp : acc.bizible2__Bizible_Touchpoints__r){ //Loop in Bizible Touchpoints of Account
                                    Case_Touch_point__c ctp = new Case_Touch_point__c (Case__c = caseId, Bizible_Touchpoint__c = bztp.Id); 
                                    
                                    if(acc.bizible2__Bizible_Touchpoints__r.indexOf(bztp)+1 == acc.bizible2__Bizible_Touchpoints__r.size()){
                                        ctp.Latest_Touchpoint__c = true;
                                    }
                                    if(!casewithCTPIdsMap.isEmpty() && casewithCTPIdsMap.containsKey(caseId)){
                                        if(!casewithCTPIdsMap.get(caseId).contains(bztp.Id)){
                                            if (ctp.Case__c != null){ 
                                                ctpToInsertList.add(ctp); 
                                            }
                                            if(!BizibleTouchpointGateway.ctpMap.isEmpty() && BizibleTouchpointGateway.ctpMap.containsKey(caseId))
                                            ctpToUpdateMap.putAll(BizibleTouchpointGateway.ctpMap.get(caseId));
                                        } 
                                    }
                                    else if(!casewithCTPIdsMap.containsKey(caseId)){
                                        if (ctp.Case__c != null){ 
                                            ctpToInsertList.add(ctp);
                                        }
                                    }
                                }
                            }
                            else{
                                if(caseWithCTPMap.containsKey(caseId) && caseWithCTPMap.get(caseId).Case_Touch_points__r.size() > 0){
                                    Case_Touch_point__c ctp = new Case_Touch_point__c ( Id = caseWithCTPMap.get(caseId).Case_Touch_points__r[caseWithCTPMap.get(caseId).Case_Touch_points__r.size()-1].Id, 
                                                                                        Latest_Touchpoint__c = true);   
                                    ctpToUpdateListOnDel.add(ctp);
                                }
                            }
                        }
                    }
                }
            } 
        }

        //Update Existing Case Touchpoints
        if(!ctpToUpdateMap.isEmpty()){
            try{
                String processName = 'Update Case Touchpoints';// list <Case_Touch_point__c> lockCaseTPForUpdate = [Select Id From Case_Touch_point__c Where Id in : ctpToUpdateMap.keySet() FOR UPDATE];
                Database.SaveResult[] results = Database.update(ctpToUpdateMap.values(),false); //Update CSTP DML Allow Partial 
                if (results != null){
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Integer errornum = 0;
                    Integer recordsAffected = 0;
                    for (Database.SaveResult result : results) {
                        if (!result.isSuccess()) {
                            recordsAffected++;
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = result.getErrors();
                            Id recordID = result.getId();
                            for(Database.Error err : errs) {
                                String emailContent = 'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage();
                                if(!errLogs.contains(emailContent)){
                                    errLogs.add(emailContent);  
                                    errornum ++;
                                } 
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',BizibleTouchpointQueueable.class.getName() + ' - ' + processName,errLogs);
                        if(recordsAffected != 0) logWrapper.numberOfRecords = recordsAffected*errornum;
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                }
            } catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }

        //Insert New Case Touchpoints
        if(ctpToInsertList.size() > 0){
            try{
                String processName = 'Insert Case Touchpoints';
                Database.SaveResult[] results = Database.insert(ctpToInsertList,false); //Insert CSTP DML Allow Partial // list <Case> lockCaseForUpdate = [Select Id From Case Where Id in : getCaseId(ctpToInsertList) FOR UPDATE];
                if (results != null){
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Integer errornum = 0;
                    Integer recordsAffected = 0;
                    for (Database.SaveResult result : results) {
                        if (!result.isSuccess()) {
                            recordsAffected++;
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = result.getErrors();
                            Id recordID = result.getId();
                            for(Database.Error err : errs) {
                                String emailContent = 'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage();
                                if(!errLogs.contains(emailContent)){
                                    errLogs.add(emailContent);  
                                    errornum ++;
                                } 
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',BizibleTouchpointQueueable.class.getName() + ' - ' + processName,errLogs);
                        if(recordsAffected != 0) logWrapper.numberOfRecords = recordsAffected*errornum;
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                }
            } catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }

        //Update Case Touchpoints on DELETE of BIZIBLE TP
        if(ctpToUpdateListOnDel.size() > 0){
            try{
                Database.SaveResult[] results = Database.update(ctpToUpdateListOnDel,false); //UPDATE CSTP DML Allow Partial //list <Case> lockCaseForUpdate = [Select Id From Case Where Id in : getCaseId(ctpToUpdateListOnDel) FOR UPDATE];// list <Case_Touch_point__c> lockCaseTPForUpdate = [Select Id From Case_Touch_point__c Where Id in : ctpToUpdateMap.keySet() FOR UPDATE];
                String processName = 'Update Case Touchpoints on Delete of Bizible TPs';
                if (results != null){
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Integer errornum = 0;
                    Integer recordsAffected = 0;
                    for (Database.SaveResult result : results) {
                        if (!result.isSuccess()) {
                            recordsAffected++;
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = result.getErrors();
                            Id recordID = result.getId();
                            for(Database.Error err : errs) {
                                String emailContent = 'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage();
                                if(!errLogs.contains(emailContent)){
                                    errLogs.add(emailContent);  
                                    errornum ++;
                                } 
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',BizibleTouchpointQueueable.class.getName() + ' - ' + processName,errLogs);
                        if(recordsAffected != 0) logWrapper.numberOfRecords = recordsAffected*errornum;
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                }
            } catch (Exception e) {
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
    }
}