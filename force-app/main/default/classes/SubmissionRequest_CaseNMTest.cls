/** 
* @FileName: SubmissionRequest_CaseNMTest
* @Description: Test Class for Submission Request NM
* @Copyright: Positive (c) 2018 
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
**/ 
@isTest
public class SubmissionRequest_CaseNMTest {
	@testsetup static void setup(){
        TestDataFactory.Case2FOSetupTemplate();
    }
    
    static testmethod void testSubmissionPage(){
        Test.startTest();
        	Case cse = [SELECT Id, CaseNumber, Application_Name__r.RecordtypeId, Application_Name__r.Recordtype.Name, Lender1__c, Lender1__r.Name FROM Case LIMIT 1];
        	System.assertNotEquals(cse, new Case());
        	PageReference pref = Page.SubmissionRequestCaseNM;
            Test.setCurrentPage(pref);
        	Test.setCurrentPageReference(pref); 
			System.currentPageReference().getParameters().put('id', cse.Id);
        	SubmissionRequest_CaseNM subCtrlr = new SubmissionRequest_CaseNM();
        	subCtrlr.subReq.Have_signed_privacy_consent__c = true;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = true;
            subCtrlr.subReq.Support_Docs_Combined__c = true;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = true;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = 'Ready_For_Submission_Notes1__c';
            subCtrlr.subReq.Notes_For_Lender1__c = 'Notes_For_Lender1__c';        	
        	subCtrlr.submit();
        	List<Submission_Request_CT__c> srList = [SELECT Id FROM Submission_Request_CT__c WHERE Case__c =: cse.Id];
        	System.assertNotEquals(srList.size(), 0);
        	subCtrlr.readyforSub();
        	subCtrlr.caseid();
        Test.stopTest();
    }
    
    static testmethod void testSubmissionPageNegative(){
        Test.startTest();
        	Case cse = [SELECT Id, CaseNumber, Application_Name__r.RecordtypeId, Application_Name__r.Recordtype.Name, Lender1__c, Lender1__r.Name FROM Case LIMIT 1];
        	System.assertNotEquals(cse, new Case());
        	PageReference pref = Page.SubmissionRequestCaseNM;
            Test.setCurrentPage(pref);
        	Test.setCurrentPageReference(pref); 
			System.currentPageReference().getParameters().put('id', cse.Id);
        	SubmissionRequest_CaseNM subCtrlr = new SubmissionRequest_CaseNM();
        	subCtrlr.subReq.Have_signed_privacy_consent__c = false;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = false;
            subCtrlr.subReq.Support_Docs_Combined__c = false;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = false;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = 'Ready_For_Submission_Notes1__c';
            subCtrlr.subReq.Notes_For_Lender1__c = 'Notes_For_Lender1__c';
        	subCtrlr.submit();
        	subCtrlr.subReq.Have_signed_privacy_consent__c = true;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = false;
            subCtrlr.subReq.Support_Docs_Combined__c = false;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = false;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = '';
            subCtrlr.subReq.Notes_For_Lender1__c = '';
        	subCtrlr.submit();
        	subCtrlr.subReq.Have_signed_privacy_consent__c = true;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = true;
            subCtrlr.subReq.Support_Docs_Combined__c = false;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = true;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = '';
            subCtrlr.subReq.Notes_For_Lender1__c = '';
        	subCtrlr.submit();
        	subCtrlr.subReq.Have_signed_privacy_consent__c = true;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = true;
            subCtrlr.subReq.Support_Docs_Combined__c = true;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = false;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = '';
            subCtrlr.subReq.Notes_For_Lender1__c = '';
        	subCtrlr.submit();
        	subCtrlr.subReq.Have_signed_privacy_consent__c = true;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = true;
            subCtrlr.subReq.Support_Docs_Combined__c = true;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = true;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = 'test';
            subCtrlr.subReq.Notes_For_Lender1__c = '';
        	subCtrlr.submit();	
        	subCtrlr.subReq.Have_signed_privacy_consent__c = true;
            subCtrlr.subReq.Have_checked_income_on_payslips__c = true;
            subCtrlr.subReq.Support_Docs_Combined__c = true;
            subCtrlr.subReq.Salesforce_Record_Accurate__c = true;
            subCtrlr.subReq.Ready_For_Submission_Notes1__c = '';
            subCtrlr.subReq.Notes_For_Lender1__c = 'test';
        	subCtrlr.submit();	
        	List<Submission_Request_CT__c> srList = [SELECT Id FROM Submission_Request_CT__c WHERE Case__c =: cse.Id];
            System.assertEquals(srList, new List<Submission_Request_CT__c>());
        	
        Test.stopTest();
    }
    
}