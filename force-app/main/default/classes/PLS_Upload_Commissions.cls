public class PLS_Upload_Commissions{
    
    //Document to Upload
    public document transactionFile {get;set;}
    
    // To Hold the CSV in a string
    public string csvAsString;
    
    //To Hold Each Line of a CSV
    public list<String> csvFileLines;
    
    // To Hold Bank Names csv and institution Picklsit
    public map<string,string> banks;
    
    // To Hold all commissions 
    public list<Commission__c> comms {get;set;}
    
    
    // To show affected commissions on vf
    public list<Commission__c> updatedComms {get;set;}
    
    
    // Constructor - Initialize all initial values here 
    public PLS_Upload_Commissions(){
        transactionFile = new document();
        csvFileLines = new list<String>();
        banks = new map<string,string>();
        banks.put('Liberty','liberty');
        banks.put('Macquarie','ean');//McQuarrie        
        banks.put('Pepper','Pepper');        
        banks.put('Finance1','financeone');
    }
    
    /*
        Name:upload
        Desc:Upload the CSV file from local Machine to Documents in Salesforce
        Variables:NO
        Return:void    
    */
    public void upload(){
        try{        
            transactionFile.folderid = UserInfo.getUserId();        
            insert transactionFile;        
            processFile(transactionFile);
            transactionFile = new document();
        }catch(exception e){
            Apexpages.addmessages(e);
            transactionFile = new document();
        }
    }
    
    /*
        Name:processFile
        Desc:This is to Process the CSV file uploaded.Read the Ref Num and Amount and process.
        Variables:Document
        Return:void    
    */
    public void processFile(document doc){
        // To Hold the Document Body 
        blob csvFileBody = doc.body;        
        //Convert the CSV to string
        csvAsString = csvFileBody.toString();
        csvFileLines = csvAsString.split('\n');        
        map<string,map<decimal,string>> fullMap = new map<string,map<decimal,string>>();        
        // Iterate over Each Bank and Each Line and Hold the Matching Values of Bank, Ref num and Amount
        for(Integer i=1;i<csvFileLines.size();i++){
            for(string b:banks.keyset()){
                string[] csvRecordData = csvFileLines[i].split(',');
                string col2 = csvRecordData[2].replaceAll( '\\s+', '');  // Remove all spaces                                           
                if(col2.containsIgnoreCase(banks.get(b))){ // Ignore the case , as csv might be uploaded small or upper case                                    
                    //To Hold the Ref Num
                    String transNumber;                    
                    // Read the Ref Number at the Last of the First Column in CSV **ex:  TRANSFER FROM LIBERTY FINANCIA 55975 **
                    Integer index = csvRecordData[2].lastIndexOf(' ');
                    transNumber = csvRecordData[2].substring(index); 
                    // For Finance One the format change as ref number is in Middle of the String **ex: TRANSFER FROM FINANCE ONE 23137165 - HEARN **
                    if(banks.get(b) == 'financeone'){
                        transNumber = col2.replaceAll('\\D+','');
                    }                    
                    //Remove any Spaces in ref Number and bank names
                    transNumber = transNumber.TRIM();                    
                    string Amt = csvRecordData[1];
                    //Add .00 as salesforce returns .00 for amount 
                    if(!Amt.contains('.')){
                        Amt = Amt+'.00';
                    }  
                    //Populate Map -> Ref<Amt-Bank+date>                 
                    if(fullMap.get(transNumber) == null ){
                        fullMap.put(transNumber,new map<decimal,string>{decimal.valueof(Amt ) => b+','+csvRecordData[0]});
                    }else{
                        fullMap.get(transNumber).put(decimal.valueof(Amt ),b+','+csvRecordData[0]);
                    }              
                    
                }
                
            }
        }        
        // Query List of Commissions Matching with Bank and ref Number
        comms = new list<Commission__c>();
        updatedcomms = new list<Commission__c>();
        comms = [SELECT Id,Name,Reference__c,Commision_Amount_Inc_GST__c,Institution__c,paid__c 
                from Commission__c 
                where Institution__c IN:banks.KEYSET() 
                AND Reference__c IN:fullMap.KEYSET() 
                AND paid__c=:false LIMIT 30000]; // LIMIT CAN BE INCREASED
        for(Commission__c c:comms){            
            if(fullMap.containskey(c.Reference__c)){ // Same Ref Num                 
                Map<decimal,string> innerMap = fullMap.get(c.Reference__c);
                //Extract Date
                List<String> parts = new list<string>();
                if(innerMap.get(c.Commision_Amount_Inc_GST__c) != null){
                    parts = innerMap.get(c.Commision_Amount_Inc_GST__c).split(',');
                }
                if(parts.size() != 0){
                    string bank; 
                    if(parts[0] != null){
                        bank = parts[0];
                    }                    
                    if( bank  == c.Institution__c){ //Bank Same      
                        if(innerMap.containskey(c.Commision_Amount_Inc_GST__c)){ // Same Amount                 
                            c.paid__c = true;
                            if(parts[1]!= null){
                                c.Date_paid__c = date.parse(parts[1]);
                            }
                            updatedcomms.add(c);
                        }    
                    }
                }
            }
        } 
        update updatedcomms;          
    }

}