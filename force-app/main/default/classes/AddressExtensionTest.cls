/*
    @author: Jesfer Baculod - Positive Group
    @history: 07/08/18 - Created
    @description: test class of AddressExtension
*/
@isTest
private class AddressExtensionTest {

    @testSetup static void setupData() {

        List<Location__c> locListUNK = TestDataFactory.createLocation(1,'UNKNOWN','','','','','');
		list <Location__c> locList = TestDataFactory.createLocation(1,'2000-BARANGAROO','2000','BARANGAROO','NSW','New South Wales','Australia');
		List<Location__c> locListTwo = TestDataFactory.createLocation(1,'2000-DAWES POINT','2000','DAWES POINT','NSW','New South Wales','Australia');
		locListTwo.addAll(locList);
		locListTwo.addAll(locListUNK);
		Database.insert(locListTwo);

    }

    static testMethod void testCreateAddress(){

        //Retrieve created test data for Location
        List <Location__c> loclist = [Select Id, Name From Location__c];
        
        Test.startTest();
            Address__c addr = new Address__c();
            ApexPages.StandardController st = new ApexPages.StandardController(addr);
            AddressExtension addex = new AddressExtension(st);

            addex.addrSave(); //Fail saving due to validation
            addex.addr.Location__c = loclist[0].Id;
            addex.populateAddresswithLocation();
            addex.addrSave(); //Successful Save

            //Retrieve created Address
            list <Address__c> addrlist = [Select Id, Location__c From Address__c];
            system.assertEquals(addrlist[0].Location__c, loclist[0].Id);

            addex.addr.Street_Number__c = '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111';
            addex.addrSave(); //Hit Exception

        Test.stopTest();

    }

}