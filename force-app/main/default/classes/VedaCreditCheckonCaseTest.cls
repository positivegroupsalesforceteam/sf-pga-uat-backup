/**
 * @History -  09/11/2017 - Created (Jesfer Baculod - Positive Group)
 */
@isTest
private class VedaCreditCheckonCaseTest {

    private static string PROFILE_LT = Label.Profile_Name_Lightning_Team; //Lightning Team
    private static string PROFILE_SYSADMIN = Label.Profile_Name_System_Administrator; //System Administrator
    private static final string ACC_RT_INDIVIDUAL = Label.Contact_Individual_RT; //Individual
    private static final string CASE_RT_PRIVATE = Label.Case_Private_RT; //Consumer Loan

    @testsetup static void setup(){

    	//create test data for Sys Admin
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : PROFILE_SYSADMIN ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;

        //create test data for Lightning team User
        ID roleID = [Select Id, Name From UserRole Where Name = : 'Team Member' ].Id;
        ID pID_st = [Select Id, Name From Profile Where Name = : 'Sales Team' ].Id;
        User stuser = new User(
                UserName = 'testlt@vedaindi.com.uat',
                LastName = 'TestLT',
                Email = 'testlt@vedaindi.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tlt',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                UserRoleId = roleID,
                ProfileId =  pID_st
            );
        insert stuser;

        system.runAs(sysadminUsr){

        	//Create test data for Person Account
        	Account acc = TestHelper.createPersonAccount();

        	//Create test data for Contact
        	Contact c = TestHelper.createContact();

        	//Create test data for Case
        	Id rtPrivateLoan = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CASE_RT_PRIVATE).getRecordTypeId();
			Case cse = new Case(
				RecordTypeId = rtPrivateLoan,
				Status = 'New',
				Sale_Type__c = Label.Case_Sale_Type_Dealer,
				ContactId = c.Id
			);
			insert cse;

        }


    }

    static testMethod void testVedaCreditCheckonCaseContactError() {
          
        Contact con = [Select Id, Name From Contact limit 1];
        Case cse = [Select Id, CaseNumber From Case];
        User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat'];
        
        Attachment att = new Attachment(
                        ParentId = con.Id,
                        Name = 'Test-CC.pdf',
                        Body = Blob.valueOf('test')
                    );
        insert att;
        
        System.assert(con!=null);
        Test.startTest();
        
            system.runAs(stusr){
                PageReference pageRef = Page.VedaCreditcheckonContact;
                Test.setCurrentPage(pageRef);
                VedaCreditCheckonCase ext = new VedaCreditCheckonCase(new ApexPages.StandardController(con));
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                ext = new VedaCreditCheckonCase(new ApexPages.StandardController(cse));    
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
            }

            //Verify that page has error messages
            system.assertEquals(ApexPages.hasMessages(),true);
        
        Test.stopTest(); 
       
    }

    static testMethod void testVedaCreditCheckonCaseContactSuccess() {
          
        Account acc = [Select Id, Name From Account];
        Contact con = [Select Id, Name From Contact limit 1];
        User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat'];
        
        System.assert(con!=null);
        Test.startTest();
        
            system.runAs(stusr){
                PageReference pageRef = Page.VedaCreditcheckonContact;
                Test.setCurrentPage(pageRef);
                VedaCreditCheckonCase ext = new VedaCreditCheckonCase(new ApexPages.StandardController(con));
                ext.testresponse = 'test';
                ext.generateCreditReport();

                ext = new VedaCreditCheckonCase(new ApexPages.StandardController(acc));
                ext.testresponse = 'test';
                ext.generateCreditReport();
            }
        
        Test.stopTest(); 

        //Retrieve created Attachment (response) on Account
        Attachment att = [Select Id, Name, ParentId From Attachment Where ParentId = : con.Id];
        system.assertEquals(att.Name.contains('CCResponse'),true);
       
    }


}