public without sharing class NewPartner_CC {
    public Boolean showForm {get;set;}
    public Boolean validationSuccess {get;set;}
    public Boolean hasErrors {get;set;}
    public Boolean hasAccErrors {get;set;}
    public Boolean hasPartnerValidationErrors {get;set;}
    public Boolean hasRelAccValidationErrors {get;set;}

    public Boolean saveRelationshipSuccess {get;set;}
    public Boolean savePartnerSuccess {get;set;}
    public Boolean saveCaseSuccess {get;set;}

    public Boolean validForSaving {get;set;}
    public Boolean displayPopup {get;set;}
    public Boolean showCreatePartner {get;set;}
    public Boolean isCreateABN {get;set;}

    public Boolean selectExistingAccount {get;set;}
    public Boolean editExistingAccount {get;set;}

    public Boolean isCreatePartnerAddress {get;set;}

    public Boolean doneSavingNewPartner {get;set;}

    public Boolean insertNewRelationship {get;set;}

    public Boolean validPartnerContactSearch {get;set;}

    public Date birthdateVariable {get;set;}

    public Partner__c partner {get;set;}
    public Account inputaccount {get;set;}
    public Contact contact {get;set;}
    public Relationship__c relationship {get;set;}
    public ABN__C inputABN  {get;set;}
    public ABN__C currentABN  {get;set;}
    public Id caseId {get;set;}

    public Address__c inputPartnerAddress {get;set;}
    public Address__c currentAddress  {get;set;}

    public Location__c location  {get;set;}

    public string conLastName {get;set;}
    public string partnersearchstring{get;set;}
    public string accountsearchstring{get;set;}

    public Id  RELATIONSHIP_PARTNER_CONTACT_RTID = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName().get('Partner Contact').getRecordTypeId();
    public Id  ACCOUNT_NM_INDIVIDUAL_RTID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('nm: Individual').getRecordTypeId();
    public Id PARTNER_FINANCE_BROKERAGE_RTID = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Brokerage').getRecordTypeId();
    public static Map<Id,Schema.RecordTypeInfo> partnerRTMapById = Schema.SObjectType.Partner__c.getRecordTypeInfosById();
    
    public Integer partnerSize {get;set;} 
    public Integer noOfPartnerRecords {get; set;} 

    public Integer relSize {get;set;} 
    public Integer noOfRelRecords {get; set;} 

    public List<SelectOption> paginationSizeOptions {get;set;}
    public String partnerQuery {get;set;} 
    public String relQuery {get;set;} 

    public Date relStartDate {get;set;} 
    public Date relEndDate {get;set;}
    
    public Id relAccId {get;set;}
    public Id accId {get;set;}
    public static String relBaseQuery = 'SELECT Id, Name, Associated_Account__r.Name, Contact_Email__c, Relationship_Type__c,Partner__c,Job_Title__c,Partner_Contact_Reference_Number1__c,m_Role__c,m_RefCo_Link__c,Start_Date__c,End_Date__c,Broker_Relationship_Manager1__c,Nodifi_Access__c,Partition__c,Comments__c,h_ContactId__c,h_ReferralCompanyId__c,Associated_Account__c FROM Relationship__c ';
    public static String partnerBaseQuery = 'SELECT Id, Name, Partner_Parent__c, RecordTypeId, Partner_Phone__c, m_Commitment_Date__c, Franchise_Partner__c, Channel__c, Partner_Type__c, Partner_Website__c, Partition__c, m_Legal_Entity_Name__c,Partner_Address__c, Bank_Account_Name__c, Bank_Account_Number__c, BSB_Number__c FROM Partner__c ';

    public NewPartner_CC(ApexPages.StandardController stdController) {//ApexPages.StandardController stdController
        partner = new Partner__c(RecordTypeId = PARTNER_FINANCE_BROKERAGE_RTID);
        relationship = new Relationship__c(RecordTypeId=RELATIONSHIP_PARTNER_CONTACT_RTID);
        inputPartnerAddress = new Address__c();
        inputABN = new ABN__C();
        currentABN = new ABN__C();
        contact = new Contact();
        inputAccount = new Account(RecordTypeId=ACCOUNT_NM_INDIVIDUAL_RTID);
        
        editExistingAccount = saveRelationshipSuccess = selectExistingAccount = isCreatePartnerAddress = doneSavingNewPartner = hasPartnerValidationErrors = hasErrors = isCreateABN = savePartnerSuccess = insertNewRelationship = saveCaseSuccess = false;
        
        showCreatePartner = apexpages.currentpage().getparameters().get('save_new') == '1' || apexpages.currentpage().getparameters().get('sfdc.sdnewedit') == '1' ? true : false;
        
        Id partnerId = apexpages.currentpage().getparameters().get('id');
        String relname = apexpages.currentpage().getparameters().get('relname');
        caseId = apexpages.currentpage().getparameters().get('caseId');

        System.debug('apexpages.currentpage() -------------> '+apexpages.currentpage());
        System.debug('ID -------------> '+partnerId);
        System.debug('RELNAME -------------> '+relname);
        System.debug('caseId -------------> '+caseId);
        System.debug('showCreatePartner -------------> '+showCreatePartner);

        if(partnerId != NULL){
            loadPartnerData(partnerId);
            if(!String.IsBlank(relationship.Partner__c))
            relQuery = relBaseQuery+' WHERE Partner__c = ' + '\'' + relationship.Partner__c + '\'' +' ORDER BY CreatedDate DESC';
        } 

        partnerSize=5;
        relSize=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));

        getPartnerTblList();
        getRelationshipTblList();

        System.debug(' init noOfRelRecords -------------> '+noOfRelRecords);

        if(noOfRelRecords == 0 && partnerId != NULL){
            System.debug(' init noOfRelRecords -------------> '+noOfRelRecords);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'No existing Partner Contact on the selected Partner record. Please click the New Partner Contact button to create a new one. '));
        }
        showForm = true;
    }

    public ApexPages.StandardSetController partnerSetCon {
        
        get {
            if(partnerSetCon == null && !String.IsBlank(partnerQuery)) {
                System.debug('>>>>>>>>>> partnerQuery = '+partnerQuery);                
                partnerSetCon = new ApexPages.StandardSetController(Database.getQueryLocator(partnerQuery));
                if(partnerSize == null) partnerSize = 5;
                partnerSetCon.setPageSize(partnerSize);  
                noOfPartnerRecords = partnerSetCon.getResultSize();
                System.debug('>>>>>>>>>> partnerSetCon METHOD --> noOfPartnerRecords == '+noOfPartnerRecords);
            }       
            return partnerSetCon;
        }
        set;
        
    }

    public ApexPages.StandardSetController relSetCon {
        get {
            if(relSetCon == null && !String.IsBlank(relQuery)) {                
                relSetCon = new ApexPages.StandardSetController(Database.getQueryLocator(relQuery));
                if(relSize == null) relSize = 5;
                relSetCon.setPageSize(relSize);  
                noOfRelRecords = relSetCon.getResultSize();
                System.debug('>>>>>>>>>> relSetCon METHOD --> noOfRelRecords == '+noOfRelRecords);
            }            
            return relSetCon;
        }
        set;
    }

    //Changes the size of pagination
    public PageReference refreshPartnerPageSize() {
         partnerSetCon.setPageSize(partnerSize);
         return null;
    }

    //Changes the size of pagination
    public PageReference refreshRelPageSize() {
         relSetCon.setPageSize(relSize);
         return null;
    }

    public void validateParNameSearchStr(){
        System.debug('>>> START validateParNameSearchStr METHOD <<< ');
        clearRelationship();
        errorloc = 'nulrelapar';
        System.debug('>>> END validateParNameSearchStr METHOD <<< ');
    }

    public void validateParConNameSearchStr(){
        System.debug('>>> START validateParConNameSearchStr METHOD <<< ');
        errorloc = '';
        validPartnerContactSearch = true;
        // clearRelationship();
        
        if(String.IsBlank(accountsearchstring)){
            relationship.Id = null;
            relationship.Associated_Account__c = null;
            validPartnerContactSearch = false;
            errorloc += 'nulparcon';
        }
            
        if(String.IsBlank(relationship.Partner__c)){
            relationship.Id = null;
            relationship.Associated_Account__c = null;
            errorloc += 'relpar';
            validPartnerContactSearch = false;
        }
        System.debug('>>> END validateParConNameSearchStr METHOD <<< ');
    }

    public PageReference validateNewEdiParCon() {
        System.debug('>>> START validateNewEdiParCon METHOD <<< ');
        Integer errCount = 0;
        errorloc = '';

        if(String.IsBlank(relationship.Partner__c)){
            system.debug('          validateNewEdiParCon1');
            errorloc += 'relpar';
            errCount++;
        }

        system.debug('          validateNewEdiParCon - errCount = '+errCount);
        
        if(errCount == 0){
            validationSuccess = true;
            hasPartnerValidationErrors = hasErrors = false;
        }
        else{
            validationSuccess = false;
            hasPartnerValidationErrors = true;
        }
        System.debug('>>> END validateNewEdiParCon METHOD <<< ');
        return null;
    }

    public PageReference searchPartner() {
        System.debug('>>>>> START searchPartner METHOD <<<<<');

        partnerSetCon = null;
        errorloc = '';
    
        partnersearchstring = String.escapeSingleQuotes(partnersearchstring);
        System.debug('partnersearchstring = '+partnersearchstring);
        partnerQuery = partnerBaseQuery+' WHERE Name LIKE \'%' + partnersearchstring+ '%\' ORDER BY CreatedDate DESC';
        system.debug('searchPartner partnerQuery -- '+partnerQuery);

        getPartnerTblList();

        if(noOfPartnerRecords == 0){ 
            system.debug('norecordfound'); 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'No partner record found with name: ' + partnersearchstring));
        }
        
        clearRelationship();

        System.debug('>>>>> END searchPartner METHOD <<<<<');
        return null;
    }
    
    public PageReference searchParCon() {
        System.debug('>>>>> START searchParCon METHOD <<<<<');
        relSetCon = null;
        relationship.Id = null;
        relationship.Associated_Account__c = null;

        accountsearchstring = String.escapeSingleQuotes(accountsearchstring);
        
        System.debug('accountsearchstring = '+accountsearchstring);
        relQuery = relBaseQuery+' WHERE Associated_Account__r.Name LIKE '+ '\'%' + accountsearchstring + '%\'' + ' AND Partner__c = ' + '\'' + relationship.Partner__c + '\'' +' ORDER BY CreatedDate DESC';
        system.debug('searchParCon relQuery -- '+relQuery);
        
        getRelationshipTblList();
        
        if(noOfRelRecords == 0){ 
            system.debug('no Relationship recordfound.'); 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'No Partner Contact record found with name: ' + accountsearchstring));
            relationship.Associated_Account__c = NULL;
        }

        System.debug('>>>>> END searchPartner METHOD <<<<<');
        return null;
    }

    public List<Partner__c> getPartnerTblList() {
        if(partnerSetCon != NULL){
            System.debug('>>>>> getPartnerTblList METHOD Start <<<<<');
            System.debug('Partner Set Controller NOT NULL ');
            // return (List<Partner__c>) partnerSetCon.getRecords();
            System.debug('Number of Partner Records --> '+noOfPartnerRecords + ' based on this QUERY --> '+partnerQuery);
            System.debug('>>>>> getPartnerTblList METHOD End <<<<<');
            return (List<Partner__c>) partnerSetCon.getRecords();
        }
        else{
            System.debug('Partner Set Controller IS NULL ');
        }
        return new List<Partner__c>();
    }

    public List<Relationship__c> getRelationshipTblList() {
        
        if(relSetCon != NULL){
            System.debug('>>>>> getRelationshipTblList METHOD Start <<<<<');
            System.debug('Relationship Set Controller NOT NULL ');
            System.debug('Number of Relationship Records --> '+noOfRelRecords + ' based on this QUERY --> '+relQuery);
            System.debug('>>>>> getRelationshipTblList METHOD End <<<<<');
            return (List<Relationship__c>) relSetCon.getRecords();
        }
        else{
            System.debug('Relationship Set Controller IS NULL ');
        }
        return new List<Relationship__c>();
    }

    public void clearRelationship(){
        relationship.Id = null;
        relationship.Associated_Account__c = null;
        relationship.Partner__c = null;
    }

    public void setRelAccId(){
        relationship.Associated_Account__c = relationship.Associated_Account__c;
        relAccId = relationship.Associated_Account__c;
        System.debug('setRelAccId ------------------> relationship.Associated_Account__c = '+relationship.Associated_Account__c);
        System.debug('setRelAccId ------------------> relAccId = '+relAccId);
    }
    
    public String errorloc {get;set;}

    public PageReference validateRelAcc() {
        system.debug('validateRelAcc -> relationship.Associated_Account__c --> '+relationship.Associated_Account__c);
        Integer errCount = 0;
        errorloc = '';

        if(String.IsBlank(conLastName) && !selectExistingAccount){
            errorloc += 'conlas';
            errCount++;
            system.debug('validateRelAcc1');
        }

        if(String.IsBlank(relationship.Associated_Account__c) && selectExistingAccount){
            // relationship.Associated_Account__c.addError('Please enter a value.');
            errorloc += 'relasscon';
            errCount++;
            system.debug('validateRelAcc2');
        }

        if(errCount == 0){
            validationSuccess = true;
            hasRelAccValidationErrors = hasErrors = false;
        }
        else{
            validationSuccess = false;
            hasRelAccValidationErrors = true;
        }
        return null;
    }

    public PageReference validatePartner() {
        system.debug('validatePartner');
        Integer errCount = 0;
        errorloc = '';

        if(String.IsBlank(partner.Name)){
            partner.Name.addError('Please enter a value.');
            errCount++;
            system.debug('validatePartner1');
        }

        if(isCreateABN && String.IsBlank(inputABN.Registered_ABN__c)){
            system.debug('validatePartner2');
            errorloc += 'regabn';
            errCount++;
        }
        if(isCreateABN && String.IsBlank(inputABN.Name)){
            system.debug('validatePartner4');
            errorloc += 'regname';
            errCount++;
        }
        
        if(isCreatePartnerAddress){
            if(inputPartnerAddress.Location__c == NULL){
                inputPartnerAddress.Location__c.addError('Please enter a value.');
                errorloc += 'loc';
                errCount++;
            }
            if(hasDupAddress(inputPartnerAddress)){
                hasErrors = true;//to show apex message if dup address exist
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Existing Address found. Please click Search Partner Address. '));
                errorloc += 'dupaddr';
                errCount++;
            }
        }

        system.debug('validatePartner - errCount = '+errCount);
        
        if(errCount == 0){
            validationSuccess = true;
            hasPartnerValidationErrors = hasErrors = false;
        }
        else{
            validationSuccess = false;
            hasPartnerValidationErrors = true;
        }

        return null;
    }

    public Boolean hasDupAddress(Address__c inputPartnerAddress){
        Boolean hasDupAddressIns = false;
        system.debug('@@@inputPartnerAddress = '+inputPartnerAddress);
        List<Address__c> dupAddress = [ SELECT Id, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, Country__c, Location__c, RecordTypeId 
                                        FROM Address__c 
                                        WHERE Street_Number__c =: inputPartnerAddress.Street_Number__c AND Street_Name__c =: inputPartnerAddress.Street_Name__c AND Street_Type__c =: inputPartnerAddress.Street_Type__c
                                        AND Suburb__c =: inputPartnerAddress.Suburb__c AND State__c =: inputPartnerAddress.State__c AND Postcode__c =: inputPartnerAddress.Postcode__c AND Country__c =: inputPartnerAddress.Country__c
                                        AND Location__c =: inputPartnerAddress.Location__c];
        system.debug('dupAddress size = '+dupAddress.size());

        if(dupAddress.size()>0){
            hasDupAddressIns = true; 
        }
        return hasDupAddressIns;
    }

    public PageReference saveRelationship() {
        system.debug('@@@@saveRelationship -> relationship.Associated_Account__c --> '+relationship.Associated_Account__c);
        system.debug('@@@@saveRelationship -> relAccId --> '+relAccId);
        relationship.Associated_Account__c = relAccId;
        system.debug('@@@@saveRelationship -> relationship.Associated_Account__c --> '+relationship.Associated_Account__c);
        system.debug('@@@@saveRelationship -> editExistingAccount --> '+editExistingAccount);
        Savepoint sp = Database.setSavepoint();
        try{
            if(!String.IsBlank(relationship.Associated_Account__c)){
                if(editExistingAccount){
                    inputAccount.FirstName = contact.FirstName;
                    inputAccount.LastName = conLastName;
                    inputAccount.PersonEmail = contact.Email;
                    inputAccount.PersonMobilePhone = contact.MobilePhone;
                    inputAccount.Id = relationship.Associated_Account__c;
                    update inputAccount;
                }
            }
            else{
                inputAccount.FirstName = contact.FirstName;
                inputAccount.LastName = conLastName;
                inputAccount.PersonEmail = contact.Email;
                inputAccount.PersonMobilePhone = contact.MobilePhone;
                insert inputAccount;
                relationship.Associated_Account__c = inputaccount.Id;        
            } 

            if(String.IsBlank(relationship.Id)){
                relationship.Id = null;
                //FOR Saving/Updating relationship in Case Level
                if(!String.IsBlank(caseId)){
                    insertNewRelationship = true;
                }
            } 
            if(relationship.Contact_Email__c != inputAccount.PersonEmail){
                System.debug('Contact Updated workflow should fire. '+ inputAccount.PersonEmail);
                relationship.Contact_Email1__c = inputaccount.PersonEmail;
            }
            relationship.Start_Date__c = relStartDate;
            relationship.End_Date__c = relEndDate;
            upsert relationship;
            //updateCase(relationship,relationship.Associated_Account__c);
            saveRelationshipSuccess = true;
            Account acc = [SELECT Id, Name, PersonMobilePhone, PersonEmail FROM Account WHERE Id =: relationship.Associated_Account__c];
            accountsearchstring = acc.Name;
            accId = relationship.Associated_Account__c;

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Partner Contact and Relationship record successfully saved.'));
        }
        catch(Exception e){
            saveRelationshipSuccess = false;
            processErrorMessage(e);
            hasErrors = true;
            //inputABN = new ABN__C();
            Database.rollback( sp );
        }
        return null;
    }

    public PageReference updateCase(){//Relationship__c relationship, Id accId
        System.debug('>>>>>>>> updateCase accId - '+ accId);
        System.debug('>>>>>>>> updateCase relationship - '+ relationship);
        System.debug('>>>>>>>> updateCase partner - '+ partner);
        Savepoint sp = Database.setSavepoint();
        Boolean updateCase = false;
        Account acc = [SELECT Id, Name, PersonMobilePhone, PersonEmail FROM Account WHERE Id =: accId];
        
        Case cse = new Case(Id = caseId);
        cse.Referral_Partner__c = partner.Id;
        if(relationship.Relationship_Type__c == 'Broker'){
            updateCase = true;
            cse.Introducer1__c = relationship.Id;
            cse.Introducer1_Name_MC__c = acc.Id;
            cse.Introducer1_Phone__c = acc.PersonMobilePhone;
            cse.Introducer1_Email__c = acc.PersonEmail;// Introducer1_State__c = acc.?,
            cse.Introducer1_Reference_Number__c = relationship.Partner_Contact_Reference_Number1__c;
            cse.Introducer1_Relationship_Manager__c = relationship.Broker_Relationship_Manager1__c ;
        }
        else if(relationship.Relationship_Type__c == 'Broker Support'){
            updateCase = true;
            cse.Introducer1_Support__c = relationship.Id;
            cse.Introducer1_Support_Name_MC__c = acc.Id;
            cse.Introducer1_Support_Phone__c = acc.PersonMobilePhone;
            cse.Introducer1_Support_Email__c = acc.PersonEmail;// Introducer1_State__c = acc.?,
            //cse.Introducer1_Reference_Number__c = relationship.Partner_Contact_Reference_Number1__c;
            cse.Introducer1_Support_Relationship_Manager__c = relationship.Broker_Relationship_Manager1__c; 
        }
    
        try{
            if(updateCase) {
                Database.update(cse);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Case successfully updated.'));
            }
            saveCaseSuccess = true;
        }
        catch(Exception e){
            saveCaseSuccess = false;
            processErrorMessage(e);
            hasErrors = true;
            //inputABN = new ABN__C();
            Database.rollback( sp );
        }
        return null;
    }

    public PageReference savePartner() {
        Savepoint sp = Database.setSavepoint();
        try{
            //system.debug('@@relationshipTypes:'+relationshipTypes);
            system.debug('@@@@isCreateABN = '+isCreateABN);
            
            if(isCreateABN){
                insert inputABN;
                partner.m_Legal_Entity_name__c = inputABN.Id;
            }
            if(isCreatePartnerAddress){
                insert inputPartnerAddress;
                partner.Partner_Address__c = inputPartnerAddress.Id;
            }
            upsert partner;
            relationship.Partner__c = partner.Id;
            // partner = new Partner__c();
            // inputPartnerAddress = new Address__c();
            // inputABN = new ABN__C();
            savePartnerSuccess = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Partner record successfully saved.'));
        }
        catch(Exception e){
            savePartnerSuccess = false;
            processErrorMessage(e);
            hasErrors = true;
            inputABN = new ABN__C();
            Database.rollback( sp );
        }
        return null;
    }

    public void processErrorMessage(Exception e){
        
        system.debug('errmsg = '+e.getMessage());

        if(!String.IsBlank(e.getMessage())){
            if(e.getMessage().containsIgnoreCase('DUPLICATES_DETECTED, Legal Entity Name already exists')){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Existing ABN with \"'+inputABN.Name +'\" Legal Entity Name ' +'found. Please click Search Legal Entity and find the \"'+inputABN.Name + '\" ABN record.'));
            }       
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            }     
        }
    }

    public PageReference newAccount() {
        showPopup();
        System.debug('newAccount relationship Id = '+relationship);
        if(!String.IsBlank(relationship.Id)){
            editExistingAccount = false;
            selectExistingAccount = true;
            relAccId = relationship.Associated_Account__c;
            String relQuery = relBaseQuery+' WHERE Id = \''+relationship.Id+'\'';
            relationship = Database.query(relQuery);//[SELECT Id, Name, Contact_Email__c, Relationship_Type__c,Partner__c,Job_Title__c,Partner_Contact_Reference_Number1__c,m_Role__c,m_RefCo_Link__c,Start_Date__c,End_Date__c,Broker_Relationship_Manager1__c,Nodifi_Access__c,Partition__c,Comments__c,h_ContactId__c,h_ReferralCompanyId__c,Associated_Account__c FROM Relationship__c WHERE Id =: relationship.Id];
            if(relationship.Start_Date__c != null) relStartDate = relationship.Start_Date__c;
            if(relationship.End_Date__c != null) relEndDate = relationship.End_Date__c;
        }
        else{
            selectExistingAccount = true;
            relationship = new Relationship__c(RecordTypeId=RELATIONSHIP_PARTNER_CONTACT_RTID, Partner__c = relationship.Partner__c);
            relStartDate = null;
            relEndDate = null;
        }
        
        return null;
    }

    public PageReference newPartner() {
        //logic for New
        if(String.IsBlank(relationship.Partner__c)){
            partner = new Partner__c();
            inputPartnerAddress = new Address__c();
            inputABN = new ABN__C();  
        }
        //logic for Update
        else{
            System.debug('load existing data = '+relationship.Partner__c);
            System.debug('currentABN= '+currentABN);
            System.debug('inputPartnerAddress= '+inputPartnerAddress);
            // load existing data
            partner = new Partner__c();
            inputPartnerAddress = new Address__c();
            inputABN = new ABN__C(); 
            loadPartnerData(relationship.Partner__c);
            //partner = [SELECT Id, Name FROM Partner__c WHERE Id =: relationship.Partner__c];
        }
        return null;
    }

    public void loadPartnerData(Id partnerId){
        // partner = [ SELECT Id, Name, Partner_Parent__c, RecordTypeId, Partner_Phone__c, m_Commitment_Date__c, Franchise_Partner__c, 
        //             Channel__c, Master_Channel__c, Sub_Channel__c, Master_Sub_Channel__c, Partner_Type__c, Partner_Website__c, Partition__c, 
        //             m_Legal_Entity_Name__c,Partner_Address__c, Bank_Account_Name__c, Bank_Account_Number__c, BSB_Number__c
        //             FROM Partner__c WHERE Id =: partnerId];//relationship.Partner__c];
        partner = Database.query(partnerBaseQuery + ' WHERE Id = \''+partnerId+'\'');
        
        if(!String.IsBlank(partner.Partner_Address__c))
        currentAddress = [ SELECT Id, Location__c, Suburb__c, Street_Number__c, Postcode__c, Street_Name__c, State__c, Street_Type__c, Country__c FROM Address__c WHERE Id =: partner.Partner_Address__c];

        if(!String.IsBlank(partner.m_Legal_Entity_Name__c))
        currentABN = [SELECT Id, Registered_ABN__c, Name FROM ABN__C WHERE Id =: partner.m_Legal_Entity_Name__c]; 
        
        relationship.Partner__c = partner.Id;
        partnersearchstring = partner.Name;
    }

    public void showPopup(){
        displayPopup = true;
    }

    public PageReference resetSaveFlags(){
        savePartnerSuccess = false;
        isCreateABN = false;
        isCreatePartnerAddress = false;
        // doneSavingNewPartner = true;
        return null;
    }

    public PageReference closePopup() {
        displayPopup = false;
        return null;
    }

    public pageReference closeAccModal(){
        system.debug('@@@ closeAccModal relationship = '+relationship);
        system.debug('@@@ closeAccModal partner = '+partner);
        system.debug('@@@ closeAccModal insertNewRelationship = '+insertNewRelationship);
        return null;
    }

    public void selExistingAcc(){
        
        if(selectExistingAccount){
            selectExistingAccount = false;
            editExistingAccount = false;
            relationship.Associated_Account__c = null;
            contact = new Contact();
            conLastName = '';
            relAccId = null;
        } 
        else{
            selectExistingAccount = true;
            editExistingAccount = false;
            relationship.Associated_Account__c = null;
            relAccId = null;
        }
        System.debug('selExistingAcc - '+selectExistingAccount);
        System.debug('editExistingAccount - '+editExistingAccount);
    }

    public void selExistingAccOnEdit(){
        relationship.Associated_Account__c = null;
        contact = new Contact();
        conLastName = '';
        relAccId = null;
    }

    public void editExistingAcc(){
        System.debug('editExistingAcc > relationship.Associated_Account__c = '+relAccId);
        
        if(!editExistingAccount){
            editExistingAccount = true;
            // relationship.Associated_Account__c = null;
            inputAccount = [SELECT Id, FirstName, LastName, PersonEmail, PersonMobilePhone FROM Account WHERE Id =: relationship.Associated_Account__c];
            System.debug('editExistingAcc > acc = '+inputAccount);
            contact.FirstName = inputAccount.FirstName;
            // contact.LastName = acc.LastName;
            conLastName = inputAccount.LastName;
            contact.Email = inputAccount.PersonEmail;
            contact.MobilePhone = inputAccount.PersonMobilePhone;
        } 
        else{
            editExistingAccount = false;
        }
        selectExistingAccount = false;
        relAccId = relationship.Associated_Account__c;
        System.debug('editExistingAcc > '+editExistingAccount);
    }

    public pageReference partnerRowClicked(){
        System.debug('           START partnerRowClicked METHOD ');
        errorloc = '';
        relSetCon = null;

        System.debug('relpartId = '+relationship.Partner__c);
        
        String relpartId = relationship.Partner__c;
        System.debug('           relationship.Partner__c = '+relpartId);
        //relationshipList = [SELECT ID,Associated_Account__r.Name, Associated_Account__c FROM Relationship__c WHERE Partner__c =: relationship.Partner__c];
        relQuery = relBaseQuery+' WHERE Partner__c = ' + '\'' + relpartId + '\'';
        System.debug('           relQuery = '+relQuery);
        //relationship.Partner__c;
        getRelationshipTblList();

        if(noOfRelRecords == 0 && !String.IsBlank(relationship.Partner__c)) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'No existing Partner Contact on the selected Partner record. Please click the New Partner Contact button to create a new one. '));
        }
        // System.debug('relationshipList = '+relationshipList.size());
        System.debug('           END partnerRowClicked METHOD ');
        return null;
    }

    public void showInfoMsg(){
        if(noOfRelRecords == 0 && !String.IsBlank(relationship.Partner__c)) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Please click the New Partner Contact button to create a new one. '));
        }
    }


    public pageReference partnerConRowClicked(){
        errorloc = '';
        System.debug('partnerConRowClicked = '+relationship.Associated_Account__c);
        return null;
    }

    public void retrieveCurrentABN(){
        currentABN = [Select Id, Name, Registered_ABN__c From ABN__c Where Id =: partner.m_Legal_Entity_Name__c];
        system.debug('@@currentABN:'+currentABN);
    }

    public void retrieveCurrentAddress(){
        currentAddress = [Select Id, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, Country__c, Location__c, RecordTypeId From Address__c Where Id =: partner.Partner_Address__c];
        system.debug('@@currentAddress:'+currentAddress);
    }

    public void retrieveLocation(){
        location = [Select Id, Postcode__c, Suburb_Town__c, State_Acr__c, State__c, Country__c From Location__c Where Id =: inputPartnerAddress.Location__c];
        if(location != NULL){
            inputPartnerAddress.Postcode__c = location.Postcode__c;
            inputPartnerAddress.Suburb__c = location.Suburb_Town__c;
            inputPartnerAddress.State_Acr__c = location.State_Acr__c;
            inputPartnerAddress.State__c = location.State__c;
            inputPartnerAddress.Country__c = location.Country__c;
        }
        system.debug('@@location:'+location);
    }

}