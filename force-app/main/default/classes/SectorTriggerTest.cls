/** 
* @FileName: SectorTriggerTest
* @Description: Test Class for Sector Object Trigger 
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 10/04/19 RDAVID Created Class
**/ 
@isTest
public class SectorTriggerTest {
    public static List<Case> caseList = new List<Case>();
    public static List<Sector__c> sectorList = new List<Sector__c>();
    // Process_Flow_Definition_Settings1__c 

    @testSetup static void setupData() {
        Process_Flow_Definition_Settings1__c processFlowDefSet = Process_Flow_Definition_Settings1__c.getOrgDefaults();
            processFlowDefSet.Enable_Sector_SLA_Flow_Definitions__c = true;
        upsert processFlowDefSet Process_Flow_Definition_Settings1__c.Id;

        Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
            trigSet.Enable_Triggers__c = true;
            trigSet.Enable_Case_Trigger__c = true;
            trigSet.Enable_Case_Create_KAR_Sales_Opp__c = true;
            trigSet.Enable_Case_Lookup_Fields__c = true;
            trigSet.Enable_Support_Request_Trigger__c = true;
            trigSet.Enable_Case_Log_Sectors__c = true;
            trigSet.Enable_Sector_Trigger__c = true;
            
        upsert trigSet Trigger_Settings1__c.Id;

        //Create Test Case
		caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_COMM_AF, 1);
        caselist[0].Partition__c = 'Nodifi';
        caselist[0].Channel__c = 'BOLT';
        caselist[0].Connective_Full_App_or_Lead__c = 'Full Application';
		Database.insert(caseList);
        List<Id> caseIds = new List<Id>();
        caseIds.add(caseList[0].Id);
        TriggerFactory.isProcessedMap = new map <Id,Boolean>();
        SectorSetAlertRecipient.setSLAWarningRecipient(caseIds);
    }

    static testmethod void testSectorSLA(){
        Test.startTest();
            caseList = [SELECT Id, Status, Stage__c FROM Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_N_COMM_AF AND Status = 'New' AND Stage__c = 'Open'];
            System.assertEquals(caseList.size(),1);
            
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),3);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            caseList[0].Stage__c = 'Owner Assigned';
            Database.update(caseList);
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),4);

            Support_Request__c srRecord = new Support_Request__c(RecordtypeId = Schema.getGlobalDescribe().get('Support_Request__c').getDescribe().getRecordTypeInfosByName().get('Asset - Submission Request (New)').getRecordTypeId(),
                                                                Status__c = 'New',
                                                                Stage__c = 'New',
                                                                Partition__c = 'Nodifi',
                                                                Support_Sub_Type__c = 'Initial - New Submission',
                                                                I_confirm_the_SF_Record_is_accurate__c = TRUE,
                                                                Case_Number__c = caseList[0].Id);

            Database.insert(srRecord);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            sectorList = [SELECT Id FROM Sector__c WHERE SR_Number__c =: srRecord.Id];
            System.assertEquals(sectorList.size(),2);
            srRecord.Status__c = 'In Progress';
            srRecord.Stage__c = 'In Progress';
            Database.update(srRecord);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            srRecord.Status__c = 'Closed';
            srRecord.Stage__c = 'Actioned';
            Database.update(srRecord);

            List<Id> sectorIds = new List<Id>();
            for(Sector__c sec : [SELECT Id FROM Sector__c WHERE Sector1__c Like '%SLA%']){
                sectorIds.add(sec.Id);
            }

            SectorSLANextWarning.setSLAWarningTime(sectorIds);

        Test.stopTest();
    }

    static testmethod void testSectorSLAConsumer(){
        Test.startTest();
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
            caselist[0].Partition__c = 'Nodifi';
            caselist[0].Channel__c = 'BOLT';
            caselist[0].Connective_Full_App_or_Lead__c = 'Full Application';
            Database.insert(caseList);
            
            caseList = [SELECT Id, Status, Stage__c FROM Case WHERE Recordtype.Name =: CommonConstants.CASE_RT_N_CONS_AF AND Status = 'New' AND Stage__c = 'Open'];
            System.assertEquals(caseList.size(),1);
            
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),3);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            caseList[0].Stage__c = 'Owner Assigned';
            Database.update(caseList);
            sectorList = [SELECT Id FROM Sector__c WHERE Case_Number__c =: caseList[0].Id];
            System.assertEquals(sectorList.size(),4);

            Support_Request__c srRecord = new Support_Request__c(RecordtypeId = Schema.getGlobalDescribe().get('Support_Request__c').getDescribe().getRecordTypeInfosByName().get('Asset - Submission Request (New)').getRecordTypeId(),
                                                                Status__c = 'New',
                                                                Stage__c = 'New',
                                                                Partition__c = 'Nodifi',
                                                                Support_Sub_Type__c = 'Initial - New Submission',
                                                                I_confirm_the_SF_Record_is_accurate__c = TRUE,
                                                                Case_Number__c = caseList[0].Id);

            Database.insert(srRecord);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            sectorList = [SELECT Id FROM Sector__c WHERE SR_Number__c =: srRecord.Id];
            System.assertEquals(sectorList.size(),2);
            srRecord.Status__c = 'In Progress';
            srRecord.Stage__c = 'In Progress';
            Database.update(srRecord);
            TriggerFactory.isProcessedMap = new map <Id,Boolean>();
            srRecord.Status__c = 'Closed';
            srRecord.Stage__c = 'Actioned';
            Database.update(srRecord);
            List<Id> sectorIds = new List<Id>();
            Sector__c initialSector = new Sector__c();
            for(Sector__c sec : [SELECT Id,Sector1__c,SLA_Warning_Time__c FROM Sector__c WHERE Sector1__c =: CommonConstants.SECTOR1_INIT_REV]){
                sectorIds.add(sec.Id);
                initialSector = sec;
            }
            initialSector.Sector_Entry__c = System.now().addHours(1);
            SectorSLANextWarning.setSLAWarningTime(sectorIds);
            BusinessHours bh = [SELECT Id,IsDefault,Name,IsActive,TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime 
                                FROM BusinessHours 
                                WHERE IsActive = TRUE AND IsDefault = TRUE]; 
            SectorGateway.setConsumerInitialReviewTimeToAppReview(initialSector,bh, SectorSLANextWarning.slaSectorMDTMap.get(CommonConstants.SECTOR1_NOD_SLA_APP_REVIEW), null, 'SectorSLANextWarning',null,System.now());
        Test.stopTest();
    }
}