/**
 * @File Name          : APITransferService.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 27/11/2019, 8:29:11 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/11/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@RestResource(urlMapping='/apitransfer/op2ma/v1/*')
global without sharing class APITransferService{

    @HttpPost
    global static string createAPITransfer(){
        string reqBody = RestContext.request.requestBody.toString();
        if (!String.isBlank(reqBody)){
            system.debug('@@:reqBody: '+reqBody);
            API_Transfer__c at = new API_Transfer__c(
                RecordTypeId = Schema.SObjectType.API_Transfer__c.getRecordTypeInfosByName().get('Op2ma').getRecordTypeId(),
                Request_Body__c = RestContext.request.requestBody.toString()
            );
            insert at;
            return 'SUCCESS';
        }
        else{ 
            RestContext.response.statusCode = 400;
            return 'FAILED';
        }
    }

}