/** 
* @FileName: RoleAddressHandler
* @Description: Trigger Handler for the Role_Address__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/3 RDAVID Created Class - SD-31 SF - NM - Role Address Trigger to populate Address lookups - Trigger in Role_Address__c object to populate Parent Account and Parent Role Address__c field.
* 1.1 4/4 RDAVID Updated Class - SD-45 SF - NM - Toggle Active Address field to ensure one "Current" Address on Role - Toggle Current Address to ensure one Current Address on Role
* 1.1 19/06/2018 RDAVID Updated Class - SD-103 SF - NM - Continuous Algorithm
* 1.2 25/06/2018 RDAVID Updated Class - SD-103 SF - NM - Continuous Algorithm
* 1.3 3/10/2018 RDAVID issuefix/RoleAddressTrigger - Issue in Case Location field not populating from Role Address Location fixed.
* 1.4 12/6/2018 JBACULOD issuefix/RoleAdddress-Residential - fixed Residential Situation push update to Role issue
* 1.5 11/03/19 JBACULOD - Added retry on final due to UNABLE_TO_LOCK_ROW issue
* 1.6 17/07/19 RDAVID - extra/task25349502-AutomateApplicationFieldInRoleAddress - Automate population of Application_Number lookup based on Role.
**/ 
//A-000184, A-000184

public without sharing class RoleAddressHandler implements ITrigger {	
	
	//SD-31 - Populate Account and Role Address lookup field 
	public static Set<Id> roleIDs = new Set<Id>();
	public Map<Id,Role__c> roleMapToUpdate = new Map<Id,Role__c>();
	public Map<Id,Account> accMapToUpdate = new Map<Id,Account>();
	public static Map<Id,Address__c> addressMap = new Map<Id,Address__c>();
	public static List<Role__c> roleList = new List<Role__c>();
	public List<Role_Address__c> roleAddressList = new List<Role_Address__c>();
	// Constructor
	public RoleAddressHandler(){	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		
		Map<Id,Role_Address__c> oldRoleAddressMap = (Trigger.isUpdate)?(Map<Id,Role_Address__c>)Trigger.oldMap:null; 
		Set<Id> roleIds = new Set<Id>(); //1.6 17/07/19 RDAVID - extra/task25349502
		if(Trigger.IsInsert || Trigger.IsUpdate){
			Set<Id> addressIds = new Set<Id>();
			//FOR LOOP 
			for(Role_Address__c roleAddress : (List<Role_Address__c>)Trigger.new){
				if(roleAddress.Address__c != NULL){
					if(Trigger.IsInsert){
						addressIds.add(roleAddress.Address__c);
					}
					if(Trigger.IsUpdate && roleAddress.Address__c != oldRoleAddressMap.get(roleAddress.Id).Address__c){
						addressIds.add(roleAddress.Address__c);
					}
				} 
				if(roleAddress.Role__c != NULL){ //1.6 17/07/19 RDAVID - extra/task25349502
					roleIds.add(roleAddress.Role__c);
				}
			}
			if(addressIds.size() > 0 && addressMap.isEmpty() && TriggerFactory.trigset.Enable_Address_Populate_Location__c){
				addressMap = new Map<Id,Address__c>([SELECT Id, Location__c FROM Address__c WHERE Id IN: addressIds AND Location__c != NULL]);
			}
			 //1.6 17/07/19 RDAVID - extra/task25349502
			if(roleIds.size() > 0){
				Map<Id,Role__c> roleMap = new Map<Id,Role__c>([SELECT Id,Application__c FROM Role__c WHERE Id IN: roleIDs AND Application__c != NULL]);
				if(!roleMap.isEmpty()){
					RoleAddressGateway.autoPopulateApplicationNumber(roleMap,(List<Role_Address__c>)Trigger.new);
				}
			}
		} 
	}
	
	public void bulkAfter(){
	
		Map<Id,Role_Address__c> oldRoleAddressMap = (Trigger.isUpdate)?(Map<Id,Role_Address__c>)Trigger.oldMap:null;
		Map<Id,Role_Address__c> newRoleAddressMap = (Trigger.IsInsert || Trigger.isUpdate)?(Map<Id,Role_Address__c>)Trigger.newMap:(Map<Id,Role_Address__c>)Trigger.oldMap;

		// if(Trigger.IsInsert || Trigger.isUpdate){
		List<Role_Address__c> newRoleAddressList = (Trigger.IsInsert || Trigger.isUpdate)?(List<Role_Address__c>)Trigger.new:Trigger.old;
			for(Role_Address__c roleAddress : newRoleAddressList){
				//if(roleAddress.Start_Date__c != NULL){
					if(Trigger.IsInsert){
						roleIDs.add(roleAddress.Role__c);
					}
					if(Trigger.isUpdate){
						/*if(roleAddress.Address__c != oldRoleAddressMap.get(roleAddress.Id).Address__c 
						|| (roleAddress.Active_Address__c != oldRoleAddressMap.get(roleAddress.Id).Active_Address__c && roleAddress.Active_Address__c == 'Current')
						|| roleAddress.Location__c != oldRoleAddressMap.get(roleAddress.Id).Location__c
						|| roleAddress.Start_Date__c != oldRoleAddressMap.get(roleAddress.Id).Start_Date__c
						|| roleAddress.End_Date__c != oldRoleAddressMap.get(roleAddress.Id).End_Date__c){ */
						if (!System.equals(roleAddress, oldRoleAddressMap.get(roleAddress.Id))){
							roleIDs.add(roleAddress.Role__c);
						}		
					}
					if(Trigger.isDelete){
						roleIDs.add(roleAddress.Role__c);
					}
				//}
			}
		// }

		System.debug('roleIDs = '+roleIDs);
		if(roleList.size() == 0 && roleIDs.size() > 0){
			roleList = [SELECT Id,Address__c,Location__c,Lead_12_Months_Address__c,Account__c,Account__r.Address__c,Account__r.Location__c,Residential_Situation__c, (SELECT Id, Start_Date__c,End_Date__c, Active_Address__c,Location__c,Address__c,Address__r.Location__c, Residential_Situation__c FROM Role_Addresses__r ORDER BY Start_Date__c ASC) FROM Role__c WHERE Id IN: roleIDs];
		}

		ContinuousUtility contUtil = new ContinuousUtility();
		// Loop in Role
		for(Role__c role : roleList){
			Role_Address__c currentRoleAddress = new Role_Address__c();
			Boolean updateRole = false;
			Map<Id,Role_Address__c> tempRoleAddMap = new Map<Id,Role_Address__c>();
			for(Role_Address__c roleAdd : role.Role_Addresses__r){
				Boolean updateAccount = false;
				Boolean updateRoleAdd = false;

				//Update if inserted/updated Role Address is Current
				//SD-31
				//Populate Address & Location field in Account and Role
				if(TriggerFactory.trigset.Enable_Role_Address_Populate_Address__c && newRoleAddressMap.containsKey(roleAdd.Id) && newRoleAddressMap.get(roleAdd.Id).Active_Address__c == 'Current'){
					currentRoleAddress = roleAdd; //Assign Current;
					
					System.debug('Added/Updated record is Current.');
					Account accToUpdate = new Account(Id = role.Account__c);
					//Populate Address in Role and Account
					if(role.Address__c != roleAdd.Address__c){
						role.Address__c = (roleAdd.Address__c != NULL)?roleAdd.Address__c:NULL;
						updateRole = true;
					}
					
					if(role.Account__r.Address__c != roleAdd.Address__c){
						accToUpdate.Address__c = (roleAdd.Address__c != NULL)?roleAdd.Address__c:NULL;
						updateAccount = true;
					}
					//Populate Location in Role and Account

					if(role.Location__c != roleAdd.Location__c){
						role.Location__c = (roleAdd.Location__c != NULL)?roleAdd.Location__c:NULL;
						updateRole = true; 
					}
					if(role.Account__r.Location__c != roleAdd.Location__c){
						accToUpdate.Location__c = (roleAdd.Location__c != NULL)?roleAdd.Location__c:NULL;
						updateAccount = true;
					}
					if(updateAccount) accMapToUpdate.put(accToUpdate.Id,accToUpdate);
				}

				tempRoleAddMap.put(roleAdd.Id,roleAdd);
			}

            system.debug('@@currentRoleAddress.Residential_Situation__c: '+currentRoleAddress.Residential_Situation__c);
			if(role.Residential_Situation__c != currentRoleAddress.Residential_Situation__c && currentRoleAddress.Residential_Situation__c != null){
				role.Residential_Situation__c = currentRoleAddress.Residential_Situation__c;
				updateRole = true;
			}

			//Toggle existing Current when a new Current is inserted/updated
			if(TriggerFactory.trigset.Enable_Role_Address_Toggle_Current__c && tempRoleAddMap.containsKey(currentRoleAddress.Id)){
				tempRoleAddMap.remove(currentRoleAddress.Id);
				for(Role_Address__c roleAdd : tempRoleAddMap.values()){
					if(roleAdd.Active_Address__c != 'Previous'){
						if (currentRoleAddress.Start_Date__c != null){ //Works only for Full Address 
							roleAdd.End_Date__c = currentRoleAddress.Start_Date__c.addDays(-1);
							roleAdd.Active_Address__c = 'Previous';
							roleAddressList.add(roleAdd);
						}
					}
				}
			}

			//Check Residency
			if(TriggerFactory.trigset.Enable_Role_Address_Check_Residency__c){
				if (currentRoleAddress.Start_Date__c != null){ //Works only for Full Address 
					Boolean isCurrentValid = contUtil.isCurrentAddressValid(currentRoleAddress);
					if(isCurrentValid && role.Lead_12_Months_Address__c != isCurrentValid) {
						role.Lead_12_Months_Address__c = isCurrentValid;
						updateRole = true;
					}
					else if(!isCurrentValid){
						Boolean isExistingValid = contUtil.isExistingAddressValid(role.Role_Addresses__r); 
						if(role.Lead_12_Months_Address__c != isExistingValid) {
							role.Lead_12_Months_Address__c = isExistingValid;
							updateRole = true;		
						}	
					}
					if(role.Role_Addresses__r == NULL || role.Role_Addresses__r.size() == 0){
						if(role.Lead_12_Months_Address__c != false){
							System.debug('updateRole 12 Months Income To False if no Income ************ ');
							role.Lead_12_Months_Address__c = false;
							updateRole = true;		
						}
					}
				}	
			}
			if(updateRole) roleMapToUpdate.put(role.Id,role);
			system.debug('@@roleMapToUpdate:'+roleMapToUpdate);
		}
	}
		
	public void beforeInsert(SObject so){
		Role_Address__c roleAddress = (Role_Address__c)so;
		if (TriggerFactory.trigset.Enable_Address_Populate_Location__c){
			RoleAddressGateway.populateLocation(null, roleAddress, addressMap, true);
		}
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Role_Address__c roleAddress = (Role_Address__c)so;
		Role_Address__c oldRoleAddress = (Role_Address__c)oldSo;
		if (TriggerFactory.trigset.Enable_Address_Populate_Location__c){
			RoleAddressGateway.populateLocation(oldRoleAddress, roleAddress, addressMap, false);
		}
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		System.debug('ROLE ADDRESS Queries :' + Limits.getQueries() + ' / '+Limits.getLimitQueries());
		System.debug('ROLE ADDRESS Rows Queried :' + Limits.getDmlRows() + ' / '+Limits.getLimitDmlRows());
		System.debug('ROLE ADDRESS DML : ' +  Limits.getDmlStatements() + ' / '+Limits.getLimitDmlStatements());
		if(!accMapToUpdate.isEmpty()){
			System.debug('**********UPDATE -> accMapToUpdate '+accMapToUpdate);
			Database.update(accMapToUpdate.values());
		}
        if(!roleMapToUpdate.isEmpty()){
			System.debug('**********UPDATE -> roleMapToUpdate '+roleMapToUpdate);
			try {
				Database.update(roleMapToUpdate.values());
			}
			catch (Exception e){ //Retry due to UNABLE_LOCK_ROW issue
				Database.update(roleMapToUpdate.values());
			}
		}
		if(roleAddressList.size()>0){
			System.debug('**********UPDATE -> roleAddressList '+roleAddressList);
			Database.update(roleAddressList);
		}		
	}
}