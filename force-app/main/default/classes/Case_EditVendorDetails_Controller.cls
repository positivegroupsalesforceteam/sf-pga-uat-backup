/*
    @author: Jesfer Baculod - Positive Group
    @history: 06/21/17 - Created
            06/23/17 - Added Search and Create functionality
            06/26/17 - Added lookup fields
            06/27/17 - Updated
            06/29/17 - Updates based on feedback
            06/30/17 - Updated display based on Sale Type
            07/20/17 - Fixed discovered issues
            07/24/17 - Updated some messages w/ custom labels
            07/28/17 - Added Company Type filter on searchReferralCompany due to Partner Referral Companies
            08/03/17 - Updated Modified Referral Company, Key Person Contact Create/Search Screens, Additional Indicators
            08/07/17 - Added Duplicate check on creating Referral Company
            11/06/17 - Updated Registered ABN Handling
    @description: controller extension class of Case_EditVendorDetails page.
*/
public class Case_EditVendorDetails_Controller {

	public string cse_DV {get;set;}
    public string cse_DC {get;set;}
    public string cse_VC {get;set;}

    public string cse_DV_id {get;set;}
    public string cse_DC_id {get;set;}
    public string cse_VC_id {get;set;}
    public string cse_s_ID {get;set;} //gen search lookup ID

    public string errmsg {get;set;}
    public string dupeoption {get;set;}
    public string nullparam {get;set;}

    public Case cse {get;set;}
    public Referral_Company__c refcomp {get;set;}
    public ABN__c inputABN {get;set;}
    public ABN__c currentABN {get;set;}
    public Referral_Company__c exrefcomp {get;set;}
    public ABN__c exABN {get;set;}
    public Contact bcon {get;set;}
    public Contact kpcon {get;set;}

    public boolean isCreateABN {get;set;}
    public boolean showRefCompCre {get;set;}
    public boolean showBConCre {get;set;}
    public boolean showKPConCre {get;set;}
    public boolean showRefCompS {get;set;}
    public boolean showBConS {get;set;}
    public boolean showKPConS {get;set;}
    public boolean noRefComp {get;set;}
    public boolean saveSuccess {get;set;}
    public boolean saveSuccessBCon {get;set;}
    private boolean withExistingVD {get;set;}
    public boolean showDuplicateRC {get;set;}

    private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business
    private static final string CON_RT_KEYPERSON = Label.Contact_Key_Person_RT; //Key Person
 
    private static final string SALE_TYPE_PRIVATE = Label.Case_Sale_Type_Private; //'Private Sale';
    private static final string SALE_TYPE_DEALER = Label.Case_Sale_Type_Dealer; //'Dealer';

    private static final string ERR_MSG_1 = Label.Case_Edit_Vendor_Details_Error_Message_1;  //Dealer Sale Vendor & Dealer Contact Person are required for Dealer Sale Type
    private static final string ERR_MSG_2 = Label.Case_Edit_Vendor_Details_Error_Message_2;  //Dealer Sale Vendor is required for Dealer Sale Type
    private static final string ERR_MSG_3 = Label.Case_Edit_Vendor_Details_Error_Message_3;  //Dealer Contact Person is required for Dealer Sale Type
    private static final string ERR_MSG_4 = Label.Case_Edit_Vendor_Details_Error_Message_4;  //Private Sale Vendor Contact is required for Private Sale Type


    private Id rt_c_b {get;set;}
    private Id rt_c_kp {get;set;}

    public integer searchMode {get;set;} //on which field does the user is doing the search

    //pagination of Dealer Sale Vendor - Referral Company
    private string RCquery {get;set;}
    public Integer RC_size {get;set;} 
    public Integer RC_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConRC { 
        get {
            if(setConRC == null) {                
                setConRC = new ApexPages.StandardSetController(Database.getQueryLocator(RCquery));
                setConRC.setPageSize(RC_size);  
                RC_noOfRecords = setConRC.getResultSize();
            }            
            return setConRC;
        }
        set;
    }

    //pagination of Dealer Contact Person - Business Contact
    private string BConquery {get;set;}
    public Integer BCon_size {get;set;} 
    public Integer Bcon_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConBcon {
        get {
            if(setConBcon == null) {                
                setConBcon = new ApexPages.StandardSetController(Database.getQueryLocator(BConquery));
                system.debug('@@setConBCon:'+setConBCon);
                system.debug('@@BCon_size:'+BCon_size);
                if (BCon_size == null) BCon_size = 5;
                setConBcon.setPageSize(BCon_size);  
                Bcon_noOfRecords = setConBcon.getResultSize();
            }            
            return setConBcon;
        }
        set;
    }

    //pagination of Dealer Contact Person - Business Contact
    private string KPConquery {get;set;}
    public Integer KPCon_size {get;set;} 
    public Integer KPcon_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConKPcon { 
        get {
            if(setConKPcon == null) {                
                setConKPcon = new ApexPages.StandardSetController(Database.getQueryLocator(KPconquery));
                setConKPcon.setPageSize(KPCon_size);  
                KPcon_noOfRecords = setConKPcon.getResultSize();
            }            
            return setConKPcon;
        }
        set;
    }

    public Case_EditVendorDetails_Controller(ApexPages.StandardController stdController) {
        errmsg = cse_DV = cse_DC = cse_VC = '';
        dupeoption = '0';
        isCreateABN = showDuplicateRC = noRefComp = saveSuccess = saveSuccessBcon = false; 
        searchMode = 1;  //default to Dealer Sale Vendor search
        KPCon_size = BCon_size = RC_size = 5; RCquery = BConquery = KPconquery = ''; 
        showRefCompCre = showBConCre = showKPConCre = showRefCompS = showBConS = showKPConS = false;
        currentABN = new ABN__c();
        inputABN = new ABN__c();
        refcomp = new Referral_Company__c(OwnerId = UserInfo.getUserId());
        rt_c_b = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
        rt_c_kp = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_KEYPERSON).getRecordTypeId();
        bcon = new Contact(RecordTypeId = rt_c_b);
        kpcon = new Contact(RecordTypeId = rt_c_kp);
        this.cse = (Case)stdController.getRecord();
        this.cse = [Select Id, CaseNUmber, Sale_Type__c, Status, Stage__c,
                    Dealer_Contact__c, Dealer_Contact__r.Name, Dealer_Vendor__c, Dealer_Vendor__r.Name, Vendor_Contact__c, Vendor_Contact__r.Name //new Vendor fields
                    //Vendor_Business_Name__c, Vendor_Contact_Person__c, Dealer_Contact_Person__c //old Vendor fields
                    From Case Where Id = : stdController.getId()];

        //Retrieve existing Vendor Details
        if (cse.Sale_Type__c == SALE_TYPE_PRIVATE){ //search for all Key Person Contacts
            if (cse.Vendor_Contact__c != null){
                cse_VC = cse.Vendor_Contact__r.Name;
                cse_VC_id = cse.Vendor_Contact__c;
            }
            searchMode = 3;
            showKPConS = true;
            showRefCompS = false;
            withExistingVD = true;
            searchKeyPersonContact();
            withExistingVD = false;
        }
        else if (cse.Sale_Type__c == SALE_TYPE_DEALER){ //search for all Referral Companies 
            if (cse.Dealer_Vendor__c != null){
                cse_DV = cse.Dealer_Vendor__r.Name;
                cse_DV_id = cse.Dealer_Vendor__c; 
                //searchReferralCompany();
                //showRefCompS = true;
            }
            if (cse.Dealer_Contact__c != null){
                cse_DC = cse.Dealer_Contact__r.Name;
                cse_DC_id = cse.Dealer_Contact__c;
            }
            searchMode = 1;
            showRefCompS = true;
            withExistingVD = true;
            searchReferralCompany();
            withExistingVD = false;
        }


    }

    public void retrieveCurrentABN(){
        currentABN = [Select Id, Name, Registered_ABN__c From ABN__c Where Id =: refComp.Legal_Entity_Name__c];
        system.debug('@@currentABN:'+currentABN);
    }

    //Changes the size of pagination
    public PageReference refreshPageSize() {
         if (searchMode == 1 ) setConRC.setPageSize(RC_size); //Searches in Referral Company
         else if (searchMode == 2) setConBcon.setPageSize(BCon_size); //Searches in Business Contact
         else if (searchMode == 3) setConKPcon.setPageSize(KPCon_size); //Searches in Key Person Contact
         return null;
    }

    //Sets Id on selected record
    public void assignLookupID(){
        system.debug('@@searchMode:'+searchMode);
        if (searchMode == 1){ 
            cse_DV_id = cse_s_ID;
        }
        if (searchMode == 2){ 
            cse_DC_id = cse_s_ID;
        }
        if (searchMode == 3){ 
            cse_VC_id = cse_s_ID;
        }
        showRefCompCre = false;
        showBConCre = false;
        showKPConCre = false;
    }

    //Clear all inputted data on Create Screens
    public void clearFormfields(){
        system.debug('@@cse_DV_id:'+cse_DV_id);
        refcomp = null;
        bcon = kpcon = null;
        saveSuccess = false;
        saveSuccessBcon = false;
        showDuplicateRC = false;
        currentABN = new ABN__c();
        inputABN = new ABN__c();
        isCreateABN = false;
        exrefcomp = new Referral_Company__c();
        exABN  = new ABN__c();
        refcomp = new Referral_Company__c(OwnerId = UserInfo.getUserId());
        bcon = new Contact(RecordTypeId = rt_c_b);
        if (cse_DV_id != null && cse_DV_id != '') bcon.Referral_Company__c = cse_DV_id; //assign selected Referral Company
        kpcon = new Contact(RecordTypeId = rt_c_kp);
    }

    public List<Referral_Company__c> getsearchRefComplist(){
         return (List<Referral_Company__c>) setConRC.getRecords();
    }

    public List<Contact> getsearchBConlist(){
         return (List<Contact>) setConBcon.getRecords();
    }

    public List<Contact> getsearchKPConlist(){
         return (List<Contact>) setConKPcon.getRecords();
    }

    //method for searching Dealer Sale Vendor - Referral Company
    public PageReference searchReferralCompany(){
        if (!withExistingVD) cse_DV_id = null;
        setConRC = null; saveSuccess = false;
        string srchvar = '%' + cse_DV + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        RCquery = 'Select Id, Name, Address_Line_1__c, Company_Type__c, State__c, Dealer_Type__c, Company_Website__c, Primary_Contact_s_Name__c, Primary_Contact_s_Mobile__c, Last_Contact_Date__c From Referral_Company__c Where Name like \'' + srchvar + '\' AND Company_Type__c != \'Mortgage Broker\' Order by Name ASC'; //filter company type due to added Partner Referral Companies
        getsearchRefComplist();
        setConRC.setpageNumber(1);
        return null;
    }

    //method for searching Dealer Contact Person - Business Contact
    public PageReference searchBusinessContact(){
        cse_DC_id = null;
        setConBcon = null; saveSuccess = false;
        if (cse_DV_id == '' || cse_DV_id == null){
            noRefComp = true;
        }
        else{ 
            noRefComp = false;
            bcon.Referral_Company__c = cse_DV_id; //assign selected Referral Company
        }
        system.debug('@@cse_DV_id:'+cse_DV_id);
        system.debug('@@noRefComp:'+noRefComp);
        string srchvar = '%' + cse_DC + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        Bconquery = 'Select Id, Name, FirstName, LastName, MobilePhone, Email, RecordTypeId, RecordType.Name, Referral_Company__c, CreatedDate From Contact Where Name like \'' + srchvar + '\' AND Referral_Company__c = : cse_DV_id AND RecordTypeId = : rt_c_b Order by Name ASC';
        getsearchBConlist();
        setConBCon.setpageNumber(1);
        return null;
    }

    //method for searching Primary Sale Vendor Contact - Key Person Contact
    public PageReference searchKeyPersonContact(){
        if (!withExistingVD) cse_VC_id = null;
        setConKPcon = null; saveSuccess = false;
        string srchvar = '%' + cse_VC + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        KPconquery = 'Select Id, Name, FirstName, LastName, MobilePhone, Email, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, Country__c, RecordTypeId, RecordType.Name, HomePhone, CreatedDate From Contact Where Name like \'' + srchvar + '\' AND RecordTypeId = : rt_c_kp Order by Name ASC';
        getsearchKPConlist();
        setConKPcon.setpageNumber(1);
        return null;
    }

    //method for assigning existing Referral Company
    public void assignExistingReferralCompany(){
            cse_DV = exrefcomp.Name;
            cse_DV_id = exrefcomp.Id;
            saveSuccess = true;
            showRefCompCre = false;
            showBConCre = true;
            showDuplicateRC = false;
    }

    //method for saving New Dealer Sale Vendor
    public void saveReferralCompany(){
        Savepoint sp = Database.setSavepoint();
        try{
            if (isCreateABN){
                insert inputABN;
                refcomp.Legal_Entity_Name__c = inputABN.Id;
            }
            else refcomp.Legal_Entity_Name__c = currentABN.Id;

            insert refcomp; 
            //Assign created Referral Company to Sale Dealer Vendor
            cse.Dealer_Vendor__c = refcomp.Id;
            cse_DV = refcomp.Name;
            cse_DV_id = refcomp.Id;
            refcomp = new Referral_Company__c(); 
            bcon = new Contact(RecordTypeId = rt_c_b);
            if (cse_DV_id != null && cse_DV_id != '') bcon.Referral_Company__c = cse_DV_id; //assign selected Referral Company
            saveSuccess = true;
            showRefCompCre = false;
            showBConCre = true;
            showDuplicateRC = false;
            dupeoption = '0';
        }
        catch(Exception e){
            Database.rollback( sp );
            string rname = '';
            if (e.getMessage().contains('DUPLICATES_DETECTED') || e.getMessage().contains('DUPLICATE_VALUE') ){
                system.debug('@@fullmessage:'+e.getMessage());
                if (e.getMessage().contains('Referral Company already exists')){
                    //if (!isCreateABN){
                        exrefcomp = new Referral_Company__c();
                        exrefcomp = [Select Id, Name, Address_Line_1__c, Company_Type__c, State__c, Dealer_Type__c, Company_Website__c, Primary_Contact_s_Name__c, Primary_Contact_s_Mobile__c, Last_Contact_Date__c, Registered_ABN_F__c
                                From Referral_Company__c Where Name = : refcomp.Name limit 1];
                        rname = exrefComp.Name;
                        //Referral Company already exists + RC Name + Click link to select existing Referral Company
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Referral_Company_Msg_1) + rName + String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Referral_Company_Msg_2) ));
                        dupeoption = '1';
                        inputABN = new ABN__c();
                        system.debug('@@duperefcomplevel');
                    //}
                }
                if (e.getMessage().contains('Registered_ABN__c duplicates') || e.getMessage().contains('Legal Entity Name already exists')){
                    list <ABN__c> exABNlist = [Select Id, Name, Registered_ABN__c From ABN__c Where (Registered_ABN__c = : inputABN.Registered_ABN__c OR Name = : inputABN.Name) ];
                    if (exABNlist .size() > 0){ 
                        exABN = exABNlist[0];
                        rname = exABN.Name;
                        isCreateABN = false;
                        inputABN = new ABN__c();
                        //Legal Entity already exists + ABN Name + Click link to select existing Legal Entity
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Legal_Entity_Msg_1) + rName + String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Legal_Entity_Msg_2) ));
                        dupeoption = '2';
                        system.debug('@@dupeABNlevel');
                    }
                }
                showDuplicateRC = true;
            }
            saveSuccess = false;
        }
    }

    //method for saving New Dealer Contact Person
    public void saveBusinessContact(){
        Savepoint sp = Database.setSavepoint();
        try{
            insert bcon; 
            //Assign created Business Contact
            cse.Dealer_Contact__c = bcon.Id;
            string confname = '';
            if (bcon.FirstName != null) confname = bcon.FirstName + ' ';
            cse_DC = confname + bcon.LastName;
            cse_DC_id = bcon.Id;
            //bcon = new Contact();
            saveSuccess = true;
            saveSuccessBcon = true;
            system.debug('@@saveSuccessBCon:'+saveSuccessBCon);
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
            saveSuccessBcon = false;
        }
    }

    //method for saving New Primary Sale Vendor Contact
    public void saveKeyPersonContact(){
        Savepoint sp = Database.setSavepoint();
        try{
            insert kpcon; 
            //Assign created Key Person Contact
            cse.Vendor_Contact__c = kpcon.Id;
            string confname = '';
            if (kpcon.FirstName != null) confname = kpcon.FirstName + ' ';
            cse_VC = confname + kpcon.LastName;
            cse_VC_id = kpcon.Id;
            //kpcon = new Contact();
            saveSuccess = true;
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
        }
    }

    //method for updating Case with Vendor Details
    public PageReference saveCase(){
        Savepoint sp = Database.setSavepoint();
        errmsg = '';
        try{    
            system.debug('@@cse_DV_id:'+cse_DV_id);
            if (cse_DV_id != '') cse.Dealer_Vendor__c = cse_DV_id; //assign Referral Company ID
            else cse.Dealer_Vendor__c = null;
            if (cse_DC_id != '') cse.Dealer_Contact__c = cse_DC_id; //assign Business Contact ID
            else cse.Dealer_Contact__c = null;
            if (cse_VC_id != '') cse.Vendor_Contact__c = cse_VC_id; //assign Key Person Contact ID
            else cse.Vendor_Contact__c = null;
            if (cse.Sale_Type__c == SALE_TYPE_DEALER){
                //Validate Dealer fields
                if (cse.Dealer_Vendor__c == null && cse.Dealer_Contact__c == null){ 
                    errmsg = ERR_MSG_1;
                    return null;
                }
                else if (cse.Dealer_Vendor__c == null && cse.Dealer_Contact__c != null){
                    errmsg = ERR_MSG_2;
                    return null;
                }
                else if (cse.Dealer_Vendor__c != null && cse.Dealer_Contact__c == null){
                    errmsg = ERR_MSG_3;
                    return null;
                }
            }
            else if (cse.Sale_Type__c == SALE_TYPE_PRIVATE){
                //Validate Private Sale fields
                if (cse.Vendor_Contact__c == null){
                    errmsg = ERR_MSG_4;
                    return null;
                }
            }
            update cse;
            saveSuccess = true;
            //return new PageReference('/'+ cse.Id);
            return null;
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
            errmsg = e.getMessage();
            return null;
        }
    }



}