public with sharing class HelloWorldController {
    @AuraEnabled
    public static List<User> getUsers(String nameFilterString) {
        String composedFilter = sanitizeQueryString(nameFilterString);
        System.debug('composedFilter== '+composedFilter);
        List<User> users =
                [SELECT Id,Profile.Name, UserRole.Name , Name, IsActive, Box_Configured__c FROM User WHERE Profile.Name LIKE '%Support%' AND Profile.Name LIKE '%NM -%' AND Name LIKE : composedFilter AND IsActive = TRUE ORDER BY Name ASC];
        //Add isAccessible() check
        return users;
    }
    
    static String sanitizeQueryString(String aQuery) {
        if (aQuery == null) return '%';
        
        String trimmedQuery = aQuery.trim();
        if (trimmedQuery.length() == 0) return '%';
        return '%' + trimmedQuery.replaceAll('\\W+', '%') + '%';
    }
    
    @AuraEnabled
    public static Boolean switchUserProfile(User u){
        System.debug('user-->'+u);
        
        List<Profile> profiles = [Select Id, Name FROM Profile WHERE Name LIKE '%NM - PLS Support%' OR Name LIKE '%NM - Nodifi Support%'];
        List<UserRole> roles = [SELECT Id, Name FROM UserRole WHERE Name LIKE '%Lightning Team%' OR Name LIKE '%Team Member%'];
        
        if(profiles[0].Id == u.ProfileId){
            u.ProfileId = profiles[1].Id;
        }
        else if(profiles[1].Id == u.ProfileId){
            u.ProfileId = profiles[0].Id;
        }
        
        if(roles[0].Id == u.UserRoleId){
            u.UserRoleId = roles[1].Id;
        }
        else if(roles[1].Id == u.UserRoleId){
            u.UserRoleId = roles[0].Id;
        }
        
        try {
            update u;
            return true;
        }        
        catch(Exception e){
            return false;
        }
    }
}