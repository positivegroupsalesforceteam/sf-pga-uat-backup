@isTest
private class AT_TestClass_Ativa_ShareRec_S2S {
	static testMethod void testShareRec_S2S() {
        
        opportunity sObopp = new opportunity(name = 'oppvsr', stagename = 'qualified', closedate = Date.newInstance(2016, 12, 9));
        insert sObopp;
        
        VS_Referral__c sObvsr = new VS_Referral__c(Client_First_Name__c = 'vsr', opportunity__c = sObopp.Id, New_or_Used_Vehicle__c = 'new vehicle');
        insert sObvsr;
        system.debug('******' + sObvsr.Id);
        
        recordtype RecTypeID = [Select Id from recordtype where sobjecttype = 'VS_Referral__c' and name = 'VS Referral - Locked'];
        
        Test.StartTest(); 
        	Ativa_ShareRec_S2S.Mtd_ShareRec_S2S(sObvsr.Id);
        Test.StopTest();
    }
}