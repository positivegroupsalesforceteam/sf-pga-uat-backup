@isTest
public class NewSupportRequestControllerTest {
    public static List<Account> accList = new List<Account>();
    public static List<Role__c> roleList = new List<Role__c>();

    @testSetup static void setupData() {
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_Auto_Create_Application__c = true
        );
        insert trigset;
		//Create Test Account
		accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
	}

    static testMethod void testNewSupportRequestVF() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Application_Name__c FROM Case LIMIT 1];
            caseRec.Status = 'Review';
            update caseRec;
            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Submission');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=true;
            newSupportRequestCtrl.submit();
            newSupportRequestCtrl.getSupportRequestRTs();
            System.assertEquals(newSupportRequestCtrl.cseCommentReadyForSubmission.CommentBody,'Notes to Support: '+newSupportRequestCtrl.supportRequest.Notes_to_Support__c);
            newSupportRequestCtrl.caseid();
            System.assertNotEquals(newSupportRequestCtrl.supportRequest.Id,null);
            NewSupportRequestController.updateSupportRequest(newSupportRequestCtrl.supportRequest,null,null,null);
            newSupportRequestCtrl.supportRequest.Id = null;
            NewSupportRequestController.updateSupportRequest(newSupportRequestCtrl.supportRequest,null,null,null);
        Test.stopTest();
    }

    static testMethod void testNewSupportRequestVFFail() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Application_Name__c FROM Case LIMIT 1];
            caseRec.Status = 'Review';
            update caseRec;
            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Submission');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.getSupportRequestRTs();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=true;
            newSupportRequestCtrl.supportRequest.Id = 'a5Ip00000009Pm2';
            newSupportRequestCtrl.submit();
            System.assertEquals(newSupportRequestCtrl.cseCommentReadyForSubmission.CommentBody,'Notes to Support: '+newSupportRequestCtrl.supportRequest.Notes_to_Support__c);
            newSupportRequestCtrl.caseid();
            SupportRequestUtils.canCreateSR(caseRec.Id);
        Test.stopTest();
    }

    static testMethod void testNewSupportRequestVFNegative() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Status,Stage__c,Application_Name__c,recordtype.name,Lender1__c,Application_Name__r.RecordTypeId FROM Case LIMIT 1];
            caseRec.Stage__c = 'Attempted Contact';
            
            Partner__c lenderRecord = new Partner__c(
                RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Lender Division').getRecordTypeId(),
                Name = 'Latitude - Personal Loan'
            );

            Database.insert(lenderRecord);
            caseRec.Lender1__c = lenderRecord.Id;
            Database.update(caseRec);

            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Contract');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=false;
            newSupportRequestCtrl.submit();
            newSupportRequestCtrl.caseid();
            System.assertEquals(caseRec.Stage__c, 'Attempted Contact');
            System.assertNotEquals(caseRec.Lender1__c, null);
            System.assertNotEquals(caseRec.Application_Name__r.RecordTypeId, null);
            SupportRequestUtils.canCreateSR(caseRec.Id);
        Test.stopTest();
    }

    static testMethod void testNewSupportRequestVFNegativeTwo() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Status,Stage__c,Application_Name__c,recordtype.name,Lender1__c,Application_Name__r.RecordTypeId FROM Case LIMIT 1];
            caseRec.Stage__c = 'Attempted Contact';
            caseRec.Regular_Monthly_Payment__c = 1000;
            Partner__c lenderRecord = new Partner__c(
                RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Lender Division').getRecordTypeId(),
                Name = 'Latitude - Personal Loan'
            );

            Database.insert(lenderRecord);
            caseRec.Lender1__c = lenderRecord.Id;
            Database.update(caseRec);

            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Contract');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=false;
            newSupportRequestCtrl.submit();
            newSupportRequestCtrl.caseid();
            System.assertEquals(caseRec.Stage__c, 'Attempted Contact');
            System.assertNotEquals(caseRec.Lender1__c, null);
            System.assertNotEquals(caseRec.Application_Name__r.RecordTypeId, null);
            SupportRequestUtils.canCreateSR(caseRec.Id);
        Test.stopTest();
    }

    static testMethod void testSupportRequestUtils() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Status,Stage__c,Application_Name__c,recordtype.name,Lender1__c,Application_Name__r.RecordTypeId FROM Case LIMIT 1];
            caseRec.Stage__c = 'Attempted Contact';
            
            // Partner__c lenderRecord = new Partner__c(
            // caseRec.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NOD: Commercial - Asset Finance').getRecordTypeId();
            //     Name = 'Latitude - Personal Loan'
            // );

            // Database.insert(lenderRecord);
            // caseRec.Lender1__c = lenderRecord.Id;
            Database.update(caseRec);

            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Contract');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=false;
            newSupportRequestCtrl.submit();
            newSupportRequestCtrl.caseid();
            // System.assertEquals(caseRec.Stage__c, 'Attempted Contact');
            // System.assertNotEquals(caseRec.Lender1__c, null);
            // System.assertNotEquals(caseRec.Application_Name__r.RecordTypeId, null);
            SupportRequestUtils.canCreateSR(caseRec.Id);
        Test.stopTest();
    }

    static testMethod void testSupportRequestUtilsTwo() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Status,Stage__c,Application_Name__c,recordtype.name,Lender1__c,Application_Name__r.RecordTypeId FROM Case LIMIT 1];
            caseRec.Stage__c = 'Attempted Contact';
            
            Partner__c lenderRecord = new Partner__c(
                RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Lender Division').getRecordTypeId(),
                Name = 'Latitude - Personal Loan'
            );
            caseRec.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NOD: Commercial - Asset Finance').getRecordTypeId();
            Database.insert(lenderRecord);
            caseRec.Lender1__c = lenderRecord.Id;
            Database.update(caseRec);

            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Contract');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=false;
            newSupportRequestCtrl.submit();
            newSupportRequestCtrl.caseid();
            // System.assertEquals(caseRec.Stage__c, 'Attempted Contact');
            // System.assertNotEquals(caseRec.Lender1__c, null);
            // System.assertNotEquals(caseRec.Application_Name__r.RecordTypeId, null);
            SupportRequestUtils.canCreateSR(caseRec.Id);
        Test.stopTest();
    }
}