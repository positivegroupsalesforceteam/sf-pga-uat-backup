/*
	@Description: handler class of ContentDocumentLink trigger
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	11/09/2017 - Created
*/
public class ContentDocumentLinkTriggerHandler {
	
	static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

	public static boolean firstRun = true; //prevents trigger to run multiple times

	//execute all After Insert event triggers
	public static void onAferInsert(list <ContentDocumentLink> newCDLinkList){
		if(trigSet.Enable_CDLink_latestCommentFromNote__c) updateLatestCommentFromNote(newCDLinkList);
	}

	//method for updating Latest Comments from created Notes
	private static void updateLatestCommentFromNote(list <ContentDocumentLink> newCDLinkList){
		
		map <id, string> cdNoteMap = new map <id, string>();
		map <id, string> upLeadMap = new map <id, string>();
		map <id, string> upOppMap = new map <id, string>();
		set <id> cdIDSet = new set <id>();

		map <id,ContentDocument> cdMap = new map <id,ContentDocument>();

		for (ContentDocumentLink cdl : newCDLinkList){
			if (String.valueof(cdl.LinkedEntityId).startsWith('00Q') || String.valueof(cdl.LinkedEntityId).startsWith('006')){ //Note linked to a Lead or Opportunity
				cdIDSet.add(cdl.ContentDocumentId);
			}
		}

		//Retrieve all created Enhanced Notes
		for (ContentVersion cversion : [Select Id, ContentDocumentId, Title, FileType, VersionData From ContentVersion Where FileType = 'SNOTE' AND ContentDocumentId in : cdIDSet]){
			if (cversion.FileType == 'SNOTE'){
				if (!cdNoteMap.containskey(cversion.ContentDocumentId)){
					Blob nbodyB = cversion.VersionData;
					string nbodyS = nbodyB.toString();
					nbodyS.unescapeunicode();
					nbodyS = nbodyS.unescapeHtml4();
					string noteToLC = cversion.Title + ' - ' + nbodyS;
					if (noteToLC.length() > 255) noteToLC = noteToLC.substring(0,250);
					cdNoteMap.put(cversion.ContentDocumentId, noteToLC);
				}
			}
		}

		for (ContentDocumentLink cdl : newCDLinkList){
			if (cdNoteMap.containskey(cdl.ContentDocumentId)){
				if (String.valueof(cdl.LinkedEntityId).startsWith('00Q')){ //Note linked to a Lead
					upLeadMap.put(cdl.LinkedEntityId, cdNoteMap.get(cdl.ContentDocumentId));
				} 
				else if (String.valueof(cdl.LinkedEntityId).startsWith('006')){ //Note linked to an Opportunity
					upOppMap.put(cdl.LinkedEntityId, cdNoteMap.get(cdl.ContentDocumentId));
				} 
			}
		}
		
		//Update Latest Comments on Leads
		if (upLeadMap.size() > 0){
			list <Lead> upLeadlist = [Select Id, Latest_Comment__c From Lead Where Id in : upLeadMap.keySet()];
			for (Lead ld : upLeadlist){
				ld.Latest_Comment__c = upLeadMap.get(ld.Id);
			}
			update upLeadlist;
		}
		//Update Latest Comments on Opportunities
		if (upOppMap.size() > 0){
			list <Opportunity> upOpplist = [Select Id, Latest_Comment__c From Opportunity Where Id in : upOppMap.keySet()];
			for (Opportunity opp : upOpplist){
				opp.Latest_Comment__c = upOppMap.get(opp.Id);
			}
			update upOpplist;
		}


	}

}