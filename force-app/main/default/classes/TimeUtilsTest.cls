@isTest
public class TimeUtilsTest {
    

    @testSetup static void setupData() {
        

    }
    
    static testMethod void getLocalClientPreferredTime_Test(){
        Datetime preferredCallTime;
        String testState = 'SA';
        Time testCallTime = Time.newInstance(9,0,0,0);
        Map<String,BusinessHours> stateBusinessHoursMap = TimeUtils.getStateBusinessHoursMap();

        Test.startTest();
        preferredCallTime = TimeUtils.getLocalClientPreferredTime(stateBusinessHoursMap.get(testState).TimeZoneSidKey, testCallTime);

        Test.stopTest();

        //System.assertEquals(expected, actual);
    }
    
    static testMethod void setTimezoneBasedReadyToDialTimeWithinBH_Test(){
        String testState = 'SA';
        Datetime callTime = System.today();
        Datetime readyToCallTimeTest;
        TimeUtils.isCurrentDayBusinessDayInCaseBH = true; 
        Test.startTest();
        
        readyToCallTimeTest = TimeUtils.setTimezoneBasedReadyToDialTime(testState, callTime);
        

        Test.stopTest();


        System.assertEquals(System.today(), readyToCallTimeTest);
    }

    static testMethod void setTimezoneBasedReadyToDialTimeBeforeBH_Test(){
        String testState = 'SA';
        Datetime callTime = DateTime.newInstance(System.Today().year(),System.Today().month(),System.Today().day(),5,0,0);
        Datetime readyToCallTimeTest;
        TimeUtils.isCurrentDayBusinessDayInCaseBH = true;
        Test.startTest();
        
        readyToCallTimeTest = TimeUtils.setTimezoneBasedReadyToDialTime(testState, callTime);
        

        Test.stopTest();


       System.assertEquals(false, TimeUtils.callNow);
    }

    static testMethod void setTimezoneBasedReadyToDialTimeAfterBH_Test(){
        String testState = 'SA';
        Datetime callTime = DateTime.newInstance(System.Today().year(),System.Today().month(),System.Today().day(),23,0,0);
        Datetime readyToCallTimeTest;
        TimeUtils.isCurrentDayBusinessDayInCaseBH = true;
        Test.startTest();
        
        readyToCallTimeTest = TimeUtils.setTimezoneBasedReadyToDialTime(testState, callTime);
        

        Test.stopTest();


       System.assertEquals(false, TimeUtils.callNow);
    }

    static testMethod void setTimezoneBasedReadyToDialTimeHolidayWithinBH_Test(){
        String testState = 'SA';
        Datetime callTime = DateTime.newInstance(System.Today().year(),System.Today().month(),System.Today().day(),10,0,0);
        Datetime readyToCallTimeTest;
        TimeUtils.isCurrentDayBusinessDayInCaseBH = false;

        Test.startTest();
        
        readyToCallTimeTest = TimeUtils.setTimezoneBasedReadyToDialTime(testState, callTime);
        

        Test.stopTest();


       System.assertEquals(true, TimeUtils.callNow);
    }
    
    static testMethod void setTimezoneBasedReadyToDialTimeHolidayBeforeBH_Test(){
        String testState = 'SA';
        Datetime callTime = DateTime.newInstance(System.Today().year(),System.Today().month(),System.Today().day(),5,0,0);
        Datetime readyToCallTimeTest;
        TimeUtils.isCurrentDayBusinessDayInCaseBH = false;
        Test.startTest();
        
        readyToCallTimeTest = TimeUtils.setTimezoneBasedReadyToDialTime(testState, callTime);
        

        Test.stopTest();


       System.assertEquals(false, TimeUtils.callNow);
    }

    static testMethod void setTimezoneBasedReadyToDialTimeHolidayAfterBH_Test(){
        String testState = 'SA';
        Datetime callTime = DateTime.newInstance(System.Today().year(),System.Today().month(),System.Today().day(),20,0,0);
        Datetime readyToCallTimeTest;
        TimeUtils.isCurrentDayBusinessDayInCaseBH = false;
        Test.startTest();
        
        readyToCallTimeTest = TimeUtils.setTimezoneBasedReadyToDialTime(testState, callTime);
        

        Test.stopTest();


       System.assertEquals(false, TimeUtils.isCurrentDayBusinessDayInCaseBH);
    }
}