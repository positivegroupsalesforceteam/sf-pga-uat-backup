/**
 * @File Name          : CaseCommentsLCTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 05/08/2019, 7:04:00 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    02/08/2019, 1:55:52 pm   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
private class CaseCommentsLCTest {

		@testsetup static void setup(){

			//Create Test Case
		    List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
            caselist[0].Suspended_Reason__c = 'test suspended Reason';
            caselist[0].Partition__c = 'Nodifi';
		    Database.insert(caseList);

            //Create test Case Comment
            CaseComment ccomment = new CaseComment(
                ParentId = caselist[0].Id,
                CommentBody = 'test comment'
            );
            insert ccomment;

		}

		static testmethod void testComment(){

			//Retrieve test Case
			Case cse = [Select Id From Case limit 1];
			PageReference pref = Page.CreateCaseCommentsLC;
			pref.getParameters().put('id', cse.Id);
			Test.setCurrentPage(pref);

			Test.startTest();
				//CaseCommentsLC ccc = new CaseCommentsLC();

                //Retrieve Case and Case Comments
				CaseCommentsLC.getCaseAndComments(cse.Id);

                //Test GetSessionID
				CaseCommentsLC.getSessionID();

                //Test error on Save
                CaseComment testComment = new CaseComment(
                    CommentBody = '',
                    ParentId = cse.Id
                );
				CaseCommentsLC.saveCaseComment(testcomment);

				//Test aura exception
                testComment.CommentBody = 'test comment';
                try{
				    CaseCommentsLC.saveCaseComment(testcomment);
                }
                catch(Exception e){
                    //Test successful save
                    CaseComment testComment2 = new CaseComment(
                        CommentBody = 'test comment',
                        ParentId = cse.Id
                    );
				    CaseCommentsLC.saveCaseComment(testcomment2);
                }

			Test.stopTest();

				//Retrieve created Case Comment on Case
				list <CaseComment> csecomments = [Select Id, CommentBody From CaseComment Where ParentId =: cse.Id];
				//Verify that Case Comment has been created
				system.assert(csecomments.size() > 0);

		}


}