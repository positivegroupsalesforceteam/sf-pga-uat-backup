/** 
* @FileName: BatchUpdateNumberAttempted
* @Description: 
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification 
* 1.0 3/9/18 RDAVID Created class
**/ 

global class BatchUpdateNumberAttempted implements Database.Batchable<sObject>{
    
    global final string query;
    
    global BatchUpdateNumberAttempted(){
        
        query = 'SELECT ID, CreatedDate,X_of_Attempts_Done_Today__c,LastModifiedDate,Last_Attempted_Contact_Date__c FROM Lead WHERE X_of_Attempts_Done_Today__c != 0 AND (Status= \'Attempted Contact\' OR Status = \'2 week callback\') AND RecordType.Name = \'Individual\' AND DAY_ONLY(Last_Attempted_Contact_Date__c) < Today Order BY LastModifiedDate desc';	
        
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope) {
        List<Lead> updateObjs = new List<Lead>();
        for(Sobject s : scope) {
            Lead obj = (Lead) s;
            obj.X_of_Attempts_Done_Today__c = 0;
            updateObjs.add(obj);
        }  
        update updateObjs;
    }
    
    global void finish(Database.BatchableContext BC) {
        system.debug('Batch Job to remove check boxes is complete!');
    }	
}