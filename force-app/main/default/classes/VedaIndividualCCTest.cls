/**
    @History -  09/08/2017 - Updated (Jesfer Baculod - Positive Group)
 */
@isTest
private class VedaIndividualCCTest {

    private static string PROFILE_LT = Label.Profile_Name_Lightning_Team; //Lightning Team
    private static string PROFILE_SYSADMIN = Label.Profile_Name_System_Administrator; //System Administrator
    private static final string ACC_RT_INDIVIDUAL = Label.Contact_Individual_RT; //Individual
    
    private static List<Account> accList = new List<Account>();
    private static List<Role__c> roleList = new List<Role__c>();

    @testsetup static void setup(){

        //create test data for Sys Admin
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : PROFILE_SYSADMIN ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;

        //create test data for Lightning team User
        ID roleID = [Select Id, Name From UserRole Where Name = : 'Team Member' ].Id;
        ID pID_st = [Select Id, Name From Profile Where Name = : 'Sales Team' ].Id;
        User stuser = new User(
                UserName = 'testlt@vedaindi.com.uat',
                LastName = 'TestLT',
                Email = 'testlt@vedaindi.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tlt',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                UserRoleId = roleID,
                ProfileId =  pID_st
            );
        insert stuser;

        system.runAs(sysadminUsr){
            Account acc = TestHelper.createPersonAccount();
            Contact c = TestHelper.createContact();
            //Lead ld = TestHelper.createLead();

            Id rtIndividualOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(ACC_RT_INDIVIDUAL).getRecordTypeId();
            Opportunity opp = new Opportunity(
                    AccountId = acc.Id,
                    StageName = 'Qualified',
                    Base_rate__c = 1,
                    Checked_Commissions_Correct__c = true,
                    Employment_Reference_Completed__c = true,
                    Residential_Reference_Completed__c = true,
                    Vehicle_Purchase_Price__c = 1000,
                    Total_amount_financed__c = 1000,
                    Sale_Type__c = Label.Opp_Sale_Type_Dealer,
                    Name = 'Test Opportunity ANZ 1',
                    CloseDate = Date.today(),
                    Approval_Date__c = Date.today(),
                    Make__c = 'test',
                    Model__c = 'triton',
                    Loan_Reference__c = '123456789',
                    New_or_Used__c = 'New',
                    Loan_Product__c = 'Consumer Loan',
                    Previous_Employment_Status__c = 'Full-time',
                    Transmission__c = 'AT',
                    Year__c = '2017',
                    Loan_Rate__c = 50,
                    Lender__c = 'ANZ',
                    RecordTypeId = rtIndividualOpp
                );
            //insert opp;

            Applicant_2__c app2 = new Applicant_2__c(
                    Name = 'test',
                    Email__c = 'app2@test.com',
                    Mobile__c = '3424234'
                );
            insert app2;

            //Create Test Account
            accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
            Database.insert(accList);

            //Create Test Case
            List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
            Database.insert(caseList);
            
            //Create Test Application
            List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
            Database.insert(appList);

            //Create Test Role 
            roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
            Database.insert(roleList);

            TestDataFactory.Case2FOSetupTemplate();

        }

    }

    static testMethod void testVedaIndividualCCControllerError() {
          
        //Opportunity opp = TestHelper.createPersonAccountOpportunity(Account acct);
        //Lead ld = [Select Id, Name From Lead];
        //Opportunity opp = [Select Id, Name From Opportunity];
        Case cse = [SELECT Id, CaseNumber, Application_Name__r.RecordtypeId, Application_Name__r.Recordtype.Name, Lender1__c, Lender1__r.Name FROM Case LIMIT 1];
        Account acc = [Select Id, Name From Account LIMIT 1];
        Applicant_2__c app2 = [Select Id, Name From Applicant_2__c LIMIT 1];
        User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat'];
        

        Attachment att = new Attachment(
                        ParentId = cse.Id,
                        Name = 'Test-CC.pdf',
                        Body = Blob.valueOf('test')
                    );
        insert att;
        
        System.assert(acc!=null);
        Test.startTest();
        
            system.runAs(stusr){
                PageReference pageRef = Page.VedaIndividualPage;
                Test.setCurrentPage(pageRef);
                VedaIndividualCC ext = new VedaIndividualCC(new ApexPages.StandardController(acc));
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                //ext = new VedaIndividualCC(new ApexPages.StandardController(ld));    
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                //ext = new VedaIndividualCC(new ApexPages.StandardController(opp));    
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                ext = new VedaIndividualCC(new ApexPages.StandardController(app2));    
                ext.testresponse = 'test';
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
            }

            //Verify that page has error messages
            system.assertEquals(ApexPages.hasMessages(),false);
        
        Test.stopTest(); 
       
    }
    
    static testMethod void testVedaIndividualCCControllerNMError() {
          
        //Opportunity opp = TestHelper.createPersonAccountOpportunity(Account acct);
        //Lead ld = [Select Id, Name From Lead];
        //Opportunity opp = [Select Id, Name From Opportunity];
        Case cse = [SELECT Id, CaseNumber, Application_Name__r.RecordtypeId, Application_Name__r.Recordtype.Name, Lender1__c, Lender1__r.Name FROM Case LIMIT 1];
        Account acc = [Select Id, Name From Account LIMIT 1];
        Applicant_2__c app2 = [Select Id, Name From Applicant_2__c LIMIT 1];
        User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat'];
        

        Attachment att = new Attachment(
                        ParentId = cse.Id,
                        Name = 'Test-CC.pdf',
                        Body = Blob.valueOf('test')
                    );
        insert att;
        
        System.assert(acc!=null);
        Test.startTest();
        
            system.runAs(stusr){
                PageReference pageRef = Page.VedaIndividualPageNM;
                Test.setCurrentPage(pageRef);
                VedaIndividualCC_NM ext = new VedaIndividualCC_NM(new ApexPages.StandardController(acc));
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                //ext = new VedaIndividualCC_NM(new ApexPages.StandardController(ld));    
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                // ext = new VedaIndividualCC_NM(new ApexPages.StandardController(opp));    
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
                ext = new VedaIndividualCC_NM(new ApexPages.StandardController(app2));    
                ext.testresponse = 'test';
                ext.generateCreditReport();
                ext.getErrorMsg();
                ext.back();
            }

            //Verify that page has error messages
            system.assertEquals(ApexPages.hasMessages(),false);
        
        Test.stopTest(); 
       
    }

    static testMethod void testVedaIndividualCCControllerSuccess() {
          
        //Opportunity opp = TestHelper.createPersonAccountOpportunity(Account acct);
        Account acc = [Select Id, Name From Account LIMIT 1];
        User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat' LIMIT 1];
        
        System.assert(acc!=null);
        Test.startTest();
        
            system.runAs(stusr){
                PageReference pageRef = Page.VedaIndividualPage;
                Test.setCurrentPage(pageRef);
                VedaIndividualCC ext = new VedaIndividualCC(new ApexPages.StandardController(acc));
                ext.testresponse = 'test';
                ext.generateCreditReport();
            }
        
        Test.stopTest(); 

        //Retrieve created Attachment (response) on Account
        Attachment att = [Select Id, Name, ParentId From Attachment Where ParentId = : acc.Id];
        system.assertEquals(att.Name.contains('CCResponse'),true);
       
    }
    
    static testMethod void testVedaIndividualCCControllerNMSuccess() {
          
        //Opportunity opp = TestHelper.createPersonAccountOpportunity(Account acct);
        Account acc = [Select Id, Name From Account LIMIT 1];
        User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat' LIMIT 1];
        
        System.assert(acc!=null);
        Test.startTest();
        
            system.runAs(stusr){
                PageReference pageRef = Page.VedaIndividualPageNM;
                Test.setCurrentPage(pageRef);
                VedaIndividualCC_NM ext = new VedaIndividualCC_NM(new ApexPages.StandardController(acc));
                ext.testresponse = 'test';
                ext.generateCreditReport();
            }
        
        Test.stopTest(); 

        //Retrieve created Attachment (response) on Account
        Attachment att = [Select Id, Name, ParentId From Attachment Where ParentId = : acc.Id];
        system.assertEquals(att.Name.contains('CCResponse'),true);
    }

    static testMethod void testVedaIndividualCCNMControllerNMSuccess() {
          
        //Opportunity opp = TestHelper.createPersonAccountOpportunity(Account acct);
        Role__c role = [Select Id From Role__c LIMIT 1];
        // User stusr = [Select Id, Profile.Name From User Where USerName = 'testlt@vedaindi.com.uat' LIMIT 1];
        
        System.assert(role!=null);
        Test.startTest();
            // system.runAs(stusr){
            PageReference pageRef = Page.VedaIndividualPageNM;
            Test.setCurrentPage(pageRef);
            VedaIndividualCC_NM ext = new VedaIndividualCC_NM(new ApexPages.StandardController(role));
            ext.testresponse = 'test';
            ext.generateCreditReport();
            ext.back();
            ext.getErrorMsg();
            // }
        Test.stopTest(); 
    }
}