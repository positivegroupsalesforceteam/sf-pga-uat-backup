/*
    @Description: handler class of AttachmentTrigger
    @Author: Jesfer Baculod - Positive Group
    @History:
        -   9/7/2017 -  Created
                     -  added preventDeletionOfCreditCheck method
                     -  moved syncAccOppAttachments method
*/
public class AttachmentTriggerHandler{

    static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

    //execute all Before Insert event triggers
    public static void onAfterInsert(map <Id,Attachment> newAttMap){
        //Execute syncAccOppAttachments
        if (trigSet.Enable_Attachment_syncAccOppAttachment__c){
            syncAccOppAttachments(newAttMap);
        }
    }

    //execute all Before Delete event triggers
    public static void onBeforeDelete(map<Id,Attachment> oldAttMap){
        if (trigSet.Enable_Attachment_preventDeletionOfCC__c){
            preventDeletionOfCreditCheck(oldAttMap);
        }
    }

    
    private static void preventDeletionOfCreditCheck(map<Id,Attachment> oldAttMap){
        system.debug('@@oldAttMap:'+oldAttMap);
        //CCResponse
        map <id,list<Attachment>> parentIdAttMap = new map <id, list<Attachment>>();
        map <id,integer> parentIdCountMap = new map <id,integer>();
        for (Attachment att : oldAttMap.values()){
            if (att.Name.endswith('-CC.pdf')){
                if (!parentIdAttMap.containskey(att.ParentId)){
                    parentIdAttMap.put(att.ParentId, new list<Attachment>());
                }
                parentIdAttMap.get(att.ParentId).add(att);
                if (!parentIdCountMap.containskey(att.ParentId)){
                    parentIdCountMap.put(att.ParentId, 1);
                }
                parentIdCountMap.put(att.ParentId, parentIdCountMap.get(att.ParentId) + 1);
            }
        }

        //Check existing attachments from parent Objects
        list <Attachment> exAttlist = [Select Id, Name, ParentId From Attachment Where ParentId in : parentIdAttMap.keySet() AND Id not in : oldAttMap.keySet()];
        system.debug('@@exAttlist:'+exAttlist.size()+exAttlist);
        for (Attachment att : exAttlist){
            if (att.Name.endswith('-CC.pdf')){
                if (parentIdCountMap.containskey(att.ParentId)){
                    parentIdCountMap.put(att.ParentId, parentIdCountMap.get(att.ParentId) + 1);
                }
            }
        }

        for (Attachment att : oldAttMap.values()){
            if (parentIdCountMap.containskey(att.ParentId)){
                if(parentIdCountMap.get(att.parentId) >= 1){
                    att.addError(Label.Veda_Credit_Check_Attachment_Delete_Error); //Record is locked and can't be deleted. Please contact your administrator.
                }
            }
        }

    }


    private static void syncAccOppAttachments(map <Id,Attachment> newAttMap){

        list <string> stagesToIgnore = new list <string>(new string[] {
                Label.Opportunity_Stage_Settled, //Settled
                Label.Opportunity_Stage_Declined_By_Lender, //Declined by Lender
                Label.Opportunity_Stage_No_Longer_Proceeding //No Longer Proceeding
            });

        //group the attachments based on the account id its attached..
        map <id, set<id>> acctsToAttachmentMap = new map <id, set<id>>();
        for (Attachment att : newAttMap.values()){
            if (att.Name.endswith('-CC.pdf')){
                if (!acctsToAttachmentMap.containskey(att.ParentId)){
                    acctsToAttachmentMap.put(att.ParentId, new set <Id>());
                }
                acctsToAttachmentMap.get(att.ParentId).add(att.Id);
            }
        }

        //filter the person accounts based on the above list
        map <id,Account> perAccts = new map <Id, Account>([Select PersonEmail, PersonContactId, IsPersonAccount From Account Where IsPersonAccount = true AND id in : acctsToAttachmentMap.keySet()]);
        if (perAccts.size() > 0){

            //for each of the person account finds the open opportunities(not equal to 'Settled' or 'Declined By Lender'  or 'No Longer Proceeding')..  
            Map<id,Opportunity> oppForPerAccount = new Map<id,opportunity>(
                                            [Select id,name,StageName,AccountId from opportunity where AccountId in :perAccts.keySet()
                                             and StageName not in :stagesToIgnore]);
        
            Map<id,Attachment> attachmentMap = new Map<id,Attachment>(
                                            [Select id, name, body, parentid from Attachment where id in: newAttMap.keyset()]);


            Map<Id, set<id>> acctsToOpportunityMap = new Map<Id, set<id>>();

            // group the opportunity based on the account id's
            for(Opportunity opp : oppForPerAccount.values()){
                if (!acctsToOpportunityMap.containskey(opp.AccountId)){
                    acctsToOpportunityMap.put(opp.AccountId, new set<Id>());
                }
                acctsToOpportunityMap.get(opp.AccountId).add(opp.id);      
            }

            // for each of the account find the opportunity that are open. For each of the opportunity attach the 
            // all the attachment that are attached to that account
            list <Attachment> insertAttlist = new list <Attachment>();
            for(Id accId : acctsToOpportunityMap.keySet()){
                
                Set<id> oppIds = acctsToOpportunityMap.get(accId);
                Set<id> attachIds = acctsToAttachmentMap.get(accId);           
                if(oppIds!=null){
                    for(Id attId : attachIds){
                        Attachment a = attachmentMap.get(attId);
                        System.debug('Attachment name ' + a);
                        for(id oppId : oppIds){                 
                             Attachment att = new Attachment(name = a.name, body = a.body, parentid = oppId);
                             insertAttlist.add(att);
                        }
                    }
                    
                }
                
            }
            System.debug('insert attachment size ' + insertAttlist.size());
            //Insert Attachments copied from Account to Opportunities
            if(insertAttlist.size() >0){
                insert insertAttlist;
            }
        
        }

    }


}