@isTest
public class SubmissionRequest_CaseTest {
    
    @testsetup static void setup(){

        //create test data for Account
        Account acc = new Account(
                Combined_Centrelink_Income__c = 50,
                Combined_Rent_Mortgage_Board__c = 50,
                Combined_Regular_Income__c = 30,
                Combined_Loan_Payments__c = 30
            );
        acc.Name = 'test Account';
        insert acc;
        
        //create test data for Contact
        COntact cnt = new COntact(
                FirstName = 'Fname',
                LastName = 'LName',
                Email = 'email@email.com',
                Drivers_Licence_State__c = 'New South Wales',
                Residential_Status__c = 'Mortgage'
            );
        insert cnt;
        
        //create test data for Assets
        Id rtConnectiveID = Schema.SObjectType.Assets__c.getRecordTypeInfosByName().get('Connective').getRecordTypeId();
        list <Assets__c> asstlist = new list <Assets__c>();
        Assets__c asst = new Assets__c(
                RecordTypeId = rtConnectiveID,
                Key_Person_Name__c = cnt.id,
                Asset_Type1__c ='Cash Savings',
                Year_of_Manufacture__c = '1998',
                Model__c = 'test'
            );
        asstlist.add(asst);
        Assets__c asst2 = new Assets__c(
                RecordTypeId = rtConnectiveID,
                Key_Person_Name__c = cnt.id,
                Asset_Type1__c ='Real Estate',
                Year_of_Manufacture__c = '1998',
                Model__c = 'test'
            );
        asstlist.add(asst2);
        insert asstlist;
        
        //create test data for Case
        Id rtConsumerLoanCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_Private_RT).getRecordTypeId();
        Case cs = new Case();
        cs.RecordTypeId = rtConsumerLoanCase;
        cs.ContactId  = cnt.Id;
        cs.AccountId = acc.Id;
        cs.Quoted_Monthly_Payments__c = 20;
        cs.Surplus__c =3;
        cs.Loan_Amount_Currency__c = 0.0;
        cs.Payout_To__c = '2';
        cs.Loan_Type__c = 'Unsecured Personal Loan';
        cs.Personal_Loan_Reason__c = '';
        cs.Lender__c ='ANZ';
        insert cs;
        
        //crate test data for Liability
        Id rtConnectiveID_L = Schema.SObjectType.Liability__c.getRecordTypeInfosByName().get('Connective').getRecordTypeId();
        Liability__c liab = new Liability__c(
                RecordTypeId = rtConnectiveID_L,
                Key_Person_Name__c = cnt.Id,
                Monthly_Payment__c = 50,
                Type1__c = 'Mortgage Loan',
                Start_Date__c = Date.newInstance(2000, 1, 10),
                Loan_Term__c = '12 months'
            );
        insert liab;
        

    }

    static testmethod void testValidateSubmission(){

        
        Case cse = [Select Id, (SELECT Id, Name, RecordType.Name, CreatedDate, Relationship__c, Phone__c , Street_Number__c, Street_Name__c , Street_Type__c , Suburb__c , State__c, Postcode__c,  Country__c From References1__r), CaseNumber From Case];
        Contact con = [Select Id, (Select Id From Assets__r), (Select Id From Liabilities__r), (Select Id, Time_in_Previous_Address_years__c From Previous_Addresses1__r), Name From Contact];
        
        
        Test.startTest();

            ApexPages.StandardController std = new ApexPages.StandardController(cse);
            SubmissionRequest_Case Subcs = new SubmissionRequest_case(std);

            subcs.cseObj.Partner_Pay_Slip_Obtained__c = false;
            subcs.cseObj.Are_You_Splitting_Expenses__c = 'Yes';
            subcs.subReq.Lender_Rating__c = null;
            subcs.cseObj.Lender__c = 'Money 3';
            subcs.cseObj.Quoted_Monthly_Payments__c = 30;
            subcs.cseObj.Asset_Type__c = 'Motor Vehicle';
            subcs.cseObj.Vehicle_Make__c = '';
            subcs.cseObj.Vehicle_Model__c = '';
            subcs.cseObj.Vehicle_Transmission__c = null;
            subcs.cseObj.Vehicle_Body_Type__c = null;
            subcs.cseObj.Vehicle_Fuel_Type__c = null;
            subcs.cseObj.Varient__c = null;
            subcs.cseObj.Trade_in_Description__c = '';
            subcs.cseObj.Trade_in_Amount__c = 10;
            subcs.cseObj.Application_Type__c = 'Joint Application';
            subcs.cseObj.Surplus__c = 10;
            subcs.cseObj.Loan_Type__c = 'Asset Loan';
            subcs.cseObj.Asset_Type__c = 'Motor Vehicle';
            subcs.cseObj.Vehicle_Purchase_Price__c = null;
            subcs.cseObj.Payout_Amount__c = 50;
            subcs.cseObj.Payout_To__c = '';
            subcs.cseObj.Combined_Dependant_Child_Expenses__c = 0;

            subcs.cntObj.Marital_Status__c = 'Married';
            subcs.cntObj.Dependants__c = '2';
            subcs.cntObj.Primary_Country_of_Citizenship__c = 'India';
            subcs.cntObj.Visa_Subclass__c = '';

            subcs.subReq.Have_checked_3_months_of_statements__c = false;
            subcs.subReq.Why_Spouse_not_on_Loan__c = '';
            subcs.subReq.Requested_Writing_Rate__c = '';
            subcs.subReq.Pre_Approval__c = false;
            subcs.subReq.Notes_for_Support__c = '';
            subcs.subReq.Notes_For_Lender__c = '';

            subcs.readyforSub();

            subcs.cseObj.Lender__c = 'Yamaha Finance';
            subcs.cseObj.Asset_Type__c = null;
            subcs.cntObj.Drivers_Licence_State__c = null;
            subcs.cseObj.Are_you_Splitting_Expenses__c = null;
            subcs.cntObj.Resident_Status__c = false;
            subcs.subReq.Pre_Approval__c = true;
            subcs.readyforSub();

            subcs.cseObj.Lender__c = 'Liberty';
            subcs.subReq.Lender_Rating__c = 'A';
            subcs.cseObj.Application_Type__c = null;
            subcs.cntObj.Marital_Status__c = null;
            subcs.cntObj.Primary_Country_of_Citizenship__c = null;
            subcs.readyforSub();

            subcs.submit();
            subcs.saveUpdates();
            subcs.caseid();
            subcs.refreshCaseDetails();

        Test.stopTest();
        
    }

    static testmethod void testSubmit(){

        Case cse = [Select Id, CaseNumber From Case];
        list <Reference1__c> ref1list = new list <Reference1__c>();

        for (Integer i=0; i<3; i++){
            Reference1__c ref = new Reference1__c(
                Case__c = cse.Id,
                Relationship__c = 'Friend',
                Phone__c = '34234234',
                Person_Name__c = 'The Person'+i,
                Street_number__c = '342343434',
                Street_name__c = '343433',
                street_type__c = 'test',
                suburb__c = 'test',
                state__c = 'Tasmania',
                postcode__c = '4343434',
                country__c = 'Australia'
            );
            ref1list.add(ref);
        }
        insert ref1list;

        cse = [Select Id, (SELECT Id, Name, RecordType.Name, CreatedDate, Relationship__c, Phone__c , Street_Number__c, Street_Name__c , Street_Type__c , Suburb__c , State__c, Postcode__c,  Country__c From References1__r), CaseNumber From Case];
        Contact con = [Select Id, (Select Id From Assets__r), (Select Id From Liabilities__r), (Select Id, Time_in_Previous_Address_years__c From Previous_Addresses1__r), Name, Time_at_Current_Address_Years__c From Contact];

        Test.startTest();

            ApexPages.StandardController std = new ApexPages.StandardController(cse);
            SubmissionRequest_Case Subcs = new SubmissionRequest_case(std);

            subcs.subReq.Have_signed_privacy_consent__c = true;
            subcs.subReq.Latitude_Privacy_Accepted__c = true;
            subcs.subReq.Have_checked_income_on_payslips__c = true;
            subcs.subReq.Positive_Calc_In_File__c = true;
            subcs.subReq.Servicing_Calc_Exp_Match_Liabilities__c = true;
            subcs.subReq.Do_You_Know_Vendor_Dealer_Name__c = 'Yes';
            subcs.subReq.Have_signed_credit_guide_and_quote__c = true;
            subcs.subReq.Partner_Pay_Slip_Obtained__c = true;
            subcs.subReq.Have_checked_3_months_of_statements__c = true;
            subcs.subReq.Why_Spouse_not_on_Loan__c = 'test';
            subcs.subReq.Support_Docs_Combined__c = true;
            subcs.cseObj.Application_Type__c = 'Individual Application';
            subcs.subReq.Gross_Annual_Income_Liberty__c = 100;
            subcs.cseObj.Paymen_Terms__c ='In Advance';
            subcs.cseObj.Loan_Rate__c = 50;
            subcs.cseObj.Loan_Term__c = '60';
            subcs.cseObj.Loan_Type__c = 'Unsecured Personal Loan';
            subcs.cseObj.Bank_fees__c = 3334;
            subcs.cseObj.Personal_Loan_Reason__c = 'test';
            subcs.cseObj.Base_rate__c = 343;
            subcs.cseObj.Application_Fee__c = 3242344;
            subcs.cseObj.Loan_Amount_Currency__c = 500;
            subcs.cseObj.Payment_Frequency__c = 'Monthly';
            subcs.cseObj.Credit_History__c = 'No Infringements';
            subcs.cseObj.Loan_Product__c = 'Consumer Loan';
            subcs.cseObj.Sale_Type__c = 'Private Sale';
            subcs.cseObj.Asset_Type__c = 'Heavy Vehicles';
            subcs.cseObj.New_or_Used__c = 'New';
            subcs.cseObj.Previous_Bankrupt__c = 'No';
            subcs.cseObj.Finance_For_Business_Purposes__c = 'No';
            subcs.cseObj.Changes_in_circumstance__c = 'No';
            subcs.subReq.Notes_for_Support__c = 'test';
            subcs.subReq.Notes_For_Lender__c = 'test';
            subcs.cseObj.Quoted_Monthly_Payments__c = 80;
            subcs.cseObj.Surplus__c = 100;
            subcs.cntObj.Residential_Situation__c = 'Mortgage';
            subcs.cntObj.Birthdate = Date.valueOf('1990-01-01');
            subcs.cntObj.Gender__c = 'male';
            subcs.cntObj.Marital_Status__c = 'Single';
            subcs.cntObj.Medicare_Number__c = '12345';
            subcs.cntObj.Primary_Passport_Number__c = '135566';
            subcs.cntObj.Primary_Country_of_Citizenship__c = 'AU';
            subcs.cntObj.Drivers_Licence_Expiry__c = Date.valueof('2017-12-20');
            subcs.cntObj.Drivers_License_Number__c = '4523454';
            subcs.cntObj.Drivers_Licence_State__c = 'Australian Capital Territory';
            subcs.cntObj.Current_Address_Start_Date__c  = Date.valueof('1955-01-01');
            subcs.cntObj.Street_Number__c = '34234234';
            subcs.cntObj.Street_Name__c = 'test';
            subcs.cntObj.Street_Type__c = 'test';
            subcs.cntObj.Suburb__c = 'test';
            subcs.cntObj.State__c = 'Australian Capital Territory';
            subcs.cntObj.Postcode__c = '343434';
            subcs.cntObj.Country__c = 'Australia';
            subcs.cseObj.Combined_Phone_TV_Internet__c = 2343243;
            subcs.cseObj.Combined_Insurance__c = 3422343;
            subcs.cseObj.Combined_Petrol_Public_Transport__c = 343433;
            subcs.cseObj.Combined_Groceries_Food__c = 34343434;
            subcs.cseObj.Combined_Other_Expenses__c =45455;

            subcs.readyforSub();
            subcs.submit();
            subcs.saveUpdates();

        Test.stopTest();

        system.assertEquals(String.valueof(ApexPages.getMessages()).contains('CONGRATULATIONS, YOUR SUBMISSION HAS CLEARED ALL VALIDATIONS'),true);

    }

}