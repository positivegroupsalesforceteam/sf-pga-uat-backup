/*
    @author: Jesfer Baculod - Positive Group
    @history: 08/04/18 - Created
              08/06/18 - Added updating of Partition on Private Seller base on Case's Partition
    @description: controller extension class of PurchasedAsset_EditVendorSeller
*/
public class PurchasedAsset_EditVendorSellerCC {

	public string pass_DV {get;set;}
    public string pass_DC {get;set;}
    public string pass_VC {get;set;}

    public string pass_DV_id {get;set;}
    public string pass_DC_id {get;set;}
    public string pass_VC_id {get;set;}
    public string pass_s_ID {get;set;} //gen search lookup ID

    public string errmsg {get;set;}
    public string dupeoption {get;set;}
    public string nullparam {get;set;}

    public Purchased_Asset__c pass {get;set;}
    public Referral_Company__c refcomp {get;set;}
    public ABN__c inputABN {get;set;}
    public ABN__c currentABN {get;set;}
    public Referral_Company__c exrefcomp {get;set;}
    public ABN__c exABN {get;set;}
    public Contact bcon {get;set;} //Business Contact
    public Account iacc {get;set;} //Private Seller Person Account
    public Contact icon {get;set;} 
    public Address__c addr {get;set;}

    public boolean isCreateABN {get;set;}
    public boolean showRefCompCre {get;set;}
    public boolean showBConCre {get;set;}
    public boolean showIconCre {get;set;}
    public boolean showRefCompS {get;set;}
    public boolean showBConS {get;set;}
    public boolean showIConS {get;set;}
    public boolean noRefComp {get;set;}
    public boolean saveSuccess {get;set;}
    public boolean saveSuccessBCon {get;set;}
    private boolean withExistingVD {get;set;}
    public boolean showDuplicateRC {get;set;}

    private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business
    private static final string CON_RT_INDIVIDUAL = Label.Contact_Individual_RT;

    private static final string SALE_TYPE_PRIVATE = 'Private Sale';
    private static final string SALE_TYPE_DEALER = 'Dealer';

    private static final string ERR_MSG_1 = Label.Case_Edit_Vendor_Details_Error_Message_1;  //Dealer Sale Vendor & Dealer Contact Person are required for Dealer Sale Type
    private static final string ERR_MSG_2 = Label.Case_Edit_Vendor_Details_Error_Message_2;  //Dealer Sale Vendor is required for Dealer Sale Type
    private static final string ERR_MSG_3 = Label.Case_Edit_Vendor_Details_Error_Message_3;  //Dealer Contact Person is required for Dealer Sale Type
    private static final string ERR_MSG_4 = Label.Case_Edit_Vendor_Details_Error_Message_4;  //Private Sale Vendor Contact is required for Private Sale Type

    private Id rt_c_b {get;set;} //record type variable for Business Contact
    private Id rt_c_i {get;set;} //record type variable for Invidividual Person Account
    private Id rt_c_ic {get;set;} //record type variable for Individual Contact

    public integer searchMode {get;set;} //on which field does the user is doing the search

    //pagination of Dealer Sale Vendor - Referral Company
    private string RCquery {get;set;}
    public Integer RC_size {get;set;} 
    public Integer RC_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConRC { 
        get {
            if(setConRC == null) {                
                setConRC = new ApexPages.StandardSetController(Database.getQueryLocator(RCquery));
                setConRC.setPageSize(RC_size);  
                RC_noOfRecords = setConRC.getResultSize();
            }            
            return setConRC;
        }
        set;
    }

    //pagination of Dealer Contact Person - Business Contact
    private string BConquery {get;set;}
    public Integer BCon_size {get;set;} 
    public Integer Bcon_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConBcon {
        get {
            if(setConBcon == null) {                
                setConBcon = new ApexPages.StandardSetController(Database.getQueryLocator(BConquery));
                system.debug('@@setConBCon:'+setConBCon);
                system.debug('@@BCon_size:'+BCon_size);
                if (BCon_size == null) BCon_size = 5;
                setConBcon.setPageSize(BCon_size);  
                Bcon_noOfRecords = setConBcon.getResultSize();
            }            
            return setConBcon;
        }
        set;
    }

    //pagination of Private Seller Person Account
    private string PSellerQuery {get;set;}
    public Integer PSeller_size {get;set;} 
    public Integer PSeller_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConPSellerAcc { 
        get {
            if(setConPSellerAcc == null) {                
                setConPSellerAcc = new ApexPages.StandardSetController(Database.getQueryLocator(PSellerQuery));
                setConPSellerAcc.setPageSize(PSeller_size);  
                PSeller_noOfRecords = setConPSellerAcc.getResultSize();
            }            
            return setConPSellerAcc;
        }
        set;
    }

    public PurchasedAsset_EditVendorSellerCC(ApexPages.StandardController stdController) {
        errmsg = pass_DV = pass_DC = pass_VC = '';
        showDuplicateRC = withExistingVD = noRefComp = saveSuccess = saveSuccessBcon = false; 
        searchMode = 1;  //default to Dealer Sale Vendor search
        PSeller_size = BCon_size = RC_size = 5; RCquery = BConquery = PSellerQuery = ''; 
        showRefCompCre = showBConCre = showIconCre = showRefCompS = showBConS = showIConS = false;
        refcomp = new Referral_Company__c(OwnerId = UserInfo.getUserId());
        inputABN = new ABN__c();
        addr = new Address__c(
            RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Full Address').getRecordTypeId()
        );
        rt_c_b = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
        rt_c_i = Schema.SObjectType.Account.getRecordTypeInfosByName().get('nm: zPrivateSeller').getRecordTypeId();
        rt_c_ic = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_INDIVIDUAL).getRecordTypeId();
        bcon = new Contact(RecordTypeId = rt_c_b);
        iacc = new Account(RecordTypeId = rt_c_i);
        icon = new Contact();
        this.pass = (Purchased_Asset__c)stdController.getRecord();
        this.pass = [Select Id, Name, Sale_Type__c, Case__r.Status, Case__r.Stage__c, Case_Partition__c, Private_Seller__c, Private_Seller_Address__c, Dealer_Sale_Vendor__r.Name, Private_Seller_Email__c, Private_Seller_Name__c, Private_Seller_Phone__c,
                    Dealer_Address__c, Dealer_Contact_Email__c, Dealer_Contact_Mobile__c, Dealer_Contact_Person__r.Name, Dealer_Contact_Person__c, Dealer_Legal_Name__c, Dealer_Registered_ABN__c, Dealer_Sale_Vendor__c
                    From Purchased_Asset__c Where Id = : stdController.getId()];

        //Retrieve existing Vendor Details
        if (pass.Sale_Type__c == SALE_TYPE_PRIVATE){ //search for all Individual Contacts
        	if (pass.Private_Seller__c != null){
	            pass_VC = pass.Private_Seller_Name__c;
	            pass_VC_id = pass.Private_Seller__c;
	        } 
            searchMode = 3;
            showIconS = true;
            showRefCompS = false;
            withExistingVD = true;
            searchPrivateSellerAccount();
            withExistingVD = false;
        }
        else if (pass.Sale_Type__c == SALE_TYPE_DEALER){ //search for all Referral Companies 
        	if (pass.Dealer_Sale_Vendor__c != null){
	            pass_DV = pass.Dealer_Sale_Vendor__r.Name;
	            pass_DV_id = pass.Dealer_Sale_Vendor__c; 
	            //searchReferralCompany();
	            //showRefCompS = true;
	        }
	        if (pass.Dealer_Contact_Person__c != null){
	            pass_DC = pass.Dealer_Contact_Person__r.Name;
	            pass_DC_id = pass.Dealer_Contact_Person__c;
	        }
            searchMode = 1;
            showRefCompS = true;
            withExistingVD = true;
            searchReferralCompany();
            withExistingVD = false;
        }


    }

    public void retrieveCurrentABN(){
        currentABN = [Select Id, Name, Registered_ABN__c From ABN__c Where Id =: refComp.Legal_Entity_Name__c];
        system.debug('@@currentABN:'+currentABN);
    }


    //Changes the size of pagination
    public PageReference refreshPageSize() {
         if (searchMode == 1 ) setConRC.setPageSize(RC_size); //Searches in Referral Company
         else if (searchMode == 2) setConBcon.setPageSize(BCon_size); //Searches in Business Contact
         else if (searchMode == 3) setConPSellerAcc.setPageSize(PSeller_size); //Searches in Private Seller Person Account Contact
         return null;
    }

    //Sets Id on selected record
    public void assignLookupID(){
        if (searchMode == 1) pass_DV_id = pass_s_ID;
        if (searchMode == 2) pass_DC_id = pass_s_ID;
        if (searchMode == 3) pass_VC_id = pass_s_ID;
        showRefCompCre = false;
        showBConCre = false;
        showIconCre = false;
    }

    //Clear all inputted data on Create Screens
    public void clearFormfields(){
        system.debug('@@pass_DV_id:'+pass_DV_id);
        refcomp = null;
        bcon = null; 
        iAcc = null;
        iCon = null;
        saveSuccess = false;
        saveSuccessBcon = false;
        currentABN = new ABN__c();
        inputABN = new ABN__c();
        isCreateABN = false;
        exrefcomp = new Referral_Company__c();
        exABN  = new ABN__c();
        refcomp = new Referral_Company__c(OwnerId = UserInfo.getUserId());
        bcon = new Contact(RecordTypeId = rt_c_b);
        if (pass_DV_id != null && pass_DV_id != '') bcon.Referral_Company__c = pass_DV_id; //assign selected Referral Company
        Iacc = new Account(RecordTypeId = rt_c_I);
        icon = new Contact();
        addr = new Address__c(
            RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Full Address').getRecordTypeId()
        );
    }

    public List<Referral_Company__c> getsearchRefComplist(){
         return (List<Referral_Company__c>) setConRC.getRecords();
    }

    public List<Contact> getsearchBConlist(){
         return (List<Contact>) setConBcon.getRecords();
    }

    public List<Account> getsearchPSellerlist(){
        list <Account> psellerlist = new list <Account>();
        psellerlist = (List<Account>) setConPSellerAcc.getRecords();
        return psellerlist;
    }


    //method for searching Dealer Sale Vendor - Referral Company
    public PageReference searchReferralCompany(){
        if (!withExistingVD) pass_DV_Id = null;
        setConRC = null; saveSuccess = false;
        string srchvar = '%' + pass_DV + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        RCquery = 'Select Id, Name, Address_Line_1__c, Company_Type__c, State__c, Dealer_Type__c, Company_Website__c, Primary_Contact_s_Name__c, Primary_Contact_s_Mobile__c, Last_Contact_Date__c From Referral_Company__c Where Name like \'' + srchvar + '\' AND Company_Type__c != \'Mortgage Broker\' Order by Name ASC';
        getsearchRefComplist();
        setConRC.setpageNumber(1);
        return null;
    }

    //method for searching Dealer Contact Person - Business Contact
    public PageReference searchBusinessContact(){
        pass_DC_Id = null;
        setConBcon = null; saveSuccess = false;
        if (pass_DV_id == '' || pass_DV_id == null){
            noRefComp = true;
        }
        else{ 
            noRefComp = false;
            bcon.Referral_Company__c = pass_DV_id; //assign selected Referral Company
        }
        system.debug('@@pass_DV_id:'+pass_DV_id);
        system.debug('@@noRefComp:'+noRefComp);
        string srchvar = '%' + pass_DC + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        Bconquery = 'Select Id, Name, FirstName, LastName, MobilePhone, Email, RecordTypeId, RecordType.Name, Referral_Company__c, CreatedDate From Contact Where Name like \'' + srchvar + '\' AND Referral_Company__c = : pass_DV_id AND RecordTypeId = : rt_c_b Order by Name ASC';
        getsearchBConlist();
        setConBCon.setpageNumber(1);
        return null;
    }

    //method for searching Primary Seller Vendor - Individual Account
    public PageReference searchPrivateSellerAccount(){
        if (!withExistingVD) pass_VC_Id = null;
        setConPSellerAcc = null; saveSuccess = false;
        string srchvar = '%' + pass_VC + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        PSellerQuery = 'Select Id, Name, FirstName, LastName, PersonMobilePhone, PersonEmail, RecordTypeId, RecordType.Name, PersonHomePhone, CreatedDate, Address__c, Address__r.Full_Address__c From Account Where Name like \'' + srchvar + '\' AND RecordTypeId =: rt_c_i Order By Name ASC limit 1000'; //: rt_c_ic Order by Name ASC limit 9999';
        getsearchPSellerlist();
        setConPSellerAcc.setpageNumber(1);
        return null;
    }

    public PageReference populateAddresswithLocation(){
        if (addr.Location__c != null){
            //Retrieve selected Location
            list <Location__c> loclist = [Select Id, Suburb_Town__c, Postcode__c, State__c, State_ACR__c, Country__c From Location__c Where Id = : addr.Location__c AND Name != 'UNKNOWN' limit 1];
            if (!loclist.isEmpty()){
                //Populate Address fields with Location
                addr.Suburb__c = loclist[0].Suburb_Town__c;
                addr.Postcode__c = loclist[0].Postcode__c;
                addr.State__c = loclist[0].State__c;
                addr.State_ACR__c = loclist[0].State_ACR__c;
                addr.Country__c = loclist[0].Country__c;
            }
        }
        return null;
    }

    //method for assigning existing Referral Company
    public void assignExistingReferralCompany(){
            pass_DV = exrefcomp.Name;
            pass_DV_id = exrefcomp.Id;
            saveSuccess = true;
            showRefCompCre = false;
            showBConCre = true;
            showDuplicateRC = false;
    }

    //method for saving New Dealer Sale Vendor
    public void saveReferralCompany(){
        Savepoint sp = Database.setSavepoint();
        try{
            system.debug('@@inputABN:'+inputABN);
            if (isCreateABN){
                insert inputABN;
                refcomp.Legal_Entity_Name__c = inputABN.Id;
            }
            else refcomp.Legal_Entity_Name__c = currentABN.Id;

            insert refcomp; 
            //Assign created Referral Company to Sale Dealer Vendor
            pass.Dealer_Sale_Vendor__c = refcomp.Id;
            pass_DV = refcomp.Name;
            pass_DV_id = refcomp.Id;
            refcomp = new Referral_Company__c(); 
            bcon = new Contact(RecordTypeId = rt_c_b);
            if (pass_DV_id != null && pass_DV_id != '') bcon.Referral_Company__c = pass_DV_id; //assign selected Referral Company
            saveSuccess = true;
            showRefCompCre = false;
            showBConCre = true;
            dupeoption = '0';
        }
        catch(Exception e){
            Database.rollback( sp );
            string rname = '';
            if (e.getMessage().contains('DUPLICATES_DETECTED') || e.getMessage().contains('DUPLICATE_VALUE') ){
                system.debug('@@fullmessage:'+e.getMessage());
                if (e.getMessage().contains('Referral Company already exists')){
                    //if (!isCreateABN){
                        exrefcomp = new Referral_Company__c();
                        exrefcomp = [Select Id, Name, Address_Line_1__c, Company_Type__c, State__c, Dealer_Type__c, Company_Website__c, Primary_Contact_s_Name__c, Primary_Contact_s_Mobile__c, Last_Contact_Date__c, Registered_ABN_F__c
                                From Referral_Company__c Where Name = : refcomp.Name limit 1];
                        rname = exrefComp.Name;
                        //Referral Company already exists + RC Name + Click link to select existing Referral Company
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Referral_Company_Msg_1) + rName + String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Referral_Company_Msg_2) ));
                        dupeoption = '1';
                        inputABN = new ABN__c();
                        system.debug('@@duperefcomplevel');
                    //}
                }
                if (e.getMessage().contains('Registered_ABN__c duplicates') || e.getMessage().contains('Legal Entity Name already exists')){
                    list <ABN__c> exABNlist = [Select Id, Name, Registered_ABN__c From ABN__c Where (Registered_ABN__c = : inputABN.Registered_ABN__c OR Name = : inputABN.Name) ];
                    if (exABNlist .size() > 0){ 
                        exABN = exABNlist[0];
                        rname = exABN.Name;
                        isCreateABN = false;
                        inputABN = new ABN__c();
                        //Legal Entity already exists + ABN Name + Click link to select existing Legal Entity
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Legal_Entity_Msg_1) + rName + String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Legal_Entity_Msg_2) ));
                        dupeoption = '2';
                        system.debug('@@dupeABNlevel');
                    }
                }
                showDuplicateRC = true;
            }
            saveSuccess = false;
        }
    }

    //method for saving New Dealer Contact Person
    public void saveBusinessContact(){
        Savepoint sp = Database.setSavepoint();
        try{
            insert bcon; 
            //Assign created Business Contact
            pass.Dealer_Contact_Person__c = bcon.Id;
            string confname = '';
            if (bcon.FirstName != null) confname = bcon.FirstName + ' ';
            pass_DC = confname + bcon.LastName;
            pass_DC_id = bcon.Id;
            //bcon = new Contact();
            saveSuccess = true;
            saveSuccessBcon = true;
            system.debug('@@saveSuccessBCon:'+saveSuccessBCon);
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
            saveSuccessBcon = false;
        }
    }

    private boolean checkDuplicateAddress(){
        boolean dupeAddrfound = false;
        list <Address__c> exdupeAddresses = [Select Id, Name, Location__c, Suburb__c, Street_Name__c, Street_Type__c, Street_Number__c From Address__c Where Location__c =: addr.Location__c];
        if (!exdupeAddresses.isEmpty()){
            for (Address__c exaddr : exdupeAddresses){
                string addrmatchKey = exaddr.Location__c + '-' + exaddr.Street_Name__c + '-' + addr.Street_Number__c + '-' + exaddr.Street_Type__c;
                string enteredAddress = addr.Location__c + '-' + addr.Street_Name__c + '-' + addr.Street_Number__c + '-' + addr.Street_Type__c;
                if (addrmatchKey == enteredAddress){
                    addr.Id = exaddr.Id; //Populate Address ID with found Address
                    dupeAddrfound = true;
                    break;
                }
            }
        }
        return dupeAddrfound;
    }

    //method for saving New Private Seller 
    public void savePrivateSellerAccount(){
        Savepoint sp = Database.setSavepoint();
        try{
            //Populate Account's Address and Location
            if (!checkDuplicateAddress()){
                insert addr;
            }
            iacc.Location__c = addr.Location__c;
            iacc.Address__c = addr.Id;
            iacc.Partition__c = pass.Case_Partition__c;

            iacc.FirstName = icon.FirstName;
            iacc.LAstName = icon.LastName;
            insert iacc; 

            //Assign created Private Seller Account
            pass.Private_Seller__c = iacc.Id;
            string confname = '';
            if (icon.FirstName != null) confname = icon.FirstName + ' ';
            pass_VC = confname + icon.LastName;
            pass_VC_id = iacc.Id;
            saveSuccess = true;
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
        }
    }

    //method for updating Purchased Asset with Vendor/Private Seller Details
    public PageReference savePAsset(){
        Savepoint sp = Database.setSavepoint();
        errmsg = '';
        try{    
            system.debug('@@pass_DV_id:'+pass_DV_id);
            if (pass_DV_id != '') pass.Dealer_Sale_Vendor__c = pass_DV_id; //assign Referral Company ID
            else pass.Dealer_Sale_Vendor__c = null;
            if (pass_DC_id != '') pass.Dealer_Contact_Person__c = pass_DC_id; //assign Business Contact ID
            else pass.Dealer_Contact_Person__c = null;
            if (pass_VC_id != '') pass.Private_Seller__c = pass_VC_id; //assign Individual Contact ID
            else pass.Private_Seller__c = null; 
            if (pass.Sale_Type__c == SALE_TYPE_DEALER){
                //Validate Dealer fields
                if (pass.Dealer_Sale_Vendor__c == null && pass.Dealer_Contact_Person__c == null){ 
                    errmsg = ERR_MSG_1;
                    return null;
                }
                else if (pass.Dealer_Sale_Vendor__c == null && pass.Dealer_Contact_Person__c != null){
                    errmsg = ERR_MSG_2;
                    return null;
                }
                else if (pass.Dealer_Sale_Vendor__c != null && pass.Dealer_Contact_Person__c == null){
                    errmsg = ERR_MSG_3;
                    return null;
                }
            }
            else if (pass.Sale_Type__c == SALE_TYPE_PRIVATE){
                //Validate Private Seller fields
                if (pass.Private_Seller__c == null){
                    errmsg = ERR_MSG_4;
                    return null;
                }
            }
            update pass;
            saveSuccess = true;
            //return new PageReference('/'+ pass.Id);
            return null;
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
            errmsg = e.getMessage();
            return null;
        }
    }



}