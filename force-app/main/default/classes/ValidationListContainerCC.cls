/**
 * @File Name          : ValidationListContainerCC.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 29/07/2019, 10:28:13 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    29/07/2019, 10:06:07 am   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
public class ValidationListContainerCC {

    public string sobjName {get;set;}

    public ValidationListContainerCC(){
        Id recId = ApexPages.currentPage().getParameters().get('id');
        sobjName = recId.getSobjectType().getDescribe().getName();
        if (sobjName.contains('__')) sobjName = sobjName.substringBefore('__');
    }

}