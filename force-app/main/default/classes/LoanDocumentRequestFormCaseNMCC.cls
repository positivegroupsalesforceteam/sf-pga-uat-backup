public class LoanDocumentRequestFormCaseNMCC {

    public Case cse {get;set;}
    public Loan_Document_Request_Form_CT__c loanReq {get;set;}
    public Loan_Document_Request_Form_CT__c cloanReq {get;set;}
    private Id curID;
    public Boolean hasErrors {get;set;}
    public Boolean saveSuccess {get;set;}
    public Boolean showLDR {get;set;}

    public LoanDocumentRequestFormCaseNMCC(){
        curID = Apexpages.currentpage().getparameters().get('id');
        showLDR = hasErrors = saveSuccess = false;
        if (curID != null){
            retrieveCase();
            showLDR = validateLoadLDR();
        }
    }

    private boolean validateLoadLDR(){
        boolean passed = true;
        if (cse.Approval_Conditions__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Please fill out Approval Condition in Case before requesting a Loan Document.'));
            passed = false;
        }
        if (cse.Lender1__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Please select Lender in Case before requesting a Loan Document.'));
            passed = false;
        }
        return passed;
    }

    private boolean validateLDR(){
        boolean passed = true;
        if (loanReq.Notes_to_Lender__c == '' || loanReq.Notes_to_Lender__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please input Notes to Lender'));
            hasErrors = true;
            passed = false;
        }
        if (loanReq.Notes_to_Support__c == '' || loanReq.Notes_to_Support__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please input Notes to Support'));
            hasErrors = true;
            passed = false;
        }
        return passed;
    }

    public pageReference saveLDR(){
        Savepoint sp = Database.setSavepoint();
        try{
            if (validateLDR()){

                //Create Loan Document Request form
                loanReq.Approval_Conditions1__c  = cse.Approval_Conditions__c;
                loanReq.Lender1__c = cse.Lender1__r.Name;
                saveSuccess = true;
                insert loanReq;
                cloanReq = [Select Id, Name From Loan_Document_Request_Form_CT__c Where Id = : loanReq.Id];

                //Create Case Comments base on Notes To Lender and Notest To Support
                List<CaseComment> caseCommentToInsert = new List<CaseComment>();
                caseCommentToInsert.add( new CaseComment(
                                            parentId = cse.Id,
                                            IsPublished = false,
                                            CommentBody = 'Notes To Lender: '+ loanReq.Notes_to_Lender__c
                                        ));
                caseCommentToInsert.add( new CaseComment(
                                            parentId = cse.Id,
                                            IsPublished = false,
                                            CommentBody = 'Notes To Support: '+ loanReq.Notes_to_Support__c
                                        ));
                if (caseCommentToInsert.size() > 0){           
                    Database.insert(caseCommentToInsert);
                }

            }
            else saveSuccess = false;
        }
        catch(Exception e){
            saveSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    private void retrieveCase(){
        string cseQuery = 'Select Id, CaseNumber, RecordTypeId, RecordType.Name, Application_Name__c, Approval_Conditions__c, Lender1__c, Lender1__r.Name, ';
        cseQuery+= 'Application_Name__r.Name, Application_Name__r.RecordTypeId, Application_Name__r.RecordType.Name, ';
        string roleQUery = 'Select Id, Name, RecordTypeId, RecordType.Name, Case__c, Account__c, Account__r.Name, Account__r.FirstName, Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.PersonMobilePhone, Account__r.isPersonAccount, Account__r.ABN__c, Account__r.Full_Address__c , Application__c ';
        roleQUery+= ' From Roles__r Order By Name ASC'; //Related Roles
        roleQUery= '(' + roleQUery + ')';
        cseQuery+= roleQUery + ' From Case ';
        if (curID.getSObjectType() == Schema.Case.SobjectType){ //ID is from Case
            cseQuery+= 'Where Id = : curID limit 1';
        }
        else if (curID.getSObjectType() == Schema.Application__c.SobjectType){ //ID is from Application
            cseQuery+= 'Where Application_Name__c = : curID limit 1';
        }
        cse = Database.query(cseQuery);

        loanReq = new Loan_Document_Request_Form_CT__c(
            Client_Name__c = cse.Id
        );
    }

}