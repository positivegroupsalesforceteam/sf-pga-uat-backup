@isTest
Public class Money3APITest {

    static testMethod void Money3APITest(){
   
   //Create test data here:
    HttpRequest req = new HttpRequest();
    HttpResponse res = new HttpResponse();
    
        Account acc = new Account();
                
       // acc.Name = 'test';
        acc.FirstName = 'Test';
        acc.LastName ='Test';
        acc.recordTypeId = '012900000004Dc6';
        acc.Gender__c = 'Male';
        acc.Date_of_Birth__c=date.today()-2000;
        acc.Identification_Type__c ='Drivers License'; 
        acc.Identification_Number__c = '123';
        acc.Residential_Status__c = 'Boarding';
        acc.BillingStreet = 'Test'; 
        acc.BillingCity ='Test';
        acc.BillingState = 'Western Australia';
        acc.BillingPostalCode = '123';     
        acc.Permanent_Resident_Status__c = 'Yes';
        acc.title__c = 'Mr';
        acc.Application_Type__c = 'test';
        acc.Loan_Reference_Test__c = '124';
        
        insert acc;

        Opportunity opp = new Opportunity(name = 'Test', stageName = 'open',  
            Loan_Type2__c = 'Bike Loan', Loan_Product__c = 'Property', Loan_Amount__c=10000, 
            Bankruptcy__c='No', Length_of_time_in_employment__c='20', 
            Available_Income__c=1000, Income_Frequency__c='monthly',
            closeDate = date.today(), AccountId=acc.Id );
        insert opp;
        
        Opportunity opp2 = new Opportunity(name = 'Test', stageName = 'open', closeDate = date.today(), loan_type2__c = 'Boat Loan');
        insert opp2;
        
        Employee__c emp = new Employee__c (Name = 'Abc', Contact_Phone__c = '1234534', 
        Contact_Person__c = 'Test', Job_Status__c='Full-Time', Job_Title__c = 'Salesman', Employment_Duration__c = '10', 
        Current_Employment__c = 'Yes',  Account__c =acc.id);
        insert emp;
          
        Assets__c asst = new Assets__c (Asset_Type__c = 'Test', Value__c = 1000, Account__c = acc.Id);
        insert asst;
        
        Liability__c lib = new Liability__c(Monthly_Payment__c=100, Account__c =acc.Id);
        insert lib;
         
        Reference__c ref = new Reference__c(Last_Name__c = 'Test', First_Name__c = 'Test2', 
        Relationship__c = 'Friend', Phone__c = '1234', Account__c = acc.Id );
        
        insert ref;
        Applicant_2__c appObj=new Applicant_2__c(First_Name__c='ads', Last_Name__c='qwewq', Title__c='mr', 
            Gender__c='male', Date_of_Birth__c=date.today()-1000, Permanent_Resident_Status__c='yes', 
            Marital_Status__c='single', Account__c=acc.id);
        insert appObj;
        
        Employee__c emp2 = new Employee__c (Name = 'Abc', Contact_Phone__c = '1234534', 
        Contact_Person__c = 'Test', Job_Status__c='Full -Time', Job_Title__c = 'Salesman', Employment_Duration__c = '10', 
        Current_Employment__c = 'Yes',  Applicant2__c =appObj.id);
        
        insert emp2;
        
        test.startTest();
        
        ApexPages.StandardController stdHR = new ApexPages.StandardController(acc);
        Money3API moneyApiCls=new Money3API(stdHR);
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',blob.valueOf('[{"Name": "sForceTest1"}]'), null);
        
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        moneyApiCls.sendToMoney3();
        test.stopTest();
//        moneyApiCls.Reports();
//        moneyApiCls.Upload();
           
    }

}