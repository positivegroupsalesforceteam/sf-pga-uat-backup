/**
 * @File Name          : RoleSMSButtonsController.cls
 * @Description        : 
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : 
 * @Last Modified By   : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Last Modified On   : 25/06/2019, 3:12:59 pm
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 12:56:48 pm   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
**/
public class RoleSMSButtonsController {

    @AuraEnabled
    public static wrappedAllowedSMSPerObject getSMSBUttonSettingForObject(String curObj, String partition){
        //Retrieve allowed SMS buttons for current object base from its Partition
        wrappedAllowedSMSPerObject wASMSpo = new wrappedAllowedSMSPerObject();
        wASMSpo.smsBtnlist = new list <allowedSMS>();
        wASMSpo.newTask = new Task();
        for (SMS_Buttons_Setting__mdt smsbtn : [Select Label, Button_Label__c, SMS_Action_Key__c 
            From SMS_Buttons_Setting__mdt Where Object__c = : curObj AND Partition__c = : partition Order By Label ASC]){
               allowedSMS sms = new allowedSMS();
               sms.smsBtn = smsbtn;
               sms.toDisable = false;
               wASMSpo.smsBtnlist.add(sms);
        }
        return wASMSpo;
    }

    @AuraEnabled
    public static Task createSMSNotification(Task tsk){
        Savepoint sp = Database.setSavepoint();
        try{
            system.debug('@@tsk:'+tsk);
            //insert tsk;
        }
        catch(Exception e){
            Database.rollback( sp );  
            system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
            AuraHandledException ae = new AuraHandledException('Error: '+ e.getLineNumber()+e.getMessage());
            ae.setMessage('Error: '+ e.getLineNumber()+e.getMessage());
            throw ae;
        }
        return tsk;
    }

    public class wrappedAllowedSMSPerObject{
        //@AuraEnabled public list <SMS_Buttons_Setting__mdt> smsBtnlist {get;set;}
        @AuraEnabled public list <allowedSMS> smsBtnlist {get;set;}
        @AuraEnabled public Task newTask {get;set;}
        
    }

    public class allowedSMS{
        @AuraEnabled public SMS_Buttons_Setting__mdt smsBtn {get;set;}
        @AuraEnabled public boolean toDisable {get;set;}
    }

}