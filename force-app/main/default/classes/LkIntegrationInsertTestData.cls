@IsTest
public class LkIntegrationInsertTestData {
    
    public static  Opportunity insertOpportunity(String accId){
      Opportunity opp = new Opportunity();
      opp.recordTypeId = GetRecordTypeId('Opportunity','Property');
      opp.AccountId = accId;
      opp.Name = 'OppTest';
      opp.StageName = 'Qualified';
      opp.Assigned_To__c = 'Rob Murdoch';
      opp.Loan_Product__c = 'Investment';
      opp.Income_Type__c = 'Bonus';
      opp.CloseDate = Date.newInstance(2016, 06, 20);
      insert opp;
      return  opp;
    }
    
    public static  Account insertAccount(){
       Account acc = new Account();
       acc.firstname = 'Saasfocus';
       acc.lastname = 'Account';
       acc.Title__c= 'Mr';   
       acc.RecordTypeId = GetRecordTypeId('Account','Property');
       acc.Current_Address_Type__c = 'Standard';
       acc.Current_Street_Number__c = '12345';
       acc.Current_Suburb__c= 'Current City';
       acc.Post_Settle_Street_Name__c = 'Post Settle Street';
       acc.Post_Settle_Suburb__c= 'Post City';
       acc.Previous_Street_Name__c = 'Prev Street';
       acc.Previous_City__c= 'Previous City';
       acc.Residential_Status__c = 'Living With Parents';
       acc.Marital_Status__pc = 'Married';
       acc.Entity_Type__c = 'Individual';
       insert acc; 
       return acc; 
    }
    
    public static  Applicant_2_Mapping__c insertApplicant2Mapping(Opportunity opp){
      Applicant_2_Mapping__c  applicant2 = new Applicant_2_Mapping__c ();
      applicant2.name = 'Appl2Test';
      applicant2.First_Name__c = 'Michael';
      applicant2.Last_Name__c = 'Clark';
      applicant2.Opportunity__c = opp.id;
      applicant2.Current_Address_Type__c = 'Standard';
      applicant2.Current_Street_Number__c = '12345';
      applicant2.Current_Suburb__c= 'Current City';
      applicant2.Post_Settle_Street_Name__c = 'Post Settle Street';
      applicant2.Post_Settle_Suburb__c= 'Post City';
      applicant2.Previous_Street__c= 'Prev Street';
      applicant2.Previous_Suburb__c= 'Previous City';
      insert applicant2  ;
      return applicant2 ;
    }
    
    public static Applicant_2_Mapping__c insertApplicant2Mapping(Applicant_2__c apl2,Opportunity opp) {
        Applicant_2_Mapping__c  apl2M = new Applicant_2_Mapping__c ();
        apl2M.Applicant_2__c = apl2.id;
        apl2M.opportunity__c = opp.id;
        insert apl2M;
        return apl2M;
    }
    
    public static  Applicant_2__c insertApplicant2(Account acc){
      Applicant_2__c appl2 = new Applicant_2__c();
      appl2.Account__c = acc.id;
      appl2.Title__c='Mr';
      appl2.Name='Robert';
      appl2.Middle_Name__c='J';
      appl2.Last_Name__c='Warren';
      appl2.Entity_Type__c='Individual';
      appl2.Residential_Status__c='With Parents';
      appl2.Current_Address_Type__c='Standard';
      appl2.Current_Street_Name__c='Curr Street';
      appl2.Current_Street_Number__c='123234';
      appl2.Current_Street_Type__c='Road';
      appl2.Current_Suburb__c='Curr City';
      appl2.Current_State__c='Queensland';
      appl2.Current_Country__c='Australia';
      appl2.Previous_Street__c='Prev Street';
      appl2.Previous_Street_Number__c='234222';
      appl2.Previous_Street_Type__c='Avenue';
      appl2.Previous_Suburb__c='Prev City';
      appl2.Previous_State__c='Victoria';
      appl2.Previous_Country__c='Australia';
      appl2.Post_Settle_Address_Type__c='Non-Standard';
      appl2.Post_Settle_Street_Name__c = 'Post street';
      appl2.Post_Settle_Street_Number__c = '232322';
      appl2.Post_Settle_Street_Type__c='Street';
      appl2.Post_Settle_Suburb__c = 'Post city';
      appl2.Post_Settle_Country__c = 'Australia';
      insert appl2;
      return appl2;
    }
    
    public static  Applicant_2__c insertApplicant2(Account acc, String name){
      Applicant_2__c appl2 = new Applicant_2__c();
      appl2.Account__c = acc.id;
      appl2.Title__c='Mr';
      appl2.Name=name;
      appl2.Middle_Name__c='J';
      appl2.Last_Name__c='Warren';
      appl2.Entity_Type__c='Individual';
      appl2.Residential_Status__c='With Parents';
      appl2.Current_Address_Type__c='Standard';
      appl2.Current_Street_Name__c='Curr Street';
      appl2.Current_Street_Number__c='123234';
      appl2.Current_Street_Type__c='Road';
      appl2.Current_Suburb__c='Curr City';
      appl2.Current_State__c='Queensland';
      appl2.Current_Country__c='Australia';
      appl2.Previous_Street__c='Prev Street';
      appl2.Previous_Street_Number__c='234222';
      appl2.Previous_Street_Type__c='Avenue';
      appl2.Previous_Suburb__c='Prev City';
      appl2.Previous_State__c='Victoria';
      appl2.Previous_Country__c='Australia';
      appl2.Post_Settle_Address_Type__c='Non-Standard';
      appl2.Post_Settle_Street_Name__c = 'Post street';
      appl2.Post_Settle_Street_Number__c = '232322';
      appl2.Post_Settle_Street_Type__c='Street';
      appl2.Post_Settle_Suburb__c = 'Post city';
      appl2.Post_Settle_Country__c = 'Australia';
      insert appl2;
      return appl2;
    }
    
    public static  Assets__c insertAsset(Account acc,Opportunity opp,String assetType){
      Assets__c asset = new Assets__c();
      asset.Account__c = acc.id;
      asset.Asset_Type__c = assetType;
      asset.Opportunity__c = opp.id;
      insert asset;
      return asset;
    }
    
    public static Asset_Mapping__c insertAssetMapping(Opportunity opp,String assetType,Assets__c asset){
    
       Asset_Mapping__c assetM  = new Asset_Mapping__c ();
       /*assetM.Name = 'AMTest';
       assetM.Opportunity__c = opp.id;
       assetM.Asset_Type__c = assetType;*/
       if(asset != null)
           assetM.Assets__c = asset.id;
       insert assetM;
       return assetM;
    }
    
    public static Asset_Mapping__c insertAssetMapping(Opportunity opp,String assetType){
    
       Asset_Mapping__c assetM  = new Asset_Mapping__c ();
       assetM.Name = 'AMTest';
       assetM.Opportunity__c = opp.id;
       assetM.Asset_Type__c = assetType;
       insert assetM;
       return assetM;
    }
    
    public static Asset_Mapping__c insertAssetMapping(Assets__c asset,Opportunity opp){
    
       Asset_Mapping__c assetM  = new Asset_Mapping__c ();
       /*assetM.Name = 'AMTest';
       assetM.Opportunity__c = opp.id;
       assetM.Asset_Type__c = assetType;*/
       assetM.Opportunity__c = opp.id;
       assetM.Assets__c = asset.id;
       insert assetM;
       return assetM;
    }
    
    public static Liability__c insertLiability(Account acc, Opportunity opp) {
        Liability__c liab = new Liability__c();
        liab.Account__c = acc.id;
        liab.Opportunity__c = opp.id;
        liab.Financial_Institution__c = 'HSBC';
        insert liab;
        return liab;
    }
    
    public static  Liability_Mapping__c insertLiabilityMapping(Opportunity opp){
       Liability_Mapping__c libM = new Liability_Mapping__c();
//       libM.Name = 'LMTest';
       libM.Opportunity__c = opp.id;
       libM.Type__c = 'Mortgage Loan';
       libM.Mortgage_Loan_For_Map__c = LkintegrationTestFactory.assetRE.id;
       insert libM ;
       return libM;
    }
    
    public static  Liability_Mapping__c insertLiabilityMapping(Liability__c liab,Opportunity opp){
       Liability_Mapping__c libM = new Liability_Mapping__c();
//       libM.Name = 'LMTest';
       libM.Opportunity__c = opp.id;
       libM.Liability__c = liab.id;
       insert libM ;
       return libM;
    }
    
    public static  Employer_Mapping__c insertEmploymentMapping(Opportunity opp){
       Employer_Mapping__c emp = new Employer_Mapping__c();
       emp.Employment_Type__c = 'PAYE';
       emp.Employment_Sector__c = 'Public';
       emp.Job_Status__c = 'Full Time';
       emp.Current_Employment__c= 'Yes';
       Date d = Date.newInstance(2016, 06, 20);
       emp.Date_commenced__c= d ; 
       emp.Date_completed__c= d;
       emp.Name = 'Johanson';
       emp.Opportunity__c = opp.id;
       emp.Employer_address__c = 'A94/9 - Sector  58';
       emp.Income_Type__c = 'Gross Yearly Income';
       emp.Addback_Type__c = 'Depreciation';
       insert emp ;
       return emp;
    }
    
    public static  Employer_Mapping__c insertEmploymentMapping(Employee__c emp, Opportunity opp){
       Employer_Mapping__c empM = new Employer_Mapping__c();
       empM.Employer__c = emp.id;
       empM.Opportunity__c = opp.id;
       insert empM ;
       return empM;
    }
    
    public static Employee__c insertEmployment(Account acc) {
        Employee__c emp = new Employee__c();
        emp.Account__c = acc.id;
        insert emp;
        return emp;
    }
    
    public static  LK_Asset_Liability_Share__c insertALSForLiabilityandAccount(Liability_Mapping__c lib,Account acc, Opportunity opp){
      LK_Asset_Liability_Share__c libL = new LK_Asset_Liability_Share__c();
      libL.Liability_Mapping__c =lib.id;
      libL.Percentage_Share__c= 12;
      libL.Account__c = acc.id ;
      libL.Opportunity__c = opp.id;
      insert libL;
      return libL;
    }
    public static  LK_Asset_Liability_Share__c insertALSForLiabilityandApplicant2(Liability_Mapping__c lib,Applicant_2_Mapping__c appl2, Opportunity opp){
      LK_Asset_Liability_Share__c libL = new LK_Asset_Liability_Share__c();
      libL.Liability_Mapping__c =lib.id;
      libL.Percentage_Share__c= 12;
      libL.Applicant_2_Mapping__c = appl2.id ;
      libL.Opportunity__c = opp.id;
      insert libL;
      return libL;
    }
    
    public static  LK_Asset_Liability_Share__c insertALSForAssetandAccount(Asset_Mapping__c asset,Account acc,Opportunity opp){
      LK_Asset_Liability_Share__c libA = new LK_Asset_Liability_Share__c();
      libA.Asset_Mapping__c =asset.id;
      libA.Percentage_Share__c= 15;
      libA.Account__c = acc.id;
      libA.Opportunity__c = opp.id;
      insert libA;
      return libA;
    }
    public static  LK_Asset_Liability_Share__c insertALSForAssetandApplicant2(Asset_Mapping__c asset,Applicant_2_Mapping__c appl2,Opportunity opp){
      LK_Asset_Liability_Share__c libA = new LK_Asset_Liability_Share__c();
      libA.Asset_Mapping__c =asset.id;
      libA.Percentage_Share__c= 15;
      libA.Applicant_2_Mapping__c = appl2.id;
      libA.Opportunity__c = opp.id;
      insert libA;
      return libA;
    }

    public static id GetRecordTypeId(string objectAPIName, string recordTypeName){
        system.debug('====Record Type Name====='+recordTypeName);
        Map<String, Schema.SObjectType> sobjectSchemaMap;
        if(IsNullOrEmptyString(objectAPIName) || IsNullOrEmptyString(recordTypeName)){
            return null;
        }
        if(sobjectSchemaMap == null)
        {
            sobjectSchemaMap = Schema.getGlobalDescribe();
        }
        Schema.SObjectType sObjType = sobjectSchemaMap.get(objectAPIName) ;
        
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id recordTypeId = RecordTypeInfo.get(recordTypeName).getRecordTypeId();
        return recordTypeId;
    }
    public static boolean IsNullOrEmptyString(String name) {
        if(name == null || name.length() == 0)
            return true;
        return false;
    }
}