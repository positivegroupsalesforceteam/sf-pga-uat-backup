public class Connective_ManualFlowController {
    
   public Connective_ManualFlowController(Apexpages.StandardController std){
        
      
     }
    
     public Connective_ManualFlowController(Apexpages.StandardSetController setstd){
        
      
     }
    
    @RemoteAction
    public static  Map<string,List<string>> doinitial(){
        
       
        Map<string,List<string>> responsemap = new Map<string,List<string>>();
        
       /* Schema.DescribeFieldResult casetype =  Case.Type.getDescribe(); //Change Type field to change options on UI and forms
        List<Schema.PicklistEntry> casetypevals = casetype.getPicklistValues();
        responsemap.put('Type', new List<string>());
        for(Schema.PicklistEntry val : casetypevals){
            responsemap.get('Type').add(val.getValue());
        }*/
        
       responsemap.put('AccountRecType', new List<string>());
        for(RecordType rectype : [select id,Name from Recordtype where SobjectType='Account']){
            responsemap.get('AccountRecType').add(rectype.Name +','+rectype.Id);
            
        }
       responsemap.put('ContactRecType', new List<string>());
        for(RecordType rectype : [select id,Name from Recordtype where SobjectType='Contact']){
            responsemap.get('ContactRecType').add(rectype.Name +','+rectype.Id);
            
        }
        responsemap.put('CaseRecType', new List<string>());
        for(RecordType rectype : [select id,Name from Recordtype where SobjectType='Case']){
            responsemap.get('CaseRecType').add(rectype.Name +','+rectype.Id);
            
        }
        
        Schema.DescribeFieldResult caseApptype =  Case.Application_Type__c.getDescribe(); //Change Type field to change options on UI and forms
        List<Schema.PicklistEntry> caseApptypevals = caseApptype.getPicklistValues();
        responsemap.put('AppType', new List<string>());
        for(Schema.PicklistEntry val : caseApptypevals){
            responsemap.get('AppType').add(val.getValue());
        }
        
        Schema.DescribeFieldResult loanproducttype =  Case.Loan_Product__c.getDescribe(); //Change Type field to change options on UI and forms
        List<Schema.PicklistEntry> loanproducttypevals = loanproducttype.getPicklistValues();
        responsemap.put('loanproducttype', new List<string>());
        for(Schema.PicklistEntry val : loanproducttypevals){
            responsemap.get('loanproducttype').add(val.getValue());
        }
        
       try{
           
           Referral_Company__c ref = new Referral_Company__c();
           ref = [select id from Referral_Company__c where Name = 'Connective' limit 1];
           responsemap.put('Connective',new List<string>());
           responsemap.get('Connective').add(ref.id);
           
       }catch(system.exception ex){
           
       }
        
        return responsemap;
    }
    
    

}