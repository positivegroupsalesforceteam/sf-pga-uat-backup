/*
    @author: Jesfer Baculod - Positive Group
    @history: 07/13/17 - Created
              07/21/17 - Updated
              07/28/17 - Added batchable update option
              01/16/18 - Added rule for Deal Value base on its Loan Type
    @description: class for updating Opportunity Deal Value on Individual Settled Opportunities which will be used on Insurance Report
*/
global class updateOpportunityDealValues implements Database.Batchable<sObject> {
	
	//to do, update triggers/workflows/validations with a setting for enabling/disabling rules ensuring that no rules (marketing cloud, etc) will run when doing mass update
	//change update to Opportunity instead on Commissions

	global String query;
	global set <Id> ownerIDs;
	global Date dteFrom;
	global Date dteTo;
	global Boolean partialUpdate;
	global Date currentDate = System.today();

	final string LENDER_ANZ = 'ANZ';
	final string LENDER_MM3 = 'Micro Money 3';
	final string LENDER_F1E = 'Finance 1 Economy';
	final string LOAN_TYPE_BIKE = Label.Loan_Type_Bike_Loan;
	final string LOAN_TYPE_CARAVAN = Label.Loan_Type_Caravan_Loan;

	global Database.QueryLocator start(Database.BatchableContext bc) {
		query = 'Select Id, Name From User'; // Where IsActive = true';
		if (ownerIDs != null)
		query+= ' Where Id in : ownerIDs';
		return Database.getQueryLocator(query);
    }

    //execute method for batch
    global void execute(Database.BatchableContext bc, List<User> scope) {
		executeUpdate(scope, ownerIDs, dteFrom, dteTo, partialUpdate);
    }

    global void finish(Database.BatchableContext bc) {

    }


	//Class constructor (Default) - For One Time Complete Mass Update
	global updateOpportunityDealValues(){ 
		//query = 'Select Id, Name From User'; // Where IsActive = true';
		this.ownerIDs = null;
		this.dteFrom = null;
		this.dteTo = null;
		this.partialUpdate = false;
		//executeUpdate(database.query(query),null,null,null,false);
	}

	//Constructor with Options #1 - For Partial Mass Update
	/* @param currentDate - Current Date - used for filtering Settled Opportunities on given month, accepts YYYY-MM-DD
	   @param partialUpdate - Used for Setting Partial Updates on Mass Update, accepts true or false (allows partial)
	*/
	global updateOpportunityDealValues(Date currentDate, boolean partialUpdate){
		//query = 'Select Id, Name From User'; // Where IsActive = true';
		this.dteFrom = Date.newInstance(currentDate.year(),currentDate.month(),1);
		integer numOfDaysInMonth = Date.daysInMonth(currentDate.year(), currentDate.month());
		this.dteTo = Date.newInstance(currentDate.year(),currentDate.month(),numOfDaysInMonth);
		this.partialUpdate = partialUpdate;
		//executeUpdate(database.query(query), null, dteFrom, dteTo, partialUpdate);		
	}

	//Constructor with Options #2 - For Partial Mass Update
	/*  @param ownerIDsSet - Set of Opportunity Owners - used for filtering Settled Opportunities, accepts User SF Ids
		@param currentDate - Current Date - used for filtering Settled Opportunities on given month, accepts YYYY-MM-DD
	*/
	global updateOpportunityDealValues(set <Id> ownerIDsSet, Date currentDate){
		//query = 'Select Id, Name From User Where Id in : ownerIDSet'; // AND IsActive = true ';
		this.ownerIds = ownerIDsSet;
		this.dteFrom = Date.newInstance(currentDate.year(),currentDate.month(),1);
		integer numOfDaysInMonth = Date.daysInMonth(currentDate.year(), currentDate.month());
		this.dteTo = Date.newInstance(currentDate.year(),currentDate.month(),numOfDaysInMonth);
		this.partialUpdate = true;
		//executeUpdate(null, ownerIDsSet, dteFrom, dteTo, true); //do not inforce partial success on Update		
	}

	//Constructor with Options #3 - For Partial Mass Update
	/* @param dteFrom - From Settled/Close Date - used for filtering Settled Opportunities, accepts YYYY-MM-DD
	   @param dteTo - To Settled/Close Date - used for filtering Settled Opportunities, accepts YYYY-MM-DD
	   @param partialUpdate - Used for Setting Partial Updates on Mass Update, accepts true or false (allows partial)
	*/
	global updateOpportunityDealValues(Date dteFrom, Date dteTo, boolean partialUpdate){
		//query = 'Select Id, Name From User'; // Where IsActive = true';
		this.dteFrom = dteFrom;
		this.dteTo = dteTo;
		this.partialUpdate = partialUpdate;
		//executeUpdate(database.query(query), null, dteFrom, dteTo, partialUpdate);		
	}

   	//Main method for doing mass update on Opportunities
   	/* @param scope - list of users 
   	   @param ownerIDs - set of Opportunity Owner Ids
   	   @param dteFrom - From Settled/Close Date - used for filtering Settled Opportunities, accepts YYYY-MM-DD
	   @param dteTo - To Settled/Close Date - used for filtering Settled Opportunities, accepts YYYY-MM-DD
	   @param partialUpdate - Used for Setting Partial Updates on Mass Update, accepts true or false (allows partial)
   	*/
   	global void executeUpdate(list <User> scope, set <ID> ownerIDs, Date dteFrom, Date dteTo, boolean partialUpdate) {

   		//Group Commissions by Opportunity-Owner-Month
		map <string,list<Commission__c>> ownerOppCommMap = new map <string,list<Commission__c>>(); //Group Commissions by Owner - Key: Opp Owner ID
		map <string,list<Opportunity>> monthOwnerOppMap = new map <string,list<Opportunity>>(); //Group Opportunities by Owner by Month - Key: opp.OwnerId + '-' + opp.CloseDate.year() + opp.CloseDate.month()
		map <string,decimal> totalProductsSoldMap = new map <string,decimal>(); //Total Products Sold on Opportunities by Owner by Month - Key: opp.OwnerId + '-' + opp.CloseDate.year() + opp.CloseDate.month()
		set <ID> ownerIdSet = new set <ID>(); //set of Owner IDs
		set <string> ownerMonthYearKeySet = new set <string>(); //set of combination of Owner x CloseDate Year x Created Date Month

		if (scope != null){
			for (User usr : scope){
	        	 ownerIDSet.add(usr.Id);
	      	}
      	}
		if (ownerIds != null){ //Set of IDs mostly used on for trigger @future
			ownerIDSet.addAll(ownerIDs);
		}

      	string oppquery = 'Select Id, Name, CreatedDate, CloseDate, Lender__c, OwnerId, Loan_Type2__c, X_of_Products_Sold__c, (Select Id, Name, Product__c, Institution__c, Opportunity_Settled__c, Opportunity__r.OwnerId, Opportunity__r.CloseDate, Opportunity__r.CreatedDate, Opportunity__r.Lender__c From Commissions1__r) From Opportunity Where RecordType.Name = \'Individual\' AND StageName =\'Settled\' AND OwnerId in : ownerIDSet ';
      	if (dteFrom != null && dteTo != null){ 
      		oppquery += ' AND (CloseDate >= :dteFrom AND CloseDate <= : dteTo)';
      	}

      	list <Opportunity> opplist = database.query(oppQuery);
      	system.debug('@@opplist:'+opplist.size()+opplist);

		for (Opportunity opp : opplist){
			ownerIdSet.add(opp.OwnerId);
			system.debug('@@opp.Name:'+opp.Name);
			for (Commission__c comm : opp.Commissions1__r){
				//Group Commissions by Owner
				if(!ownerOppCommMap.containskey(opp.OwnerId)) ownerOppCommMap.put(opp.OwnerId, new list<Commission__c>());
   				ownerOppCommMap.get(opp.OwnerId).add(comm);	
			}
			//Identify Total # of Insurances Sold of Owner within a month
			string oppOwnerDateYearMonthKey = opp.OwnerId + '-' + opp.CloseDate.year() + opp.CloseDate.month();
			ownerMonthYearKeySet.add(oppOwnerDateYearMonthKey);
			system.debug('@@oppOwnerDateYearMonthKey:'+oppOwnerDateYearMonthKey);
			if (!totalProductsSoldMap.containskey(oppOwnerDateYearMonthKey)) totalProductsSoldMap.put(oppOwnerDateYearMonthKey, 0);
   			totalProductsSoldMap.put(oppOwnerDateYearMonthKey, totalProductsSoldMap.get(oppOwnerDateYearMonthKey) + opp.X_of_Products_Sold__c); //Set the Total # of Insurances Sold of a Broker within a month
   			if (!monthOwnerOppMap.containskey(oppOwnerDateYearMonthKey)) monthOwnerOppMap.put(oppOwnerDateYearMonthKey, new list <Opportunity>());
   			monthOwnerOppMap.get(oppOwnerDateYearMonthKey).add(opp);
		}

   		//Selling Rule:
   		//ANZ - 2 out of 5: 40%
   		//Micro Money 3 - 3 out of 5: 60%
   		//Finance 1 Economy- 3 out of 5: 60%

   		//Update Opportunity Deal Values on Commissions
   		list <Opportunity> upOppList = new list <Opportunity>();
   		for (string key : ownerMonthYearKeySet){
   			if (monthOwnerOppMap.containskey(key)){
   				system.debug('@@monthOwnerOppMap.get(key):'+monthOwnerOppMap.get(key).size()+monthOwnerOppMap.get(key));
   				for (Opportunity opp : monthOwnerOppMap.get(key)){
   					if (opp.Lender__c == LENDER_ANZ){
   						if (totalProductsSoldMap.get(key) > 2) opp.Deal_Value__c = 0.4; //Set Deal Value to 0.4 due to ANZ Selling Rule
	   					else opp.Deal_Value__c = 1; //Normal Deal
   					}
   					else if (opp.Lender__c == LENDER_MM3 || opp.Lender__c == LENDER_F1E){
	   					if (totalProductsSoldMap.get(key) > 3) opp.Deal_Value__c = 0.6; //Set Deal Value to 0.6 due to Micro Money 3/Finance 1 Economy Selling Rule
	   					else opp.Deal_Value__c = 1; //Normal Deal	
	   				}
	   				else opp.Deal_Value__c = 1; //Normal Deal for other lenders
	   				if (opp.Loan_Type2__c == LOAN_TYPE_BIKE || opp.Loan_Type2__c == LOAN_TYPE_CARAVAN) opp.Deal_Value__c = 0.8; //Set Deal Value to 0.8 due to Loan Type being a Bike / Caravan Loan
	   				upOppList.add(opp);
   				}
   			}
   		}

   		if (upOppList.size() > 0){
   			database.update(upOppList,partialUpdate); //Update Opportunity Deal Values on Commissions, allows partial success
   		}

   		system.debug('@@upOppList:'+upOppList.size()+upOppList);

   		system.debug('@@ownerMonthYearKeySet:'+ownerMonthYearKeySet.size()+' '+ownerMonthYearKeySet);

   		system.debug('@@monthOwnerOppMap:'+monthOwnerOppMap.size()+' '+monthOwnerOppMap);
   		system.debug('@@totalProductsSoldMap.size:'+totalProductsSoldMap.size());
   		system.debug('@@totalProductsSoldMap:'+totalProductsSoldMap);

	}

	
}