/*
	@Description: Utility Class for functions related to integrating TrustPilot (www.trustpilot.com)
	@Author: Jesfer Baculod - Positive Group
	@History:
		11/2/2017	-	Created
*/
public class TrustPilot_Util {
	
	static TrustPilot_Settings__c tpSet = TrustPilot_Settings__c.getInstance(UserInfo.getProfileId());

	//method for generating Trust Pilot unique Link which will be pulled and used in Marketing Cloud for customer's reviews
	public static string generateTrustPilotLink(string refnum, string cemail, string cname){
		string uniquelink, cemailb64, cnameEncoded, linkhash;
		blob cemailblob = Blob.valueOf(cemail);
		cemailb64 = EncodingUtil.base64Encode(cemailblob);
		cemailb64 = EncodingUtil.urlEncode(cemailb64,'UTF-8');
		system.debug('@@cemailb64: '+cemailb64);
		cnameEncoded = EncodingUtil.urlEncode(cname, 'UTF-8'); //encode spaces as '+' in Apex instead of '%20'
		system.debug('@@cnameEncode-pre: '+cnameEncoded);
		cnameEncoded = cnameEncoded.replace('+','%20');
		system.debug('@@cnameEncode-post: '+cnameEncoded);

		linkhash = tpSet.TrustPilot_Secret_Key__c + cemail + refnum; //secretkey + customer email + reference number; 'mykey12email@email.com1234' - sample input
		system.debug('@@linkhash-pre:'+linkhash);
		blob linkhashBlob = Blob.valueof(linkhash);
		linkhash = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1',linkhashBlob)); //b232dc87ad2a69963720a29f5b3db2df6f80dfd - expected result from sample
		//linkhash = EncodingUtil.urlEncode(linkhash,'UTF-8');
		system.debug('@@linkhash-post:'+linkhash);

		uniquelink = tpSet.TrustPilot_Site__c + '/evaluate/' + tpSet.TrustPilot_Domain_Name__c + '?a=' + refnum + '&b=' + cemailb64 + '&c=' + cnameEncoded + '&e=' + linkhash;
		system.debug('@@uniquelink:'+uniquelink);
		return uniquelink;
	}


}