/** 
* @FileName: NewPartnerControllerTest
* @Description: Test Class for New Partner Lightning Apex controller
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 12/06/19 RDAVID Created Class
* 1.1 14/09/19 JBACULOD
**/ 
@isTest
public class NewPartnerControllerTest {
    @testSetup static void setupData() {

        //Create test data for Nodifi Partner Lead
        Lead nodlead = new Lead(
            LastName = 'nodleadtestL',
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Nodifi Partner').getRecordTypeId(),
            Status = 'New',
            Stage__c = 'Open'
        );
        insert nodlead;

    }

    static testmethod void testNewPartner(){
        Test.startTest();
        NewPartnerController.getNewPartnerValidRTMap();
        NewPartnerController.getForm(null, 'Partner__c', 'Dealership Group', null);
        List<Address__c> addressList = NewPartnerController.getAddress('ADL');
        System.assertEquals(addressList.size(),0);
        Test.stopTest();
    }

    static testmethod void testLeadtoPartner(){
        //Retrieve test data for Lead
        Lead nodlead = [Select Id, Status, Stage__c From Lead];
        
        //Create test notes for Lead
        ContentVersion lnote = new ContentVersion(
            Title = 'Test Note for Lead'
        );
        Blob bodyBlob = Blob.valueof('Test Note Body');
		string tempbody =  EncodingUtil.base64Encode(bodyBlob);
        lnote.PathonClient = note.Title + '.snote';
        lnote.VersionData = EncodingUtil.base64Decode(tempbody);
        insert lnote;
        ContentDocumentLink cdl = new ContentDocumentLink(
                ContentDocumentId = [Select ContentDocumentId From ContentVersion Where Id = : lnote.Id].ContentDocumentId,
                LinkedEntityId = nodlead.Id,
                ShareType = 'I',
                Visibility = 'AllUsers'
            );
        insert cdl;

        //Create test Partner for testing Lead to Partner
        Partner__c partner = new Partner__c(
            Name = 'Test LeadtoPartner',
            RecordTypeId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId()
        );
        insert partner;

        Test.startTest();
            NewPartnerController.getNodifiPartnerLead(nodlead.Id);
            NewPartnerController.updateNodifiPartnerLeadToWon(nodlead, partner.Id);
        Test.stopTest();

        //Verify Lead notes got copied to Partner
        //list <ContentDocumentLink> cdlinks = [Select Id, LinkedEntityId From ContentDocumentLink Where Id = : partner.Id];
        //system.assert(cdlinks.size() > 0);

    }
}