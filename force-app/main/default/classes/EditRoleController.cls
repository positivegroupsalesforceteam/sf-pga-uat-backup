public class EditRoleController {
    public Case cse {get;set;}
    public string rowId{get;set;}

    public ID curID {get;set;}
    public boolean hasErrors {get;set;}
    public List<roleWrap> roleWrapList {get;set;}
    public List<roleWrap> roleWrapListClone {get;set;}
    public MAp<Id,roleWrap> roleMapWrapList {get;set;}
    public List<SelectOption> personRoleRTMap {get;set;}
    public List<SelectOption> businessRoleRTMap {get;set;}
    public boolean validForSaving {get;set;}
    public boolean successSave {get;set;}
    
    
    public EditRoleController(){
        curID = Apexpages.currentpage().getparameters().get('id');
        roleWrapList = new List<roleWrap>();
        roleMapWrapList = new Map<Id,roleWrap>(); 
        roleWrapListClone = new List<roleWrap>();
        personRoleRTMap = new List<SelectOption>();
        businessRoleRTMap = new List<SelectOption>();
        hasErrors = successSave = false;
        validForSaving = true;
        retrieveCase();
        getRTPicklistMap();
    }

    private void getRTPicklistMap(){
        for (List<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Role__c' AND IsActive = TRUE ORDER BY name]) {
            for (RecordType rt : rts) {
                if(rt.Name.containsIgnoreCase('- Company') || rt.Name.containsIgnoreCase('- Partnership') || rt.Name.containsIgnoreCase('- Trust') || rt.Name.containsIgnoreCase('- Business') ){
                    businessRoleRTMap.add(new SelectOption(rt.ID, rt.Name));
                }
                else if (rt.Name.containsIgnoreCase('- Individual') || rt.Name.containsIgnoreCase('- Sole Trader') || rt.Name.containsIgnoreCase('Director') || rt.Name.containsIgnoreCase('Spouse')
                || rt.Name.containsIgnoreCase('Purchaser -') || rt.Name.containsIgnoreCase('Lead - Green')){
                    personRoleRTMap.add(new SelectOption(rt.ID, rt.Name));
                }
            } 
        }
    }

    private void retrieveCase(){
        string cseQuery = 'Select Id, CaseNumber, RecordTypeId, RecordType.Name, Application_Name__c, ';
        cseQuery+= 'Application_Name__r.Name, Application_Name__r.RecordTypeId, Application_Name__r.RecordType.Name, ';
        string roleQUery = 'Select Id, Name, RecordTypeId, Role_Type__c, RecordType.Name, Case__c, Account__c, Account__r.Name, Account__r.FirstName, Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.PersonMobilePhone, Account__r.isPersonAccount, Account__r.ABN__c, Account__r.ACN__c, Account__r.Full_Address__c , Application__c ';
        roleQUery+= ' From Roles__r Order By Name ASC'; //Related Roles
        roleQUery= '(' + roleQUery + ')';
        cseQuery+= roleQUery + ' From Case ';

        if (curID.getSObjectType() == Schema.Case.SobjectType){ //ID is from Case
            cseQuery+= 'Where Id = : curID limit 1';
        }
        else if (curID.getSObjectType() == Schema.Application__c.SobjectType){ //ID is from Application
            cseQuery+= 'Where Application_Name__c = : curID limit 1';
        }
        cse = Database.query(cseQuery);

        if(cse.Roles__r.size() > 0){
            for(Role__c role : cse.Roles__r){
                roleWrap rwrap = new roleWrap(role);
                if(role.Account__r.isPersonAccount){
                    rwrap.validRTs = personRoleRTMap;
                } 
                else if(!role.Account__r.isPersonAccount){
                    rwrap.validRTs = businessRoleRTMap;
                } 
                roleWrapList.add(rwrap);
                roleMapWrapList.put(role.Id,rwrap);
            }
        } 
    }

    public pageReference saveRole(){
        Savepoint sp = Database.setSavepoint();
        System.debug('@@@roleWrapList - '+roleWrapList);
        List<Role__c> roleUpdateList = new List<Role__c>();
        for(roleWrap rwrap : roleWrapList){
            roleUpdateList.add(rwrap.role);
        }
        try{
            Database.update(roleUpdateList);
            successSave = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Changes successfuly saved.'));
        }
        catch(Exception e){
            successSave = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Error in saving changes.'));
        }
        return null;
    }
    public pageReference cancel(){
        Savepoint sp = Database.setSavepoint();
        return null;
    }

    public pageReference setRowValues(){
        roleMapWrapList.get(rowId).role.Role_Type__c = null;
        return null;
    }

    public class roleWrap {
        public Role__c role {get;set;}
        public List<SelectOption> validRTs {get;set;}
        public roleWrap(Role__c r){
            role = r;
            validRTs = new List<SelectOption>();
        }
    }
}