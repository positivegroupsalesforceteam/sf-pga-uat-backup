/** 
* @FileName: IncomeHandler
* @Description: Trigger Handler for the Income1__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/15 RDAVID Created Class
* 1.1 2/16 JBACULOD Added Method Trigger toggle for object
* 1.2 4/18/2018 RDAVID [SD-21] Modified Classes for Rollup Mapping Updates
* 2.0 4/25/18 JBACULOD Removed existing triggers, added AutoCreateFOShare
* 2.1 5/19/18 JBACULOD Added method for updating Role Employment Status
* 2.2 6/25/2018 RDAVID Updated Class - SD-103 SF - NM - Continuous Algorithm: Added Logic for Role 12 Month Address and Employment
* 2.3 6/26/18 JBACULOD Added deleteFOShareOnFODelete
* 2.4 2/08/18 RDAVID Added Logic to Update Sole Trader fields in Role when Self-Employed Income is created/updated.
* 2.5 11/7/18 JBACULOD Updated Role Update from Income
* 2.6 11/03/19 JBACULOD - Added retry on final due to UNABLE_TO_LOCK_ROW issue
* 2.7 18/07/19 RDAVID extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
* 2.8 30/09/19 RDAVID extra/task26227377-GlobalPicklist-IncomeVerificationMethod : Added logic to propagate Income1__c.Income_Verification_Method__c to Role__c.Income_Verification_Method__c when Income1__c.Income_Situation__c = 'Current'
* 2.9 2/10/19 RDAVID extra/task26264691-IncomeTriggerIssue : Fixed condition in updating Role 
* 3.0 4/10/19  RDAVID extra/task26245769 extra/task26161988 - Added logic to auto populate Lead_Income_Type__c, Lead_Employment_Situation__c and Lead_Employment_Type__c
**/ 

public without sharing class IncomeHandler implements ITrigger {	

	private Set<Id> roleIds = new Set<Id>(); //Parent Role Ids
	//SD-82 SF - NM - Auto Create FO Share in Income
	private Map <string, FO_Share__c> FOShareToCreateMap = new map <string, FO_Share__c>();
	//SD-XX
	private Map <string, FO_Share__c> FOShareToDeleteMap = new map <string, FO_Share__c>();
	private List<Role__c> roleToUpdateList = new List<Role__c>();
	private Map<Id,Role__c> roleToUpdateMap = new Map<Id,Role__c>();
	//Update Role Employment Job Status
	private map <ID, string> empjobStatusMap = new map <Id, string>(); //key - Role, value - Employment Job Status

	public static List<Role__c> roleList = new List<Role__c>();
	public static Map<String,Schema.RecordTypeInfo> incomeRTByName = Schema.SObjectType.Income1__c.getRecordTypeInfosByName();
	
	// Dont include Business,Company,Investment,Spouse
	//public Set<String> notValidRTsString = new Set<String>{'Business Income','Company Director','Investment Income','Spousal Income (non-borrowing)'};

	// private map <Id, Id> incAccMap = new map <Id, Id>(); //income account map

	private list <Account> accToUpdateList = new list <Account>();
	// Constructor
	public IncomeHandler(){

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
		//Delete FO Share on FO Delete
		if(Trigger.isDelete){
			if (TriggerFactory.trigSet.Enable_FO_Delete_FO_Share_on_FO_Delete__c){
					system.debug('@@test');
					FOShareToDeleteMap = RollupUtility.deleteFOShareOnFODelete('Income1__c', Trigger.old);
			}
		}
		else{
			//2.7 18/07/19 RDAVID extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
			IncomeGateway.setApplicationLookup(trigger.new,incomeRTByName);
			
			if(TriggerFactory.trigSet.Enable_Income_BA_Automation__c){
				IncomeGateway.setBusinessAccount(trigger.new,incomeRTBYName);
			}
			
		}
	}
	
	public void bulkAfter(){
		ContinuousUtility contUtil = new ContinuousUtility();
		if(Trigger.IsInsert || Trigger.isUpdate){
			//Get Parent Role Ids
			roleIds = IncomeGateway.getRoleIds(Trigger.new);
			//Auto Create FO Share
			/*if(Trigger.isInsert || Trigger.isUpdate){
                roleToUpdateList = IncomeGateway.tick12MonthsIncome(trigger.new);
                System.debug('roleToUpdateList ->>>>>>> '+ roleToUpdateList);
        	} */
			if (TriggerFactory.trigSet.Enable_FO_Auto_Create_FO_Share__c){
				if (Trigger.IsInsert){
					FOShareToCreateMap = RollupUtility.autocreateFOShare('Income1__c', Trigger.new , true);
				}
				if(Trigger.isUpdate){
					FOShareToCreateMap = RollupUtility.autocreateFOShare('Income1__c', Trigger.new, false);
					//Delete FO Share on removed Roles
					FOShareToDeleteMap = RollupUtility.deleteFOSHareOnRemovedRoles('Income1__c', Trigger.new);
                    
                    //FOSharetoDeleteMap = RollupUtility.deleteFOShareOnFOUpdate('Income1__c', Trigger.new);
				}
 			}

			//Update Role Employment Job Status from PAYG/Self-Employed Current Income
			/*if (TriggerFactory.trigSet.Enable_Income_Parent_Role_Updates__c){
				empjobStatusMap = IncomeGateway.updateRoleEmploymentStatus(Trigger.new);
			} */
		}

		if(Trigger.IsDelete){
			roleIds = IncomeGateway.getRoleIds(Trigger.old);
		}
		
		if(roleList.size() == 0 && roleIDs.size() > 0){
			roleList = [SELECT Id,Lead_12_Months_Income__c,RecordType.Name,Account__c,Business_Total_Revenue__c, Business_Total_Depreciation__c, Reporting_Period__c, Profit_Before_Tax_Annual__c,Business_Income_Verification_Method__c,
						(SELECT Id, Start_Date__c,End_Date__c,Lead_Employment_Situation__c, Revenue__c, Income_Verification_Method__c, Employment_Job_Status__c, Income_Situation__c,Income_Type__c,Business_Name__c,Self_Employed_Date_Registered_ABN__c,ABN_manual__c,Self_Employed_GST_Registered__c,Nature_of_Business__c,Self_Employed_Date_Registered_GST__c,Depreciation__c, Reporting_Period__c, Profit_Before_Tax_Annual__c FROM Incomes1__r) 
						FROM Role__c WHERE Id IN: roleIDs];
		}

		System.debug('roleList size ===========   '+roleList.size());
		for(Role__c role : roleList){
			Boolean updateRole12Months = false;
			Boolean addRoleToMapForUpdate = false;
			Role__c roleToUpdateInstance = new Role__c(Id = role.Id);	
			if (TriggerFactory.trigSet.Enable_Income_Parent_Role_Updates__c){	
				if(role.Incomes1__r != NULL && role.Incomes1__r.size() > 0 && role.Account__c != NULL) {
					updateRole12Months = contUtil.isExistingIncomeValid(role.Incomes1__r);
					if(role.Lead_12_Months_Income__c != updateRole12Months){
						roleToUpdateInstance.Lead_12_Months_Income__c = updateRole12Months;
						addRoleToMapForUpdate = true;
					}
					for(Income1__c inc : role.Incomes1__r){
						Boolean addRoleToMapForUpdateInner = false;
						if(inc.Income_Situation__c == CommonConstants.INCOME_IS_CI && !String.isBlank(inc.Employment_Job_Status__c)){
							roleToUpdateInstance.Employment_Status__c = inc.Employment_Job_Status__c; //2.8 30/09/19 RDAVID extra/task26227377 - Assume there is only one Current Income per Role, update Role if the Income is Current Income
							addRoleToMapForUpdateInner = true;
						} 
						if(role.RecordType.Name == CommonConstants.ROLE_RT_APP_SOLE){ // If Role is Sole Trader
							if(inc.Income_Type__c == CommonConstants.INCOME_RT_SE){ // If Income is Self Employed
								if (inc.Income_Situation__c == CommonConstants.INCOME_IS_CI){ // If Income is Current Income
									roleToUpdateInstance.Registered_Business_Name1__c = inc.Business_Name__c;
									roleToUpdateInstance.Date_Registered_ABN1__c = inc.Self_Employed_Date_Registered_ABN__c;
									roleToUpdateInstance.Business_ABN1__c = inc.ABN_manual__c;
									roleToUpdateInstance.GST_Registered1__c= inc.Self_Employed_GST_Registered__c;
									roleToUpdateInstance.Nature_of_Business1__c = inc.Nature_of_Business__c;
									roleToUpdateInstance.Date_Registered_GST1__c = inc.Self_Employed_Date_Registered_GST__c;
									roleToUpdateInstance.Business_Income_Verification_Method__c = inc.Income_Verification_Method__c;
									roleToUpdateInstance.Business_Total_Revenue__c = inc.Revenue__c;
									roleToUpdateInstance.Business_Total_Depreciation__c = inc.Depreciation__c;
									roleToUpdateInstance.Reporting_Period__c = inc.Reporting_Period__c;
									roleToUpdateInstance.Profit_Before_Tax_Annual__c = inc.Profit_Before_Tax_Annual__c;
									addRoleToMapForUpdateInner = true;
								}
							}
							else if (inc.Income_Type__c == CommonConstants.INCOME_RT_BI){ // If Income is Business Income
								roleToUpdateInstance.Business_Income_Verification_Method__c = inc.Income_Verification_Method__c;
								roleToUpdateInstance.Business_Total_Revenue__c = inc.Revenue__c;
								roleToUpdateInstance.Business_Total_Depreciation__c = inc.Depreciation__c;
								roleToUpdateInstance.Reporting_Period__c = inc.Reporting_Period__c;
								roleToUpdateInstance.Profit_Before_Tax_Annual__c = inc.Profit_Before_Tax_Annual__c;
								addRoleToMapForUpdateInner = true;
							}
						}
						// If Role RT is either 'Applicant - Company' , 'Trust', 'Applicant Partnership' AND Income_Situation__c = 'Current Income' OR Income_Situation__c = 'Business Income'
						else if ( role.RecordType.Name == CommonConstants.ROLE_RT_APP_COMP || String.valueof(role.RecordType.Name).contains('- Company') || String.valueof(role.RecordType.Name).contains('- Trust')  || role.RecordType.Name == 'Applicant - Partnership' || role.RecordType.Name == 'zAPP - Commercial'){							
							if (inc.Income_Type__c == CommonConstants.INCOME_RT_BI || inc.Income_Type__c == CommonConstants.INCOME_RT_SE){
								roleToUpdateInstance.Business_Income_Verification_Method__c = inc.Income_Verification_Method__c;
								roleToUpdateInstance.Business_Total_Revenue__c = inc.Revenue__c;
								roleToUpdateInstance.Business_Total_Depreciation__c = inc.Depreciation__c;
								roleToUpdateInstance.Reporting_Period__c = inc.Reporting_Period__c;
								roleToUpdateInstance.Profit_Before_Tax_Annual__c = inc.Profit_Before_Tax_Annual__c;
								addRoleToMapForUpdateInner = true;
							}
						}
						if(addRoleToMapForUpdateInner) addRoleToMapForUpdate = true;
					}
				}
				else if(role.Incomes1__r == NULL || role.Incomes1__r.size() == 0){
					if(role.Lead_12_Months_Income__c != false){
						System.debug('updateRole 12 Months Income To False if no Income ************ ');
						roleToUpdateInstance.Lead_12_Months_Income__c = false;
						addRoleToMapForUpdate = true;
					}
				}
			}
			if(addRoleToMapForUpdate){
				roleToUpdateMap.put(role.Id,roleToUpdateInstance); 
			}
			// System.debug('incAccMap =  '+incAccMap);
		}
	}
		
	public void beforeInsert(SObject so){
		if (TriggerFactory.trigSet.Enable_Income_Populate_Lead_Fields__c){	
			IncomeGateway.setLeadIncomeFields((Income1__c) so);
		}

	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		Income1__c oldinc = (Income1__c) oldSo;
        Income1__c inc = (Income1__c) so;
		if (TriggerFactory.trigSet.Enable_Income_Populate_Lead_Fields__c){	
			IncomeGateway.setLeadIncomeFields(inc);
		}
		if (!inc.h_Auto_Create_FO_Share__c) inc.h_Auto_Create_FO_Share__c = TRUE; //Updated due to manual Fo Shares being created for Nodifi
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		
	}
	
	public void afterUpdate(SObject oldSo, SObject so){

	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		//Auto Create FO Share for Liability
		if (!FOShareToCreateMap.isEmpty()){
			try{
				Database.upsert(FOShareToCreateMap.values());
			}
			catch (Exception e){ //Retry due to UNABLE_LOCK_ROW issue
				Database.upsert(FOShareToCreateMap.values());
			}

			/* //Update FO Uniform Share % in Role
			set <Id> FOsIdSet = new set <Id>();
			for (FO_Share__c fos : FOShareToCreateMap.values()){
				FOsIdSet.add(fos.Id);
			}
			list <FO_Share__c> foslist = [Select Id, Role_Share_Across_FOs__c, Role_Share__c, Role__c, Role__r.FO_Uniform_Share__c From FO_Share__c Where Id in : FOsIdSet];
			map <id, Role__c> uproleMap = RollupUtility.updateRoleShareinRole(foslist);
			if (uproleMap.size() > 0) Database.update(uproleMap.values()); */

		}
		//Update Role Employment Job Status from PAYG/Self-Employed Current Income
		/*if (empjobStatusMap.size() > 0){
			list <Role__c> upRolelist = IncomeGateway.roleToUpdateEmploymentStatus(empjobStatusMap);
			if (upRolelist.size() > 0) Database.update(upRolelist);
		} */
		System.debug('###roleToUpdateMap.values() '+roleToUpdateMap.values());
        if(roleToUpdateMap.values().size()>0){
			System.debug('### TRY Update roleToUpdateMap');
			try{
            	Database.update(roleToUpdateMap.values());
			}
			catch (Exception e){ //Retry due to UNABLE_LOCK_ROW issue
				System.debug('### CATCH Update roleToUpdateMap Error Message : '+e.getMessage());
				Database.update(roleToUpdateMap.values());
			}
        }
		//Delete created FO Share when Income got deleted
		if (FOShareToDeleteMap.size() > 0){
			Database.delete(FOSHareToDeleteMap.values());
		} 
		/*if(accToUpdateList.size() > 0){
			Database.update(accToUpdateList);
		} */
	}
}