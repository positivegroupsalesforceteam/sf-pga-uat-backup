/** 
* @FileName: CaseGateway
* @Description: Provides finder methods for accessing data in the Case object.
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 2/19/18 JBACULOD Created Class
* 1.1 3/22/18 RDAVID [SD-27] SF - New Model - Setup Scenario Cases (Automations) - Added recordStageStamps and calculateSLATime Methods
* 1.2 4/04/18 JBACULOD. Modified getApp2CreateList - setting of default Application Record Type
* 1.3 5/11/18 JBACULOD Modified recordStageStamps, merged stagestamps from OM to NM
* 1.4 8/22/18 JBACULOD Added logic (retrieveRelatedFieldLookups, populateRelatedFieldsFromLookups) for updating related fields from lookups (created due to object reference limit)
* 1.5 10/03/18 JBACULOD Added recordStatusStageFunnels
* 2.1 10/05/18 RDAVID - extra/MergeCaseTrigger - Merge recordStatusStageFunnels and extra/DialerAutomationUpdates (Jesfer and Rex Changes) in UAT to deploy to Production; Applied CommonConstants.CASE_STAGE_ATTEMPTED_CONTACT
* 2.2 12/04/18 JBACULOD - extra/CaseKARSalesOpp - Added creating of KAR - Sales Opportunity on Submitted NOD/POS Consumer Asset Finance
* 2.3 12/13/18 JBACULOD - Fixed populateRelatedFieldsFromLookups (not bulkified issue), transferred logic of CaseKARSalesOpp to CaseHandler due to CreateSRManagerReview
* 2.4 01/16/19 JBACULOD - added stopping of Active SLAs when Case got Closed
* 2.5 01/23/19 RDAVID - extra/PHLOpp-Tasks - Move "Case - Create POS: PHL Opportunity" Process Builder to Trigger. 
* 2.5 26/04/19 RDAVID - extra/task24368245-SetOwnerWhenCaseNewToClosed - Set Owner to Current User When Updated to Closed
* 2.6 17/04/2019 - RDAVID - branch:extra/task24148707-SectorSLAFixBusinessHours - Updated, Commented code to remove the SLA Scenario fields reference to proceed with deletion. Comment calculateScenarioSLAThree method
* 2.7 2/05/19 RDAVID - extra/task24399056-InitialReviewSRandSLA - Added logic to auto-create "NOD Initial Review SR" 
* 2.8 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment SLA Related methods in this class
* 2.9 03/06/2019 - JBACULOD - Removed Old automation (recordStageStamps, calculateNodifiSLATimeOnInsert, calculateNodifiSLATimeOnUpdate, calculateScenarioSLATimeOnInsert, calculateScenarioSLATimeOnUpdate, calculateScenarioSLAOne, calculateScenarioSLATwo, calculateScenarioSLAThree)
* 3.0 17/06/19 RDAVID - extra/task24943632-BugFixOverFlowAssignedCases - Added logic to exclude users && Cases with Auto_Referred_Out__c = TRUE and Only allow it to Team/Queue
* 3.1 21/06/19 JBACULOD Moved Referred Out By Owner process from Process Builder to Trigger
* 3.2 24/07/2019 RDAVID extra/tasks25429652-PopulateLeadCreationMethod - Populate Lead_Creation_Method__c
* 3.3 25/07/2019 RDAVID extra/tasks25446451-ExcludeTomAndAPI - On our rule to change the case owner to the user who closes the case when the case is a Queue. Can we exclude it if the User is Tom Caesar or APi Admin.
* 3.4 6/08/2019  RDAVID extra/tasks25582692-TomCaesarCaseOwnerIssue - Need to update condition from || to && 
**/ 
public without sharing class CaseGateway {

	private static General_Settings1__c genSet = General_Settings1__c.getInstance(UserInfo.getUserId());
	private static string strDnow = '';

	private static set <Id> caseRT2Disregard = setcaseRT2Disregard();
	
	//2.0 9/26/18 RDAVID 
	public static List<BusinessHours> busiHoursList = new List<BusinessHours>();
	public static BusinessHours busiHours = new BusinessHours();
	public static Map<String,BusinessHours> stateBusinessHoursMap = new Map<String,BusinessHours>();
	public static Map<String,List<Boolean>> stateBusinessDayMap = new Map<String,List<Boolean>>();
	
	public static Map<String,List<DateTime>> stateBusinessStartTimeMap = new Map<String,List<DateTime>>();
	public static Map<String,List<DateTime>> stateBusinessEndTimeMap = new Map<String,List<DateTime>>();
	
	public static Time holidayST = Time.newInstance(9, 0, 0, 0); //9 a.m. Start Time during Holiday or weekend Business Hours 
	public static Time holidayET = Time.newInstance(17, 0, 0, 0); //5 p.m. End Time during Holiday or weekend Business Hours 
	private static DateTime currentStartDateInCaseTZ = DateTime.newInstance(Date.today(), Time.newInstance(9, 0, 0, 0));//.addMilliseconds(offset) 5pm
	private static DateTime currentEndDateInCaseTZ = DateTime.newInstance(Date.today(), Time.newInstance(17, 0, 0, 0));//.addMilliseconds(offset)	7pm

	public static Date myDateTomorrow = System.today().addDays(1);
	public static Map<String,DateTime> AUStateDateTimeMapStart = new Map<String,DateTime>();
	public static Integer i = Math.mod(Math.abs(date.newInstance(2013, 1, 6).daysBetween(System.today())),7);

	private static final String NOD_FULLAPP = 'nod-fullapp';

	private static set <Id> setcaseRT2Disregard(){
		if (caseRT2Disregard == null){
			set <Id> caseRTNotId = new set <Id>();
			for (Schema.RecordTypeInfo cseRT : Schema.SObjectType.Case.getRecordTypeInfos()){
				string cRTName = cseRT.getName();
				if (cRTName == CommonConstants.CASE_RT_OM_COMM_LOAN || cRTName == CommonConstants.CASE_RT_OM_CONS_LOAN || cRTName == CommonConstants.CASE_RT_OM_SERV){ //Disregard Case Record Types from Old Model
					caseRTNotId.add(cseRT.getRecordTypeId());
				}
			}
			return caseRTNotId;
		}
		else return caseRT2Disregard;
	}

	public static set <string> lookupRelFields = new set <string>(); //Used later for identifying fields to populate
	public static map <id,Sobject> retrieveRelatedFieldLookups(list <Case> newCaseListEX, list<Lookup_Related_Field_Update_Setting__mdt> lrfusetCase){
		map <Id, Sobject> sobjMap = new map <Id,Sobject>();
		for (Case cse : newCaseListEx){
			if(String.valueof(cse.RecordType.Name).startsWith(CommonConstants.PREFIX_N) || String.valueof(cse.RecordType.Name).startsWith(CommonConstants.PREFIX_P) || String.valueof(cse.RecordType.Name).startsWith(CommonConstants.PREFIX_PWM) || String.valueof(cse.RecordType.Name).startsWith(CommonConstants.PREFIX_KAR) ){ //PLS - Nodifi - PWM - KAR Case RTs
				Sobject sobj = cse;
				sobj.put('Id', cse.Id);
				//Iterate in all related fields identified for update in Case
				for (Lookup_Related_Field_Update_Setting__mdt lrfuset : lrfusetCase){
					map <string, string> relatedFieldMap = new map <string, string>();
					map <string, string> FieldToUpdateMap = new map <string, string>();
					//Get Related Fields
					string lrelfields = lrfuset.Related_Fields__c;
					boolean breaklrelfieldslvl1 = false;
					do {
						string rfields = lrelfields.substringBetween('{','}');
						if (rfields == null || rfields == '') breaklrelfieldslvl1 = true;
						if (rfields != null && rfields != ''){
							string fieldkey = rfields.substringBefore('[');
							rfields = rfields.remove(fieldkey);
							fieldkey = fieldkey.remove(' = ');
							boolean breaklrelfieldslvl2 = false;
							do {
								string rfields2 = rfields.substringBetween('[',']');
								if (rfields2 == null || rfields2 == ''){ 
									lrelfields = lrelfields.remove('{' + lrelfields.substringBetween('{','}') + '}');
									breaklrelfieldslvl2 = true;
								}
								if (rfields2 != null && rfields != ''){
									relatedFieldMap.put(fieldkey, rfields2);
									rfields = rfields.remove('['+rfields2+']');
								}
							}
							while (!breaklrelfieldslvl2);
						}
					}
					while (!breaklrelfieldslvl1);
					//Get Field To Updates
					string ftufields = lrfuset.Fields_To_Update__c;
					boolean breaklftufieldslvl1 = false;
					do {
						string objfields = ftufields.substringBetween('{','}');
						if (objfields == null || objfields == '') breaklftufieldslvl1 = true;
						if (objfields != null && objfields != ''){
							string fieldUpkey = objfields.substringBefore('[');
							objfields = objfields.remove(fieldUpkey);
							fieldUpkey = fieldUpkey.remove(' = ');
							boolean breaklftufieldslvl2 = false;
							do {
								string objfields2 = objfields.substringBetween('[',']');
								if (objfields2 == null || objfields2 == ''){ 
									ftufields = ftufields.remove('{' + ftufields.substringBetween('{','}') + '}');
									breaklftufieldslvl2 = true;
								}
								if (objfields2 != null && objfields != ''){
									FieldToUpdateMap.put(fieldUpkey, objfields2);
									lookupRelFields.add(objfields2);
									objfields = objfields.remove('['+objfields2+']');
								}
							}
							while (!breaklftufieldslvl2);
						}
					}
					while (!breaklftufieldslvl1);
					for (String key : FieldToUpdateMap.keySet()){
						if (relatedFieldMap.containskey(key)){
							if (cse.get(lrfuset.Lookup_Field_API_Name__c) != null){
								boolean breakRelField = false; boolean includeField = true;
								string relField = relatedFieldMap.get(key); 
								sobject fieldVal = cse.getSobject(lrfuset.Lookup_Field__c);
								do {
									string tempRelField;
									tempRelField = relField.substringBefore('.'); relField  = relField.remove(tempRelField+'.');
									if ( relField.substringBefore('.') == relField) breakRelField = true;
									if (!breakRelField){ 
										if (fieldVal.getSobject(tempRelField) != null) fieldVal = fieldVal.getSobject(tempRelField);
										else { breakRelField = true; includeField = false; }
									}
									//system.debug('@@tempRelField:'+relField);
								}
								while (!breakRelField); 
								if (includeField) sobj.put(FieldToUpdateMap.get(key), fieldVal.get(relField));
								else sobj.put(FieldToUpdateMap.get(key), null);
							}
							else sobj.put(FieldToUpdateMap.get(key), null);
							sobjMap.put(cse.Id, sobj);
						}
					}
				}
			}
		}
		return sobjMap;
	}

	/**
	* @Description: Returns a list of Applications to be created for each Cases
	* @Arguments:	List<Case> newCaseList - trigger.new
	* @Returns:		List<Application__c> getApp2CreateList - Applications to be created
	*/
	public static list <Application__c> getApp2CreateList(list <Case> newCaseList){

		/*set <Id> caseRT2Disregard = new set <Id>();
		for (Schema.RecordTypeInfo cseRT : Schema.SObjectType.Case.getRecordTypeInfos()){
			string cRTName = cseRT.getName();
			if (cRTName == CommonConstants.CASE_RT_OM_COMM_LOAN || cRTName == CommonConstants.CASE_RT_OM_CONS_LOAN || cRTName == CommonConstants.CASE_RT_OM_SERV){ //Disregard Case Record Types from Old Model
				caseRT2Disregard.add(cseRT.getRecordTypeId());
			}
		} */

		//Retrieve all Application Record Types
		map <string,id> appRTMap = new map <string,Id>();
		for (Schema.RecordTypeInfo appRT : Schema.SObjectType.Application__c.getRecordTypeInfos()){
			appRTMap.put(appRT.getName(),appRT.getRecordTypeId());
		}
		

		list <Application__c> applist = new list <Application__c>();
		for (Case cse : newCaseList){
			if (!caseRT2Disregard.contains(cse.RecordTypeId)){ //Only allow creating of Applications on new RTs of New Model
				Application__c app = new Application__c(
						Name = String.valueof(Integer.valueof(cse.CaseNumber)),
						Case__c = cse.Id
					);
				if (cse.Application_Record_Type__c != null){
					if (appRTMap.containskey(cse.Application_Record_Type__c)){
						app.RecordTypeId = appRTMap.get(cse.Application_Record_Type__c); //Set Record Type base from Case's Applicant Type field
					}
				}
				applist.add(app);
			}
		}
		return applist;
	}


	public static list <Case> getCase2UpdateList(list <Application__c> applist){

		map <Id,Id> case2appMap = new map <Id,Id>();
		for (Application__c app : applist){
			if (app.Case__c != null) case2appMap.put(app.Case__c,app.Id);
		}

		list <Case> upCaseList = [Select Id, Application_Name__c From Case Where Id in : case2appMap.keySet()];
		for (Case cse : upCaseList){
			cse.Application_Name__c = case2appMap.get(cse.Id);
		}
		return upCaseList;

	}

	public static void recordStatusStageFunnels(Case cse, Case oldCs){
		boolean isStatusChanged = false;
		boolean isStageChanged = false;
		if (!caseRT2Disregard.contains(cse.RecordTypeId)){
			if (oldCs != null){ //isUpdate
				if (cse.Status != oldCs.Status) isStatusChanged = true;
				if (cse.Stage__c != oldCs.Stage__c) isStageChanged = true;
			}
			else{ //isInsert
				if (cse.Status != null) isStatusChanged = true; 
				if (cse.Stage__c != null) isStageChanged = true; 
			}
			if (isStatusChanged){ //Record Status update in Status Funnel
				if (cse.Status_Funnel__c == null) cse.Status_Funnel__c = cse.Status;
				else{
					if (!String.valueof(cse.Status_Funnel__c).contains(cse.Status)){
						cse.Status_Funnel__c+= '; ' + cse.Status;
					}
				}
			}
			if (isStageChanged){ //Record Stage update in Stage Funnel
				if (cse.Stage__c != null){
					if (cse.Stage_Funnel__c == null) cse.Stage_Funnel__c = cse.Stage__c;
					else{
						if (!String.valueof(cse.Stage_Funnel__c).contains(cse.Stage__c)){
							cse.Stage_Funnel__c += '; ' + cse.Stage__c;
						}
					}
				}
			}
		}
	}
	
	//Generate Trust Pilot Unique Link in Role
	public static map <Id, Role__c> generateTrustPilotLink(map <Id, Boolean> isProcessedCaseMap){

		map <Id, Role__c> roleMap = new map <Id, Role__c>();
		for (Role__c role : [Select Id, TrustPilot_Unique_Link__c, Account__c, Account__r.PersonEmail, Account__r.Name, Case__c, Case__r.RecordTypeId, Case__r.Stage__c, Case__r.Status From Role__c Where Case__c in : isProcessedCaseMap.keySet()]){ 
			if (!isProcessedCaseMap.get(role.Case__c)){
				//system.debug('@@role in Case:'+role);
				if (!caseRT2Disregard.contains(role.Case__r.RecordTypeId)){
					if (role.Case__r.Status == CommonConstants.CASE_STATUS_APPROVED && role.TrustPilot_Unique_Link__c == null){ //populate only when TrustPilot Unique Link is not yet generated/updated
						string stremail; string strname;
						if (!Test.isRunningTest()){ 
							stremail = 	role.Account__r.PersonEmail;
							strname = role.Account__r.Name;
						}
						else { 
							stremail = 'test@email.com'; strName = 'Test Coeman';
						}
						if (stremail != null){
							role.TrustPilot_Unique_Link__c = TrustPilot_Util.generateTrustPilotLink(role.Case__c, stremail, strname);
							//system.debug('@@Role TPLink:'+role.TrustPilot_Unique_Link__c);
							roleMap.put(role.Id, role);
						}
					}
				}
			}
		}
		return roleMap;
	}

	public static map <Id, Case> populateRelatedFieldsFromLookups(map <Id,Sobject> RelFieldsMap, list<Lookup_Related_Field_Update_Setting__mdt> lrfusetCase){
		map <Id, Case> caseToUpdateMap = new map <Id,Case>();
		for (ID sobjID : RelFieldsMap.keyset()){ //Iterate on Case IDs
			//Iterate in all available lookup fields for Update in Case
			sobject relFieldUp = RelFieldsMap.get(sobjID);
			Case cse = new Case(Id = sobjID);
			for (Lookup_Related_Field_Update_Setting__mdt lrfuset : lrfusetCase){
				//if (cs.get(lrfuset.Lookup_Field_API_Name__c) != oldcs.get(lrfuset.Lookup_Field_API_Name__c)){
				for (String csefield : lookupRelFields){
					//Update all identified fields for update in Case 
					cse.put(csefield, relFieldUp.get(csefield));
				}
				caseToUpdateMap.put(sobjID, cse);
				//}
			}
		}
		return caseToUpdateMap;
	}

	public static void initBusinessHoursVariables(){
		if(stateBusinessHoursMap.isEmpty()){		
			if(busiHoursList.size() == 0) busiHoursList = [select Id,IsDefault,Name,IsActive,TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime from BusinessHours where IsActive = true];
			busiHours = getDefaultBusinessHours(busiHoursList);
			AUStateDateTimeMapStart = AUStateDateTimeMap(true);
		} 
	}

	public static BusinessHours getDefaultBusinessHours(List<BusinessHours> busiHoursList){
		BusinessHours defaultBH = new BusinessHours();
	
		for(BusinessHours bh : busiHoursList){
			if(bh.IsDefault) defaultBH = bh;
			stateBusinessHoursMap.put(bh.Name,bh);
			
			List<Boolean> businessDay = new Boolean[7];
			List<DateTime> startHours = new DateTime [7];
    		List<DateTime> endHours = new DateTime [7];

			businessDay[0] = (bh.SundayStartTime != null);
			businessDay[1] = (bh.MondayStartTime != null);
			businessDay[2] = (bh.TuesdayStartTime != null);
			businessDay[3] = (bh.WednesdayStartTime != null);
			businessDay[4] = (bh.ThursdayStartTime != null);
			businessDay[5] = (bh.FridayStartTime != null);
			businessDay[6] = (bh.SaturdayStartTime != null);
			
			startHours[0] = (bh.SundayStartTime != null) ? DateTime.newInstance(Date.today(),bh.SundayStartTime) : null;
			startHours[1] = (bh.MondayStartTime != null) ? DateTime.newInstance(Date.today(),bh.MondayStartTime) : null;
			startHours[2] = (bh.TuesdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.TuesdayStartTime) : null;
			startHours[3] = (bh.WednesdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.WednesdayStartTime) : null;
			startHours[4] = (bh.ThursdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.ThursdayStartTime) : null;
			startHours[5] = (bh.FridayStartTime != null) ? DateTime.newInstance(Date.today(),bh.FridayStartTime) : null;
			startHours[6] = (bh.SaturdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.SaturdayStartTime) : null;

			endHours[0] = (bh.SundayEndTime != null) ? DateTime.newInstance(Date.today(),bh.SundayEndTime) : null;
			endHours[1] = (bh.MondayEndTime != null) ? DateTime.newInstance(Date.today(),bh.MondayEndTime) : null;
			endHours[2] = (bh.TuesdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.TuesdayEndTime) : null;
			endHours[3] = (bh.WednesdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.WednesdayEndTime) : null;
			endHours[4] = (bh.ThursdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.ThursdayEndTime) : null;
			endHours[5] = (bh.FridayEndTime != null) ? DateTime.newInstance(Date.today(),bh.FridayEndTime) : null;
			endHours[6] = (bh.SaturdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.SaturdayEndTime) : null;
			
			stateBusinessDayMap.put(bh.Name,businessDay);
			stateBusinessStartTimeMap.put(bh.Name,startHours);
			stateBusinessEndTimeMap.put(bh.Name,endHours);
			//break;
		}
		return defaultBH;
	}

	public static void processCaseOutsideBusinessHours(Case caseRecord){
		
		Timezone caseStateTZ = Timezone.getTimeZone(String.valueOf(stateBusinessHoursMap.get(caseRecord.State_Code__c).TimeZoneSidKey));
		Timezone currUserTZ = UserInfo.getTimeZone();
		
		// System.debug('caseStateTZ - > '+caseStateTZ);
		// System.debug('currUserTZ - > '+currUserTZ);
		 // index i is index into the businessDay array based on inputDate
		Boolean isCurrentDayBusinessDayInCaseBH = stateBusinessDayMap.get(caseRecord.State_Code__c)[i]; // check if current day is a business day;
		
		Datetime nextStart = BusinessHours.nextStartDate(caseRecord.BusinessHoursId, System.now());//.addHours(1)); // Get next Start Date of Case Business Hour
		Date nextStartDate = date.newinstance(nextStart.year(), nextStart.month(), nextStart.day());

		Integer offsetInSeconds = (currUserTZ.getOffset(System.now()) - caseStateTZ.getOffset(System.now()))/1000  ;
		// System.debug('@@@ 1084 offsetInSeconds -> '+offsetInSeconds);
		// DateTime EndTimeTodayInPerthDT = datetime.valueOf(EndTimeTodayInPerthStr);
		// system.debug('@@@ 1084 stateBusinessStartTimeMap.get(caseRecord.State_Code__c)[i] ----'+stateBusinessStartTimeMap.get(caseRecord.State_Code__c)[i]);
		// system.debug('@@@ 1084 stateBusinessEndTimeMap.get(caseRecord.State_Code__c)[i] ----'+stateBusinessEndTimeMap.get(caseRecord.State_Code__c)[i]);
		// system.debug('@@@ 1084 System.now() IN GMT ----'+System.now());
		// system.debug('@@@ 1084 '+System.now().format('E') +' is Business Day in '+ caseRecord.State_Code__c + ' --- ' +caseStateTZ +' ? ---- '+isCurrentDayBusinessDayInCaseBH);
		//To see conversion Enter this as GMT Time in https://savvytime.com/converter/gmt-to-australia-adelaide-australia-perth
		//Need to add the offset between the User TZ and Case TZ this will output the start time / end time in GMT Format
		DateTime businessDayStartDateTimeInGMT = (stateBusinessStartTimeMap.get(caseRecord.State_Code__c)[i] != NULL) ? stateBusinessStartTimeMap.get(caseRecord.State_Code__c)[i].addSeconds(offsetInSeconds) : null; 
		DateTime businessDayEndDateTimeInGMT = (stateBusinessEndTimeMap.get(caseRecord.State_Code__c)[i] != NULL) ? stateBusinessEndTimeMap.get(caseRecord.State_Code__c)[i].addSeconds(offsetInSeconds) : null; 
		
		if(isCurrentDayBusinessDayInCaseBH){
			if(System.now() >= businessDayStartDateTimeInGMT && System.now() <= businessDayEndDateTimeInGMT){
				System.debug('Today is '+ System.now().format('E') +' and current time ('+ System.now().format('HH:mm:ss') + ' -- ' + currUserTZ + ') is within business hours ('+businessDayStartDateTimeInGMT.format('HH:mm:ss') + ' To '+ businessDayEndDateTimeInGMT.format('HH:mm:ss') + ')');
				caseRecord.Ready_For_Call__c = true;
				caseRecord.Ready_To_Dial_Time__c = null;	
			}
			else if(System.now() < businessDayStartDateTimeInGMT && System.now()  < businessDayEndDateTimeInGMT){
				System.debug('Today is '+ System.now().format('E') +' and current time ('+ System.now().format('HH:mm:ss') + ' -- ' + currUserTZ + ') is before business hours ('+businessDayStartDateTimeInGMT.format('HH:mm:ss') + ' To '+ businessDayEndDateTimeInGMT.format('HH:mm:ss') + ')');
				caseRecord.Ready_For_Call__c = false;
				caseRecord.Ready_To_Dial_Time__c = businessDayStartDateTimeInGMT;				
			}

			else if(System.now() > businessDayStartDateTimeInGMT && System.now() > businessDayEndDateTimeInGMT){
				System.debug('Today is '+ System.now().format('E') +' and current time ('+ System.now().format('HH:mm:ss') + ' -- ' + currUserTZ + ') is after business hours ('+businessDayStartDateTimeInGMT.format('HH:mm:ss') + ' To '+ businessDayEndDateTimeInGMT.format('HH:mm:ss') + ')');
				System.debug('myDateTomorrow is '+myDateTomorrow);
				System.debug('nextStartDate is '+nextStartDate);
				System.debug('nextStart is '+nextStart);
				if(myDateTomorrow <= nextStartDate){
					caseRecord.Ready_For_Call__c = false;
					caseRecord.Ready_To_Dial_Time__c = nextStart;					
					System.debug('Set Ready_To_Dial_Time__c to Next Business Start Date ('+ nextStart +')');
				}
			}
		}

		else{
			// System.debug('@@@ 1084 currentStartDateInCaseTZ ---- '+currentStartDateInCaseTZ);
			// System.debug('@@@ 1084 currentEndDateInCaseTZ ---- '+currentEndDateInCaseTZ);
			if(System.now() >= currentStartDateInCaseTZ && System.now()  <= currentEndDateInCaseTZ){
				caseRecord.Ready_For_Call__c = true;
				caseRecord.Ready_To_Dial_Time__c = null;	
				System.debug('Today is '+ System.now().format('E') +' and current time ('+ System.now().format('HH:mm:ss') + ' -- ' + currUserTZ + ') is within Holiday business hours ('+currentStartDateInCaseTZ.format('HH:mm:ss') + ' To '+ currentEndDateInCaseTZ.format('HH:mm:ss') + ')');			
				// caseRecord.Test_Field__c = 'Current Date Time in Case TZ '+ currentTimeINCaseTZ.format() +' Within HOLIDAY Office hours, make call now.' + ' CurrentHSDICTZ='+currentStartDateInCaseTZ.format() + '-' + 'CurrentHEDICTZ=' +currentEndDateInCaseTZ.format();
			}
			else if(System.now()  < currentStartDateInCaseTZ && System.now()  < currentEndDateInCaseTZ){
				caseRecord.Ready_For_Call__c = false;
				caseRecord.Ready_To_Dial_Time__c = currentStartDateInCaseTZ;//startDateToSetInCaseTZ;//currentStartDateInCaseTZ;				
				System.debug('Today is '+ System.now().format('E') +' and current time ('+ System.now().format('HH:mm:ss') + ' -- ' + currUserTZ + ') is before Holiday business hours ('+currentStartDateInCaseTZ.format('HH:mm:ss') + ' To '+ currentEndDateInCaseTZ.format('HH:mm:ss') + ')');	
				// caseRecord.Test_Field__c = 'Current Date Time in Case TZ '+ currentTimeINCaseTZ.format() +' Less Than Current HOLIDAY Day BH. Set Current Date to Business Start Date.' + ' CurrentHSDICTZ='+currentStartDateInCaseTZ.format() + '-' + 'CurrentHEDICTZ=' +currentEndDateInCaseTZ.format();
			}
			else if(System.now()  > currentStartDateInCaseTZ && System.now()  > currentEndDateInCaseTZ){
				System.debug('Today is '+ System.now().format('E') +' and current time ('+ System.now().format('HH:mm:ss') + ' -- ' + currUserTZ + ') is after Holiday business hours ('+currentStartDateInCaseTZ.format('HH:mm:ss') + ' To '+ currentEndDateInCaseTZ.format('HH:mm:ss') + ')');
				System.debug('myDateTomorrow is '+myDateTomorrow);
				System.debug('nextStartDate is '+nextStartDate);
				System.debug('nextStart is '+nextStart);
				if(myDateTomorrow <= nextStartDate){
					caseRecord.Ready_For_Call__c = false;
					caseRecord.Ready_To_Dial_Time__c = nextStart;					
					// caseRecord.Test_Field__c = 'Current Date Time in Case TZ '+ currentTimeINCaseTZ.format() +' More Than Current HOLIDAY BH. Set Next Date to Business Start Date.' + ' CurrentHSDICTZ='+currentStartDateInCaseTZ.format() + '-' + 'CurrentHEDICTZ=' +currentEndDateInCaseTZ.format();
				}
			}
		}
		system.debug('@@@ 1084 caseRecord.Ready_To_Dial_Time__c ----'+caseRecord.Ready_To_Dial_Time__c);
	}
	//RDAVID 12/09/2019 Might be DEPRECATED
	public static Map<String,DateTime> AUStateDateTimeMap (boolean IsStartDateTime){
		Map<String,DateTime> AUStateDateTimeMapInstance = new Map<String,DateTime>();

		Time timeInstance = (IsStartDateTime) ? Time.newInstance(9, 0, 0, 0) : Time.newInstance(17, 0, 0, 0);

		DateTime currentStartDateInCaseGMT;
		for(String bhName : stateBusinessHoursMap.keySet()){
			currentStartDateInCaseGMT = Datetime.valueOf(DateTime.newInstance(Date.today(), timeInstance).format('yyyy-MM-dd HH:mm:ss', stateBusinessHoursMap.get(bhName).TimeZoneSidKey));
			AUStateDateTimeMapInstance.put(bhName,currentStartDateInCaseGMT);
			//System.debug(bhName +'-------'+ currentStartDateInCaseGMT.format());
		}

		return AUStateDateTimeMapInstance;
	}	

	public static Case initiatePHLCase(Case cse, String PHLOppRTId, String busiHoursId){
		Case cseInstance = new Case(	Channel__c = 'PHL', 
										Parent_Case__c = cse.Id,
										Partition__c = 'Positive',
										BusinessHoursId = busiHoursId,
										Primary_Contact_Email_MC__c = cse.Primary_Contact_Email_MC__c,
										Primary_Contact_Location_MC__c = cse.Primary_Contact_Location_MC__c,
										Primary_Contact_Name_MC__c = cse.Primary_Contact_Name_MC__c,
										Primary_Contact_Phone_MC__c = cse.Primary_Contact_Phone_MC__c,
										RecordTypeId = PHLOppRTId,
										Stage__c = 'Open',
										Status = 'New',
										Lead_Creation_Method__c = 'Opportunity Automation'); //3.2 24/07/2019 RDAVID extra/tasks25429652-PopulateLeadCreationMethod 
		return cseInstance;
	}

	public static Sector__c getSectorInstance (Case cse, String sectorFieldStr){
		Sector__c secIns = new Sector__c(	Case_Number__c = cse.Id, 
											Sector_Entry__c = System.now(), 
											Channel__c = cse.Channel__c, 
											Partition__c = cse.Partition__c, 
											Entry_Owner__c = UserInfo.getUserId(),
											Sector1__c = sectorFieldStr);
		return secIns;
	}

	public static Set<String> getvalidSectors(){
		List<Schema.Picklistentry> sectorPickFieldResult= Sector__c.Sector1__c.getDescribe().getPicklistValues();
		Set<String> validSecSet = new Set<String>();
		for(Schema.PicklistEntry f : sectorPickFieldResult) {
			validSecSet.add(f.getValue());
		}
		return validSecSet;
	}

	public static void processSectors(Map<Id,Case> casewithSectorMap, Case cs, Case oldCS, Set<String> validSectors, List<Sector__c> sectorListToUps){
		//Case has existing Sectors
		Boolean statusUpdated = (cs.Status != NULL && cs.Status != oldCS.Status) ? true : false;
		Boolean stageUpdated = (cs.Stage__c != NULL && cs.Stage__c != oldCS.Stage__c) ? true : false;
		Boolean attemptedContactStatusUpdated = (!String.IsBlank(cs.Attempted_Contact_Status__c) && cs.Attempted_Contact_Status__c != oldCS.Attempted_Contact_Status__c) ? true : false;
		Boolean hasExistingAttemptedSector = false;
		Boolean hasExistingStatusSector = false;
		Boolean hasExistingStageSector = false;
		Boolean caseChildSectorMapIsNotEmpty = (casewithSectorMap.get(cs.Id).Sectors__r != NULL && casewithSectorMap.get(cs.Id).Sectors__r.size() > 0) ? true : false;

		if(caseChildSectorMapIsNotEmpty){
			for(Sector__c sec : casewithSectorMap.get(cs.Id).Sectors__r){
				//If sector is not SLA 
				if(!sec.Sector1__c.containsIgnoreCase('- SLA -')){
					if(statusUpdated){
						String sectorToPutUpd = 'Status - '+oldCS.Status;
						String sectorToPutIns = 'Status - '+cs.Status;
						//Will work only if there are existing open oldCS.Status sector
						if(sectorToPutUpd == sec.Sector1__c){
							hasExistingStatusSector = true;
							Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
							sectorListToUps.add(sectorRecUp);
							addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
						}
					}
					if(stageUpdated){
						String sectorToPutUpd = 'Stage - '+oldCS.Stage__c;
						String sectorToPutIns = 'Stage - '+cs.Stage__c;
						//Will work only if there are existing open oldCS.Stage sector
						if(sectorToPutUpd == sec.Sector1__c){
							hasExistingStageSector = true;
							Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
							sectorListToUps.add(sectorRecUp);
							addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
						}
					}
					if(attemptedContactStatusUpdated){
						String sectorToPutUpd = 'Attempted Contact Status - '+oldCS.Attempted_Contact_Status__c;
						String sectorToPutIns = (!String.IsBlank(cs.Attempted_Contact_Status__c)) ? 'Attempted Contact Status - '+cs.Attempted_Contact_Status__c : '';
						//Will work only if there are existing open attempted contact status sector
						if(sectorToPutUpd == sec.Sector1__c){
							hasExistingAttemptedSector = true;
							Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
							sectorListToUps.add(sectorRecUp);
							addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);		
						}
					}
					else{
						//If Attempted Contact Status is updated to blank 
						if(String.IsBlank(cs.Attempted_Contact_Status__c) && !String.IsBlank(oldCS.Attempted_Contact_Status__c)){
							String sectorToPutUpd = 'Attempted Contact Status - '+oldCS.Attempted_Contact_Status__c;
							if(sectorToPutUpd == sec.Sector1__c){
								hasExistingAttemptedSector = true;
								Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
								sectorListToUps.add(sectorRecUp);
							}
						}
					}
				}
				//If sector is SLA, this condtiion handles update of the SLA Sector
				else{
					if(sec.Sector_Exit__c == NULL){
						System.debug('>>>>>>>>>>> SECTORSLA to update -> '+ sec.Sector1__c);
						Sector__c sectorRecUp = new Sector__c(Id = sec.Id, Sector_Exit__c = System.now(), Exit_Owner__c = UserInfo.getUserId());
						sectorListToUps.add(sectorRecUp);
					}

				}
			}
			//After checking all child sector & there are still no Attempted Contact Sector
			System.debug('*** Sector  hasExistingAttemptedSector - '+hasExistingAttemptedSector);
			System.debug('*** Sector  hasExistingStatusSector - '+hasExistingStatusSector);
			System.debug('*** Sector  hasExistingStageSector - '+hasExistingStageSector);
			if(!hasExistingAttemptedSector) {
				if(attemptedContactStatusUpdated){
					System.debug('*** Sector  hasExistingAttemptedSector - '+hasExistingAttemptedSector);
					String sectorToPutIns = (!String.IsBlank(cs.Attempted_Contact_Status__c)) ? 'Attempted Contact Status - '+cs.Attempted_Contact_Status__c : '';
					addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
				}
			}
			if(!hasExistingStatusSector) {
				if(statusUpdated){
					String sectorToPutIns = 'Status - '+cs.Status;
					addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
				}
			}
			if(!hasExistingStageSector) {
				if(stageUpdated){
					String sectorToPutIns = 'Stage - '+cs.Stage__c;
					addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
				}				
			}
		}
		//Handle Attempted Contact if no Attempted Contact Sector
		else if(!caseChildSectorMapIsNotEmpty){
			if(statusUpdated){
				String sectorToPutIns = 'Status - '+cs.Status;
				addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
			}
			if(stageUpdated){
				String sectorToPutIns = 'Stage - '+cs.Stage__c;
				addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
			}

			if(attemptedContactStatusUpdated){
				String sectorToPutIns = 'Attempted Contact Status - '+cs.Attempted_Contact_Status__c;
				addValidSector (validSectors, cs, sectorToPutIns, sectorListToUps);
			}
		}
	}

	public static void addValidSector (Set<String> validSectors, Case cs, String sectorToPutIns, List<Sector__c> sectorListToUps){
		if(validSectors.contains(sectorToPutIns)){
			Sector__c sectorRec = getSectorInstance(cs,sectorToPutIns);
			sectorListToUps.add(sectorRec);
		}
	}

	//RDAVID - 26/04/2019 - extra/task24368245-SetOwnerWhenCaseNewToClosed - Set Owner to Current User When Updated to Closed
	public static void updateCaseOwnerToCurrentUserWhenClosed (Case cse, Case oldCse){
		// 17/06/19 RDAVID - extra/task24943632-BugFixOverFlowAssignedCases - Added logic to exclude users && Cases with Auto_Referred_Out__c = TRUE and Only allow it to Team/Queue 
		String ownerId = String.valueOf(cse.OwnerId);
		if(ownerId.startsWith('00G') && !cse.Auto_Referred_Out__c){
			if(cse.Partition__c == 'Positive' && oldCse.Status != cse.Status && oldCse.Status == 'New' && cse.Status == 'Closed' && cse.Stage__c != 'Referred Out' && cse.Attempted_Contact_Status__c != 'Attempted 10'){
				if(userInfo.getName() != 'Tom Caesar' && userInfo.getName() != 'Api Admin'){ //3.4 6/08/2019  RDAVID extra/tasks25582692-TomCaesarCaseOwnerIssue //3.3 25/07/2019 RDAVID extra/tasks25446451-ExcludeTomAndAPI 
					cse.OwnerId = UserInfo.getUserId();
				}
			}
		}
	}

	// 2/05/19 RDAVID - extra/task24399056-InitialReviewSRandSLA
	public static void createInitialReviewSR (Case cse, String validCaseRTsForInitialReviewSR, Id nodInitialReviewSRRTId, List<Support_Request__c> nodInitialReviewSRList){
		String caseRTName = Schema.SObjectType.Case.getRecordTypeInfosById().get(cse.RecordTypeId).getName();
		if(cse.Partition__c == 'Nodifi' && cse.Connective_Full_App_or_Lead__c == 'Full Application' && validCaseRTsForInitialReviewSR.containsIgnoreCase(caseRTName)){
			Support_Request__c srInitial = new Support_Request__c( 	RecordTypeId = nodInitialReviewSRRTId,
																	Case_Number__c = cse.Id,
																	Status__c = 'New',
																	Stage__c = 'New',
																	Partition__c = cse.Partition__c);
			nodInitialReviewSRList.add(srInitial);
		}
	}

	//21/06/19 JBACULOD - Moved Referred Out By Owner process from Process Builder to Trigger
	public static void referredOutByOwner(Case cse, Case oldCse, PLS_Bucket_Settings__c plsbucketset){
		//M3
		if (cse.Status == 'New' && oldCse.OwnerId != cse.OwnerId && cse.OwnerId == plsbucketset.M3_Queue_ID__c && 
			cse.Partition__c == 'Positive' && (cse.Channel__c == 'PLS' || cse.Channel__c == 'LFPWBC') ){
				cse.Status = 'Closed';
				cse.Stage__c = 'Referred Out';
				cse.Closed_Reason__c = 'Referred to Money 3';
		}
	}

	/** 
	* @FileName: checkIfCaseHasNOFullAppFE
	* @Description: RDAVID extra/task25953355-NODFlowV3SFAutomations 
                        - If Case has a child FE Sub where Positive Form Type is 'nod-fullapp' OR 'lfp-fullapp' return FALSE
    * @Arguments:	Case cse
	* @Returns:		Boolean checkIfCaseHasNOFullAppFE               
	**/ 
    public static Boolean checkIfCaseHasNOFullAppFE(Case cse){
        Boolean hasNOFullApp = true;
		if(cse.Frontend_Submissions__r.size() > 0){
			for(Frontend_Submission__c fe : cse.Frontend_Submissions__r){
				if(fe.Positive_Form_Type__c == NOD_FULLAPP) hasNOFullApp = false;
				break;
			}
		}
        return hasNOFullApp;
    }

	/** 
	* @FileName: getRequestCallFE
	* @Description: RDAVID extra/task25953355-NODFlowV3SFAutomations 
                        - Returns the call request FE
    * @Arguments:	Case cse
	* @Returns:		Frontend_Submission__c getRequestCallFE               
	**/ 
    public static Frontend_Submission__c getRequestCallFE(Case cse){
        Frontend_Submission__c reqCallFE = new Frontend_Submission__c();
		if(cse.Frontend_Submissions__r.size() > 0){
			for(Frontend_Submission__c fe : cse.Frontend_Submissions__r){
				if(fe.Positive_Form_Type__c == FrontEndSubmissionHandler.NOD_REQUEST_CALL) reqCallFE = fe;
                break;//RDAVID 27/11/2019
			}
		}
        return reqCallFE;
	}
}