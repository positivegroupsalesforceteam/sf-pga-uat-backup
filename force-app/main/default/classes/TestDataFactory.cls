/** 
* @FileName: TestDataFactory
* @Description: stores record creation on each object which will be used on setup in unit tests
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/16/18 JBACULOD Created Class, added test data creation for Rollup Custom Settings
* 1.1 2/19/18 JBACULOD [SD-19], Added createGenericCase, createGenericAsset
* 1.2 2/20/18 JBACULOD [SD-19], Added createGenericLiability, Modified hardcoded Record Type labels with CommonConstants
* 1.3 2/20/18 RDAVID [SD-19], Added createGenericAccount,createGenericCase,createGenericApplication,createGenericRole,createGenericIncome,createGenericExpense
* 1.4 3/27/18 Added create_User_SysAd
* 1.5 4/3/18 JBACULOD, Updated createGenericRole
* 1.6 4/9/18 RDAVID Created createGenericPersonAccount
* 1.6.1 4/11/18 JBACULOD Removed duplicate createGenericPersonAccount method
* 1.7 4/25/18 JBACULOD Created Case2FOSetupTemplate
* 1.8 4/25/18 RDAVID Added methods for FO_Share__c object
* 1.9 20/06/19 RDAVID Added methods for Support_Request__c object
* 2.0 24/09/19 RDAVID Added activateAllTriggerSettingsNM method to activate all triggers in Trigger_Settings1__c;
**/ 
@isTest
public class TestDataFactory {

    //Create User Sys Ad
    //Create test data for Income to Role Rollup Field Mapping custom setting
    public static User create_User_SysAd(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User usr1 = new User(Alias = 'sysAd', Email='sysAd@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='sysAd@testorg.com');
        return usr1;
    }
    
    //Create test data for Accounts
    public static list <Account> createGenericAccount(String name, string accRTname, Integer ctr){

        Id curAccRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(accRTname).getRecordTypeId();
        list <Account> acclist = new list <Account>();
        for (Integer i = 0; i < ctr; i++){
            Account acc = new Account(
                    Name = name+i,
                    RecordTypeId = curAccRT
                );
            acclist.add(acc);
        }
        return acclist;
    }
    
    //Create test data for Person Accounts
    public static list <Account> createGenericPersonAccount(String fName, String lName, string accRTname, Integer ctr){

        Id curAccRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(accRTname).getRecordTypeId();
        list <Account> acclist = new list <Account>();
        for (Integer i = 0; i < ctr; i++){
            Account acc = new Account(
                    FirstName = fName+i,
                    LastName = lName+i,
                	PersonEmail = lName+i+'@test.com',
                    RecordTypeId = curAccRT
                );
            acclist.add(acc);
        }
        return acclist;
    }

    //Create test data for Cases
    public static List <Case> createGenericCase(string caseRTName, Integer ctr){
        Id curCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(caseRTName).getRecordTypeId();
        list <Case> caseList = new list <Case>();

        for (Integer i = 0; i < ctr; i++){
            Case cse = new Case(
                RecordTypeId = curCaseRT,
                Status = 'New',
                Origin = 'Phone',
                Subject = 'Test Subject '+i,
                Priority = 'Medium',
                Stage__c = 'Open'
            );
            caseList.add(cse);
        }
        return caseList;
    }

    //Create test data for Applications
    public static List <Application__c> createGenericApplication(string appRTName, String caseId, Integer ctr){
        Id curApplicationRT = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get(appRTName).getRecordTypeId();
        list <Application__c> applicationList = new list <Application__c>();

        for (Integer i = 0; i < ctr; i++){
            Application__c app = new Application__c(
                RecordTypeId = curApplicationRT,
                Case__c = caseId,
                Name = 'Test Application '+i
            );
            applicationList.add(app);
        }
        return applicationList;
    }

    //Create test data for Roles
    public static List <Role__c> createGenericRole(string roleRTName, String caseId, String accId, string appId, Integer ctr){
        Id curRoleRT = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(roleRTName).getRecordTypeId();
        list <Role__c> roleList = new list <Role__c>();

        for (Integer i = 0; i < ctr; i++){
            Role__c role = new Role__c(
                RecordTypeId = curRoleRT,
                Case__c = caseId,
                Account__c = accId,
                Application__c = appId,
                Role_Type__c = (i==0)?'Primary Applicant':''
            );
            roleList.add(role);
        }
        return roleList;
    }

    //Create test data for Incomes
    public static Income1__c createGenericIncome(string incomeRTName, String appId, Integer amount){
        Id curIncomeRT = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get(incomeRTName).getRecordTypeId();
        Income1__c income = new Income1__c(
            RecordTypeId = curIncomeRT,
            Application1__c = appId,
            //Centrelink_Income__c = (incomeRTName == CommonConstants.INCOME_RT_C)?amount:0,
            Family_Support_Income__c = (incomeRTName == CommonConstants.INCOME_RT_CS)?amount:0,
            Director_Annual_Net_Income__c = (incomeRTName == CommonConstants.INCOME_RT_CD)?amount:0,
            Investment_Income__c = (incomeRTName == CommonConstants.INCOME_RT_II)?amount:0
            //Employer_Net_Monthly_Standard_Income__c = (incomeRTName == CommonConstants.INCOME_RT_PE)?amount:0,
            //Employer_Net_Monthly_Overtime__c = (incomeRTName == CommonConstants.INCOME_RT_PE)?amount:0,
            //Self_Employed_Annual_Net_Income__c = (incomeRTName == CommonConstants.INCOME_RT_SE)?amount:0,
            //work_cover_Monthly_Net_Amount__c = (incomeRTName == CommonConstants.INCOME_RT_WIP)?amount:0
        );
        return income;
    }

    //Create test data for Expenses
    public static Expense1__c createGenericExpense(string expenseRTName, String accId, String appId, Integer amount){
        Id curExpenseRT = Schema.SObjectType.Expense1__c.getRecordTypeInfosByName().get(expenseRTName).getRecordTypeId();
        Expense1__c expense = new Expense1__c(
            RecordTypeId = curExpenseRT,
            Payment_Amount__c = amount,
            Application1__c = appId
            //Account__c = accId
        );
        return expense;
    }

    //Create test data for Assets
    public static list <Asset1__c> createGenericAsset(string assRTName, String caseId, String appId, Integer ctr){
        Id curAssRT = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName().get(assRTName).getRecordTypeId();
        list <Asset1__c> asslist = new list <Asset1__c>();

        for (Integer i = 0; i < ctr; i++){
            Asset1__c ass = new Asset1__c(
                    RecordTypeId = curAssRT,
                    //Case__c = caseId,
                    Application1__c = appId,
                    Description_Other__c = 'test description'+i
                );
            asslist.add(ass);
        }
        return asslist;
    }

    //Create test data for Liabilities
    public static list <Liability1__c> createGenericLiability(string lRTName, string caseId, string appId, integer ctr){
        Id curLiaRT = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName().get(lRTName).getRecordTypeId();
        list <Liability1__c> lialist = new list <Liability1__c>();

        for (Integer i = 0; i < ctr; i++){
            Liability1__c lia = new Liability1__c(
                    RecordTypeId = curLiaRT,
                    //Case__c = caseId,
                    Application1__c = appId,
                    Description_Other__c = 'test description'+i
                );
            lialist.add(lia);
        }
        return lialist;
    }

    //Create test data for Addresses
    public static Address__c createGenericAddress(Boolean isVerifiedByGoogle, String country, String postCode, String state, String streetName, String streetNum, String suburb, Boolean conRequiresConfirm){
        Address__c address = new Address__c(
            Address_Verified_by_Google__c = isVerifiedByGoogle,
            Country__c = country,
            Postcode__c = postCode,
            State__c = state,
            Street_Name__c = streetName,
            Street_Number__c = streetNum,
            Suburb__c = suburb,
            connective_requires_Confirmation__c = conRequiresConfirm
        );
        return address;
    }

    //Create test data for Role Addresses
    public static Role_Address__c createGenericRoleAddress(Id accountId, Boolean isCurrent, Id roleId, Id addressId){
        Role_Address__c roleaddress = new Role_Address__c(
            Account__c = accountId,
            Active_Address__c = (isCurrent)?'Current':'Previous',
            Role__c = roleId,
            Address__c = addressId
        );

        return roleaddress;
    }

    //Create test data for Case
    public static Case createGenericCase(string caseRTId, string status, string stage){
        Case cse = new Case(
            RecordTypeId = caseRTId,
            Status = status,
            Stage__c = stage
        );
        return cse;
    }

    //Create test data for Application
    public static Application__c createGenericApplication(string caseNumber, string appRTId, string caseId){
        Application__c app = new Application__c(
            Name = caseNumber,
            RecordTypeId = appRTId,
            Case__c = caseId
        );
        return app;
    }
    
    //Create test data for Business Account
    public static Account createGenericBusinessAccount(string accRTId, string accname){
        Account acc = new Account(
            RecordTypeId = accRTId,
            Name = accname
        );
        return acc;
    }

    //Create test data for Person Account
    public static Account createGenericPersonAccount(string accRTId, string fname, string lname){
        Account acc = new Account(
            RecordTypeId = accRTId,
            FirstName = fname,
            LastName = lname
        );
        return acc;
    }

    //Create test data for Role
    public static Role__c createGenericRole(string roleRTId, string accId, string caseId, string appId){
        Role__c role = new Role__c(
            RecordTypeId = roleRTId,
            Account__c = accId,
            Case__c = caseId,
            Application__c = appId,
            Role_Type__c = 'Primary Applicant'
        );
        return role;
    }

    //Create test data for Income
    public static Income1__c createGenericIncome(Id incomeRT, Id appId, String employmentSituation, String jobTitle, String incomePeriod, Double netPay){
        Income1__c income = new Income1__c(
            RecordTypeId = incomeRT,
            Application1__c = appId,
            Income_Situation__c = employmentSituation,
            Job_Title__c = jobTitle,
            Income_Period__c = incomePeriod,
            Net_Standard_Pay__c = netPay
        );
        return income;
    }

    //RDAVID - Create test data for FO Share
    public static FO_Share__c createGenericFOShare (Id foShareRT, Id roleId, Id caseId, Id appId, Id incomeId, Id expenseId,
                                                    Id assetID, Id liabilityId, Double roleShare, Double roleShareAmount, 
                                                    String description, Double monthlyAmount, String foRTString, String foType){
        FO_Share__c foShare = new FO_Share__c(
            RecordTypeId = foShareRT,
            Role__c = roleId,
            Case__c = caseId,
            Income__c = incomeId,
            Expense__c = expenseId,
            Asset__c = assetID,
            Liability__c = liabilityId,
            Role_Share__c = roleShare,
            Role_Share_Amount__c = roleShareAmount,
            Description_Information__c = description,
            Monthly_Amount__c = monthlyAmount,
            FO_Record_Type__c = foRTString,
            Type__c = foType
        );
        if (incomeId != null) foShare.Application_Income__c = appId;
        if (expenseId != null) foShare.Application_Expense__c = appId;
        if (assetID != null) foShare.Application_Asset__c = appId;
        if (liabilityId != null) foShare.Application_Liability__c = appId;

        return foShare;
    }

    //Pre-Built Test Setup covering Case, Application, Person Accounts and Roles
    public static void Case2FOSetupTemplate(){
        
        User sysadmin = TestDataFactory.create_User_SysAd();
		insert sysadmin;

		system.runAs(sysadmin){

			//Create test data for Case (x10)
			Id cseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_N_CONS_AF).getRecordTypeId(); //n: Consumer - Asset Finance
			list <Case> cselist = new list <Case>();
			for (Integer i=0; i<10; i++){
				Case cse = TestDataFactory.createGenericCase(cseRTId, CommonConstants.CASE_STATUS_NEW, CommonConstants.CASE_STAGE_OPEN);
				cselist.add(cse);
			}
			insert cselist;

			//Create test data for Application (x10)
			Id appRTId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get(CommonConstants.APP_RT_CONS_I).getRecordTypeId(); //Consumer Individual
			list <Application__c> applist = new list <Application__c>();
			for (Integer i=0; i<10; i++){
				Application__c app = TestDataFactory.createGenericApplication(cselist[i].Id, appRTId, cselist[i].Id);
				applist.add(app);

			}
			insert applist;

			//Create test data for Person Accounts (x3)
			Id accRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId(); //nm: Individual
			list <Account> pacclist = new list <Account>();
			for (Integer i=0; i<3; i++){
				Account pacc = TestDataFactory.createGenericPersonAccount(accRTId, 'testFName', 'testLName');
				pacclist.add(pacc);
			}
			insert pacclist;

			//Create test data for Roles (x30) - 3 per each Case/Application
			Id roleRTId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(CommonConstants.ROLE_RT_APP_INDI).getRecordTypeId(); //Applicant - Individual
			list <Role__c> rolelist = new list <Role__c>();
			for (Integer i=0; i<10; i++){
				for (Integer z=0; z<3; z++){
					Role__c role = TestDataFactory.createGenericRole( roleRTId, pacclist[z].Id, cselist[i].Id, applist[i].Id);
					rolelist.add(role);
				}
			}
			insert rolelist;

		}

    }
    //Create Test Location RDAVID 18/06/2018
    public static List<Location__c> createLocation(Integer count, String locname, String postcode, String suburbtown, String stateacr, String state, String country){
        List <Location__c> locList = new List <Location__c>();
        for (Integer i = 0; i < count; i++){
            Location__c loc = new Location__c(
                Name = locname,
                Postcode__c = postcode,
                Suburb_Town__c = suburbtown,
                State__c = state,
                State_Acr__c = stateacr,	
                Country__c = country
            );
            locList.add(loc);
        }
        return locList;
    }

    public static List<Partner__c> createPartner(Integer count, String rtName, Id partnerParent){
        List <Partner__c> partnerList = new List <Partner__c>();
        Id partnerRT = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
        for (Integer i = 0; i < count; i++){
            Partner__c partner = new Partner__c(
                Name = 'Test Parntner',
                Partner_Parent__c = (partnerParent != NULL) ? partnerParent : NULL,
                RecordTypeId = (partnerRT != NULL) ? partnerRT : NULL
            );
            partnerList.add(partner);
        }
        return partnerList;
    }

    public static List<CommissionCT__c> createCommission(Integer count, String rtName, Id caseId){
        List <CommissionCT__c> commList = new List <CommissionCT__c>();
        Id commRT = Schema.SObjectType.CommissionCT__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
        for (Integer i = 0; i < count; i++){
            CommissionCT__c comm = new CommissionCT__c(
                Reference__c = 'Test Reference',
                Case__c = caseId,
                RecordTypeId = (commRT != NULL) ? commRT : NULL
            );
            commList.add(comm);
        }
        return commList;
    }

    public static List<Commission_Line_Items__c> createCommissionLine(Integer count, String rtName, Id commId){
        List <Commission_Line_Items__c> commLine = new List <Commission_Line_Items__c>();
        Id commRT = Schema.SObjectType.Commission_Line_Items__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
        for (Integer i = 0; i < count; i++){
            Commission_Line_Items__c comm = new Commission_Line_Items__c(
                Commission_Parent__c = commId,
                RecordTypeId = (commRT != NULL) ? commRT : NULL
            );
            commLine.add(comm);
        }
        return commLine;
    }

    // 1.9 20/06/19 RDAVID
    public static List<Support_Request__c> createSRList(Integer count, String rtName, Id caseId, String partition){
        List <Support_Request__c> srList = new List <Support_Request__c>();
        Id srRT = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
        for (Integer i = 0; i < count; i++){
            Support_Request__c sr = new Support_Request__c(
                Case_Number__c = caseId,
                RecordTypeId = srRT,
                Partition__c = partition,
                Status__c = 'New',
                Stage__c = 'New'
            );
            srList.add(sr);
        }
        return srList;
    }

    // 2.0 4/07/19 RDAVID
    public static List<Frontend_Submission__c> creatFESubs(Integer count, Id cseId, String formType, String guid, String channel, String formLocation){
        List <Frontend_Submission__c> feList = new List <Frontend_Submission__c>();
        
        for (Integer i = 0; i < count; i++){
            Frontend_Submission__c fe = new Frontend_Submission__c(
                Positive_Form_Type__c = formType, //Quick Quote
                GUID__c = guid, //6641C95F-69CE-4E4C-B48D-BAD1E53042D5
                Channel__c = channel,//'LFPWBC',
                FE_Form_Location__c = formLocation,//'Bad Credit Loans - Banks Say No? We Say Yes'
                Case__c = cseId
            );
            feList.add(fe);
        }
        return feList;
    }

    // 2.0 24/09/19 RDAVID
    public static Trigger_Settings1__c activateAllTriggerSettingsNM (Trigger_Settings1__c triggerSettings){
        triggerSettings.Enable_Account_Add_as_Campaign_Member__c = TRUE;
        triggerSettings.Enable_Account_Assocs_in_Role__c = TRUE;
        triggerSettings.Enable_Account_Is_Delacon_Lead__c = TRUE;
        triggerSettings.Enable_Account_Sync_Name_to_Role__c = TRUE;
        triggerSettings.Enable_Account_Trigger__c = TRUE;
        triggerSettings.Enable_Address_Populate_Location__c = TRUE;
        triggerSettings.Enable_Address_Trigger__c = TRUE;
        triggerSettings.Enable_AgentWork_Trigger__c = TRUE;
        triggerSettings.Enable_Application_Primary_Applicants__c = TRUE;
        triggerSettings.Enable_Application_Sync_App_2_Case__c = TRUE;
        triggerSettings.Enable_Application_Trigger__c = TRUE;
        triggerSettings.Enable_Asset_Trigger__c = TRUE;
        triggerSettings.Enable_Attachment_Create_File_in_Box__c = TRUE;
        triggerSettings.Enable_Attachment_Trigger__c = TRUE;
        triggerSettings.Enable_Bizible_FB_Lead_UTM__c = TRUE;
        triggerSettings.Enable_Bizible_Trigger__c = TRUE;
        triggerSettings.Enable_Case_Auto_Create_Application__c = TRUE;
        triggerSettings.Enable_Case_Box_Partition_Auto_Create__c = TRUE;
        triggerSettings.Enable_Case_Calculate_Nodifi_SLA_Time__c = TRUE;
        triggerSettings.Enable_Case_Comment_Create_For_Parent__c = TRUE;
        triggerSettings.Enable_Case_Comment_New_Email_Alert__c = TRUE;
        triggerSettings.Enable_Case_Comment_Trigger__c = TRUE;
        triggerSettings.Enable_Case_Comment_Update_Latest_Comm__c = TRUE;
        triggerSettings.Enable_Case_Create_Initial_Review_SR__c = TRUE;
        triggerSettings.Enable_Case_Create_KAR_Sales_Opp__c = TRUE;
        triggerSettings.Enable_Case_Create_POS_PHL_Opp__c = TRUE;
        triggerSettings.Enable_Case_Create_SR_Manager_Review__c = TRUE;
        triggerSettings.Enable_Case_Delete_CommCT_on_Lost__c = TRUE;
        triggerSettings.Enable_Case_generate_TrustPilot_Link__c = TRUE;
        triggerSettings.Enable_Case_Log_Sectors__c = TRUE;
        triggerSettings.Enable_Case_Lookup_Fields__c = TRUE;
        triggerSettings.Enable_Case_NVM_Timezone_based_Routing__c = TRUE;
        triggerSettings.Enable_Case_Record_Stage_Timestamps__c = TRUE;
        triggerSettings.Enable_Case_Referred_Out_By_Owner__c = TRUE;
        triggerSettings.Enable_Case_Scenario_Trigger__c = TRUE;
        triggerSettings.Enable_Case_SCN_calculateSLATime__c = TRUE;
        triggerSettings.Enable_Case_SCN_recordStageTimestamps__c = TRUE;
        triggerSettings.Enable_Case_Set_User_as_Owner_on_Close__c = TRUE;
        triggerSettings.Enable_Case_SMS_Alerts__c = TRUE;
        triggerSettings.Enable_Case_Status_Stage_Funnels__c = TRUE;
        triggerSettings.Enable_Case_TP_Map_Delacon_Lead__c = TRUE;
        triggerSettings.Enable_Case_TP_Trigger__c = TRUE;
        triggerSettings.Enable_Case_Trigger__c = TRUE;
        triggerSettings.Enable_Case_Update_Commission_LI__c = TRUE;
        triggerSettings.Enable_CommissionCT_GetCaseLender__c = TRUE;
        triggerSettings.Enable_CommissionCT_Trigger__c = TRUE;
        triggerSettings.Enable_CommissionLine_GetCommission__c = TRUE;
        triggerSettings.Enable_Commission_Line_Trigger__c = TRUE;
        triggerSettings.Enable_Contact_Trigger__c = TRUE;
        triggerSettings.Enable_Expense_Trigger__c = TRUE;
        triggerSettings.Enable_FO_Auto_Create_FO_Share__c = TRUE;
        triggerSettings.Enable_FO_Delete_FO_Share_on_FO_Delete__c = TRUE;
        triggerSettings.Enable_FOShare_Rollup_to_Role__c = TRUE;
        triggerSettings.Enable_FOShare_Trigger__c = TRUE;
        triggerSettings.Enable_Frontend_Sub_Complete_Load__c = TRUE;
        triggerSettings.Enable_Front_End_Submission_Trigger__c = TRUE;
        triggerSettings.Enable_Frontend_Sub_SetNVMContact__c = TRUE;
        triggerSettings.Enable_Income_Check_Employment__c = TRUE;
        triggerSettings.Enable_Income_Parent_Role_Updates__c = TRUE;
        triggerSettings.Enable_Income_Populate_Sole_Trader__c = TRUE;
        triggerSettings.Enable_Income_Role_Employment_Status__c = TRUE;
        triggerSettings.Enable_Income_Trigger__c = TRUE;
        triggerSettings.Enable_Lender_Automation_Call_Lender__c = TRUE;
        triggerSettings.Enable_Liability_Trigger__c = TRUE;
        triggerSettings.Enable_NVM_Call_Summary_Trigger__c = TRUE;
        triggerSettings.Enable_NVM_CS_Pull_Call_Purpose__c = TRUE;
        triggerSettings.Enable_Partner_Box_Auto_Create__c = TRUE;
        triggerSettings.Enable_Partner_Sync_Account_Contact__c = TRUE;
        triggerSettings.Enable_Partner_Trigger__c = TRUE;
        triggerSettings.Enable_Referral_Company_Trigger__c = TRUE;
        triggerSettings.Enable_Relationship_Assocs_in_Account__c = TRUE;
        triggerSettings.Enable_Relationship_Spouse_Duplicate__c = TRUE;
        triggerSettings.Enable_Relationship_Trigger__c = TRUE;
        triggerSettings.Enable_Role_Address_Check_Residency__c = TRUE;
        triggerSettings.Enable_Role_Address_Populate_Address__c = TRUE;
        triggerSettings.Enable_Role_Address_Toggle_Current__c = TRUE;
        triggerSettings.Enable_Role_Address_Trigger__c = TRUE;
        triggerSettings.Enable_Role_Assocs_in_Application__c = TRUE;
        triggerSettings.Enable_Role_Auto_Populate_Case_App__c = TRUE;
        triggerSettings.Enable_Role_Create_Case_Touch_Point__c = TRUE;
        triggerSettings.Enable_Role_Rollup_to_Application__c = TRUE;
        triggerSettings.Enable_Role_To_Account_Propagation__c = TRUE;
        triggerSettings.Enable_Role_Toggle_Primary_Applicant__c = TRUE;
        triggerSettings.Enable_Role_Trigger__c = TRUE;
        triggerSettings.Enable_Sector_Trigger__c = TRUE;
        triggerSettings.Enable_SR_Call_Request_NVM_Routing__c = TRUE;
        triggerSettings.Enable_Support_Request_Create_CAR_SR__c = TRUE;
        triggerSettings.Enable_Support_Request_Trigger__c = TRUE;
        triggerSettings.Enable_Sync_Referral_Company_and_Partner__c = TRUE;
        triggerSettings.Enable_Task_Trigger__c = TRUE;
        triggerSettings.Enable_Task_Update_Case_Attempt__c = TRUE;
        triggerSettings.Enable_Triggers__c = TRUE;
        return triggerSettings;
    }

    // 2.0 24/09/19 RDAVID
    public static Process_Flow_Definition_Settings1__c activateAllProcessFlowSettingsNM (Process_Flow_Definition_Settings1__c processFlowSettings){
        processFlowSettings.Enable_CA_Refered_out_Cases__c = TRUE;
        processFlowSettings.Enable_Case_Assignment__c = TRUE;
        processFlowSettings.Enable_Case_Email_Alerts__c = TRUE;
        processFlowSettings.Enable_Case_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_Case_NVM_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_Case_Scenario_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_Case_SMS_Alerts__c = TRUE;
        processFlowSettings.Enable_Case_Webmerge__c = TRUE;
        processFlowSettings.Enable_CA_Training_Team_Cases__c = TRUE;
        processFlowSettings.Enable_FE_NOD_Flow_v3__c = TRUE;
        processFlowSettings.Enable_Partner_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_Purchase_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_Role_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_Role_SMS_Alerts__c = TRUE;
        processFlowSettings.Enable_Sector_SLA_Flow_Definitions__c = TRUE;
        processFlowSettings.Enable_SR_Call_Request_NVM__c = TRUE;
        processFlowSettings.Enable_Support_Request_Email_Alerts__c = TRUE;
        processFlowSettings.Enable_Support_Request_Flow_Definitions__c = TRUE;
        return processFlowSettings;
    }
}