/*
Author: Original: Jesfer Baculod (08/10/2017)
History: 08/10/2017 - Created based from LoanDocumentRequestForm on Opportunity
         08/16/2017 - Referenced page to new Loan Document Request Form CT object
         TODO: Finalize Loan Type and validation, identify Stage to be Set
Purpose: This Visualforce page will create Loan Document Request Form and save it on Case record.
*/

public with sharing class LoanDocumentRequestFormCase {

// Property Declaration

    public Case cseObj {get;set;}
    public Id cseId {get;set;}
    //public Account accObj {get;set;}
    public Loan_Document_Request_Form_CT__c loanObj {get;set;}
    public String notesToLender {get;set;}
    public String notesToSupport {get;set;}
    public Boolean payoutValidMin5Days {get;set;}
    public Boolean payoutLetterInFile {get;set;}
    public Boolean receivedInsuranceWaiver {get;set;}
    public String insurancePackage {get;set;}
    public String insurancePackageNotesCOMPULSORY {get;set;}
    public Boolean figuresAreCorrect {get;set;}
    public boolean vendorDetailsCorrect {get;set;}
    public Decimal brokerage {get;set;}
    public String gapProvider {get;set;}
    public Decimal gapPremium {get;set;}
    public string gapProduct {get;set;}
    public String lpiCoverOption {get;set;}
    public String lpiCoverType {get; set;}
    public Decimal lpiamount {get; set;}
    public String warrantyProvider {get;set;}
    public String warrantyProductName {get;set;}
    public String warrantyTerm {get;set;}
    public Decimal warrantyPremium {get;set;}
    private String str_cse_query {get;set;}
    
	// Variable Declaration 
    
    public Boolean errorMessages;    

    //private final string OPP_STAGE_LOANDOCSREQ = Label.Loan_Document_Request_Form_Set_Opp_Stage; //Loan Docs Requested

    //Constructor 
    public LoanDocumentRequestFormCase(ApexPages.StandardController con) {
        cseId = con.getRecord().Id;
        
        errorMessages = true;
        cseObj = new Case();

        //Get all fields from Case
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Case.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        str_cse_query = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues){
           // Continue building your dynamic query string
           str_cse_query += s.getDescribe().getName() + ',';
        }
        //str_cse_query = str_cse_query.subString(0, str_cse_query.length() - 1); // Trim last comma
        str_cse_query+= ' Dealer_Vendor__r.Name, Dealer_Vendor__r.Business_Phone_Number__c, Dealer_Vendor__r.Address_Line_1__c, Dealer_Vendor__r.Suburb__c, Dealer_Vendor__r.Postcode__c, Dealer_Vendor__r.State__c, Dealer_Contact__r.Name, Dealer_Contact__r.Email, Dealer_Contact__r.MobilePhone, Vendor_Contact__r.Name, Vendor_Contact__r.Email, Vendor_Contact__r.MobilePhone, Vendor_Contact__r.Phone, Vendor_Contact__r.Street_Number__c, Vendor_Contact__r.Street_Type__c, Vendor_Contact__r.Postcode__c, Vendor_Contact__r.State__c, Vendor_Contact__r.Country__c, Vendor_Contact__r.Suburb__c, RecordType.Name';
        str_cse_query+= ' FROM Case Where Id =: cseId';
        //Retrieve current Case
        cseObj = database.query(str_cse_query);

        loanObj = new Loan_Document_Request_Form_CT__c();
    }

    public PageReference refreshCaseDetails(){
        //Mainly refreshing Vendor Details and other Case data
        cseObj = database.query(str_cse_query);
        return null;
    }

    
    public List<SelectOption> getinsurancePackageSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from Insurance Package picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form_CT__c.Insurance_Package__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
  
    public List<SelectOption> getgapProviderSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from GAP Provide picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form_CT__c.GAP_Provider__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getlipCoverOptionSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LPI Cover Option picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form_CT__c.LPI_Cover_Option__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getlpiCoverTypeSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LPI Cover Type picklist\
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form_CT__c.LPI_Cover_Type__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getwarrantyProvidersp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from Warranty Provider Type picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form_CT__c.Warranty_Provider__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getwarrantyTermSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LPI Cover Type picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form_CT__c.Warranty_Term__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public PageReference validateLdr(){

        string curProfileId = UserInfo.getProfileId();
        
        //Validate these fields as these are needed when updating Stage to Loan Docs Requested
        if(cseObj.Loan_Type__c != 'Personal Loan' && !curProfileId.contains(Label.Profile_ID_Used_Cars_Adelaide) && !curProfileId.contains(Label.Profile_ID_Sales_BDM)  ){             
            if(cseObj.Internal_Customer_Rating__c == null){ //Insurance Rating
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_34));
            }
            if(cseObj.Base_Rate__c == null){    
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_35));
            }
        }
        if (cseObj.Lender__c == 'ANZ' && cseObj.RecordType.Name == 'Consumer Loan'){
            if (cseObj.Residential_Reference_Completed__c == false){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_36 + ' on Case.'));
            }
            if (cseObj.Employment_Reference_Completed__c == false){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_37 + ' on Case.'));
            }
        }

        //********************************* Docs Request Checklist *****************************************

        if(cseObj.Approval_Conditions__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_1));
        }
        if(notesToLender == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_2)); 
        }
        if(notesToSupport == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_3));
        }
        if(receivedInsuranceWaiver == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_4));
        }
        if(insurancePackage == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_5));
        }
        if(insurancePackageNotesCOMPULSORY == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_6));
        }
        
        //********************************* Finance Details ************************************************
        
        if(cseObj.Payout_Amount__c >0 && cseObj.Payout_To__c == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_7));
        }
        if(cseObj.Payout_Amount__c >1 && payoutValidMin5Days == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_8));
        }
        if(cseObj.Payout_Amount__c>1 && payoutLetterInFile == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_9));
        }
        if(cseObj.Lender__c == 'ANZ' && (LoanObj.Brokerage__c == null || LoanObj.Brokerage__c == 0 )){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_10));
        }
        if(cseObj.Lender__c == 'Liberty' || cseObj.Lender__c == 'Macquarie' || cseObj.Lender__c == 'Pepper'){
            if(cseObj.Trail__c == null || cseObj.Trail__c == 0 ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_11));
            }
        }
        if(cseObj.Loan_Rate__c == null || cseObj.Loan_Rate__c == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_12));
        }
        if(cseObj.Application_Fee__c == null || cseObj.Application_Fee__c == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_13));
        }
        if(figuresAreCorrect == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_14));
        }
        if(cseObj.Asset_Type__c == 'Heavy Vehicles' 
            && (cseObj.Asset_Sub_Type__c == 'Rigid Truck > 4.5t' || cseObj.Asset_Sub_Type__c == 'Trailer (truck)') 
            && (cseObj.Gross_Vehicle_Mass__c == null || cseObj.Gross_Vehicle_Mass__c == 0)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_39));
        }
        if(cseObj.Loan_Type__c != 'Personal Loan' && cseObj.Vehicle_Purchase_Price__c  != null){
            if (cseObj.Cash_Deposit__c == null) cseObj.Cash_Deposit__c = 0;
            if (cseObj.Trade_In_Amount__c == null) cseObj.Trade_In_Amount__c = 0;
            if (cseObj.Payout_Amount__c == null) cseObj.Payout_Amount__c = 0;
            cseObj.Required_Loan_Amount__c = ((cseObj.Vehicle_Purchase_Price__c - cseObj.Cash_Deposit__c - cseObj.Trade_In_Amount__c) + (cseObj.Payout_Amount__c));
        }
        

        //****************************** Insurance Details *********************************

        if(gapProvider == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_16));
        }
        if(gapProvider != '-- no Cover --' && gapProvider != null){
            if(gapPremium == null || gapPremium == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_17));
            }
        }
        if(lpiCoverOption == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_18));
        }
        if(lpiCoverOption != '-- no Cover --' && lpiCoverOption != null){
            if(lpiCoverType == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_19));
            }
        }
        if(warrantyProvider == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_20));
        }
        if(warrantyProvider != '-- no Cover --' && warrantyProvider != null){
            if(warrantyProductName == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_21));
            }
        }
        if(warrantyProvider != '-- no Cover --' && warrantyProvider != null){
            if(warrantyTerm == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_22));
            }
        }
        if(warrantyProvider != '-- no Cover --' && warrantyProvider != null){
            if(warrantyPremium == null || warrantyPremium == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_23));
            }
        }

        //************************* Vendor Details ***********************************
        
        if(cseObj.Loan_Type__c != 'Personal Loan'){
            if (vendorDetailsCorrect == false){
                ApexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_38));
            }
            /*
            if(cseObj.Dealer_Contact_Person__c == null){ //cseObj.Sale_Type__c == 'Dealer' && 
                ApexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_24));
            }
            if(cseObj.Dealer_Name__c == null || cseObj.Dealer_Name__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_25));
            }
            if(cseObj.Dealer_Phone__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_28));
            } 
            if(cseObj.Dealer_Address__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_26));
            }
            if(cseObj.Dealer_Email__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_27));
            }
            */
        } 

        //********************************* Primary Asset Details Validations ***************************************    

        if(cseObj.Loan_Type__c != 'Personal Loan'){
            if(cseObj.Vehicle_Vin__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_29));
            }
            if(cseObj.Vehicle_Type__c == 'Motor Vehicle' || cseObj.Vehicle_Type__c == 'Caravan' || cseObj.Vehicle_Type__c == 'Motor Cycle'){    
                System.debug('vinLength===========>'+cseObj.Vehicle_Vin__c.length());
                if( cseObj.Vehicle_Vin__c != null && cseObj.Vehicle_Vin__c.Length() <17){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'VIN length is'+' '+cseObj.Vehicle_Vin__c.Length()+','+' '+'It should be 17 characters long'));
                }
            }
            if(cseObj.Vehicle_Type__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_30));
            }
            if(cseObj.Vehicle_Year__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_31));
            }
            if(cseObj.Vehicle_Make__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_32));
            }
            if(cseObj.Vehicle_Model__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_33));
            }
        }

        //************************** Final check for Error Messages *******************************     
        
        if(!ApexPages.hasMessages()){
            errorMessages = ApexPages.hasMessages();
            System.debug('errorMessages==============>'+errorMessages);
            try{
                update cseObj;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.Loan_Document_Request_Form_Info_Msg_1));
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getLineNumber()+' '+e.getMessage()));   
            }
        }
        return null;
    }
        
    //Save Updates on Case
    public PageReference saveUpdates(){
        Savepoint sp = Database.setSavepoint();
        try{
            update cseObj;    
        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getLineNumber()+ ' ' + e.getMessage()));
        }
        return null;
    }
       
    public PageReference submit(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(errorMessages == true){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Loan_Document_Request_Form_Info_Msg_2));
                return null;
            }else{ 
                //create a new Loan Document Request Form with Case field mapping
                Loan_Document_Request_Form_CT__c creloanObj = new Loan_Document_Request_Form_CT__c(
                        Client_Name__c = cseObj.Id,
                        Application_Type__c = cseObj.Application_Type__c,
                        // Docs Request Checklist
                        Approval_Conditions_Notes__c = cseObj.Approval_Conditions__c,
                        Notes_to_Lender__c = notesToLender,
                        Notes_To_Support__c = notesToSupport,
                        Payout_Valid_min_5_days__c = payoutValidMin5Days,
                        Payout_Letter_in_File__c = payoutLetterInFile,
                        Received_Insurance_Waiver__c = receivedInsuranceWaiver,
                        Insurance_Package__c = insurancePackage,
                        Insurance_Package_Notes_COMPULSORY__c = insurancePackageNotesCOMPULSORY,
                        // Finance Details   
                        Vehicle_Price__c = cseObj.Vehicle_Purchase_Price__c,
                        Opp_Loan_Type__c = cseObj.Loan_Type__c,
                        Loan_Term2__c = cseObj.Loan_Term__c,
                        Cash_Deposit__c = cseObj.Cash_Deposit__c,
                        Interest_Rate__c = cseObj.Loan_Rate__c,
                        Balloon_N__c = cseObj.Balloon__c,
                        Brokerage__c = loanObj.Brokerage__c,
                        Trade_In_Amount__c = cseObj.Trade_In_Amount__c,
                        App_Fee__c = cseObj.Application_Fee__c,
                        Trade_In_Description__c = cseObj.Trade_In_Description__c,
                        Trail_N__c = cseObj.Trail__c,
                        Payout__c = cseObj.Payout_Amount__c,
                        Payout_To__c = cseObj.Payout_To__c,
                        Required_Loan_Amount__c = cseObj.Required_Loan_Amount__c,
                        Figures_are_correct__c = figuresAreCorrect,
                        // Insurance Details:
                        GAP_Provider__c = gapProvider,
                        GAP_Premium__c = gapPremium,
                        Product__c = LoanObj.Product__c, //added 7/20/17 requested by Tim Wells
                        LPI_Cover_Option__c = lpiCoverOption,
                        LPI_Cover_Type__c = lpiCoverType,
                        LPI_Amount__c = lpiamount, //added 7/19/17 requested by Tim Wells
                        Warranty_Provider__c = warrantyProvider,
                        Warranty_Product_Name__c = warrantyProductName,
                        Warranty_Term__c = warrantyTerm,
                        Warranty_Premium__c = warrantyPremium,
                        // Vendor Details
                        Sale_Type__c = cseObj.Sale_Type__c,
                        Vendor_Phone__c = cseObj.Dealer_Vendor__r.Business_Phone_Number__c,
                        Dealer_Contact_Person__c = cseObj.Dealer_Contact__r.Name,
                        Vendor_Name__c = cseObj.Dealer_Vendor__r.Name,
                        Dealer_Contact_Mobile__c = cseObj.Dealer_Contact__r.MobilePhone,
                        Vendor_Address__c = cseObj.Dealer_Vendor__r.Address_Line_1__c + ' ' + cseObj.Dealer_Vendor__r.Suburb__c + ' ' + cseObj.Dealer_Vendor__r.Postcode__c + ' ' + cseObj.Dealer_Vendor__r.State__c + ' ' + cseObj.Dealer_Vendor__r.State__c,
                        Vendor_Email__c = cseObj.Dealer_Contact__r.Email,
                        /* Sale_Type__c = cseObj.Sale_Type__c,
                        Vendor_Phone__c = cseObj.Dealer_Phone__c,
                        Dealer_Contact_Person__c = cseObj.Dealer_Contact_Person__c,
                        Vendor_Name__c = cseObj.Dealer_Name__c,
                        Dealer_Contact_Mobile__c = cseObj.Dealer_Mobile__c,
                        Vendor_Address__c = cseObj.Dealer_Address__c,
                        Vendor_Email__c = cseObj.Dealer_Email__c, */
                        //Primary Asset Details
                        Asset_Type__c = cseObj.Vehicle_Type__c,
                        Transmission__c = cseObj.Vehicle_Transmission__c,
                        Year__c = cseObj.Vehicle_Year__c,
                        Body_Type__c = cseObj.Vehicle_Body_Type__c,
                        Make__c = cseObj.Vehicle_Make__c,
                        Colour__c = cseObj.Vehicle_Colour__c,
                        Model__c = cseObj.Vehicle_Model__c,
                        Fuel_Type__c = cseObj.Vehicle_Fuel_Type__c,
                        Variant__c = cseObj.Varient__c,
                        Odometer__c = cseObj.Vehicle_Odometer_Reading__c,
                        Vin__c = cseObj.Vehicle_VIN__c,
                        Date_First_Registered__c = cseObj.Vehicle_Date_First_Registered__c,
                        Engine_No__c = cseObj.Engine_No__c,
                        Vehicle_Registration_Number__c = cseObj.Vehicle_Registration_Number__c,
                        Gross_Vehicle_Mass__c = cseObj.Gross_Vehicle_Mass__c,
                        Vehicle_Registration_State__c = cseObj.Vehicle_Registration_State__c
                    );
                //loanObj.Loan_Type__c = cseObj.Loan_Product__c;
                if (cseObj.Sale_Type__c == 'Private'){
                    creloanObj.Vendor_Email__c = cseObj.Vendor_Contact__r.Email;
                    creloanObj.Dealer_contact_Person__c = cseObj.Vendor_Contact__r.Name;
                    creloanObj.Dealer_Contact_Mobile__c = cseObj.Vendor_Contact__r.MobilePhone;
                    creloanObj.Vendor_Phone__c = cseObj.Vendor_Contact__r.Phone;
                    creloanObj.Vendor_Name__c = cseObj.Vendor_Contact__r.Name;
                    creloanObj.Vendor_Address__c = cseObj.Vendor_Contact__r.Street_Number__c + ' ' + cseObj.Vendor_Contact__r.Street_Type__c + cseObj.Vendor_Contact__r.Suburb__c + ' ' + cseObj.Vendor_Contact__r.Postcode__c + ' ' + cseObj.Vendor_Contact__r.State__c + ' ' + cseObj.Vendor_Contact__r.Country__c;
                }
                insert creloanObj;     
                system.debug('@@creloanObj:'+creloanObj);

                //Update Stage to Loan Docs Requested
                //cseObj.Stage__c = OPP_STAGE_LOANDOCSREQ;
                //update cseObj;

                //redirect page to created LDR
                return new PageReference('/'+creloanObj.Id);
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getLineNumber()+ ' ' + e.getMessage()));
            return null;
        }

    }
    
    public PageReference CaseId(){
        return new PageReference('/'+cseId);
    }
    
}