/*
Author:  	Steven Herod, Feb 2014
Purpose:	Responsible for making the actual call out to Veda.
*/

public with sharing class VedaAPI {
	 
	public static VedaDTO.IndividualResponse  individualProductCall(VedaDTO.IndividualRequest individualRequest) {
	
		HttpRequest request = new HttpRequest();
		request.setMethod('POST');		
		request.setEndpoint(CustomSettingUtil.getVedaConfigValue('EndPointURL'));		
		request.setHeader('Content-Type', 'text/xml');
		request.setBody(individualRequest.toXml());
		System.debug(individualRequest.toXml()); 
		Http http = new Http();
		HttpResponse response = http.send(request);
		System.debug(response.getStatusCode());
		System.debug(response.getBody());
		if (response.getStatusCode() == 200) {
			return new VedaDTO.IndividualResponse().fromXml(response.getBody());
		} else {
			throw new VedaRequestException('Received error response from Veda: ' + response.getStatusCode() + ' ' + response.getBody());
		}
		

		
	}

	public class VedaRequestException extends Exception {}
	
	public class VedaResponseException extends Exception {}
	
}