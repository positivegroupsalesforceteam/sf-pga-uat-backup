/** 
* @FileName: SupportProfileSwitcherController 
* @Description: Apex Controller for SupportProfileSwitcher lightning component
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 9/04 RDAVID Created Class
**/ 

public with sharing class SupportProfileSwitcherController {

    @AuraEnabled
    public static List<User> getUsers(String nameFilterString) {
        String composedFilter = sanitizeQueryString(nameFilterString);
        System.debug('composedFilter== '+composedFilter);
        List<User> users =
                [SELECT Id,Profile.Name, UserRole.Name , Name, IsActive, Box_Configured__c FROM User WHERE Profile.Name LIKE '%Support%' AND Profile.Name LIKE '%NM -%' AND Name LIKE : composedFilter AND IsActive = TRUE ORDER BY Name ASC];
        //Add isAccessible() check
        return users;
    }
    
    static String sanitizeQueryString(String aQuery) {
        if (aQuery == null) return '%';
        
        String trimmedQuery = aQuery.trim();
        if (trimmedQuery.length() == 0) return '%';
        return '%' + trimmedQuery.replaceAll('\\W+', '%') + '%';
    }

    @AuraEnabled
    public static Map<String,Support_Profile_Switcher_Setting__mdt> getSPSettingMetaMap() {
        Map<String,Support_Profile_Switcher_Setting__mdt> spSettingMap = new Map<String,Support_Profile_Switcher_Setting__mdt>();
        
        for(Support_Profile_Switcher_Setting__mdt sps : [SELECT Id, MasterLabel, Active__c, Profile__c, Profile_Id__c, Role__c, Role_Id__c FROM Support_Profile_Switcher_Setting__mdt WHERE Active__c = TRUE]){
            if(sps.Active__c){
                spSettingMap.put(sps.MasterLabel,sps);
            }
        }
        return spSettingMap;
    }
    
    @AuraEnabled
    public static String switchUserProfile(User u, String switchStr){
        System.debug('user-->'+u);

        Map<String,Support_Profile_Switcher_Setting__mdt> spSettingMetaList = getSPSettingMetaMap();
        
        if(spSettingMetaList.containsKey(switchStr)){
            u.ProfileId = Id.valueOf(spSettingMetaList.get(switchStr).Profile_Id__c);
            u.UserRoleId = Id.valueOf(spSettingMetaList.get(switchStr).Role_Id__c);
            try {
                update u;
                return 'true;'+spSettingMetaList.get(switchStr).Profile__c;
            }        
            catch(Exception e){
                return 'false;false';
            }
        }
        else return 'false;false';
    }
}