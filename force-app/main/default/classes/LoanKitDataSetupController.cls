public class LoanKitDataSetupController 
{
    //--------- Public Properties ----------//
    public integer intSelectedStep {get; Set;}
    public Steps objSteps {get; Set;}
    public string strErrorMsg {get; Set;}
    
    public Account objAccount {get; Set;}
    public Opportunity objOpportuninty {get; Set;}
    public List<wrapApplicant2> lstApplicant2 {get; Set;}
    public List<wrapAssets> lstAssets {get; Set;}
    public List<wrapLiability> lstLiability {get; Set;}
    public List<wrapEmployee> lstEmployee {get; Set;}
    public List<LK_Asset_Liability_Share__c> lstAstShare {get; Set;}
    public List<LK_Asset_Liability_Share__c> lstLbltShare {get; Set;}
    //------- End Public Properties --------//
    
    //-------- Class Variables --------//
    public Boolean createAll;
    private List<Applicant_2_Mapping__c> lstApplicantToUpsert;
    //----- End Class Variables -------//
    
    /**
        * @Constructor 
        * @param controller 
        * @return LoanKitDataSetupController 
    */
    public LoanKitDataSetupController(ApexPages.StandardController controller) 
    {
        if (controller != null) 
        {
            objOpportuninty = (Opportunity)controller.getRecord();
            initializeData(objOpportuninty.id);
        }
    }
    
    /**
        * @description: Method used to initialize public properties and fetch data.
        * @param OpportunityId 
    */
    public void initializeData(string OpportunityId)
    {
        objSteps = new Steps();
        createAll = false;
        intSelectedStep = objSteps.selectApplicant2;
        lstApplicant2 = new List<wrapApplicant2>();
        lstAssets = new List<wrapAssets>();
        lstLiability = new List<wrapLiability>();
        lstEmployee = new List<wrapEmployee>();
        lstLbltShare = new List<LK_Asset_Liability_Share__c>();
        lstAstShare = new List<LK_Asset_Liability_Share__c>();
        
        
        if (OpportunityId != null)
        {
            //---------- Fetching Opportunity data and related list object data -----------------// 
            string  strSOQLOpp =  'select Id, AccountId, CreatedDate, Loankit_Application_ID__c '; 
            strSOQLOpp += ',(select ' + String.join( new List<String>(Applicant_2_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Applicant_2_Mapping__r)';
            strSOQLOpp += ',(select ' + String.join( new List<String>(Liability_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Liability_Mapping__r)';
            strSOQLOpp += ',(select ' + String.join( new List<String>(Employer_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Employers_Mapping__r)';
            strSOQLOpp += ',(select ' + String.join( new List<String>(Asset_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Asset_Mapping__r)';
            strSOQLOpp += ',(select ' + String.join( new List<String>(LK_Asset_Liability_Share__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Asset_Liability_Share__r)';
            strSOQLOpp += 'from Opportunity ';
            strSOQLOpp += 'where Id =: OpportunityId';
            
            objOpportuninty = Database.query(strSOQLOpp);
            //------------ End Fetching Opportunity data and related list object data -----------// 
            
            if (objOpportuninty != null)
            {
                
                
                //---------- Fetching Account data and related list object data -----------------//
                string  strSOQLAcc =  ' select ' + String.join( new List<String>(Account.SObjectType.getDescribe().fields.getMap().keySet()), ',');
                strSOQLAcc += ' ,(select ' + String.join( new List<String>(Applicant_2__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Applicant_2__r)';
                strSOQLAcc += ' ,(select ' + String.join( new List<String>(Assets__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Assets__r)';
                strSOQLAcc += ' ,(select ' + String.join( new List<String>(Employee__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Employment__r)';
                strSOQLAcc += ' ,(select ' + String.join( new List<String>(Liability__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Liabilities__r)';
                strSOQLAcc += ' from Account ';
                strSOQLAcc += ' where Id = \''+ objOpportuninty.AccountId +'\'';
                
                List<Account> lstAccount = Database.query(strSOQLAcc);
                if(lstAccount != null && lstAccount.size() > 0)
                { 
                    setupdata(objOpportuninty, lstAccount[0]);
                    objAccount = lstAccount[0];
                }
                //------------ End Fetching Account data and related list object data -----------// 
            }
        }
    }
    
    /**
        * @description : Method used auto create data in case of 1st opportunity
    */
    public void AutoCreateData()
    {       
        List<Opportunity> lstPreviousOpp = [select Id, CreatedDate from Opportunity where AccountId =: objOpportuninty.AccountId and CreatedDate <: objOpportuninty.CreatedDate];
        if (!(lstPreviousOpp != null && lstPreviousOpp.size() > 0))
        {
            createAll = true;
            SaveApplicant_2();
            SaveAssets(); 
            SaveLiability();
            SaveEmployee(); 
            intSelectedStep = objSteps.selectAstLbltShare;
        }
        
        if(objOpportuninty != null && objOpportuninty.Loankit_Application_ID__c != null)
        { 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'This application is already sent to Loankit.');
            ApexPages.addMessage(myMsg);
            intSelectedStep = objSteps.showSuccessOrError;
        }
    }
    
    /**
        * @description : Method used to save Applicant2 data
    */
    public void SaveApplicant_2()
    {
        if(lstApplicant2 != null && lstApplicant2.size() > 0 )
        {
            lstApplicantToUpsert = new List<Applicant_2_Mapping__c>();
            
            //List of Applicants need to be deleted
            List<Applicant_2_Mapping__c> lstApplicantToDelete = new List<Applicant_2_Mapping__c>();
            set<Id> setApp2Mapping = new set<Id>();
            for(wrapApplicant2 objWrapApp2 : lstApplicant2) 
            {
                if((objWrapApp2.isSelected != null && objWrapApp2.isSelected) || createAll)
                {
                    Map<String, Schema.SObjectField> mapMainObject = Schema.SObjectType.Applicant_2__c.fields.getMap();
                    Map<String, Schema.SObjectField> mapMappingObject = Schema.SObjectType.Applicant_2_Mapping__c.fields.getMap();
                    
                    Applicant_2_Mapping__c objApp2 = new Applicant_2_Mapping__c();
                    if(objWrapApp2.objApplicant2Map != null && objWrapApp2.objApplicant2Map.Id != null) objApp2 = new Applicant_2_Mapping__c(Id = objWrapApp2.objApplicant2Map.Id);
                    
                    for(String fieldName : mapMappingObject.keySet()) 
                    if(mapMappingObject.get(fieldName).getDescribe().isUpdateable() && mapMainObject.containsKey(fieldName)) objApp2.put(fieldName, objWrapApp2.objApplicant2.get(fieldName));
                    
                    objApp2.Applicant_2__c = objWrapApp2.objApplicant2.Id;
                    objApp2.Opportunity__c = objOpportuninty.Id;
                    lstApplicantToUpsert.add(objApp2);
                }
                else if(objWrapApp2.objApplicant2Map != null && objWrapApp2.objApplicant2Map.Id != null)
                {
                    lstApplicantToDelete.add(objWrapApp2.objApplicant2Map);
                    setApp2Mapping.add(objWrapApp2.objApplicant2Map.Id);
                }
            }
            if(lstApplicantToUpsert.size() > 0 ) upsert lstApplicantToUpsert;
            
            //Deleting unselected Applicants if exists
            if(setApp2Mapping.size() > 0 )
            {
                list<Employer_Mapping__c> lstEmpMap = [select Id from Employer_Mapping__c where Applicant_2_Mapping__c in : setApp2Mapping ];
                if(lstEmpMap != null && lstEmpMap.size() > 0) delete lstEmpMap;             
            }
            if(lstApplicantToDelete.size() > 0 )  delete lstApplicantToDelete;
        }
        
        //Setting-up employement data needs to show.
        EmpDataSetup();
        
        //Initializing next wizard display
        intSelectedStep = objSteps.selectAssets;
    }
    
    /**
        * @description : Method used to save Asset data
    */
    map<Id,Id> mapAstMap;
    public void SaveAssets()
    {
        mapAstMap = new map<Id,Id>();
        if(lstAssets != null && lstAssets.size() > 0 )
        {
            List<Asset_Mapping__c> lstAssetToUpsert = new List<Asset_Mapping__c>();
            List<Asset_Mapping__c> lstAssetToDelete = new List<Asset_Mapping__c>();
            
            for(wrapAssets objAssets : lstAssets) 
            {
                if((objAssets.isSelected != null && objAssets.isSelected)  || createAll)
                {
                    Map<String, Schema.SObjectField> mapMainObject = Schema.SObjectType.Assets__c.fields.getMap();
                    Map<String, Schema.SObjectField> mapMappingObject = Schema.SObjectType.Asset_Mapping__c.fields.getMap();
                    
                    Asset_Mapping__c obAstMap = new Asset_Mapping__c();
                    if(objAssets.objAssetmapping != null && objAssets.objAssetmapping.Id != null) obAstMap = new Asset_Mapping__c(Id = objAssets.objAssetmapping.Id);
                    
                    for(String fieldName : mapMappingObject.keySet()) 
                    if(mapMappingObject.get(fieldName).getDescribe().isUpdateable() && mapMappingObject.get(fieldName).getDescribe().getType() != Schema.DisplayType.Reference && mapMainObject.containsKey(fieldName)) obAstMap.put(fieldName, objAssets.objAsset.get(fieldName));
                    
                    obAstMap.Opportunity__c = objOpportuninty.Id;
                    obAstMap.Assets__c = objAssets.objAsset.Id;
                    
                    lstAssetToUpsert.add(obAstMap);
                }
                else if(objAssets.objAssetmapping != null && objAssets.objAssetmapping.Id != null) lstAssetToDelete.add(objAssets.objAssetmapping);
            }
            if(lstAssetToUpsert.size() > 0 )
            {
                upsert lstAssetToUpsert;
                for(Asset_Mapping__c obj : lstAssetToUpsert) mapAstMap.put(obj.Assets__c,obj.Id);
            }
            
            //Deleting unselected Assets if exists
            if(lstAssetToDelete .size() > 0 ) delete lstAssetToDelete;
        }
        
        //Initializing next wizard display
        intSelectedStep = objSteps.selectLiability;
    }
    
    /**
        * @description : Method used to save Liablity Data
    */
    public void SaveLiability()
    {
        if(lstLiability != null && lstLiability.size() > 0 )
        {
            List<Liability_Mapping__c> lstLiabilityToUpsert = new List<Liability_Mapping__c>();
            List<Liability_Mapping__c> lstLiabilityToDelete = new List<Liability_Mapping__c>();
            
            for(wrapLiability objLiability : lstLiability) 
            {
                if((objLiability.isSelected != null && objLiability.isSelected)  || createAll)
                {
                    Map<String, Schema.SObjectField> mapMainObject = Schema.SObjectType.Liability__c.fields.getMap();
                    Map<String, Schema.SObjectField> mapMappingObject = Schema.SObjectType.Liability_Mapping__c.fields.getMap();
                    
                    Liability_Mapping__c obLiabilityMap = new Liability_Mapping__c();
                    
                    if(objLiability.objLiabilitymapping !=null && objLiability.objLiabilitymapping.Id != null) obLiabilityMap = new Liability_Mapping__c(Id = objLiability.objLiabilitymapping.Id);
                    
                    for(String fieldName : mapMappingObject.keySet()) 
                    if(mapMappingObject.get(fieldName).getDescribe().isUpdateable()&& mapMappingObject.get(fieldName).getDescribe().getType() != Schema.DisplayType.Reference  && mapMainObject.containsKey(fieldName)) obLiabilityMap.put(fieldName, objLiability.objLiability.get(fieldName));
                    
                    obLiabilityMap.Liability__c = objLiability.objLiability.Id;
                    obLiabilityMap.Opportunity__c = objOpportuninty.Id;
                    if(mapAstMap != null && mapAstMap.ContainsKey(objLiability.objLiability.Mortgage_Loan_For__c)) obLiabilityMap.Mortgage_Loan_For_Map__c = mapAstMap.get(objLiability.objLiability.Mortgage_Loan_For__c);
                    
                    lstLiabilityToUpsert.add(obLiabilityMap);
                }
                else if(objLiability.objLiabilitymapping !=null && objLiability.objLiabilitymapping.Id != null) lstLiabilityToDelete.add(objLiability.objLiabilitymapping);
            }
            if(lstLiabilityToUpsert.size() > 0 ) upsert lstLiabilityToUpsert;
            if(lstLiabilityToDelete.size() > 0 ) delete lstLiabilityToDelete;
        }
        //Initializing next wizard display
        intSelectedStep = objSteps.selectEmployee;
        
    }
    
    
    /**
        * @description  : These methods and properties used to setup employement data
    */
    private map<Id,Id> mapApplicant2mapping;
    private void EmpDataSetup()
    {
        mapApplicant2mapping = new map<Id,Id>();
        List<Employer_Mapping__c> lstEmployeeToDelete = new List<Employer_Mapping__c>();
        set<Id> setApp2Id = new set<Id>();
        if(lstApplicantToUpsert != null && lstApplicantToUpsert.size() > 0) 
        {
            for(Applicant_2_Mapping__c objApp2map : lstApplicantToUpsert) 
            {
                mapApplicant2mapping.put(objApp2map.Applicant_2__c, objApp2map.Id);
                setApp2Id.add(objApp2map.Applicant_2__c);
            }
        }
        
        string strSOQEmployee   = 'select ' + String.join( new List<String>(Employee__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') ;
                strSOQEmployee += ',(select ' + String.join( new List<String>(Employer_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',') + ' from Employers_Mapping__r Where Opportunity__c = \''+objOpportuninty.Id+'\')';
                strSOQEmployee += ' from Employee__c where (Account__c = \''+objOpportuninty.AccountId+'\' or Applicant2__c in: setApp2Id)';
        
        List<Employee__c> lstEmployeeTemp = Database.query(strSOQEmployee);
        
        lstEmployee = new list<wrapEmployee>();
        if(lstEmployeeTemp != null ) for(Employee__c objEmp : lstEmployeeTemp)
        {
            wrapEmployee objwrapEmployee = new wrapEmployee();
            objwrapEmployee.objEmployee = objEmp;
            objwrapEmployee.objEmployeemapping = objEmp.Employers_Mapping__r != null && objEmp.Employers_Mapping__r.size() > 0 ? objEmp.Employers_Mapping__r[0] : null;
            lstEmployee.add(objwrapEmployee);
        }
    }
    
    /**
        * @description : Method used to Save Employees
    */
    public void SaveEmployee()
    {
        if(lstEmployee != null && lstEmployee.size() > 0 )
        {
            List<Employer_Mapping__c> lstEmployeeToUpsert = new List<Employer_Mapping__c>();
            List<Employer_Mapping__c> lstEmployeeToDelete = new List<Employer_Mapping__c>();
            
            for(wrapEmployee objEmployee : lstEmployee) 
            {
                if((objEmployee.isSelected != null && objEmployee.isSelected) || createAll)
                {
                    Map<String, Schema.SObjectField> mapMainObject = Schema.SObjectType.Employee__c.fields.getMap();
                    Map<String, Schema.SObjectField> mapMappingObject = Schema.SObjectType.Employer_Mapping__c.fields.getMap();
                    
                    Employer_Mapping__c obEmployeeMap = new Employer_Mapping__c();
                    if(objEmployee.objEmployeemapping != null && objEmployee.objEmployeemapping.Id != null) obEmployeeMap = new Employer_Mapping__c(Id = objEmployee.objEmployeemapping.Id);
                    
                    for(String fieldName : mapMappingObject.keySet()) 
                    if(mapMappingObject.get(fieldName).getDescribe().isUpdateable() && mapMainObject.containsKey(fieldName)) obEmployeeMap.put(fieldName, objEmployee.objEmployee.get(fieldName));
                    
                    obEmployeeMap.Opportunity__c = objOpportuninty.Id;
                    obEmployeeMap.Employer__c = objEmployee.objEmployee.Id;
                    
                    if(objEmployee.objEmployee.Applicant2__c != null && mapApplicant2mapping.containsKey(objEmployee.objEmployee.Applicant2__c)) obEmployeeMap.Applicant_2_Mapping__c = mapApplicant2mapping.get(objEmployee.objEmployee.Applicant2__c);
                    else obEmployeeMap.Applicant_2_Mapping__c = null;
                    
                    lstEmployeeToUpsert.add(obEmployeeMap);
                }
                else if(objEmployee.objEmployeemapping != null && objEmployee.objEmployeemapping.Id != null) lstEmployeeToDelete.add(objEmployee.objEmployeemapping);
            }
            if(lstEmployeeToUpsert.size() > 0 ) upsert lstEmployeeToUpsert;
            if(lstEmployeeToDelete.size() > 0 ) delete lstEmployeeToDelete;
        }
        
        //Initializing next wizard display
        intSelectedStep = objSteps.selectAstLbltShare;
        
        //Creating Asset Liability Share
        CreateAstLbtShare();
    }
    
    /**
        * @description : Method used to create asset liability share data
    */
    private void CreateAstLbtShare()
    {
        //------ Deleteing grbage share data needs to be deleted --------//
        list<LK_Asset_Liability_Share__c> lstInvalidSharesToDelete = [select Id, Account__c, Applicant_2_Mapping__c from LK_Asset_Liability_Share__c where (Account__r.Name = null and Applicant_2_Mapping__r.Name = null) or (Liability_Mapping__r.Name = null and Asset_Mapping__r.Name = null)];
        if(lstInvalidSharesToDelete != null && lstInvalidSharesToDelete.size() > 0) delete lstInvalidSharesToDelete;
        //----- End Deleteing grbage share data needs to be deleted -----//
        
        //-------- Fetching existing liability share data -----------//
        string  strSOQLOpp =  'select Id, AccountId, CreatedDate'; 
        strSOQLOpp += ',(select Id from Applicant_2_Mapping__r)';
        strSOQLOpp += ',(select Id, Name from Liability_Mapping__r)';
        strSOQLOpp += ',(select Id, Asset_Type__c, Name from Asset_Mapping__r)';
        strSOQLOpp += ',(select Account__c, Applicant_2_Mapping__c, Applicant_2_Mapping__r.Name, Liability_Mapping__c, Liability_Mapping__r.Name, Asset_Mapping__c, Asset_Mapping__r.Name, Asset_Mapping__r.Asset_Type__c from Asset_Liability_Share__r )';
        strSOQLOpp += 'from Opportunity ';
        strSOQLOpp += 'where Id = \'' + objOpportuninty.Id +'\'';
        
        Opportunity objOpp = Database.query(strSOQLOpp);
        //------ End Fetching existing liability share data --------//
        
        map<string,LK_Asset_Liability_Share__c> mapAssetLbtShare = new map<string,LK_Asset_Liability_Share__c>();
        
        if(objOpp.Liability_Mapping__r != null ) for(Liability_Mapping__c objLbtMapping : objOpp.Liability_Mapping__r)
        {   
            if(objOpp.Applicant_2_Mapping__r != null ) for(Applicant_2_Mapping__c objAppMapping : objOpp.Applicant_2_Mapping__r)
            mapAssetLbtShare.put((string)objAppMapping.Id + (string)objLbtMapping.Id, new LK_Asset_Liability_Share__c( Applicant_2_Mapping__c = objAppMapping.Id, Liability_Mapping__c = objLbtMapping.Id, Name = objLbtMapping.Name, Opportunity__c = objOpportuninty.Id, Percentage_Share__c = 0.0 ));
            
            mapAssetLbtShare.put((string)objOpp.AccountId + (string)objLbtMapping.Id, new LK_Asset_Liability_Share__c( Account__c = objOpp.AccountId, Liability_Mapping__c = objLbtMapping.Id, Name = objLbtMapping.Name, Opportunity__c = objOpportuninty.Id, Percentage_Share__c = 0.0 ));
        }
        
        if(objOpp.Asset_Mapping__r != null ) for(Asset_Mapping__c objAstMapping : objOpp.Asset_Mapping__r)
        {
            if(objOpp.Applicant_2_Mapping__r != null ) for(Applicant_2_Mapping__c objAppMapping : objOpp.Applicant_2_Mapping__r)
            mapAssetLbtShare.put((string)objAppMapping.Id + (string)objAstMapping.Id, new LK_Asset_Liability_Share__c(Applicant_2_Mapping__c = objAppMapping.Id, Asset_Mapping__c = objAstMapping.Id, Name = objAstMapping.Asset_Type__c, Opportunity__c = objOpportuninty.Id, Percentage_Share__c = 0.0 ));
            
            mapAssetLbtShare.put((string)objOpp.AccountId + (string)objAstMapping.Id, new LK_Asset_Liability_Share__c(Account__c = objOpp.AccountId, Asset_Mapping__c = objAstMapping.Id, Name = objAstMapping.Asset_Type__c, Opportunity__c = objOpportuninty.Id, Percentage_Share__c = 0.0 ));
        }        
        
        if(objOpp.Asset_Liability_Share__r != null ) for(LK_Asset_Liability_Share__c objAppMapping : objOpp.Asset_Liability_Share__r)
        {
            if(objAppMapping.Applicant_2_Mapping__c != null && objAppMapping.Liability_Mapping__c != null )
            {
                mapAssetLbtShare.put((string)objAppMapping.Applicant_2_Mapping__c + (string)objAppMapping.Liability_Mapping__c,objAppMapping);
            }
            else if (objAppMapping.Applicant_2_Mapping__c != null && objAppMapping.Asset_Mapping__c != null )
            {
                mapAssetLbtShare.put((string)objAppMapping.Applicant_2_Mapping__c + (string)objAppMapping.Asset_Mapping__c,objAppMapping);
            }
            else if (objAppMapping.Account__c != null && objAppMapping.Asset_Mapping__c != null )
            {
                mapAssetLbtShare.put((string)objAppMapping.Account__c + (string)objAppMapping.Asset_Mapping__c,objAppMapping);
            }
            else if (objAppMapping.Account__c != null && objAppMapping.Liability_Mapping__c != null )
            {
                mapAssetLbtShare.put((string)objAppMapping.Account__c + (string)objAppMapping.Liability_Mapping__c,objAppMapping);
            }
        }
        
        if(mapAssetLbtShare.size() > 0) upsert mapAssetLbtShare.values();
        
        string  strlstLbltShare  =  ' select Account__r.Name, Applicant_2_Mapping__r.Name, Applicant_2_Mapping__r.Full_Name_F__c,' + String.join( new List<String>(LK_Asset_Liability_Share__c.SObjectType.getDescribe().fields.getMap().keySet()), ',');
        strlstLbltShare +=  ' ,Liability_Mapping__r.' + String.join( new List<String>(Liability_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',Liability_Mapping__r.');
        strlstLbltShare +=  ' From LK_Asset_Liability_Share__c ';
        strlstLbltShare +=  ' where  Opportunity__c =\'' + objOpportuninty.Id +'\'' ;
        strlstLbltShare +=  ' and Liability_Mapping__c != null';
        strlstLbltShare +=  ' Order By Name';
        lstLbltShare = Database.query(strlstLbltShare);
        if(lstLbltShare == null) lstLbltShare = new list<LK_Asset_Liability_Share__c>();
        
        
        string  strlstAstShare  =  ' select Account__r.Name, Applicant_2_Mapping__r.Name, Applicant_2_Mapping__r.Full_Name_F__c,' + String.join( new List<String>(LK_Asset_Liability_Share__c.SObjectType.getDescribe().fields.getMap().keySet()), ',');
        strlstAstShare +=  ' ,Asset_Mapping__r.' + String.join( new List<String>(Asset_Mapping__c.SObjectType.getDescribe().fields.getMap().keySet()), ',Asset_Mapping__r.');
        strlstAstShare +=  ' From LK_Asset_Liability_Share__c ';
        strlstAstShare +=  ' where  Opportunity__c =\'' + objOpportuninty.Id +'\'' ;
        strlstAstShare +=  ' and Asset_Mapping__c != null';
        strlstAstShare +=  ' Order By Name';
        
        lstAstShare = Database.query(strlstAstShare);
        if(lstAstShare == null) lstAstShare = new list<LK_Asset_Liability_Share__c>();
    }   
    
    
    /**
        * @description : Method used to save asset liablity share data
    */
    public void SaveAstLbltShare()
    {
        if(lstLbltShare != null && lstLbltShare.size() > 0 )
        {
            map<Id,Decimal> mapAstLbtShare = new  map<Id,Decimal>();
            for(LK_Asset_Liability_Share__c objASTLBT : lstLbltShare )
            { 
                if(objASTLBT.Percentage_Share__c != null )
                {
                    if((objASTLBT.Liability_Mapping__c != null || objASTLBT.Asset_Mapping__c != null))
                    {
                        id idAstLbtId = objASTLBT.Liability_Mapping__c != null ? objASTLBT.Liability_Mapping__c : objASTLBT.Asset_Mapping__c;
                        
                        if(mapAstLbtShare.ContainsKey(idAstLbtId)) mapAstLbtShare.put(idAstLbtId,mapAstLbtShare.get(idAstLbtId) + objASTLBT.Percentage_Share__c);
                        else mapAstLbtShare.put(idAstLbtId, objASTLBT.Percentage_Share__c);
                        
                        if(mapAstLbtShare.get(idAstLbtId) > 100)
                        { 
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Total share of one Liability cannot be more than 100.'));
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Total share of ' + objASTLBT.Name + ' is more than 100.'));
                            return;
                        }
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Share of '+ objASTLBT.Name +' cannot be null.'));
                    return;
                }
            }
            upsert lstLbltShare;
            
        }
        if(lstAstShare != null && lstAstShare.size() > 0 )
        {
            map<Id,Decimal> mapAstLbtShare = new  map<Id,Decimal>();
            for(LK_Asset_Liability_Share__c objASTLBT : lstAstShare )
            { 
                if(objASTLBT.Percentage_Share__c != null )
                {
                    if((objASTLBT.Liability_Mapping__c != null || objASTLBT.Asset_Mapping__c != null))
                    {
                        id idAstLbtId = objASTLBT.Liability_Mapping__c != null ? objASTLBT.Liability_Mapping__c : objASTLBT.Asset_Mapping__c;
                        
                        if(mapAstLbtShare.ContainsKey(idAstLbtId)) mapAstLbtShare.put(idAstLbtId,mapAstLbtShare.get(idAstLbtId) + objASTLBT.Percentage_Share__c);
                        else mapAstLbtShare.put(idAstLbtId, objASTLBT.Percentage_Share__c);
                        
                        if(mapAstLbtShare.get(idAstLbtId) > 100)
                        { 
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Total share of one Asset cannot be more than 100.'));
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Total share of ' + objASTLBT.Name + ' is more than 100.'));
                            return;
                        }
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Share of '+ objASTLBT.Name +' cannot be null.'));
                    return;
                }
            }
            upsert lstAstShare;
        }
        //Initializing next wizard display
        intSelectedStep = objSteps.selectPreview;
    }
    
    /**
        * @description : Mthod used to send infor to loankit
    */
    public void sendInfoToLoanKit()
    {
        strErrorMsg = LKIntegration.requestApi(objOpportuninty.Id);
        
        ApexPages.Message myMsg;
        if(strErrorMsg != null && strErrorMsg.Contains('success')) myMsg = new ApexPages.Message(ApexPages.Severity.Info, strErrorMsg);
        else  myMsg = new ApexPages.Message(ApexPages.Severity.Error, strErrorMsg);
        ApexPages.addMessage(myMsg);
        intSelectedStep = objSteps.showSuccessOrError;
    }
    
    /**
        * @description : Method used to setup data which will be shown on wizard
        * @param objVarOpp 
        * @param objVarAcc 
    */
    public void setupdata(Opportunity objVarOpp, Account objVarAcc)
    {
        if(objVarOpp != null && objVarAcc != null )
        {
            Map<Id,wrapApplicant2> mapApplicants = new Map<Id,wrapApplicant2>();
            Map<Id,wrapAssets> mapAssets = new Map<Id,wrapAssets>();
            Map<Id,wrapLiability> mapLiability = new Map<Id,wrapLiability>();
            Map<Id,wrapEmployee> mapEmployee = new Map<Id,wrapEmployee>();
            
            //--------------- Creating Map Opportunity Related Objects-------------------//
            if(objVarOpp.Applicant_2_Mapping__r != null ) for(Applicant_2_Mapping__c objMapping : objVarOpp.Applicant_2_Mapping__r)
            {
                if(objMapping.Applicant_2__c != null)
                {
                    wrapApplicant2 objWrapApp2 = new wrapApplicant2();
                    objWrapApp2.objApplicant2Map = objMapping;
                    objWrapApp2.isSelected = true;
                    mapApplicants.put( objMapping.Applicant_2__c, objWrapApp2 );
                }
            }
            
            if(objVarOpp.Liability_Mapping__r != null ) for(Liability_Mapping__c objMapping : objVarOpp.Liability_Mapping__r)
            {
                if(objMapping.Liability__c != null)
                {
                    wrapLiability objLiability = new wrapLiability();
                    objLiability.objLiabilitymapping = objMapping;
                    objLiability.isSelected = true;
                    mapLiability.put( objMapping.Liability__c, objLiability );
                }
            }
            
            if(objVarOpp.Asset_Mapping__r != null ) for(Asset_Mapping__c objMapping : objVarOpp.Asset_Mapping__r)
            {
                if(objMapping.Assets__c != null)
                {
                    wrapAssets objwrapAssets = new wrapAssets();
                    objwrapAssets.objAssetmapping = objMapping;
                    objwrapAssets.isSelected = true;
                    mapAssets.put( objMapping.Assets__c, objwrapAssets );
                }
            }  
            //------------- End Creating Map Opportunity Related Objects ------------------//
            
            
            //------------- End Creating Wrapper Data ------------------//
            if(objVarAcc.Applicant_2__r != null ) for(Applicant_2__c objApplicant : objVarAcc.Applicant_2__r)
            {
                wrapApplicant2 objWrapApp2 = mapApplicants.ContainsKey(objApplicant.Id) ? mapApplicants.get(objApplicant.Id) : new wrapApplicant2();
                objWrapApp2.objApplicant2 = objApplicant;
                lstApplicant2.add(objWrapApp2);
            }
            
            if(objVarAcc.Liabilities__r != null ) for(Liability__c objLbt : objVarAcc.Liabilities__r)
            {
                wrapLiability objLiability = mapLiability.ContainsKey(objLbt.Id) ? mapLiability.get(objLbt.Id) : new wrapLiability();
                objLiability.objLiability = objLbt;
                lstLiability.add(objLiability);
            }
            
            if(objVarAcc.Assets__r != null ) for(Assets__c objAst : objVarAcc.Assets__r)
            {
                wrapAssets objwrapAssets = mapAssets.ContainsKey(objAst.Id) ? mapAssets.get(objAst.Id) : new wrapAssets();
                objwrapAssets.objAsset = objAst;
                lstAssets.add(objwrapAssets);
            }
            //------------- End Creating Wrapper Data ------------------//
        }
    }
    
    
    //------------------ Wrapper Classes --------------------//
    public class wrapApplicant2
    {
        public Applicant_2__c objApplicant2 {get; Set;}
        public Applicant_2_Mapping__c objApplicant2Map {get; Set;}
        public Boolean isSelected {get; Set;}
    } 
    
    public class wrapAssets
    {
        public Assets__c objAsset {get; Set;}
        public Asset_Mapping__c objAssetmapping {get; Set;}
        public Boolean isSelected {get; Set;}
    }
    
    public class wrapLiability
    {
        public Liability__c objLiability {get; Set;}
        public Liability_Mapping__c objLiabilitymapping {get; Set;}
        public Boolean isSelected {get; Set;}
    }
    
    public class wrapEmployee
    {
        public Employee__c objEmployee {get; Set;}
        public Employer_Mapping__c objEmployeemapping {get; Set;}
        public Boolean isSelected {get; Set;}
    }
    
    
    public class Steps
    {
        public integer selectApplicant2 {get {return 1;} Set;}
        public integer selectAssets {get {return 2;} Set;}
        public integer selectLiability {get {return 3;} Set;}
        public integer selectEmployee {get {return 4;} Set;}
        public integer selectAstLbltShare {get {return 5;} Set;}
        public integer selectPreview {get {return 6;} Set;}
        public integer showSuccessOrError {get {return 7;} Set;}
    }
    //---------------- End Wrapper Classes ------------------//
}