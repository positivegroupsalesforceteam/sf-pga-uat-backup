/** 
* @FileName: BizibleTouchpointGateway
* @Description: Provides finder methods for accessing data in the bizible2__Bizible_Touchpoint__c object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 26/10 RDAVID Created Trigger extra/BizibleTouchpointTrigger
**/ 

public class BizibleTouchpointGateway {
    public static Map<Id,Map<Id,Case_Touch_Point__c>> ctpMap = new Map<Id,Map<Id,Case_Touch_Point__c>>();

    public static Set<Id> getCaseIDs(Map<Id,Account> accMap){
        Set<Id> caseIds = new Set<Id>();
        for(Id accId : accMap.keySet()){
            if(accMap.get(accId).Roles__r != NULL && accMap.get(accId).Roles__r.size() > 0)
            caseIds.add(accMap.get(accId).Roles__r[0].Case__c);
        }
        return caseIds;
    }

    public static Map<Id,Set<Id>> getCasewithCTPIdsMap(Map<Id,Case> caseWithCTPMap){
        Map<Id,Set<Id>> casewithCTPIdsMap = new Map<Id,Set<Id>>();

        for(Id csID : caseWithCTPMap.keySet()){
            System.debug('CTP Size = '+caseWithCTPMap.get(csID).Case_Touch_Points__r.size());
            for(Case_Touch_Point__c ctp : caseWithCTPMap.get(csID).Case_Touch_Points__r){
                
                if(casewithCTPIdsMap.containsKey(csID)){
                    Set<Id> existingBiziTPIdSet = casewithCTPIdsMap.get(csID);
                    existingBiziTPIdSet.add(ctp.Bizible_Touchpoint__c);
                    casewithCTPIdsMap.put(csID, existingBiziTPIdSet);
                }
                else if(!casewithCTPIdsMap.containsKey(csID)) {
                    casewithCTPIdsMap.put(csID, new Set<Id> {ctp.Bizible_Touchpoint__c});
                }

                if(ctp.Latest_Touchpoint__c){
                    ctp.Latest_Touchpoint__c = FALSE;
                    if(ctpMap.containsKey(csId)){
                        Map<Id,Case_Touch_Point__c> ctpList = ctpMap.get(csId);
                        ctpList.put(ctp.Id,ctp);
                        ctpMap.put(csID,ctpList);
                    }
                    else if (!ctpMap.containsKey(csId)){
                        ctpMap.put(csID,new Map<Id,Case_Touch_Point__c> {ctp.Id => ctp});
                    }  
                } 
            }
        }
        return casewithCTPIdsMap;
    }
}