/** 
* @FileName: fixCaseBizibleTouchpointsBatch
* @Description: Implement Batchable Apex, Case Touchpoint is a junction object between Bizible Touchpoint and Case object. The Case TPs should automatically be create once the Bizible Touchpouints are create in Salesforce. If the trigger doesn't work for some reason(error in locking issue), this Class can be used to create missing Case TPs.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 23/10/2019 RDAVID Created Class - tasks/26493146-BizibleTPITriggerIssue
**/ 

global class fixCaseBizibleTouchpointsBatch implements Database.Batchable<sObject> {
    global final String queryString; 
    global final DateTime createdDateTime;
    global Datetime startDateParam;// = Datetime.newInstance(2019, 10, 25, 12, 04, 50);
    global Datetime endDateParam;
    private Set<String> logs = new Set<String>();
    private String processName = 'Batch Fix Case Touchpoints';
    global final String action;
    global final Boolean deleteOn;// = true;
    global final Boolean createOn;// = true;
    global fixCaseBizibleTouchpointsBatch(String query){
        Bizible_Batch_Setting__mdt bbstCustomMeta = [SELECT Id,Action__c,Allow_Case_TP_Deletion__c,Allow_Case_TP_Creation__c,Created_To__c,Created_From__c FROM Bizible_Batch_Setting__mdt WHERE DeveloperName ='BBS_001'];
        deleteOn = bbstCustomMeta.Allow_Case_TP_Deletion__c;
        createOn = bbstCustomMeta.Allow_Case_TP_Creation__c;
        if(bbstCustomMeta.Action__c == null) action = 'DELETE';
        else action = bbstCustomMeta.Action__c;
        startDateParam = bbstCustomMeta.Created_From__c;
        endDateParam = bbstCustomMeta.Created_To__c;

        if(action == 'DELETE'){
            queryString = 'SELECT ID,Name,CreatedDate,bizible2__Account__c,(SELECT Id,Case__c,Case__r.CaseNumber,Bizible_Touchpoint__c,Case_Bizible_Touchpoint_Id__c FROM Case_Touch_Points__r ORDER BY Case__r.CaseNumber DESC) FROM bizible2__Bizible_Touchpoint__c WHERE CreatedDate >=: startDateParam AND CreatedDate <=: endDateParam AND bizible2__Account__c != NULL ORDER BY CreatedDate DESC';
        }
        else if (action == 'CREATE'){
            if(query != null) queryString = query;
            else queryString =  'SELECT ID,Name,CreatedDate,bizible2__Account__c FROM bizible2__Bizible_Touchpoint__c WHERE CreatedDate >=: startDateParam AND CreatedDate <=: endDateParam AND bizible2__Account__c != NULL AND bizible2__Account__r.RecordType.Name = ' +  '\'nm: Individual\'' + ' ORDER BY CreatedDate DESC';
            //'SELECT Id, (SELECT Id,CreatedDate FROM bizible2__Bizible_Touchpoints__r ORDER BY bizible2__Touchpoint_Date__c DESC) FROM Account WHERE CreatedDate >: startDateParam AND CreatedDate <=: endDateParam AND RecordType.Name = ' + '\'nm: Individual\'';//'SELECT  Id, CreatedDate, Primary_Contact_Name_MC__c, (SELECT Id,Account__c FROM Roles__r WHERE Primary_Contact_for_Application__c = TRUE),(SELECT Id,Bizible_Touchpoint__c FROM Case_Touch_Points__r ORDER BY Bizible_Touchpoint__r.CreatedDate DESC) FROM Case WHERE CreatedDate >: startDateParam AND CreatedDate <=: endDateParam AND Lead_Creation_Method__c = ' + '\'Frontend Load\'' + ' ORDER BY CreatedDate DESC';
        }   
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('createCaseBizibleTouchpointsBatch startDateParam = '+startDateParam);
        System.debug('createCaseBizibleTouchpointsBatch endDateParam = '+endDateParam);
        System.debug('createCaseBizibleTouchpointsBatch queryString = '+queryString);
        System.debug('createCaseBizibleTouchpointsBatch action = '+action);
        System.debug('createCaseBizibleTouchpointsBatch deleteOn = '+deleteOn);
        System.debug('createCaseBizibleTouchpointsBatch createOn = '+createOn);
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        System.debug('action ========== '+action);
        if(action == 'DELETE'){
            List<bizible2__Bizible_Touchpoint__c> bizibleTPList = (List <bizible2__Bizible_Touchpoint__c>)scope;
            deleteDuplicateCaseTouchpoints(bizibleTPList);
        }
        else if(action == 'CREATE'){
            List<bizible2__Bizible_Touchpoint__c> bizibleTPList = (List <bizible2__Bizible_Touchpoint__c>)scope;
            Set<Id> accIds = new Set<Id>();
            for(bizible2__Bizible_Touchpoint__c bztp : bizibleTPList){
                // if(acc.bizible2__Bizible_Touchpoints__r != NULL && acc.bizible2__Bizible_Touchpoints__r.size() > 0) 
                accIds.add(bztp.bizible2__Account__c);
            }
            if(accIds.size() > 0) createMissingCaseTouchpoints(accIds);
        }
    }

    global void deleteDuplicateCaseTouchpoints(List<bizible2__Bizible_Touchpoint__c> bizibleTPList){
        Map<Id,List<Case_Touch_Point__c>> ctpDuplicateToDeleteMap = new Map<Id,List<Case_Touch_Point__c>>(); //bztp Id as Key, List of CaseTP as values
        List<Case_Touch_Point__c> ctpDuplicateToDelList = new List<Case_Touch_Point__c>();
        System.debug('bizibleTPList size = ' +bizibleTPList.size());
        if(bizibleTPList.size() > 0){
            for(bizible2__Bizible_Touchpoint__c bztp : bizibleTPList){ //Loop in BZTP
                Integer numOfDupe = 0;
                Boolean dupeFound = false;  
                if(bztp.Case_Touch_Points__r != NULL && bztp.Case_Touch_Points__r.size() > 0){
                    List<Case_Touch_Point__c> ctpDuplicateToDeleteList = new List<Case_Touch_Point__c>();
                    for(Integer x = 0; x < bztp.Case_Touch_Points__r.size(); x++){ //Loop on Child Case TPs
                        Integer y = x+1;
                        Case_Touch_Point__c currentCaseTP = bztp.Case_Touch_Points__r[x];
                        if(y < bztp.Case_Touch_Points__r.size()){
                            Case_Touch_Point__c nextCaseTP = bztp.Case_Touch_Points__r[y];
                            Boolean dupe = currentCaseTP.Case__r.CaseNumber.equals(nextCaseTP.Case__r.CaseNumber); //check Case Number if similar
                            if(dupe){
                                ctpDuplicateToDeleteList.add(nextCaseTP);
                                dupeFound = true;
                            }
                        }
                    }
                    if(dupeFound){
                        ctpDuplicateToDeleteMap.put(bztp.Id,ctpDuplicateToDeleteList);
                    }
                }
            }
            
            for(Id bztpId : ctpDuplicateToDeleteMap.keySet()){
                if(ctpDuplicateToDeleteMap.containsKey(bztpId)) {
                    logs.add( ctpDuplicateToDeleteMap.get(bztpId).size() + ' duplicates found for ' + bztpId + ' Bizible touchpoint.');
                    for(Case_Touch_Point__c ctp : ctpDuplicateToDeleteMap.get(bztpId)){
                        ctpDuplicateToDelList.add(ctp);
                    }
                }
            }

            System.debug('finish - > '+logs);
            if(logs.size() > 0){
                CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Log',fixCaseBizibleTouchpointsBatch.class.getName() + ' - ' + processName,logs);
                cerhand.logWrap = logWrapper;
                cerhand.sendMail();
            }
            System.debug('deleteOn --> '+deleteOn);
            if(ctpDuplicateToDelList.size() > 0 && deleteOn){
                try{
                    Database.DeleteResult[] ctpDelResult = Database.delete(ctpDuplicateToDelList, false);
                    // Iterate through each returned result
                    Boolean sendCustomErrorHandlingEmailNotif = false;
                    Boolean sendCustomSuccessHandlingEmailNotif = false;
                    Set<String> errLogs = new Set<String>();
                    Set<String> successLogs = new Set<String>();
                    Integer errornum = 0;
                    Integer recordsAffected = 0;
                    
                    for(Database.DeleteResult dr : ctpDelResult) {
                        if (dr.isSuccess()) {
                            successLogs.add('Successfully deleted duplicate CTP with ID: ' + dr.getId());
                            sendCustomSuccessHandlingEmailNotif = true;
                        }
                        else {
                            processName += ' '+ action;
                            recordsAffected++;
                            sendCustomErrorHandlingEmailNotif = true;
                            Database.Error[] errs = dr.getErrors();
                            Id recordID = dr.getId();
                            System.debug('recordsAffected = '+recordsAffected);
                            for(Database.Error err : errs) {
                                String emailContent = 'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage();
                                if(!errLogs.contains(emailContent)){
                                    errLogs.add(emailContent);  
                                    errornum ++;
                                } 
                            }
                        }
                    }
                    if(sendCustomErrorHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',fixCaseBizibleTouchpointsBatch.class.getName() + ' - ' + processName,errLogs);
                        if(recordsAffected != 0) logWrapper.numberOfRecords = recordsAffected*errornum;
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                    if(sendCustomSuccessHandlingEmailNotif){
                        CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                        CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Log',fixCaseBizibleTouchpointsBatch.class.getName() + ' - ' + processName,successLogs);
                        cerhand.logWrap = logWrapper;
                        cerhand.sendMail();
                    }
                }  catch (Exception e) {
                    System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                }
            }
            System.debug('logs = ' + logs);   
        }
    }

    global void createMissingCaseTouchpoints(Set<Id> accountIds){//Map<Id,Case> caseMainMap) {
        System.debug('createMissingCaseTouchpoints');
        System.debug('createMissingCaseTouchpoints startDateParam - '+startDateParam);
        System.debug('createMissingCaseTouchpoints endDateParam - '+endDateParam);
        Map<Id,Set<Id>> caseBZTPMap = new Map<Id,Set<Id>>(); //USED TO CHECK IF BIZIBLE IS EXISTING IN CASE
        Map<Id,Case> caseMap = new Map<Id,Case>();
        List<Case_Touch_Point__c> caseTPListToInsert = new List<Case_Touch_Point__c>();
        // Map<Id,Id> casePrimaryContactAccountMap = new Map<Id,Id>();
        Map<Id,Account> accBizibleTPMap = new Map<Id,Account>([ SELECT Id,
                                                                    (SELECT Id,CreatedDate,CaseNumber,Lead_Creation_Method__c
                                                                    FROM Cases5__r 
                                                                    WHERE CreatedDate >=: startDateParam AND CreatedDate <=: endDateParam
                                                                    //WHERE (Lead_Creation_Method__c = 'Frontend Load' OR Lead_Creation_Method__c = 'New Lead Button')
                                                                    ORDER BY CaseNumber DESC LIMIT 1),
                                                                    (SELECT Id,Name,CreatedDate 
                                                                    FROM bizible2__Bizible_Touchpoints__r 
                                                                    ORDER BY CreatedDate DESC) 
                                                                FROM Account WHERE Id IN: accountIds ORDER BY CreatedDate DESC]);

        Map<Id,Case> accountLatestCaseMapUnfiltered = new Map<Id,Case>();
        Map<Id,Case> accountLatestCaseMapFiltered = new Map<Id,Case>();

        Set<Id> cseIds = new Set<Id>();
        for(Id accId : accBizibleTPMap.keySet()){
            if(accBizibleTPMap.containsKey(accId)){
                if(accBizibleTPMap.get(accId).Cases5__r != NULL && accBizibleTPMap.get(accId).Cases5__r.size() > 0){
                    
                    Case latestUnfilteredCase = accBizibleTPMap.get(accId).Cases5__r[(accBizibleTPMap.get(accId).Cases5__r.size()-1)];
                    for(Case cse : accBizibleTPMap.get(accId).Cases5__r){
                        if(cse.Lead_Creation_Method__c == 'Frontend Load' ){//|| cse.Lead_Creation_Method__c == 'New Lead Button'
                            // if(cse.Id == latestUnfilteredCase.Id){ //The Latest is valid
                                if(!accountLatestCaseMapFiltered.containsKey(accId)){
                                    accountLatestCaseMapFiltered.put(accId,cse);//The Latest is valid Case to Update;
                                    cseIds.add(cse.Id);
                                    caseMap.put(cse.Id,cse);
                                }
                            // }
                        }
                    }
                }
            }
        }
        //Dupe Checking of existing Cases
        for(Case cse : [SELECT Id,(SELECT Id,Bizible_Touchpoint__c FROM Case_Touch_Points__r ORDER BY Bizible_Touchpoint__r.CreatedDate DESC) FROM Case WHERE Id IN: cseIds]){
            if(cse.Case_Touch_Points__r != NULL && cse.Case_Touch_Points__r.size()>0){
                for(Case_Touch_Point__c ctp : cse.Case_Touch_Points__r){
                    if(caseBZTPMap.containsKey(cse.Id)){
                        Set<Id> bztpIdSet = caseBZTPMap.get(cse.Id);
                        bztpIdSet.add(ctp.Bizible_Touchpoint__c);
                        caseBZTPMap.put(cse.Id,bztpIdSet);
                    }
                    else{
                        caseBZTPMap.put(cse.Id,new Set<Id> {ctp.Bizible_Touchpoint__c});
                    }
                }
            }
            else{
                caseBZTPMap.put(cse.Id,new Set<Id>());
            }
        }

        for(Account acc : accBizibleTPMap.values()){ //LOOP IN Account
            if(acc.bizible2__Bizible_Touchpoints__r != NULL && acc.bizible2__Bizible_Touchpoints__r.size() > 0 && accountLatestCaseMapFiltered.containsKey(acc.Id)){ //Check if it has Bizible and has Cases
                Case cse = accountLatestCaseMapFiltered.get(acc.Id);
                bizible2__Bizible_Touchpoint__c latestBizibleTouchpoint = acc.bizible2__Bizible_Touchpoints__r[acc.bizible2__Bizible_Touchpoints__r.size()-1];
                if(caseBZTPMap.containsKey(cse.Id)){
                    if(cse.CreatedDate < latestBizibleTouchpoint.CreatedDate){
                        for(bizible2__Bizible_Touchpoint__c bztp : acc.bizible2__Bizible_Touchpoints__r){
                            Boolean latest = false;
                            if(bztp.Id == latestBizibleTouchpoint.Id){
                                latest = true;
                            }
                            if(caseBZTPMap.get(cse.Id).size() > 0){
                                if(!caseBZTPMap.get(cse.Id).contains(bztp.Id)){ //CHECK IF BIZIBLE TP IS ON CASE
                                    caseTPListToInsert.add(new Case_Touch_Point__c(Case__c = cse.Id,Bizible_Touchpoint__c=bztp.Id,Latest_Touchpoint__c = latest));
                                    System.debug('Create Case TP for Bizible Touchpoint ' + bztp.Name +' and Case '+cse.CaseNumber);
                                    System.debug('CASE HAS EXISTING TP - BUT NO Bizible Touchpoint ' + bztp.Name +' -> Create it for Case '+cse.CaseNumber);
                                }                                
                            }
                            //CHECK IF CASE HAS NO EXISTING TP 
                            else{
                                caseTPListToInsert.add(new Case_Touch_Point__c(Case__c = cse.Id,Bizible_Touchpoint__c=bztp.Id,Latest_Touchpoint__c = latest));
                                System.debug('CASE HAS NO EXISTING TP -- Create Case TP for Bizible Touchpoint ' + bztp.Name +' and Case '+cse.CaseNumber);
                            }
                        }
                    }
                }
            }
        }
        if(caseTPListToInsert.size() > 0){
            Map<Id,List<String>> cseCTPMap = new Map<Id,List<String>>();
            for(Case_Touch_Point__c ctp : caseTPListToInsert){
                if(cseCTPMap.containsKey(ctp.Case__c)){
                    List<String> ctpList = cseCTPMap.get(ctp.Case__c);
                    ctpList.add(ctp.Bizible_Touchpoint__c);
                    cseCTPMap.put(ctp.Case__c,ctpList);
                }
                else cseCTPMap.put(ctp.Case__c,new List<String> {ctp.Bizible_Touchpoint__c});
            }

            if(!cseCTPMap.isEmpty()){
                for(Id cseID : cseCTPMap.keySet()){
                    String caseNumber = (caseMap.containsKey(cseID)) ? caseMap.get(cseID).CaseNumber : 'blank';
                    String logString = 'Case : ' + caseNumber + ' -- Bizible Touchpoints ['+cseCTPMap.get(cseID).size()+'] : '+ string.join(cseCTPMap.get(cseID),',');
                    logs.add(logString);
                }
            }
            if(logs.size() > 0){
                CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Log',fixCaseBizibleTouchpointsBatch.class.getName() + ' - ' + processName+'[CREATE MISSING]',logs);
                cerhand.logWrap = logWrapper;
                cerhand.sendMail();
            }
        }
        System.debug('caseTPListToInsert == ' + caseTPListToInsert.size());
        if(caseTPListToInsert.size() > 0 && createOn){
            try{
                Database.SaveResult[] ctpToInsertResult = Database.insert(caseTPListToInsert, false);
                // Iterate through each returned result
                Boolean sendCustomErrorHandlingEmailNotif = false;
                Boolean sendCustomSuccessHandlingEmailNotif = false;
                Set<String> errLogs = new Set<String>();
                Set<String> successLogs = new Set<String>();
                Integer errornum = 0;
                Integer recordsAffected = 0;

                for(Database.SaveResult dr : ctpToInsertResult) {
                    if (dr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        // System.debug('Successfully inserted ctp with ID: ' + dr.getId());
                        successLogs.add('Successfully created CTP with ID: ' + dr.getId());
                        sendCustomSuccessHandlingEmailNotif = true;
                    }
                    else {
                        processName += ' '+ action;
                        recordsAffected++;
                        sendCustomErrorHandlingEmailNotif = true;
                        Database.Error[] errs = dr.getErrors();
                        Id recordID = dr.getId();
                        System.debug('recordsAffected = '+recordsAffected);
                        for(Database.Error err : errs) {
                            String emailContent = 'Record Id: '+recordID +'<br/> Status Code: ' + err.getStatusCode() + '<br/> Fields: ' + String.join(err.getFields(),',') + '<br/> Error Message: ' + err.getMessage();
                            if(!errLogs.contains(emailContent)){
                                errLogs.add(emailContent);  
                                errornum ++;
                            } 
                        }
                    }
                }
                if(sendCustomErrorHandlingEmailNotif){
                    CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                    CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Error',fixCaseBizibleTouchpointsBatch.class.getName() + ' - ' + processName,errLogs);
                    if(recordsAffected != 0) logWrapper.numberOfRecords = recordsAffected*errornum;
                    cerhand.logWrap = logWrapper;
                    cerhand.sendMail();
                }
                if(sendCustomSuccessHandlingEmailNotif){
                    CustomHandlingEmailNotification cerhand = new CustomHandlingEmailNotification();
                    CustomHandlingEmailNotification.logWrapper logWrapper = new CustomHandlingEmailNotification.logWrapper ('Log',fixCaseBizibleTouchpointsBatch.class.getName() + ' - ' + processName,successLogs);
                    cerhand.logWrap = logWrapper;
                    cerhand.sendMail();
                }
            }
            catch(Exception e){
                System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            }
        }
    }

    global void createCTP(Map<Id,Set<Id>> caseBZTPMap, Case cse, bizible2__Bizible_Touchpoint__c bztp, List<Case_Touch_Point__c> caseTPListToInsert){
        if(caseBZTPMap.get(cse.Id).size() > 0){
            if(!caseBZTPMap.get(cse.Id).contains(bztp.Id)){ //CHECK IF BIZIBLE TP IS ON CASE
                Case_Touch_Point__c ctpToInsert = new Case_Touch_Point__c(Case__c = cse.Id, Bizible_Touchpoint__c = bztp.Id);
                caseTPListToInsert.add(new Case_Touch_Point__c(Case__c = cse.Id,Bizible_Touchpoint__c=bztp.Id));
                System.debug('CASE HAS EXISTING TP - Create Case TP for Bizible Touchpoint ' + bztp.Name +' and Case '+cse.CaseNumber);
            }                                
        }
        //CHECK IF CASE HAS NO EXISTING TP 
        else{
            caseTPListToInsert.add(new Case_Touch_Point__c(Case__c = cse.Id,Bizible_Touchpoint__c=bztp.Id));
            System.debug('CASE HAS NO EXISTING TP -- Create Case TP for Bizible Touchpoint ' + bztp.Name +' and Case '+cse.CaseNumber);
        }
    }

    global void finish(Database.BatchableContext BC) {
        
    } 

    public static void hack() {
            Integer i = 0;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            
    }
}