/**
 * @File Name          : NewRoleControllerTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 22/08/2019, 8:07:09 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    22/08/2019, 5:27:01 am   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
public class NewRoleControllerTest {
    public static List<Account> accList = new List<Account>();
    public static List<Role__c> roleList = new List<Role__c>();

    @testSetup static void setupData() {
		//Create Test Account
		accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
        
        //Create test data for Location
        Location__c loc = new Location__c(
            Name = 'Location test',
            Country__c = 'Australia',
            Postcode__c = '132455',
            State__c = 'Australian Capital Territory',
            State_Acr__c = 'ACT'
        );
        insert loc;
        
        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        Database.insert(address);

        Address__c address2 = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address2.Location__c = loc.Id;
        Database.insert(address2);
	}

    static testMethod void testNewRoleVF() {
        Test.startTest();
            PageReference newRolePage = Page.NewRole;
            Case caseRec = [SELECT Id,Application_Name__c FROM Case LIMIT 1];
            List<Account> personAccList = TestDataFactory.createGenericPersonAccount('TestFirst', 'TestLast', 'nm: Individual',1);
            personAccList[0].PersonEmail = 'testlast0@test.com';
            personAccList[0].PersonMobilePhone = '0123456789';
            Database.insert(personAccList);

            roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseRec.Id, personAccList[0].Id, caseRec.Application_Name__c,  1);
            Database.insert(roleList);
            roleList = [SELECT Id,Account__r.isPersonAccount FROM Role__c];

            System.assertNotEquals(personAccList.size(),0);
            System.assertNotEquals(caseRec,new Case());

            for(Role__c role : roleList){
                System.debug('role IsPersonAccount = '+role.Account__r.isPersonAccount);
            }

            for(Account acc : [SELECT Id,FirstName,LastName,PersonEmail, isPersonAccount FROM Account]){
                System.debug('isPersonAccount = '+acc);
            }

            Test.setCurrentPage(newRolePage);
        	Test.setCurrentPageReference(newRolePage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);

            NewRoleController newRoleCtrl = new NewRoleController();
            newRoleCtrl.role.RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get('Partner - Individual').getRecordTypeId();
            newRoleCtrl.checkIfPersonOrBusiness();
            newRoleCtrl.role.RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get('Partner - Company').getRecordTypeId();
            newRoleCtrl.checkIfPersonOrBusiness();
            newRoleCtrl.ignoreDuplicate();
            newRoleCtrl.getRelAllowedRTs();
            newRoleCtrl.defaultRelationshipType();
            newRoleCtrl.rel.RecordTypeId = Schema.SObjectType.Relationship__c.getRecordTypeInfosByName().get('Spouse').getRecordTypeId();
            newRoleCtrl.defaultRelationshipType();
            //Try Business
            System.debug('1 - '+newRoleCtrl.roleDesignation);
            newRoleCtrl.checkDuplicates();
            newRoleCtrl.role.RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get('Partner - Trust').getRecordTypeId();
            System.debug('2 - '+newRoleCtrl.roleDesignation);
            newRoleCtrl.checkDuplicates();
            newRoleCtrl.dupeAccFound = newRoleCtrl.ignoreDupe = true;
            newRoleCtrl.saveAccount();

            System.debug('2 - '+newRoleCtrl.roleDesignation);

            newRoleCtrl.acc.ABN__c = '3213213213';
            newRoleCtrl.acc.Registered_Business_Name__c = 'Test Business Name';
            newRoleCtrl.acc.Business_Legal_Name__c = 'Test Business Name';
            newRoleCtrl.checkDuplicates();

            //Try Person
            newRoleCtrl.role.RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get('Partner - Individual').getRecordTypeId();
            newRoleCtrl.checkIfPersonOrBusiness();
            newRoleCtrl.conLastName = 'TestLast0';
            System.assertEquals('Person',newRoleCtrl.roleDesignation);
            newRoleCtrl.con.FirstName = 'TestFirst0';
            // newRoleCtrl.con.BirthDate = System.today()-7300;
            //newRoleCtrl.con.MobilePhone = '09231232113';
            newRoleCtrl.con.Email = 'testlast0@test.com';
            newRoleCtrl.con.MobilePhone = '0123456789';
            Id accId = [Select Id From Account Where IsPersonAccount = true  AND FirstName = 'TestFirst0'  AND LastName = 'TestLast0' Order By Name ASC].Id;
            System.assertNotEquals(accId,null);
            newRoleCtrl.checkDuplicates();
            newRoleCtrl.accDupeWrlist[0].isSelected=true;
            newRoleCtrl.populateAccountBaseOnSelectedDuplicate();
            newRoleCtrl.dupeAccFound = newRoleCtrl.ignoreDupe = true;
            newRoleCtrl.saveAccount();
            newRoleCtrl.validateAddRole();

            newRoleCtrl.conLastName = '';
            newRoleCtrl.checkDuplicates();
            newRoleCtrl.saveRole();
            newRoleCtrl.saveRoleAddress();
            newRoleCtrl.saveRelationships();
            newRoleCtrl.showNewAddress();
            newRoleCtrl.role.Account__c = NULL;
            newRoleCtrl.saveRole();
            newRoleCtrl.raddr = new Role_Address__c();
            newRoleCtrl.saveRoleAddress();

            // newRoleCtrl.addr.Location__c = [SELECT Id FROM Location__c].Id;
            //newRoleCtrl.addr.Street_Name__c = 'High St';
            newRoleCtrl.addr.Location__c = null;
            newRoleCtrl.saveAssignNewAddress();

            newRoleCtrl.addr.Location__c = [SELECT Id FROM Location__c].Id;
            newRoleCtrl.populateAddresswithLocation();
            newRoleCtrl.addr.Street_Name__c = 'test';
            newRoleCtrl.addr.Street_Type__c = 'test';
            newRoleCtrl.addr.Street_Number__c = 'test';
            newRoleCtrl.saveAssignNewAddress();

        Test.stopTest();
    }
}