trigger TaskTrigger on Task (before insert, before update, after update, after insert, after delete, after undelete) {

	Integer trigger_insert_loopCount = 0;
    Integer trigger_update_loopCount = 0;

    Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    
    if (trigset.Enable_Task_Trigger__c){

            if (trigger.isUpdate){
                if (trigger_update_loopCount == 0){ //Execute Task Trigger just once
                    if (trigger.isBefore){
                        //call before update event triggers
                        TaskTriggerHandler.onBeforeUpdate(trigger.oldMap,trigger.newMap);
                    }
                    else if (trigger.isAfter){
                        //call after update event triggers 
                        TaskTriggerHandler.onAfterUpdate(trigger.oldMap,trigger.newMap);
                    }
                }
                trigger_update_loopCount++; //increment loop count for every end of object event transaction
            }
            else if (trigger.isInsert){
            	if (trigger_insert_loopCount == 0){  //Execute Task Trigger just once
            		if (trigger.isBefore){
                        //call before insert event triggers
                        TaskTriggerHandler.onBeforeInsert(trigger.new);
                    }
                    else if (trigger.isAfter){
                        //call after insert event triggers 
                        TaskTriggerHandler.onAfterInsert(trigger.oldMap,trigger.newMap);
                    }
            	}
            }
            else if (trigger.isUndelete){
            	if (trigger.isAfter){
            		//call after undelete event triggers
            		TaskTriggerHandler.onAfterUndelete(trigger.oldMap, trigger.newMap);
            	}
            }
    }

}