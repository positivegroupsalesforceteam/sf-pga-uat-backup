trigger RemoveNullAssets on Assets__c (after insert, after update) {
    Assets__c m = [select Value__c from Assets__c where id =:trigger.new[0].id];
    if(m.Value__c == 0 || m.Value__c == null){
        delete m;
    }
}