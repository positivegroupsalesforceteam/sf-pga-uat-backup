/** 
* @FileName: IncomeTrigger
* @Description: Main Trigger for Income1__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 2/15 RDAVID Created Trigger
* 1.1 2/16 JBACULOD Added Trigger Settings in object's trigger
**/ 
trigger IncomeTrigger on Income1__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {


	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Income_Trigger__c){
			TriggerFactory.createHandler(Income1__c.sObjectType);
		}
	}

}