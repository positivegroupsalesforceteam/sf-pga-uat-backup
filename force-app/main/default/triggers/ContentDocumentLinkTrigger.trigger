/**
 * @File Name          : ContentDocumentLinkTrigger.trigger
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 07/01/2020, 11:11:46 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/01/2020   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {

	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_ContentDocumentLink_Trigger__c){
			TriggerFactory.createHandler(ContentDocumentLink.sObjectType);
		}
	}
}