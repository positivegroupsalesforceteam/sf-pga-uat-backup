trigger ApplicationChangeTrigger on Application__ChangeEvent (after insert) {
    
    for (Application__ChangeEvent event : Trigger.New){
        EventBus.ChangeEventHeader header = event.ChangeEventHeader;
        System.debug('Received change event for ' +  header.entityName + ' for the ' + header.changeType + ' operation.'); 
    	// For updates, get all the fields that were explicitly set to null
    	if (header.changetype == 'UPDATE') {
      		if (header.nulledFields.size() > 0) {
                System.debug('The following fields were set to null in the update operation:');
                for (String field : header.nulledFields) {
                    System.debug(field);
                }
            } 
        } 
    }

}