/*
    @Description: handlers all DML events on Liability__c
    @Author: Jesfer Baculod - Positive Group 
    @History:
        09/11/17 - Created, restructured LiabilityTrigger
*/
trigger LiabilityTrigger on Liability__c (after insert,after update, after delete) {

	Integer trigger_insert_loopCount = 0;
    Integer trigger_update_loopCount = 0;

    Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    
    if (trigset.Enable_Liability_Trigger__c){
    	if (trigger.isInsert){
    		if (trigger_insert_loopCount == 0){ //Execute Case Trigger just once
	    		if (trigger.isAfter){
	    			//call after insert event triggers 
	    			LiabilityTriggerHandler.onAfterInsert(trigger.oldMap,trigger.newMap);
	    		}
    		}
    		trigger_insert_loopCount++; //increment loop count for every end of object event transaction
    	}
    	else if (trigger.isUpdate){
    		if (trigger_update_loopCount == 0){ //Execute Case Trigger just once
	    		if (trigger.isAfter){
	    			//call after update event triggers 
	    			LiabilityTriggerHandler.onAfterUpdate(trigger.oldMap,trigger.newMap);
	    		}
    		}
    		trigger_update_loopCount++; //increment loop count for every end of object event transaction
    	}
    	else if (trigger.isDelete){
    		if (trigger.isAfter){
    				//call after delete event triggers 
    				LiabilityTriggerHandler.onAfterDelete(trigger.oldMap);
    		}
    	}

    }

}