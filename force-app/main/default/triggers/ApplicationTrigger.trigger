/** 
* @FileName: ApplicationTrigger
* @Description: Main Trigger for Application object used in New Model
* @Copyright: Positive (c) 2018
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 4/05/18 JBACULOD Created
**/ 
trigger ApplicationTrigger on Application__c (before insert, before update, 
                                        before delete, after insert, 
                                        after update, after delete, after undelete) {
    
    if (TriggerFactory.trigset.Enable_Triggers__c){
        if (TriggerFactory.trigset.Enable_Application_Trigger__c){
            TriggerFactory.createHandler(Application__c.sObjectType);
        }
    } 

}