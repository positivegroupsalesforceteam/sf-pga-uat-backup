trigger AddressChangeTrigger on Address__ChangeEvent (after insert) {
    public map <Id, boolean> isProcessedMap = TriggerFactory.isProcessedMap;
    system.debug('@@isProcessedMap pre:'+isProcessedMap);
    for (Address__ChangeEvent event : Trigger.New){
        EventBus.ChangeEventHeader header = event.ChangeEventHeader;
        System.debug('Received change event for ' +  header.entityName + ' for the ' + header.changeType + ' operation.'); 
    	// For updates, get all the fields that were explicitly set to null
    	if (header.changetype == 'UPDATE') {
            string recID1 = '';
            for (string recID : header.recordids){
                recID1 = recID;
            }
            if (!isProcessedMap.containskey(recID1)){ 
                isProcessedMap.put(recID1, false);
                if (!isProcessedMap.get(recID1)){
                    //ValidationListController.getValidationListForObject(recID1, 'Address__c');
                    isProcessedMap.put(recID1, true);
                }
            }
            
      		/*if (header.nulledFields.size() > 0) {
                System.debug('The following fields were set to null in the update operation:');
                for (String field : header.nulledFields) {
                    System.debug(field);
                }
            } */ 
        } 
    }
    system.debug('@@isProcessedMap post:'+isProcessedMap);
}