({
	loadFormData: function(component,event) {
		console.log('loadFormData');
        var loadDataToSubmit = component.get("c.loadData");
        loadDataToSubmit.setParams({  returnwrapperClass : component.get("v.initWrapper")  });
        loadDataToSubmit.setCallback(this, function(response) {
			var state = response.getState();
			// alert(state);
            if(state === 'SUCCESS'){
				console.log('success');
				var initWrap = response.getReturnValue();
				component.set("v.apiTransfer", initWrap.apiTransferData);
                component.set("v.apiTransfer.EQ_Role__c",initWrap.selectedRoleId);
                component.set("v.initWrapper",initWrap);
                console.log('initWrap = ',initWrap);
            }
            else if(state === 'ERROR'){
                //var list = response.getReturnValue();
                //component.set("v.picvalue", list);
                // component.set("v.doneInitApexCall", true);
                console.log('error');
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
            }
        })
        $A.enqueueAction(loadDataToSubmit);
    },

    waitForAPITransferUpdate: function(component,event,helper) {
        console.log('im waitForAPITransferUpdate');
        component._interval = setInterval($A.getCallback(function () {
            component.set("v.isProgressing",true);
            component.set("v.progress",0);
            component.find('recordLoader').reloadRecord(true);
            var status = component.get('v.apiTransferRecordLoader').EQ_Request_Status__c;
            console.log('waitForAPITransferUpdate = '+status);
            if($A.util.isUndefined(status)){ //Info
                //clearInterval(component._interval);
                component.set("v.progressMessage",'Creating API Transfer for Equifax...'); 
                component.set("v.progress",15);
                component.set("v.msgType",'info');
                
                console.log('Creating API Transfer for Equifax...');
            } 
            if(status == 'Created'){ //Info
                component.set("v.msgType",'info');
                component.set("v.progressMessage",'API Transfer Created in Salesfore, sending request to Equifax...'); 
                component.set("v.progress",30);
                console.log('API Transfer Created in Salesfore, sending request to Equifax...');
            } 
            if(status == 'Processing'){ //Info
                component.set("v.msgType",'info');
                component.set("v.progressMessage",'Equifax processing the report...'); 
                component.set("v.progress",45);
                console.log('Equifax processing the report...');
            } 

            if(status == 'EQ Done'){ //Info
                component.set("v.msgType",'info');
                // clearInterval(component._interval); 
                component.set("v.progressMessage",'Equifax retrieved the report, processing PDF file...'); 
                component.set("v.progress",60);
                console.log('Equifax retrieved the report, processing PDF file...');
            } 

            if(status == 'PE Done'){ //Info
                component.set("v.msgType",'info');
                component.set("v.progressMessage",'PDF file retrieved, creating attachment in Salesforce...'); 
                component.set("v.progress",75);
                console.log('PDF file retrieved, creating attachment in Salesforce...');
            } 

            if(status == 'SF Done'){ //Confirm
                component.set("v.msgType",'confirm');
                component.set("v.progressMessage",'Attachment created, moving to Case Box folder...'); 
                console.log('Attachment created, moving to Case Box folder...');
                component.set("v.progress",90);
            } 

            if(status == 'Box Done'){ //Confirm
                
                component.set("v.msgType",'confirm');
                component.set("v.progressMessage",'PDF moved to Box Folder, opening Box folder...'); 
                console.log('PDF moved to Box Folder, opening Box folder...');
                component.set("v.isProgressing",false); 
                helper.fireEquifaxAppEvt(component, 'redirecttobox');
                component.set("v.progress",100); 
                clearInterval(component._interval);
                
            } 

            if(status == 'JB Failed'){ //Error
                component.set("v.msgType",'error');
                component.set("v.progressMessage",'Jitterbit Operation failed, Please contact System Admin.'); 
                console.error('Jitterbit Operation failed, Please contact System Admin.');
                component.set("v.progress",100);
                component.set("v.isProgressing",false);
                clearInterval(component._interval); 
            } 

            if(status == 'EQ Failed'){ //Error
                component.set("v.msgType",'error');
                component.set("v.progressMessage",'Equifax Operation failed, Please contact System Admin.'); 
                component.set("v.errMsg",component.get('v.apiTransferRecordLoader').Integration_Log__c); 
                console.error('Equifax Operation failed, Please contact System Admin.');
                component.set("v.progress",100);
                component.set("v.isProgressing",false);
                clearInterval(component._interval); 
            } 

            if(status == 'PE Failed'){
                component.set("v.msgType",'error');
                component.set("v.progressMessage",'Previous Enquiry failed (PDF Retrieval), Please contact System Admin.'); //Error 
                console.error('Previous Enquiry failed (PDF Retrieval), Please contact System Admin.');
                component.set("v.progress",100);
                component.set("v.isProgressing",false);
                clearInterval(component._interval); 
            } 

            if(status == 'SF Failed'){
                component.set("v.msgType",'error');
                component.set("v.progressMessage",'Unable to attach file to SF, Please contact System Admin.'); //Error 
                console.error('Unable to attach file to SF, Please contact System Admin.');
                component.set("v.progress",100);
                component.set("v.isProgressing",false);
                clearInterval(component._interval); 
            }
            
            if(status == 'Box Failed'){
                component.set("v.msgType",'error');
                component.set("v.progressMessage",'Unable to move PDF file to Box, Please contact System Admin.'); //Error 
                component.set("v.errMsg",component.get('v.apiTransferRecordLoader').Integration_Log__c);
                console.error('Unable to move PDF file to Box, Please contact System Admin.');
                component.set("v.progress",100);
                component.set("v.isProgressing",false);
                clearInterval(component._interval); 
            }             

        }), 2000);
    },

    fireEquifaxAppEvt : function(component, actionStr) {
        // var action = 'redirecttobox';
        var caseID = component.get('v.apiTransfer').Case__c;
        console.log('Im firing fireEquifaxToVFAppEvt - '+actionStr);
        var appEvent = $A.get("e.c:EquifaxToVFAppEvt");
        if(actionStr == 'redirecttobox'){
            appEvent.setParams({    "action" : actionStr,
                                    "param"  : '/apex/box__CaseBoxSection?id='+caseID });
            appEvent.fire();
        }
        else if (actionStr == 'cancel'){
            appEvent.setParams({    "action" : actionStr });
            appEvent.fire();
        }
    },

    saveAPITransfer : function(component,event,helper) {
        var apiTransfer = JSON.parse(JSON.stringify(event.getParam("apiTransfer")));
        component.set("v.progressMessage",'Creating API Transfer for Equifax...');
        component.set("v.isProgressing",true);
        console.log("Received component event with param = ", apiTransfer);
        component.set("v.apiTransfer",apiTransfer);
        var saveAPITrans = component.get("c.saveAPITransfer");
        saveAPITrans.setParams({  apitransfer : component.get("v.apiTransfer")  });
        saveAPITrans.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var apitransferRecord = response.getReturnValue();
                console.log("saveAPITransfer SUCCESS = ", apitransferRecord);
                component.set("v.apiTransfer",apitransferRecord);//causing issue
                component.set("v.apiTransferRecordLoader",apitransferRecord);
                component.set("v.showPopup",true);
                component.set("v.showRoleSelect",false);
                helper.waitForAPITransferUpdate(component,event,helper);//polling
            }
            else if(state === 'ERROR'){
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error('ERROR - ',message);
            }
        })
        $A.enqueueAction(saveAPITrans);
    },

    buildHasCurrentEnquiryMsg : function(component,event,helper) {
        console.log('buildHasCurrentEnquiryMsg');
        var apiTransfer = component.get("v.apiTransfer");
        var initWrap = component.get("v.initWrapper");
        var msg = 'Selected Role [' + apiTransfer.EQ_First_Name__c + ' ' + apiTransfer.EQ_Last_Name__c + '] has existing Equifax Enquiry Id ['+initWrap.equifaxEnquiryId+']. Please check Case Box Folder first before submitting an Enquiry to Equifax.';
        return msg;
    }
})