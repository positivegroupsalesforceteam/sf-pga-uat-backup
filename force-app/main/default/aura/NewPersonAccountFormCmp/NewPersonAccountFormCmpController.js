({
	init: function(component, event, helper) {
        component.set("v.initContentloaded",true);
        let isFromLead = component.get("v.fromLead");
        if (isFromLead){ //New Partner is accessed from Convert button of Lead record
            let leadToConvert = component.get("v.leadObj");
            //Pre-populate Account with Lead fields for conversion
            if (leadToConvert != undefined){
                component.find("personFName").set("v.value", leadToConvert.FirstName);
                component.find('personLName').set('v.value', leadToConvert.LastName);
                component.find('personMobile').set('v.value', leadToConvert.Phone);
                component.find('personEmail').set('v.value', leadToConvert.Email); 
            }
        }
    },
    
    handleButton: function(component, event, helper) {
        // console.log('whatBtn --> ',event.getSource().get("v.name"));
        var btnName = event.getSource().get("v.name");
        if (btnName == 'save') {
            var personEmailValid = component.find('personEmail').get("v.value");
            var personFNameValid = component.find('personFName').get("v.value");
            var personLNameValid = component.find('personLName').get("v.value");
            var valid = false;
            
            if(!$A.util.isEmpty(personEmailValid) && personEmailValid.replace(/\s/g, '').length &&
            !$A.util.isEmpty(personFNameValid) && personFNameValid.replace(/\s/g, '').length &&
            !$A.util.isEmpty(personLNameValid) && personLNameValid.replace(/\s/g, '').length){
                // alert('save address');
                valid = true;
            }

            else{
                valid = false;
                window.scrollTo(0, 0);
                helper.fireCustomToast(component,'error','Please enter Firstname, Lastname and Email.');
            }


            if(valid){
                component.set('v.showSpinner', true);
                component.set('v.disabled', true);
                component.set('v.whatBtn', btnName);
                component.find("personAccountForm").submit();
            }
        }
    },

    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
	},
	handleLoad: function(component, event, helper) {
        component.set("v.showSpinner", false);
        var toggleNewAccForm = component.find("newAccForm");
        $A.util.removeClass(toggleNewAccForm, "slds-hidden");
		// component.find('partnerInput').set('v.value',component.get("v.partnerId"));
    },

    handleSubmit: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disabled', true);
    },

    handleSuccess: function(component, event, helper) {
        
        var payload = event.getParams().response;
        var whatBtn = component.get('v.whatBtn');

		// console.log('Account Saved! payload = '+payload.id);
		component.set('v.personAccountId', payload.id);
		// component.set('v.personAccountId', component.find('assAcc').get('v.value'));
		component.set('v.saved', true);
        component.set('v.disabled', false);
        component.set('v.showSpinner', false);
        if(whatBtn == 'save'){
            helper.fireCustomToast(component,'success','Account successfully saved!');
            component.set('v.selectedTab', 'relationship_details');
        }
    },

    handleError: function(component, event, helper) {
        component.set('v.saved', false);
		component.set('v.disabled', false);
		component.set('v.showSpinner', false);
	},
})