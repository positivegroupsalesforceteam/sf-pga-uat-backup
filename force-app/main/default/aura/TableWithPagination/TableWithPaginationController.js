({
	doInit : function(component, event, helper) {      
        helper.initData(component, helper);
    },
    
    onNext : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.buildData(component, helper);
    },
    
    onPrev : function(component, event, helper) {        
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.buildData(component, helper);
    },
    
    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        helper.buildData(component, helper);
    },
    
    onFirst : function(component, event, helper) {        
        component.set("v.currentPageNumber", 1);
        helper.buildData(component, helper);
    },
    
    onLast : function(component, event, helper) {        
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        helper.buildData(component, helper);
    },

    search : function(component, event, helper) {   
        var args = event.getParam("arguments");
        var testString = args.searchStr;
        helper.initData(component, helper);
    },

    handleOnRowAction : function(component, event, helper) {   
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'linkToPartner':
                // alert('linkToPartner ' +row.Id);
                component.set("v.recordId",row.Id);
                component.set("v.selectedTab","partner_details");
                // helper.switchProfile(row,cmp,event);
                break;
            default:
                // alert('linkToPartner default ' +row.Id);
                // helper.switchProfile(row,cmp,event);
                break;
        }
    },
  
})