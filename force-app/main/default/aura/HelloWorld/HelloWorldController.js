({
    init: function (cmp, event, helper) {
        var actions = [
            { label: 'Switch Profile', name: 'switch_profile' }
        ]
        cmp.set('v.mycolumns', [
            { label: 'Name', fieldName: 'Name', type: 'text'},
            { label: 'Profile', fieldName: 'ProfileName', type: 'text'},
            { label: 'Role', fieldName: 'UserRoleName', type: 'text'},
            { label: 'Action', type: 'button', initialWidth: 250, typeAttributes: { label: {fieldName: 'actionLabel'}, name: 'switch_profile', iconName: 'utility:sort', disabled: {fieldName: 'actionDisabled'}, variant:{fieldName: 'actionVariant'}, title: 'Click to Switch Profile'}},
        ]);
        helper.getData(cmp);
            
       	console.log(cmp.get('v.mycolumns'));
    },
            
    handleKeyUp: function (cmp, evt, helper) {
        var isEnterKey = evt.keyCode === 13;
        var queryTerm = cmp.find('enter-search').get('v.value');
        //if (isEnterKey) {
            cmp.set('v.issearching', true);
            //setTimeout(function() {
                console.log('Searched for "' + queryTerm + '"!');
                cmp.set('v.issearching', false);
                helper.getData(cmp);
            //}, 2000);
        //}
    },
            
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'switch_profile':
                helper.switchProfile(row,cmp,event);
                break;
            default:
                helper.switchProfile(row,cmp,event);
                break;
        }
    },
})