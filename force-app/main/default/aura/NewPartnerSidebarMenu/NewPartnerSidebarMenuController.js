({
    // init - Runs on component load
    init: function (component) {

        console.log('>> NewPartnerSidebarMenu fromLead: '+component.get("v.fromLead"));
        console.log('>> NewPartnerSidebarMenu leadObj: '+JSON.stringify(component.get("v.leadObj")));

        // console.log('NewPartnerSidebarMenuController');
        if($A.util.isEmpty(component.get("v.selectedTab"))){
            // console.log('blank selected tab set to partner_details');
            component.set('v.selectedTab', 'partner_details');
        }
        if(!$A.util.isEmpty(component.get("v.branchParentId"))){
            component.set('v.title', 'Partner Branch Details');
        }
        else{
            component.set('v.title', 'Partner Details');
        }
    },

    // handleMenuSelected - Invoked when user clicks menu item
    handleMenuSelected: function(component, event, helper) {
		var selected = event.getParam('name');
		// console.log(selected);
        component.set('v.selectedTab', selected);

    },

    // handleCreateBranch - Invoked when Create Branch btn is clicked
    handleCreateBranch: function(component, event, helper) {
        
        var parentPartnerId = event.getParam("branchParentId");
        var branchRTName = event.getParam("branchRTName");
        var branchRTId = event.getParam("branchRTId");
        var branchParentRTName = event.getParam("branchParentRTName");
        
        var myEvent = component.getEvent("creBraBtnClk2");
		myEvent.setParams({	"branchParentId": parentPartnerId,
                            "branchRTName": branchRTName,
                            "branchRTId": branchRTId,
                            "branchParentRTName": branchParentRTName
                        });
        myEvent.fire(); 

    },
})