({
    // init - Runs on component load
    init: function (component, event, helper) {
        var actions = [
            { label: 'Link to Partner', name: 'linkToPartner' }
        ]
        component.set('v.addressTblColumns', [
            { label: 'Name', fieldName: 'Name', type: 'text' },
            { label: 'Full Address', fieldName: 'Full_Address__c', type: 'text' },
            {
                label: 'Action', type: 'button', typeAttributes: {
                    label: 'Link to Partner',
                    name: 'linkToPartner',
                    size: 'medium',
                    disabled: { fieldName: 'actionDisabled' },
                    variant: 'base',
                    title: 'Click to Link Address to Partner'
                }
            },
        ]);

        // component.set('v.doneloading', true);
        // component.set('v.showSpinner', false);
        // console.log(component.get('v.addressTblColumns'));
        helper.getData(component, helper);
    },
    
    // handleButtonClick - handles button action for Search & New button
    handleButtonClick: function (component, event, helper) {
        var btnLbl = event.getSource().get("v.title");
        // console.log('btnLbl -> '+btnLbl);
        if (btnLbl == 'Search') {
            component.set('v.searchStr', component.find("enter-search").get("v.value"));
            component.set('v.searchAddress', true);
            helper.getData(component, helper, true);
        }
        else if (btnLbl == 'New') {
            component.set('v.showSpinner', true);
            component.set('v.title', 'New Address');
            component.set('v.pageSubHeader', 'Use this page to create Address records.');
            component.set('v.searchAddress', false);
        }
    },

    // handleButton - handles button action of the Address footer form/card
    handleButton: function (component, event, helper) {
        // alert('handleButton');
        var btnLbl = event.getSource().get("v.label");
        if (btnLbl == 'Back to Search') {
            // component.set('v.searchStr', component.find("enter-search").get("v.value"));
            // component.set('v.disableSearchBtns', false);
            component.set('v.title', 'Address Search');
            component.set('v.searchAddress', true);
            component.set('v.pageSubHeader', 'Use this page to search Address record.');
            // component.set('v.showSpinner', true);
        }
        else if (btnLbl == 'Save & Link to Partner') {
            var addressLocationValid = component.find('locIf').get("v.value");
            var strNumValid = component.find('strNumIf').get("v.value");
            var strNamValid = component.find('strNamIf').get("v.value");
            var strTypValid = component.find('strTypIf').get("v.value");

            // alert(partnerNameValid);
            // console.log( 'test -- '+ addressLocationValid + '--- test');
            if(!$A.util.isEmpty(addressLocationValid) && addressLocationValid.replace(/\s/g, '').length &&
            !$A.util.isEmpty(strNumValid) && strNumValid.replace(/\s/g, '').length &&
            !$A.util.isEmpty(strNamValid) && strNamValid.replace(/\s/g, '').length &&
            !$A.util.isEmpty(strTypValid) && strTypValid.replace(/\s/g, '').length ){
                // alert('save address');
                component.set('v.whatBtn', btnLbl);
                component.find("newAddressForm").submit();
            }
            else{
                window.scrollTo(0, 0);
                helper.fireCustomToast(component,'error','Please populate all address fields.');
            }
        }

    },

    handleSectionToggle: function (component, event) {
        // alert('handleSectionToggle');
    },

    // handleLoad - Invokes when lightning:recordEditForm is loaded
    handleLoad: function (component, event, helper) {
        component.set("v.showSpinner", false);
        var toggleAddressFooter = component.find("addressFooter");
        $A.util.removeClass(toggleAddressFooter, "slds-hidden");
    },

    // handleSubmit - Invokes when lightning:recordEditForm is submitted
    handleSubmit: function (component, event, helper) {
        // alert('handleSubmit');
        component.set('v.showSpinner', true);
        component.set('v.disabled', true);
    },

    // handleSuccess - Invokes when record is successfully saved
    handleSuccess: function (component, event, helper) {
        // alert('handleSuccess');
        var payload = event.getParams().response;
        var whatBtn = component.get('v.whatBtn');
        // console.log('Address Saved! payload = ' + payload.id);
        component.set('v.addressId', payload.id);
        // component.set('v.personAccountId', component.find('assAcc').get('v.value'));
        component.set('v.saved', true);
        component.set('v.disabled', false);
        component.set('v.showSpinner', false);

        if (whatBtn == 'Save & Link to Partner') {
            helper.fireCustomToast(component,'success','Address record successfully saved.')
            component.set('v.selectedTab', 'partner_details');
        }
    },
    
    // handleError - Invokes when record is NOT successfully saved
    handleError: function (component, event, helper) {
        component.set('v.saved', false);
        component.set('v.disabled', false);
        component.set('v.showSpinner', false);
    },

})