({
	getRoleOptions : function(component) {
		var roleRec = component.get("v.initWrapper").mainRec.Roles__r;
		console.log('getRoleOptions == ',component.get("v.initWrapper"));
		var roles = [];
		console.log('roleRec = ',roleRec);
		for (var x in roleRec) {
			console.log(roleRec[x].Account_Name__c);
			roles.push({
				label: roleRec[x].Account_Name__c + ' - ' +roleRec[x].Role_Type__c,
				value: roleRec[x].Id
			});
		}
		component.set("v.options", roles); 
	},

	proceedExisting : function(component,event,helper,option) {
		// retrieve // apply
		var myEvent = component.getEvent("submitBtnClkEvt");
		switch(option){
			case 'openBox':
				myEvent.setParams({	"action": 'openBox'});
				break;
			case 'retrieve':
				component.set("v.hasExistingEnquiryId",false);
				component.set("v.apiTransfer.Equifax_Request_Type__c","Previous Enquiry"); 
				myEvent.setParams({	"apiTransfer": component.get("v.apiTransfer"),
				"action": 'retrieve'});
				break;
			case 'apply':
				component.set("v.hasExistingEnquiryId",false);
				component.set("v.apiTransfer.Equifax_Request_Type__c","Equifax Apply"); 
				myEvent.setParams({	"apiTransfer": component.get("v.apiTransfer"),
									"action": 'apply'});
				break;
		}
		// if(option == 'openBox'){
			myEvent.fire();
		// }
	},

	fireEquifaxAppEvt : function(component, actionStr) {
        // var action = 'redirecttobox';
        console.log('Im firing fireEquifaxToVFAppEvt - '+actionStr);
        var appEvent = $A.get("e.c:EquifaxToVFAppEvt");
        if (actionStr == 'cancel'){
            appEvent.setParams({    "action" : actionStr });
            appEvent.fire();
        }
    },
})