({
	setRoleRTHelper : function(component, roleRTs, selRoleRTId) {
		var newRole = component.get("v.newRole");
		if(selRoleRTId != undefined){
			newRole.RecordTypeId = selRoleRTId; //Set Record Type
            if (roleRTs != undefined){
                for (var i=0; i< roleRTs.length; i++){
                    if (selRoleRTId === roleRTs[i].id){
                        component.set("v.selRoleRTName", roleRTs[i].name);
                        console.log("aSelected Role RT: "+ roleRTs[i].name);
                        break;
                    }
                }
            }
		}
		else{
			//Set default Role Record Type base from existing Role
			component.set("v.selRoleRT", component.get("v.wrappedRoleData.role.RecordTypeId"));
			component.set("v.selRoleRTName", component.get("v.wrappedRoleData.role.RecordType.Name"));
			//component.set("v.selRoleRT", roleRTs[0].id);
			//component.set("v.selRoleRTName", roleRTs[0].name);
		}
	},

	setRelatedListRows : function(component, response, helper){
		//Set Role Addresses
		var raddrrowdata = response.getReturnValue().raddrlist;
        if (raddrrowdata != undefined){
            for (var i=0; i <raddrrowdata.length; i++){
                if (raddrrowdata[i].RecordType) raddrrowdata[i].recordTypeName = raddrrowdata[i].RecordType.Name;
            }
            component.set("v.roleAdrRows", raddrrowdata); 
        }

		//Set FO Shares
		var incRows = []; var expRows = []; var assRows = []; var liaRows = [];
		var foshareData = response.getReturnValue().fosharelist;
        if (foshareData != undefined){
            for (var i=0; i < foshareData.length; i++){
                if (foshareData[i].RecordType.Name == 'Income') incRows.push(foshareData[i]);
                if (foshareData[i].RecordType.Name == 'Expense') expRows.push(foshareData[i]);
                if (foshareData[i].RecordType.Name == 'Asset') assRows.push(foshareData[i]);
                if (foshareData[i].RecordType.Name == 'Liability') liaRows.push(foshareData[i]);
            }
            console.log('liaRows: '+liaRows);
            component.set("v.incRows", incRows);
            component.set("v.expRows", expRows);
            component.set("v.assRows", assRows);
            component.set("v.liaRows", liaRows);
        }
	},

	setRelatedListColumns : function(component, helper){
		//Role Address
		component.set('v.roleAdrColumns',
			[
				{label: 'Role Address ID', fieldName: 'Name', type:'text'},
				{label: 'Active Address?', fieldName: 'Active_Address__c', type:'text'},
				{label: 'Full Address', fieldName: 'Full_Address__c', type:'text'},
				{label: 'Start Date', fieldName: 'Start_Date__c', type:'date'},
				{label: 'End Date', fieldName: 'End_Date__c', type:'date'},
				{label: 'Record Type', fieldName: 'recordTypeName', type:'text'} 
			]
		);
		//Incomes
		component.set('v.incColumns',
			[
				{label: 'Income', fieldName: 'Income_Name__c', type:'text'},
				{label: 'FO Record Type', fieldName: 'FO_Record_Type__c', type:'text'},
				{label: 'Type', fieldName: 'Type__c', type:'text'},
				{label: 'Monthly Amount', fieldName: 'Monthly_Amount__c', type:'currency', cellAttributes : { alignment : 'left'} },
				{label: 'Role Share (%)', fieldName: 'Role_Share__c', type:'number', cellAttributes : { alignment : 'left'} }, //using percent has display issues at the moment as per SF
				{label: 'Role Share Amount', fieldName: 'Role_Share_Amount__c', type:'currency', cellAttributes : { alignment : 'left'}}
			]
		);
		//Expenses
		component.set('v.expColumns',
			[
				{label: 'Expense', fieldName: 'Expense_Name__c', type:'text'},
				{label: 'FO Record Type', fieldName: 'FO_Record_Type__c', type:'text'},
				{label: 'Type', fieldName: 'Type__c', type:'text'},
				{label: 'Monthly Amount', fieldName: 'Monthly_Amount__c', type:'currency', cellAttributes : { alignment : 'left'} },
				{label: 'Role Share (%)', fieldName: 'Role_Share__c', type:'number', cellAttributes : { alignment : 'left'}},
				{label: 'Role Share Amount', fieldName: 'Role_Share_Amount__c', type:'currency', cellAttributes : { alignment : 'left'}}
			]
		);
		//Assets
		component.set('v.assColumns',
			[
				{label: 'Asset', fieldName: 'Asset_Name__c', type:'text'},
				{label: 'FO Record Type', fieldName: 'FO_Record_Type__c', type:'text'},
				{label: 'Type', fieldName: 'Type__c', type:'text'},
				{label: 'Monthly Amount', fieldName: 'Monthly_Amount__c', type:'currency', cellAttributes : { alignment : 'left'}},
				{label: 'Role Share (%)', fieldName: 'Role_Share__c', type:'number', cellAttributes : { alignment : 'left'} },
				{label: 'Role Share Amount', fieldName: 'Role_Share_Amount__c', type:'currency', cellAttributes : { alignment : 'left'}}
			]
		);
		//Liabilities
		component.set('v.liaColumns',
			[
				{label: 'Liability', fieldName: 'Liability_Name__c', type:'text'},
				{label: 'FO Record Type', fieldName: 'FO_Record_Type__c', type:'text'},
				{label: 'Type', fieldName: 'Type__c', type:'text'},
				{label: 'Monthly Amount', fieldName: 'Monthly_Amount__c', type:'currency', cellAttributes : { alignment : 'left'}},
				{label: 'Role Share (%)', fieldName: 'Role_Share__c', type:'number', cellAttributes : { alignment : 'left'}},
				{label: 'Role Share Amount', fieldName: 'Role_Share_Amount__c', type:'currency', cellAttributes : { alignment : 'left'}}
			]
		);
	},

	cloneRoleAddress : function(component, createdRole, createdCase, helper){
		//Clone Role Addresses
		var raddrlist = component.get("v.wrappedRoleData.raddrlist");
		for(var i=0; i< raddrlist.length; i++){
			if (raddrlist[i].Id != undefined) delete raddrlist[i].Id; //Removes Id from source Role Addresses
			if (raddrlist[i].Role__c != undefined) delete raddrlist[i].Role__c;
			raddrlist[i].Role__c = createdRole.Id;
		}
		// Create the action
		var actionRA = component.get("c.cloneRoleAddresses");
		actionRA.setParams(
			{ roleAddresslist : raddrlist }
		);
		// Add callback behavior for when response is received
        actionRA.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
				console.log('Role Addresses created');
				console.log('createdCase: '+createdCase.Id);
				helper.afterRoleAddress(component,createdRole,createdCase,helper);
			}
			else {
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
            }
		});
		// Send action off to be executed
		$A.enqueueAction(actionRA);
	},

	afterRoleAddress : function(component, createdRole, createdCase, helper){
		console.log('cloning Income');
		helper.cloneIncome(component, createdRole, createdCase, helper);
	},

	afterCloning : function(createdCase){
		if (createdCase != undefined){
			//Dipsplay Prompt on New Case
			var displayPromptCaseEvent = $A.get("e.c:NewCaseEvent");
			displayPromptCaseEvent.fire();
			console.log('event '+displayPromptCaseEvent);
		} 
	},

	cloneIncome : function(component, createdRole, createdCase, helper){
		var wRD = component.get("v.wrappedRoleData");
		var inclist = wRD.inclist;
		var foLinklist = wRD.foLinklist;
		var newinclist = [];
		var retryctr = 0;
		if (inclist != undefined){
			for(var i=0; i< inclist.length; i++){
				var incWr = inclist[i];
				if (incWr.inc.Id != undefined){
					delete incWr.inc.Id; //Removes Id from source Income
				}
				if (incWr.inc.Role__c != undefined) delete incWr.inc.Role__c;
				incWr.inc.Role__c = createdRole.Id;
				incWr.inc.Application1__c = createdRole.Application__c;
				incWr.inc.h_Auto_Create_FO_Share__c = true;
				incWr.inc.Equal_Share_for_Roles__c = true;
				newinclist.push(incWr);
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneIncome");
		actionFO.setParams(
			{ incWrlist : newinclist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Income created');
				else console.log('No Income increated');
				component.set("v.newIncome", res);
				//Clone Expense next
				setTimeout(
					$A.getCallback(function() {
						console.log('Cloning Expenses');
						helper.cloneExpenses(component, createdRole, createdCase, helper) 
					}), 1000);
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				//Retry Cloning of Expenses
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneExpenses : function(component, createdRole, createdCase, helper){
		var wRD = component.get("v.wrappedRoleData");
		var newRole = component.get("v.newRole");
		var explist = wRD.explist;
		var newexplist = [];
		var retryctr = 0;
		if (explist != undefined){
			for(var i=0; i< explist.length; i++){
				var expWr = explist[i];
				/*if ( ( (expWr.exp.To_Be_Paid_Out__c || !expWr.exp.To_Be_Paid_Out__c) && newRole.Case__r.Status != 'Closed' && newRole.Case__r.Stage__c != 'Won' ) ||
					( !expWr.exp.To_Be_Paid_Out__c && newRole.Case__r.Status == 'Closed' && newRole.Case__r.Stage__c == 'Won' )
				){*/
				if(!expWr.exp.To_Be_Paid_Out__c){
					if (expWr.exp.Id != undefined) delete expWr.exp.Id; //Removes Id from source Expense
					if (expWr.exp.Application1__c != undefined) delete expWr.exp.Application1__c;
					expWr.exp.Application1__c = createdRole.Application__c;
					expWr.exp.h_Auto_Create_FO_Share__c = true;
					expWr.exp.Equal_Share_for_Roles__c = true;
					/*var rolesplit = expWr.exp.h_FO_Share_Role_IDs__c.split(';');
					var roleshare = rolesplit.length;
					if (rolesplit[roleshare - 1] == '') roleshare  = roleshare - 1;
					expWr.exp.Payment_Amount__c  = expWr.exp.Payment_Amount__c / roleshare; */
					expWr.exp.h_FO_Share_Role_IDs__c = createdRole.Id +';';
					newexplist.push(expWr);
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneExpenses");
		actionFO.setParams(
			{ expWrlist : newexplist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Expenses created');
				else console.log('No Expense created');
				component.set("v.newExpenses", res);
				//Clone Asset next
				setTimeout(
					$A.getCallback(function() {
						console.log('Cloning Assets');
						helper.cloneAssets(component, createdRole, createdCase, helper) 
					}), 1000);
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Expenses [' + retryctr + ']' );
				//Retry Cloning of Assets
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneAssets : function(component, createdRole, createdCase, helper){
		var wRD = component.get("v.wrappedRoleData");
		var asslist = wRD.asslist;
		var newasslist = [];
		var retryctr = 0;
		if (asslist != undefined){
			for(var i=0; i< asslist.length; i++){
				var assWr = asslist[i];
				if (!assWr.ass.Being_Sold_Traded__c){
					if (assWr.ass.Id != undefined) delete assWr.ass.Id; //Removes Id from source Asset
					if (assWr.ass.Application1__c != undefined) delete assWr.ass.Application1__c;
					assWr.ass.Application1__c = createdRole.Application__c;
					assWr.ass.h_Auto_Create_FO_Share__c = true;
					assWr.ass.Equal_Share_for_Roles__c = true;
					/*var rolesplit = ass.h_FO_Share_Role_IDs__c.split(';');
					var roleshare = rolesplit.length;
					if (rolesplit[roleshare - 1] == '') roleshare = roleshare - 1;
					assWr.ass.Asset_Value__c = assWr.ass.Asset_Value__c / roleshare; */
					assWr.ass.h_FO_Share_Role_IDs__c = createdRole.Id +';';
					newasslist.push(assWr);
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneAssets");
		actionFO.setParams(
			{ assWrlist : newasslist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Assets created');
				else console.log('No Assets created');
				component.set("v.newAssets", res);
				//Clone Liabilities next
				setTimeout(
					$A.getCallback(function() {
						console.log('cloning liabilities');
						helper.cloneLiabilities(component, createdRole, createdCase, helper) 
					}), 1000);
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Assets [' + retryctr + ']' );
				//Retry Cloning of Liabilities
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneLiabilities : function(component, createdRole, createdCase, helper){
		var wRD = component.get("v.wrappedRoleData");
		var newRole = component.get("v.newRole");
		var lialist = wRD.lialist;
		var newlialist = [];
		var retryctr = 0;
		if (lialist != undefined){
			for(var i=0; i< lialist.length; i++){
				var liaWr = lialist[i];
				/*if ( ( (liaWr.lia.To_Be_Paid_Out__c || !liaWr.lia.To_Be_Paid_Out__c) && newRole.Case__r.Status != 'Closed' && newRole.Case__r.Stage__c != 'Won' ) ||
					( !liaWr.lia.To_Be_Paid_Out__c && newRole.Case__r.Status == 'Closed' && newRole.Case__r.Stage__c == 'Won' )
				){ */
				if (!liaWr.lia.To_Be_Paid_Out__c){
					if (liaWr.lia.Id != undefined) delete liaWr.lia.Id; //Removes Id from source Liability
					if (liaWr.lia.Application1__c != undefined) delete liaWr.lia.Application1__c;
					liaWr.lia.Application1__c = createdRole.Application__c;
					liaWr.lia.h_Auto_Create_FO_Share__c = true;
					liaWr.lia.Equal_Share_for_Roles__c = true;
					/*var rolesplit = liaWr.lia.h_FO_Share_Role_IDs__c.split(';');
					var roleshare = rolesplit.length;
					if (rolesplit[roleshare - 1] == '') roleshare = roleshare - 1;
					liaWr.lia.Amount_Owing__c = liaWr.lia.Amount_Owing__c / roleshare; */
					liaWr.lia.h_FO_Share_Role_IDs__c = createdRole.Id +';';
					newlialist.push(liaWr);
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneLiabilities");
		actionFO.setParams(
			{ liaWrlist : newlialist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Liabilities created');
				else console.log('No Liabilities created');
				component.set("v.newLiabilities", res);
				//Clone FO Links next
				console.log('cloning FO Links');
				helper.cloneFOLinks(component, createdRole, createdCase, helper);
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Liabilities [' + retryctr + ']' );
				//Retry Cloning of Liabilities
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneFOLinks : function(component, createdRole, createdCase, helper){
		var incWrlist = component.get("v.newIncome");
		var expWrlist = component.get("v.newExpenses");
		var assWrlist = component.get("v.newAssets");
		var liaWrlist = component.get("v.newLiabilities");
		var folinklist = component.get("v.wrappedRoleData.foLinklist");
		if (folinklist != undefined ){
			for (var i=0; i < folinklist.length; i++){
				if (folinklist[i].Id != undefined) delete folinklist[i].Id;
				if (folinklist[i].h_Application__c != undefined) delete folinklist[i].h_Application__c;
				if (folinklist[i].h_Case__c != undefined) delete folinklist[i].h_Case__c;
			}
		}
		var appID = createdRole.Application__c;
		// Create the action
		var actionFO = component.get("c.cloneFOLinks");
		actionFO.setParams(
			{ incWrlist : incWrlist,
			  expWrlist : expWrlist,
			  assWrlist : assWrlist,
			  liaWrlist : liaWrlist,
			  folinklist : folinklist,
			  appID : appID,
			  caseID : createdCase.Id
			}
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('FO Links created');
				else console.log('No FO Links created');
				//Display Message Prompt
				helper.afterCloning(createdCase);
			}
			else {
				let errors = response.getError();
				var errmsg;
                if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
                    console.error(errors[0].message);
                } else {
					errmsg = 'Unknown error';
                    console.error("Unknown error");
                }
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	}


})