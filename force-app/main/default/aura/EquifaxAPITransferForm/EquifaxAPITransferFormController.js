({
	init: function(component,event,helper) {
		var initWrap = component.get("v.initWrapper");
		console.log('initWrap.showRoleSelectionOnInit = ',initWrap.showRoleSelectionOnInit);
		helper.setPicklistValues(component,initWrap);
	},
	handleSectionToggle : function(component, event, helper) {
		var openSections = event.getParam('openSections');
        if (openSections.length === 0) {
            console.log("All sections are closed");
        } else {
            console.log(openSections.join(', '));
        }
	},
	handleLoad : function(component, event, helper) {
		console.log("handleLoad");
	},

	handleButton : function(component, event, helper) {
		var whichBtn = event.getSource().get("v.title");
		if(whichBtn == 'save'){
			var allFieldsValid = helper.validateInputFields(component);
			if(allFieldsValid){
				helper.showPopup(component,whichBtn);
			}
		}
		else if (whichBtn == 'select'){
			helper.showPopup(component,whichBtn);
		}
		
	},

	checkboxSelect: function(component, event, helper) {
		var cmpName = event.getSource().get("v.name");
		console.log('cmpName == '+cmpName);
		if(cmpName == 'EQ_Include_Individual_Trading_History__c'){
			event.getSource().set('v.value',event.getSource().get('v.checked'));
		}
		if(cmpName == 'EQ_Current_Address__c'){
			event.getSource().setCustomValidity('Please create a current address using the Role Address page.');
			event.getSource().reportValidity();
			event.getSource().set('v.value',false);
		}
		if(cmpName == 'EQ_Previous_Address__c'){
			event.getSource().set('v.value',event.getSource().get('v.checked'));
		}
	},

	handleChange : function(component, event, helper) {
		console.log('handleChange ');
	},

})