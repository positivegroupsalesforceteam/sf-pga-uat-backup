<!-- 
/*
Author: Original: Jesfer Baculod (Positive Group)
History: 08/27/2018 Created
Purpose: page for creating Sourcing Case (VSR)
*/
--> 
<apex:page controller="Case_VehicleSourcingCC" sidebar="false">
    <apex:includeScript value="/support/console/30.0/integration.js"/>
    <style>
        #overlay{
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.5); /* rgba(0,0,0,0.2); Black background with opacity */
            z-index: 3; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }

        #ovdiv{
            position: absolute;
            top: 50%;
            left: 50%;
            font-size: 50px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }
    </style>
    <apex:outputpanel id="opScripts">
    <script>
        var closeSubtab = function closeSubtab(result) {
                var tabId = result.id;
                console.log(tabId);
                sforce.console.closeTab(tabId);
                };
            
        var showTabId = function showTabId(result){
                var tabId = result.id;
                console.log(tabId);
                sforce.console.refreshPrimaryTabById(tabId , true);
        };

        function toggleCheck(elem,divelem){
            var docdivelem = document.getElementById(divelem);
            var boxes = docdivelem.getElementsByTagName("input");
            for (var x = 0; x < boxes.length; x++) {
                var obj = boxes[x];
                if (obj.type == "checkbox") {
                if (obj.name != "check")
                    obj.checked = false;
                }
            }
            document.getElementById(elem).checked = true;
        }

        function saveRedirect(){
                if ('{!hasErrors}' == 'false'){
                    console.log('@@crescse: {!crescse}');
                    if (sforce.console.isInConsole()){ 
                        sforce.console.getEnclosingPrimaryTabId(showTabId);
                        sforce.console.setTabTitle('{!crescse.CaseNumber}');
                        window.location.href = '/{!crescse.Id}?isdtp=vw';
                    }
                    else window.location.href = '/{!crescse.Id}';
                }
            }

        function exit(){
                console.log(sforce.console.isInConsole());
                if (sforce.console.isInConsole()){
                    console.log('a');
                    console.log(sforce.console.getEnclosingTabId(closeSubtab));
                    sforce.console.getEnclosingPrimaryTabId(showTabId);
                    sforce.console.getEnclosingTabId(closeSubtab);
                    //setSidebarVisible(false);
                }
                else{
                    console.log('b');
                    window.location.href = '/{!pcpwr.parentCse.Id}';
                }
            }

        function setTabTitle() {
            console.log('IsinConsole:'+sforce.console.isInConsole());
            if (sforce.console.isInConsole()){
                sforce.console.setTabTitle('Vehicle Sourcing');
            }
        }

        var previousOnload = window.onload;        
        window.onload = function() { 
            if (previousOnload) { 
                previousOnload();
            }
            if (sforce.console.isInConsole()){                
                setTimeout('setTabTitle()', '500'); 
            }
        }
    </script>
    </apex:outputpanel>
    <apex:sectionHeader subtitle="{!pcpwr.parentCse.CaseNumber}" title="Sourcing Case Edit" />
    <apex:form id="mainFrm">
        <apex:pageMessages />
        <apex:pageMessage summary="VSR Successfully created" severity="INFO" strength="3" rendered="{!saveSuccess}" />
        <apex:actionFunction name="afSave" action="{!saveSourcingCase}" rerender="mainFrm,opScripts" onComplete="saveRedirect();" status="status1"  />
        <div id="overlay">
            <div id="ovdiv"><img src="/img/loading.gif" /></div>
        </div>
        <apex:actionStatus id="status1" onstart="document.getElementById('overlay').style.display = 'block';" onstop="document.getElementById('overlay').style.display = 'none'; " style="align:left;" />
        <apex:pageBlock mode="edit" title="Sourcing Case Edit" rendered="{!displaySourcing}">
            <apex:pageBlockButtons >
                <apex:outputlabel style="padding:4px;" onclick="afSave();" styleclass="btn" value="Save" />
                <apex:outputlabel style="padding:4px;" onclick="exit();" styleclass="btn" value="Cancel" />
            </apex:pageBlockButtons>
            <apex:outputpanel id="opExPurchase">
                <apex:pageBlockSection title="Purchases" columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$ObjectType.Case.fields.Parent_Case__c.label}" />
                        <apex:outputText value="{!pcpwr.parentCse.CaseNumber}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputlabel value="{!$ObjectType.Case.fields.Lender1__c.label}" />
                        <apex:outputText value="{!pcpwr.parentCse.Lender1__r.Name}" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Purchase(s)" />
                        <apex:outputPanel >
                            <apex:pageBlockTable value="{!pcpwr.pWrlist}" var="pwr" style="width: 60%;" >
                                <apex:column width="5%">
                                    <apex:inputCheckBox id="icbExPurchase" value="{!pwr.isSelected}" onchange="toggleCheck('{!$Component.icbExPurchase}','{!$Component.opExPurchase}');" />
                                </apex:column>
                                <apex:column headerValue="ID" value="{!pwr.purchase.Name}" />
                                <apex:column headerValue="Record Type" value="{!pwr.purchase.RecordType.Name}" />
                                <apex:column headerValue="Purchase Type" value="{!pwr.purchase.Asset_Type__c}" />
                                <apex:column headerValue="Purchase Price" value="{!pwr.purchase.Purchase_Price__c}" />
                            </apex:pageBlockTable>
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$ObjectType.Case.fields.Estimate_Max_Purchase_Price__c.label}" />
                        <apex:outputPanel styleClass="requiredInput" layout="block">
                            <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                            <apex:inputField value="{!scse.Estimate_Max_Purchase_Price__c}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$ObjectType.Case.fields.Submission_Comments_Karlon__c.label}" />
                        <apex:outputPanel styleClass="requiredInput" layout="block">
                            <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                            <apex:inputTextArea value="{!scse.Submission_Comments_Karlon__c}" rows="10" style="width: 250px;" />
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
            </apex:outputpanel>
        </apex:pageblock>
        <apex:outputpanel id="opNoPurchase" rendered="{!!displaySourcing}">
            <apex:pageMessage summary="No Purchases found on Loan Case" severity="ERROR" strength="2" />
            <br/>
            <center>
                <apex:outputlabel style="padding:4px;" onclick="exit();" styleclass="btn" value="Cancel" />
            </center>
        </apex:outputpanel>
    </apex:form>
</apex:page>